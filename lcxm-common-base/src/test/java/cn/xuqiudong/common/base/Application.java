package cn.xuqiudong.common.base;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-02-02 11:26
 */
@SpringBootApplication(scanBasePackages = "cn.xuqiudong.common.base.aspect.lock", exclude = DataSourceAutoConfiguration.class)
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
