package cn.xuqiudong.common.base;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述: base test
 * @author Vic.xu
 * @since 2024-02-02 9:32
 */
@SpringBootTest(classes = Application.class)
public abstract class AbstractBaseTest {
}
