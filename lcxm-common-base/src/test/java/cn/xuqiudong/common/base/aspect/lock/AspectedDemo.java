package cn.xuqiudong.common.base.aspect.lock;

import cn.xuqiudong.common.base.aspect.annotation.SerialRequest;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-02-02 11:36
 */
@Component
public class AspectedDemo {

    /**
     * lockParameter = "#map['id']"
     * lockParameter = "#demo.id"
     */
    @SerialRequest( name = "demo", lockParameter = "#map['id']", waitTimeSeconds = 3)
    public String test(String arg,  Map<String, String> map, DemoObject demo){
        String result = arg + " called test...";
        try {
            TimeUnit.SECONDS.sleep(4);
        } catch (InterruptedException e) {
        }
        System.out.println(result);
        return result;
    }

    public static class DemoObject {

        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }
    }

}


