package cn.xuqiudong.common.base.aspect;

/**
 * 描述: 被切对象
 * @author Vic.xu
 * @since 2024-02-02 9:58
 */

import org.springframework.test.context.transaction.AfterTransaction;

public class Person {

    @AfterTransaction
    public int run() {
        System.out.println("我在run...");
        return 0;
    }

    public void run(int i) {
        System.out.println("我在run...<" + i + ">");
    }

    public void say() {
        System.out.println("我在say...");
    }

    public void sayHi(String name) {
        System.out.println("Hi," + name + ",你好");
    }

    public int say(String name, int i) {
        System.out.println(name + "----" + i);
        return 0;
    }
}
