package cn.xuqiudong.common.base.aspect;

import cn.xuqiudong.common.base.Application;
import cn.xuqiudong.common.base.aspect.lock.AspectedDemo;
import org.junit.jupiter.api.Test;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-02-02 11:22
 */
@SpringBootTest(classes = RequestAspectApplication.class)
public class RequestAspectTest {

    @Autowired
    AspectedDemo aspectedDemo;

    @Test
    public void testSerialRequestAdvisor() throws InterruptedException {
        String arg = "arg1";
        Map<String, String> map = new HashMap<>();
        map.put("id", "1");
        map.put("name", "zhangsan");
        AspectedDemo.DemoObject demoObject = new AspectedDemo.DemoObject();
        demoObject.setId("789");
        Thread t1 = new Thread(()->{
            try {
                aspectedDemo.test("arg1", map, demoObject);
            } catch (Exception e) {
                System.out.println("1 出错了：" + e.getMessage());
            }

        });
        Thread t2 = new Thread(()->{
            try {
                aspectedDemo.test("arg2", map, demoObject);
            } catch (Exception e) {
                System.out.println("2 出错了：" + e.getMessage());
            }

        });

        Thread t3 = new Thread(()->{
            try {
                aspectedDemo.test("arg3", map, demoObject);
            } catch (Exception e) {
                System.out.println("3 出错了：" + e.getMessage());
            }

        });
        //方法内部等待4s 获取锁的等待时间为3s
        t1.start();
        t2.start();
        TimeUnit.SECONDS.sleep(2);
        t3.start();
        t1.join();
        t2.join();
        t3.join();
        System.out.println("end..........");
    }



    public static void main(String[] args) throws NoSuchMethodException {
        Method test = AspectedDemo.class.getMethod("test", String.class, Map.class);
        System.out.println(test.getParameters()[1].getName());
    }

}

@SpringBootApplication(scanBasePackages = "cn.xuqiudong.common.base.aspect.lock", exclude = DataSourceAutoConfiguration.class)
class RequestAspectApplication {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
    @Bean
    public SerialRequestAdvisor enableSerialRequestAspect(){
        Config config = new Config();
        config.useSingleServer()
                //可以用"rediss://"来启用SSL连接，前缀必须是redis:// or rediss://
                .setAddress("redis://127.0.0.1:6379")
                .setPassword("123456!@#$%^");
        RedissonClient redissonClient = Redisson.create(config);
        return new SerialRequestAdvisor(null, redissonClient);
    }
}
