package cn.xuqiudong.common.base.authentication.annotation;

/**
 * 多个权限标示的关系
 * @author VIC.xu
 *
 */
public enum Logical {
    /**adn*/
    AND,
    /**or*/
    OR
}
