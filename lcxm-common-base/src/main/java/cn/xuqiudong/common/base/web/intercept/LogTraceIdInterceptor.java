package cn.xuqiudong.common.base.web.intercept;

import cn.xuqiudong.common.base.web.intercept.log.TraceUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述: 通过拦截为日志加入traceId，方便统一追加
 * @author Vic.xu
 * @since 2022-12-12 10:57
 */
public class LogTraceIdInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        TraceUtils.createTraceId();
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        TraceUtils.destroyTraceId();
    }


}
