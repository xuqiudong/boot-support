package pers.vic.attachment.config.processor;

import cn.xuqiudong.attachment.dao.mysql.MysqlAttachmentRelationDao;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

/**
 * 描述:Bean对象在实例化和依赖注入完毕后 的后置处理器
 * @author Vic.xu
 * @since 2022-07-11 9:15
 */
//@Component
public class SomeBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof MysqlAttachmentRelationDao) {
            MysqlAttachmentRelationDao dao = (MysqlAttachmentRelationDao) bean;
            ((MysqlAttachmentRelationDao) bean).setCurrentUserIdSupplier(() -> {
                return 9;
            });
        }

        return BeanPostProcessor.super.postProcessAfterInitialization(bean, beanName);
    }
}
