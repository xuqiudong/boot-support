package pers.vic.attachment;

import cn.xuqiudong.attachment.dao.BaseAttachmentRelationDao;
import cn.xuqiudong.attachment.model.AttachmentFetchResult;
import cn.xuqiudong.attachment.model.AttachmentRelation;
import cn.xuqiudong.attachment.util.AttachmentFetchByAnnotationUtil;
import cn.xuqiudong.common.base.service.AttachmentStatusOperationServiceI;
import cn.xuqiudong.common.util.JsonUtil;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-08 17:49
 */
@SpringBootTest
public class AttachmentRelationTest {

    @Resource
    private BaseAttachmentRelationDao attachmentRelationDao;
    @Resource
    private AttachmentStatusOperationServiceI attachmentStatusOperationService;

    @Test
    public void query() {
        List<AttachmentRelation> attachmentRelations = attachmentRelationDao.attachmentRelations("321", "test");
        JsonUtil.printJson(attachmentRelations);
    }

    @Test
    public void insert() {
        AttachmentRelation attachmentRelation = new AttachmentRelation();
        attachmentRelation.setTableName("test");
        attachmentRelation.setAttachmentId(5);
        attachmentRelation.setColumnName("images");
        attachmentRelation.setOriginalName("test5.png");
        attachmentRelation.setBusinessId("321");
        attachmentRelationDao.insert(attachmentRelation);
        JsonUtil.printJson(attachmentRelation);

    }

    @Test
    @DisplayName("batch insert relation")
    public void batchInsert() {
        AttachmentRelation attachmentRelation = new AttachmentRelation();
        attachmentRelation.setTableName("batch");
        attachmentRelation.setAttachmentId(14);
        attachmentRelation.setColumnName("images");
        attachmentRelation.setOriginalName("batch.jpn");
        attachmentRelation.setBusinessId("123");

        AttachmentRelation relation = new AttachmentRelation();
        relation.setTableName("batch");
        relation.setAttachmentId(15);
        relation.setColumnName("images");
        relation.setOriginalName("batch.jpn");
        relation.setBusinessId("123");
        List<AttachmentRelation> list = new ArrayList<>();
        list.add(attachmentRelation);
        list.add(relation);
        attachmentRelationDao.batchInsert(list);
        JsonUtil.printJson(list);
    }

    @Test
    public void delete() {
        attachmentRelationDao.delete(6, 7);
    }

    @Test
    public void delete3() {
        attachmentRelationDao.delete("123", "test_b");
    }

    @Test
    public void attachmentRelations() {
        List<AttachmentRelation> list = attachmentRelationDao.attachmentRelations("123", "test_b");
        JsonUtil.printJson(list);

        List<AttachmentRelation> list3 = attachmentRelationDao.attachmentRelations("321", "test", "images");
        JsonUtil.printJson(list3);
    }


    @Test
    public void fill() {
        TestRelationModel model = new TestRelationModel();
        model.setId(321);
        attachmentStatusOperationService.fillAttachmentInfo(model);
        JsonUtil.printJson(model);
        System.out.println("-----------------------------------------");
        AttachmentFetchResult result = AttachmentFetchByAnnotationUtil.fetchAttachmentInfoFromObject(model);
        JsonUtil.printJson(result);
    }


    @Test
    public void testUpdateObject() {
        TestRelationModel old = new TestRelationModel();
        old.setId(9);
        attachmentStatusOperationService.fillAttachmentInfo(old);
        JsonUtil.printJson(old);

        TestRelationModel now = new TestRelationModel();
        now.setId(9);
        AttachmentRelation icon = new AttachmentRelation();
        icon.setAttachmentId(888);
        now.setAttachmentRelation(icon);
        List<AttachmentRelation> relations = new ArrayList<>(old.getRelations());
        //保留个22
        List<AttachmentRelation> collect = relations.stream().filter(r -> {
            return r.getAttachmentId() == 22;
        }).collect(Collectors.toList());
        AttachmentRelation img = new AttachmentRelation();
        img.setAttachmentId(55);
        collect.add(img);
        now.setRelations(collect);

   /*     // 提取出当前对象和数据库中对象的附件信息
        AttachmentFetchResult oldResult = AttachmentFetchByAnnotationUtil.fetchAttachmentInfoFromObject(old);
        AttachmentFetchResult nowResult = AttachmentFetchByAnnotationUtil.fetchAttachmentInfoFromObject(now);
        //处理附件
        CalcDiffCollection<Integer> idDiff = CalcDiffCollection.instance(oldResult.getIdList(), nowResult.getIdList());
        System.out.println("getOnlyInOld");
        JsonUtil.printJson(idDiff.getOnlyInOld());
        System.out.println("getOnlyInNew");
        JsonUtil.printJson(idDiff.getOnlyInNew());

        //处理关系， 新增的关系没有id
        //  先处理新增的关系。即没有id的关系
        List<AttachmentRelation> relationList = nowResult.getRelationList();
        List<AttachmentRelation> onlyNeedAdd = relationList.stream().filter(r -> r.getId() == null).collect(Collectors.toList());
        System.out.println("onlyNeedAdd");
        JsonUtil.printJson(onlyNeedAdd);
        // 再处理 删除的关系
        CalcDiffCollection<Integer> relationIdDiff = CalcDiffCollection.instance(oldResult.getRelationIdList(), nowResult.getRelationIdList());
        List<Integer> onlyInOld = relationIdDiff.getOnlyInOld();
        System.out.println("onlyInOld");
        JsonUtil.printJson(onlyInOld);*/


        attachmentStatusOperationService.handleOldAndNowAttachment(old, now);

    }

    @Test
    @DisplayName("从对象中新增附件")
    public void addFromObject() {
        TestRelationModel entity = model();
        AttachmentFetchResult result = AttachmentFetchByAnnotationUtil.fetchAttachmentInfoFromObject(entity);
        JsonUtil.printJson(result);
        attachmentStatusOperationService.addAttachmentFromObj(entity);
    }


    public static void main(String[] args) {
        TestRelationModel now = model();
        AttachmentFetchResult oldResult = AttachmentFetchByAnnotationUtil.fetchAttachmentInfoFromObject(now);
        JsonUtil.printJson(oldResult);
    }


    private static TestRelationModel model() {
        TestRelationModel now = new TestRelationModel();
        now.setId(9);
        AttachmentRelation icon = new AttachmentRelation();
        icon.setAttachmentId(88);
        now.setAttachmentRelation(icon);

        List<AttachmentRelation> relations = new ArrayList<>();
        AttachmentRelation old1 = new AttachmentRelation();
    /*    old1.setId(1);
        old1.setAttachmentId(1);
        AttachmentRelation old2 = new AttachmentRelation();
        old2.setId(2);
        old2.setAttachmentId(2);
        relations.add(old1);
        relations.add(old2);*/

        AttachmentRelation now1 = new AttachmentRelation();
        now1.setAttachmentId(22);
        relations.add(now1);
        AttachmentRelation now2 = new AttachmentRelation();
        now2.setAttachmentId(33);
        relations.add(now2);
        AttachmentRelation now3 = new AttachmentRelation();
        now3.setAttachmentId(44);
        relations.add(now3);
        now.setRelations(relations);
        return now;
    }


}
