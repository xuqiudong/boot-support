package pers.vic.attachment;

import cn.xuqiudong.attachment.annotation.AttachmentFlag;
import cn.xuqiudong.attachment.annotation.AttachmentRelationInfo;
import cn.xuqiudong.attachment.model.AttachmentRelation;
import cn.xuqiudong.common.base.model.BaseEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-11 16:48
 */
@AttachmentRelationInfo("test")
public class TestRelationModel extends BaseEntity {


    private static final long serialVersionUID = 9020018308594083996L;

    @AttachmentFlag(value = AttachmentFlag.AttachmentType.RELATION, relationColumn = "images")
    private List<AttachmentRelation> relations;

    @AttachmentFlag(value = AttachmentFlag.AttachmentType.RELATION, relationColumn = "icon")
    private AttachmentRelation attachmentRelation;

    public List<AttachmentRelation> getRelations() {
        return relations;
    }

    public void setRelations(List<AttachmentRelation> relations) {
        this.relations = relations;
    }

    public AttachmentRelation getAttachmentRelation() {
        return attachmentRelation;
    }

    public void setAttachmentRelation(AttachmentRelation attachmentRelation) {
        this.attachmentRelation = attachmentRelation;
    }

    public static void main(String[] args) {
        Class<TestRelationModel> clazz = TestRelationModel.class;
        String relationDesc = clazz.getSimpleName();
        if (clazz.isAnnotationPresent(AttachmentRelationInfo.class)) {
            AttachmentRelationInfo attachmentRelationInfo = (AttachmentRelationInfo) clazz.getAnnotation(AttachmentRelationInfo.class);
            if (StringUtils.isNotBlank(attachmentRelationInfo.value())) {
                relationDesc = attachmentRelationInfo.value();
            }

        }
        System.out.println(relationDesc);
    }
}
