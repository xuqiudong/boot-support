package cn.xuqiudong.linuxapi.nginx;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalTime;

/**
 * 描述:nginx 脚本
 * @author Vic.xu
 * @since 2022-12-14 17:20
 */
@RestController
@RequestMapping("/nginx")
public class NginxRestartController {

    @Value("${nginx.restart.script.path}")
    private String scriptPath;

    @GetMapping("restart")
    public Object restart() throws IOException {
        Runtime.getRuntime().exec(scriptPath);
        return LocalTime.now();
    }
}
