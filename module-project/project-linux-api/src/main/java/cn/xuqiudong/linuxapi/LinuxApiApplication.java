package cn.xuqiudong.linuxapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * linux api  entry
 * @author Vic.xu
 * @since 2022-12-14
 */
@SpringBootApplication
public class LinuxApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LinuxApiApplication.class, args);
    }

}
