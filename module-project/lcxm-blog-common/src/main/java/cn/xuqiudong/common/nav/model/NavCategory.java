package cn.xuqiudong.common.nav.model;


import cn.xuqiudong.common.base.annotation.NoneColumn;
import cn.xuqiudong.common.base.model.BaseEntity;

import java.util.List;


/**
 * 导航分类 实体类
 *
 * @author Vic.xu
 */
public class NavCategory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * code
     */
    private String code;

    /**
     * 分类标题
     */
    private String title;

    /**
     * linecons
     */
    private String icon;

    private Integer sort;


    @NoneColumn
    List<NavSite> sites;

    /***************** set|get  start **************************************/
    /**
     * set：code
     */
    public NavCategory setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * get：code
     */
    public String getCode() {
        return code;
    }

    /**
     * set：分类标题
     */
    public NavCategory setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * get：分类标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * set：linecons
     */
    public NavCategory setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    /**
     * get：linecons
     */
    public String getIcon() {
        return icon;
    }

    public List<NavSite> getSites() {
        return sites;
    }

    public void setSites(List<NavSite> sites) {
        this.sites = sites;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /***************** set|get  end **************************************/
}
