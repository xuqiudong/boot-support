package cn.xuqiudong.common.blog.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 * 博文按年月归档 实体类
 *
 * @author Vic.xu
 */
public class BlogArticleFile extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 归档年
     */
    private Integer year;

    /**
     * 归档月
     */
    private Integer month;

    /**
     * 当月文章总数
     */
    private Integer num;


    /* **************** set|get  start **************************************/

    /**
     * set：归档年
     */
    public BlogArticleFile setYear(Integer year) {
        this.year = year;
        return this;
    }

    /**
     * get：归档年
     */
    public Integer getYear() {
        return year;
    }

    /**
     * set：归档月
     */
    public BlogArticleFile setMonth(Integer month) {
        this.month = month;
        return this;
    }

    /**
     * get：归档月
     */
    public Integer getMonth() {
        return month;
    }

    /**
     * set：当月文章总数
     */
    public BlogArticleFile setNum(Integer num) {
        this.num = num;
        return this;
    }

    /**
     * get：当月文章总数
     */
    public Integer getNum() {
        return num;
    }
    /* **************** set|get  end **************************************/
}
