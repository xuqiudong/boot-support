package cn.xuqiudong.common.nav.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.nav.model.NavCategory;
/**
 *功能: :导航分类 Mapper
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
public interface NavCategoryMapper extends BaseMapper<NavCategory> {
	
}
