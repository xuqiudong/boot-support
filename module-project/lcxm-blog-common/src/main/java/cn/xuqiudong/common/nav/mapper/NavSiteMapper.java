package cn.xuqiudong.common.nav.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.nav.model.NavSite;
/**
 *功能: :导航站点 Mapper
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
public interface NavSiteMapper extends BaseMapper<NavSite> {
	
}
