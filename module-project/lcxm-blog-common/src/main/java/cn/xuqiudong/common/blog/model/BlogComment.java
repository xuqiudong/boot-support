package cn.xuqiudong.common.blog.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import cn.xuqiudong.common.base.model.PageInfo;


/**
 * 评论 实体类
 *
 * @author Vic.xu
 */
public class BlogComment extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 评论人
     */
    private Integer userId;

    /**
     * 内容
     */
    private String content;

    /**
     * 可能是评论文章的
     */
    private Integer articleId;

    /**
     * 当子前子回复在哪条回复下
     */
    private Integer replyId;

    /**
     * 回复的是哪条回复
     */
    private Integer atId;

    /* *********************************************************************** */
    /**
     * 评论人昵称
     */
    private String nickname;
    /**
     * 评论人头像
     */
    private String avatar;
    /**
     * 子评论列表
     */
    private PageInfo<BlogComment> subComments;


    /***************** set|get  start **************************************/
    /**
     * set：评论人
     */
    public BlogComment setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    /**
     * get：评论人
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * set：内容
     */
    public BlogComment setContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * get：内容
     */
    public String getContent() {
        return content;
    }

    /**
     * set：可能是评论文章的
     */
    public BlogComment setArticleId(Integer articleId) {
        this.articleId = articleId;
        return this;
    }

    /**
     * get：可能是评论文章的
     */
    public Integer getArticleId() {
        return articleId;
    }

    /**
     * set：当子前子回复在哪条回复下
     */
    public BlogComment setReplyId(Integer replyId) {
        this.replyId = replyId;
        return this;
    }

    /**
     * get：当子前子回复在哪条回复下
     */
    public Integer getReplyId() {
        return replyId;
    }

    /**
     * set：回复的是哪条回复
     */
    public BlogComment setAtId(Integer atId) {
        this.atId = atId;
        return this;
    }

    /**
     * get：回复的是哪条回复
     */
    public Integer getAtId() {
        return atId;
    }

    /***************** set|get  end **************************************/

    public PageInfo<BlogComment> getSubComments() {
        return subComments;
    }

    public void setSubComments(PageInfo<BlogComment> subComments) {
        this.subComments = subComments;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
