package cn.xuqiudong.common.nav.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 * 导航站点 实体类
 *
 * @author Vic.xu
 */
public class NavSite extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 类别编码
     */
    private String categoryCode;

    /**
     * 站点名称
     */
    private String name;

    /**
     * 站点地址
     */
    private String url;

    /**
     * 站点标题或简单说明
     */
    private String title;

    /**
     * 站点描述
     */
    private String description;

    /**
     * 站点logo地址
     */
    private String logo;

    private Integer sort;


    /***************** set|get  start **************************************/
    /**
     * set：类别编码
     */
    public NavSite setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
        return this;
    }

    /**
     * get：类别编码
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * set：站点名称
     */
    public NavSite setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：站点名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：站点地址
     */
    public NavSite setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * get：站点地址
     */
    public String getUrl() {
        return url;
    }

    /**
     * set：站点标题或简单说明
     */
    public NavSite setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * get：站点标题或简单说明
     */
    public String getTitle() {
        return title;
    }

    /**
     * set：站点描述
     */
    public NavSite setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * get：站点描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * set：站点logo地址
     */
    public NavSite setLogo(String logo) {
        this.logo = logo;
        return this;
    }

    /**
     * get：站点logo地址
     */
    public String getLogo() {
        return logo;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /***************** set|get  end **************************************/
}
