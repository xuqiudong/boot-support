package cn.xuqiudong.common.blog.model;


import cn.xuqiudong.attachment.annotation.AttachmentFlag;
import cn.xuqiudong.common.base.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;


/**
 * 全局配置 实体类
 *
 * @author Vic.xu
 */
public class GlobalConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 配置code
     */
    private String code;

    /**
     * 配置名称
     */
    private String name;

    /**
     * 展示类型:1-input;2-textarea;3-图片;4-富文本
     */
    private Integer showType;

    /**
     * 内容
     */
    @AttachmentFlag(value = AttachmentFlag.AttachmentType.CONTENT)
    private String content;


    /***************** set|get  start **************************************/
    /**
     * set：配置code
     */
    public GlobalConfig setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * get：配置code
     */
    public String getCode() {
        return code;
    }

    /**
     * set：配置名称
     */
    public GlobalConfig setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：配置名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：展示类型:1-input;2-textarea;3-图片;4-富文本
     */
    public GlobalConfig setShowType(Integer showType) {
        this.showType = showType;
        return this;
    }

    /**
     * get：展示类型:1-input;2-textarea;3-图片;4-富文本
     */
    public Integer getShowType() {
        return showType;
    }

    /**
     * set：内容
     */
    public GlobalConfig setContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * get：内容
     */
    public String getContent() {
        return content;
    }


    /***************** set|get  end **************************************/

    @JsonIgnore
    public int getIntContent() {
        if (StringUtils.isNotBlank(content)) {
            try {
                return Integer.parseInt(content.trim());

            } catch (Exception e) {
                //NumberFormatException
            }
        }
        return 0;
    }

    @JsonIgnore
    public long getLongContent() {
        if (StringUtils.isNotBlank(content)) {
            try {
                return Long.parseLong(content.trim());

            } catch (Exception e) {
                //NumberFormatException
            }
        }
        return 0L;
    }

    @JsonIgnore
    public double getDoubleContent() {
        if (StringUtils.isNotBlank(content)) {
            try {
                return Double.parseDouble(content.trim());

            } catch (Exception e) {
                //NumberFormatException
            }
        }
        return 0.0;
    }

    @JsonIgnore
    public BigDecimal getBigDecimalContent() {
        if (StringUtils.isNotBlank(content)) {
            try {
                return new BigDecimal(content.trim()).setScale(2, BigDecimal.ROUND_HALF_UP);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return BigDecimal.ZERO;
    }
}
