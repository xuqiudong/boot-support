package cn.xuqiudong.common.blog.config.common;

import cn.xuqiudong.common.base.service.CommonBaseMapperService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述: common configuration
 * @author Vic.xu
 * @since 2023-10-16 15:42
 */
@Configuration
public class CommonBlogConfig {

    @Bean
    public CommonBaseMapperService commonBaseMapperService(){
        return new CommonBaseMapperService();
    }
}
