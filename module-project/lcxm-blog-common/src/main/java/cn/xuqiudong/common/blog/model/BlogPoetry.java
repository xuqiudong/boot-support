package cn.xuqiudong.common.blog.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 诗歌 实体类
 *
 * @author Vic.xu
 */
public class BlogPoetry extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 写作时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
   @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date writeDate;


    /***************** set|get  start **************************************/
    /**
     * set：标题
     */
    public BlogPoetry setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * get：标题
     */
    public String getTitle() {
        return title;
    }

    /**
     * set：内容
     */
    public BlogPoetry setContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * get：内容
     */
    public String getContent() {
        return content;
    }

    /**
     * set：写作时间
     */
    public BlogPoetry setWriteDate(Date writeDate) {
        this.writeDate = writeDate;
        return this;
    }

    /**
     * get：写作时间
     */
    public Date getWriteDate() {
        return writeDate;
    }
    /***************** set|get  end **************************************/
}
