package cn.xuqiudong.common.blog.model;

import cn.xuqiudong.common.base.model.BaseEntity;

/**
 * 博客专题表 实体类
 *
 * @author Vic.xu
 */
public class BlogTopic extends BaseEntity {

    private static final long serialVersionUID = 1L;


    /**
     * 名称
     */
    private String name;

    /**
     * 点击次数
     */
    private Integer clickNum;

    /**
     * 文章数量
     */
    private Integer articleNum;

    /**
     * 排序
     */
    private Integer sort;


    /**
     * 专题介绍
     */
    private String introduce;

    /**
     * 是否首页展示
     */
    private Boolean showHome;

    /* **************** set|get start ************************************* */

    /**
     * set：名称
     */
    public BlogTopic setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：点击次数
     */
    public BlogTopic setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
        return this;
    }

    /**
     * get：点击次数
     */
    public Integer getClickNum() {
        return clickNum;
    }

    public Integer getArticleNum() {
        return articleNum;
    }

    public void setArticleNum(Integer articleNum) {
        this.articleNum = articleNum;
    }

    /**
     * set：排序
     */
    public BlogTopic setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    /**
     * get：排序
     */
    public Integer getSort() {
        return sort;
    }


    public Boolean getShowHome() {
        return showHome;
    }

    public void setShowHome(Boolean showHome) {
        this.showHome = showHome;
    }

    /**
     * set：专题介绍
     */
    public BlogTopic setIntroduce(String introduce) {
        this.introduce = introduce;
        return this;
    }

    /**
     * get：专题介绍
     */
    public String getIntroduce() {
        return introduce;
    }
    /* **************** set|get end ************************************* */
}
