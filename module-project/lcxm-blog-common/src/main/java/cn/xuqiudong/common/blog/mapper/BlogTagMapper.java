package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogTag;

/**
 * 描述:标签表 Mapper
 * @author Vic.xu
 * @since  2020-05-08 09:38
 */
public interface BlogTagMapper extends BaseMapper<BlogTag> {

}
