package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogComment;

import java.util.List;

/**
 * @author Vic.xu
 * 描述:评论 Mapper
 * @since  2020-07-21 14:59
 */
public interface BlogCommentMapper extends BaseMapper<BlogComment> {

    /**
     * 1. 留言板的评论 articleId = 0， 某个文章下的评论：articleId = #｛articleId｝
     * 2. 主评论：replyId = 0, 子评论： replyId = #｛id｝
     * @param lookup
     * @return
     */
    List<BlogComment> commentsList(BlogComment lookup);
}
