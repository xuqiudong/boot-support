package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogThirdUser;

/**
 * 描述:第三方用户 Mapper
 * @author Vic.xu
 * @since  2020-07-21 17:33
 */
public interface BlogThirdUserMapper extends BaseMapper<BlogThirdUser> {

    /**
     * 如果类型和唯一标识重复则更新，不然则新增
     * @param user
     * @return
     */
    BlogThirdUser checkByTypeAndIdentifier(BlogThirdUser user);
}
