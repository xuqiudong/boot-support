package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogCategory;
import org.apache.ibatis.annotations.Param;

/**
 * 描述:博客分类表 Mapper
 * @author Vic.xu
 * @since  2020-05-08 09:38
 */
public interface BlogCategoryMapper extends BaseMapper<BlogCategory> {
    /**
     * 更新文章的数量
     * @param id id
     * @return 数量
     */
    int updateArticleNum(@Param("id") Integer id);
}
