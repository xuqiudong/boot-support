package cn.xuqiudong.common.blog.mapper;


import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.GlobalConfig;
import org.apache.ibatis.annotations.Param;

/**
 * @author Vic.xu
 * 描述:全局配置 Mapper
 * @since  2020-06-10 14:37
 */
public interface GlobalConfigMapper extends BaseMapper<GlobalConfig> {

    /**
     * 根据code查找配置
     * @param name code
     * @return 配置
     */
    GlobalConfig getByCode(@Param("code") String name);

    /**
     * 根据code更新content
     * @param code
     * @param content
     * @return
     */
    int updateContentByCode(@Param("code") String code, @Param("content") String content);
}
