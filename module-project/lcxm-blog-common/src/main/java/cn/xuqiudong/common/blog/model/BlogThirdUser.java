package cn.xuqiudong.common.blog.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 * 第三方用户 实体类
 *
 * @author Vic.xu
 */
public class BlogThirdUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 第三方类型
     */
    private String type;

    /**
     * 第三方唯一标识，账户
     */
    private String identifier;

    /**
     * 凭证，其实这里不需要
     */
    private String credential;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别 0-保密 1-男 2-女
     */
    private Integer gender;


    /***************** set|get  start **************************************/
    /**
     * set：昵称
     */
    public BlogThirdUser setNickname(String nickname) {
        this.nickname = nickname;
        return this;
    }

    /**
     * get：昵称
     */
    public String getNickname() {
        return nickname == null ? identifier : nickname;
    }

    /**
     * set：第三方类型
     */
    public BlogThirdUser setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * get：第三方类型
     */
    public String getType() {
        return type;
    }

    /**
     * set：第三方唯一标识，账户
     */
    public BlogThirdUser setIdentifier(String identifier) {
        this.identifier = identifier;
        return this;
    }

    /**
     * get：第三方唯一标识，账户
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * set：凭证，其实这里不需要
     */
    public BlogThirdUser setCredential(String credential) {
        this.credential = credential;
        return this;
    }

    /**
     * get：凭证，其实这里不需要
     */
    public String getCredential() {
        return credential;
    }

    /**
     * set：头像
     */
    public BlogThirdUser setAvatar(String avatar) {
        this.avatar = avatar;
        return this;
    }

    /**
     * get：头像
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * set：性别 0-保密 1-男 2-女
     */
    public BlogThirdUser setGender(Integer gender) {
        this.gender = gender;
        return this;
    }

    /**
     * get：性别 0-保密 1-男 2-女
     */
    public Integer getGender() {
        return gender;
    }

    /***************** set|get  end **************************************/
}
