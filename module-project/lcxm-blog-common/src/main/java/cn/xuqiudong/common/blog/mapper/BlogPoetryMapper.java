package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogPoetry;

/**
 * 描述:诗歌 Mapper
 * @author Vic.xu
 * @since  2020-08-19 14:26
 */
public interface BlogPoetryMapper extends BaseMapper<BlogPoetry> {

}
