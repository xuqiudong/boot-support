package cn.xuqiudong.common.blog.model;

import cn.xuqiudong.common.base.model.BaseEntity;

import java.util.Date;

/**
 * 博客分类表 实体类
 *
 * @author Vic.xu
 */
public class BlogCategory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 点击次数
     */
    private Integer clickNum;

    /**
     * 文章数量
     */
    private Integer articleNum;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 分类介绍
     */
    private String introduce;

    /* **************** set|get start ************************************* */

    /**
     * set：名称
     */
    public BlogCategory setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：点击次数
     */
    public BlogCategory setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
        return this;
    }

    /**
     * get：点击次数
     */
    public Integer getClickNum() {
        return clickNum;
    }

    public Integer getArticleNum() {
        return this.articleNum;
    }

    public BlogCategory setArticleNum(Integer articleNum) {
        this.articleNum = articleNum;
        return this;
    }

    /**
     * set：排序
     */
    public BlogCategory setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    /**
     * get：排序
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * set：创建时间
     */
    @Override
    public BlogCategory setCreateTime(Date createTime) {
        this.createTime = createTime;
        return this;
    }


    /**
     * set：分类介绍
     */
    public BlogCategory setIntroduce(String introduce) {
        this.introduce = introduce;
        return this;
    }

    /**
     * get：分类介绍
     */
    public String getIntroduce() {
        return introduce;
    }
    /***************** set|get end **************************************/
}
