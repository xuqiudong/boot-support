package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogArticle;

/**
 * 描述:博客表 Mapper
 * @author Vic.xu
 * @since  2020-05-08 10:53
 */
public interface BlogArticleMapper extends BaseMapper<BlogArticle> {

}
