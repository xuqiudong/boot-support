package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogArticleFile;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * 描述:博文按年月归档 Mapper
 * @author Vic.xu
 * @since  2020-07-10 11:15
 */
public interface BlogArticleFileMapper extends BaseMapper<BlogArticleFile> {

    /**
     * 查询最后一条归档信息
     * @return last file
     */
    BlogArticleFile findLast();

    /**
     * 统计某个时间之后的数量
     * @param lastDate
     *                 某个时间之后
     * @return 数量
     */
    int statisticsNumAfterDate(@Param("lastDate") Date lastDate);

    /**
     * 统计全部归档
     */
    void statisticsAll();
}
