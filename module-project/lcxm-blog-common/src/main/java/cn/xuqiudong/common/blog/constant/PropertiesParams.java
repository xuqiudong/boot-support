package cn.xuqiudong.common.blog.constant;

import org.springframework.beans.factory.annotation.Value;

/**
 * 描述: 静态注入一些属性
 * @author Vic.xu
 * @since 2022-07-22 17:21
 */
public class PropertiesParams {
    /**
     * 附件前缀    附件访问地址：相对于项目; 附件地址 = attachment.visit + /attachment//visit/{id}
     */
    public static String ATTACHMENT_PREFIX;

    @Value("${attachment.server:}")
    public void setAttachmentPrefix(String attachmentPrefix) {
        PropertiesParams.ATTACHMENT_PREFIX = attachmentPrefix;
    }

}
