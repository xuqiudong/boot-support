package cn.xuqiudong.common.blog.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.common.blog.model.BlogTopic;
import org.apache.ibatis.annotations.Param;

/**
 * 描述:博客专题表 Mapper
 * @author Vic.xu
 * @since  2020-05-08 09:36
 */
public interface BlogTopicMapper extends BaseMapper<BlogTopic> {

    /**
     * 更新文章的数量
     * @param id id
     * @return update number
     */
    int updateArticleNum(@Param("id") Integer id);

}
