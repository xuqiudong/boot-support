package cn.xuqiudong.common.blog.model;

import cn.xuqiudong.common.base.model.BaseEntity;

/**
 * 标签表 实体类
 *
 * @author Vic.xu
 */
public class BlogTag extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 点击次数
     */
    private Integer clickNum;

    /**
     * 排序
     */
    private Integer sort;


    /* **************** set|get start **************************************/

    /**
     * set：名称
     */
    public BlogTag setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：点击次数
     */
    public BlogTag setClickNum(Integer clickNum) {
        this.clickNum = clickNum;
        return this;
    }

    /**
     * get：点击次数
     */
    public Integer getClickNum() {
        return clickNum;
    }

    /**
     * set：排序
     */
    public BlogTag setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    /**
     * get：排序
     */
    public Integer getSort() {
        return sort;
    }


    /* **************** set|get end **************************************/
}
