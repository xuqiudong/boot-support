package cn.xuqiudong.common.blog.config.es.model;

import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.elasticsearch.model.EsBaseModel;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;

/**
 * 说明 :  博客存入es中的实体
 * @author Vic.xu
 * @since  2020/9/16 0016 11:28
 */
public class EsBlogArticleModel extends EsBaseModel {

    private static final long serialVersionUID = 4117136836820801554L;
    /**
     * 标题
     */
    private String title;

    /**
     * 内容
     */
    private String content;

    /**
     * 作者
     */
    private String author;

    /**
     * 分类
     */
    private String categoryName;

    /**
     * 专题
     */
    private String topicName;

    /**
     * 多标签
     */
    private String tagNames;

    /**
     * 图标
     */
    private String iconUrl;

    private Date createTime;

    public EsBlogArticleModel() {
    }

    public EsBlogArticleModel(BlogArticle blogArticle) {
        this.id = String.valueOf(blogArticle.getId());
        this.title = blogArticle.getTitle();
        this.content = blogArticle.getContent();
        this.author = blogArticle.getAuthor();
        this.categoryName = blogArticle.getCategoryName();
        this.topicName = blogArticle.getTopicName();
        this.tagNames = blogArticle.getTagNames();
        this.iconUrl = blogArticle.getIconUrl();
        this.createTime = blogArticle.getCreateTime();

    }

    /**summary显示最大长度*/
    public static int SUMMARY_CONUT = 300;

    /**
     *给列表页使用的
     */
    public String getSummary() {
        if (StringUtils.isNotBlank(content) && content.length() > SUMMARY_CONUT) {
            return content.substring(0, 300);
        }
        return content;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTopicName() {
        return topicName;
    }

    public void setTopicName(String topicName) {
        this.topicName = topicName;
    }

    public String getTagNames() {
        return tagNames;
    }

    public void setTagNames(String tagNames) {
        this.tagNames = tagNames;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
