CREATE
    DATABASE duoduo DEFAULT CHARSET utf8mb4;
-- GRANT ALL PRIVILEGES ON duoduo.* TO 'duoduo'@'localhost' IDENTIFIED BY 'duoduo12345678';
-- mysql8
CREATE USER
    'duoduo'@'localhost' IDENTIFIED BY 'duoduo12345678';
GRANT ALL PRIVILEGES ON duoduo.* TO 'duoduo'@'localhost' WITH GRANT OPTION;

FLUSH
    PRIVILEGES;


-- 追加相同的字段
/*
ALTER TABLE table_name ADD (
 `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "修改时间",
  `is_enable` TINYINT(1) DEFAULT '1' COMMENT '是否启用',
  `is_delete` TINYINT(1) DEFAULT '0' COMMENT '是否删除'
);

*/