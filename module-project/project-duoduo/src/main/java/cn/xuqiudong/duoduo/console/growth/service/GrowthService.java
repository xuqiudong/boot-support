package cn.xuqiudong.duoduo.console.growth.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.duoduo.console.growth.mapper.GrowthMapper;
import cn.xuqiudong.duoduo.console.growth.model.Growth;
import org.springframework.stereotype.Service;

/**
 *功能: :身高体重记录 Service
 * @author Vic.xu
 * @since  2024-04-08 15:32
 */
@Service
public class GrowthService extends BaseService<GrowthMapper, Growth>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
