package cn.xuqiudong.duoduo.base.vo;

import java.io.Serializable;

/**
 * 描述:Vo 基类
 * @author Vic.xu
 * @since 2024-04-08 17:19
 */
public abstract class BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
