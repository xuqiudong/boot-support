package cn.xuqiudong.duoduo.enums;

/**
 * 描述: 资源来源
 * @author Vic.xu
 * @since 2024-04-08 16:12
 */
public enum ResourceOriginEnum {

    /** 本地磁盘*/
    local,
    /**网络地址*/
    network,
    /**附件表*/
    attachment;
}
