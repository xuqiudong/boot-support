package cn.xuqiudong.duoduo.api.service;

import cn.xuqiudong.duoduo.api.vo.GrowthVo;
import cn.xuqiudong.duoduo.console.growth.model.Growth;
import cn.xuqiudong.duoduo.console.growth.service.GrowthService;
import cn.xuqiudong.duoduo.console.resource.service.ResourceService;
import cn.xuqiudong.duoduo.helper.AgeHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 描述: 长成记 service
 * @author Vic.xu
 * @since 2024-04-08 17:24
 */
@Service
public class GrowthApiService extends BaseApiService{

    @Resource
    private GrowthService growthService;

    @Resource
    private ResourceService resourceService;

    @Resource
    private ResourceApiService  resourceApiService;

    private static final String CONTENT_PATTERN = "身高: {0}cm, 体重: {1}Kg";

    public List<GrowthVo> list(){
        List<Growth> list = growthService.list(new Growth());
        List<GrowthVo> result = new ArrayList<>();
        for (Growth growth : list) {
            GrowthVo vo = new GrowthVo();
            vo.setId(growth.getId());
            vo.setAge(AgeHelper.showAge(growth.getMeasureDate()));
            vo.setDate(AgeHelper.showDate(growth.getMeasureDate()));
            vo.setImg(resourceApiService.getResourcePreviewUrl(growth.getIcon()));
            vo.setContent(MessageFormat.format(CONTENT_PATTERN, growth.getHeight(), growth.getWeight()));
            result.add(vo);
        }
        return result;
    }

}
