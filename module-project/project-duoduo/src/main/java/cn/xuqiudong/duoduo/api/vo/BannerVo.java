package cn.xuqiudong.duoduo.api.vo;

import cn.xuqiudong.duoduo.base.vo.BaseVo;

/**
 * 描述: banner vo
 * @author Vic.xu
 * @since 2024-04-09 15:17
 */
public class BannerVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 图片
     */
    private String image;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
