package cn.xuqiudong.duoduo.console.growth.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.duoduo.console.growth.model.Growth;

/**
 *功能: :身高体重记录 Mapper
 * @author Vic.xu
 * @since  2024-04-08 15:32
 */
public interface GrowthMapper extends BaseMapper<Growth> {
	
}
