package cn.xuqiudong.duoduo.console.banner.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.duoduo.console.banner.mapper.BannerMapper;
import cn.xuqiudong.duoduo.console.banner.model.Banner;
import org.springframework.stereotype.Service;

/**
 *功能: :banner图 Service
 * @author Vic.xu
 * @since  2024-04-09 11:06
 */
@Service
public class BannerService extends BaseService<BannerMapper, Banner>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
