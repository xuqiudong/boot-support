package cn.xuqiudong.duoduo.console.resource.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.duoduo.console.resource.mapper.ResourceCategoryMapper;
import cn.xuqiudong.duoduo.console.resource.model.ResourceCategory;
import org.springframework.stereotype.Service;

/**
 *功能: :资源分类 Service
 * @author Vic.xu
 * @since  2024-04-08 14:46
 */
@Service
public class ResourceCategoryService extends BaseService<ResourceCategoryMapper, ResourceCategory>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
