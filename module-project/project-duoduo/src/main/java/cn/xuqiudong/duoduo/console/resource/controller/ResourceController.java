package cn.xuqiudong.duoduo.console.resource.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.duoduo.console.resource.model.VicResource;
import cn.xuqiudong.duoduo.console.resource.service.ResourceService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :资源数据 控制层
 * @author Vic.xu
 * @since  2024-04-08 14:46
 */
@RestController
@RequestMapping("/resource")
public class ResourceController extends BaseController<ResourceService, VicResource>{



}
