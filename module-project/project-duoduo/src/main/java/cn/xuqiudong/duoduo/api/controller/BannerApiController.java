package cn.xuqiudong.duoduo.api.controller;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.duoduo.api.service.BannerApiService;
import cn.xuqiudong.duoduo.api.vo.BannerVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述: banner 图api
 * @author Vic.xu
 * @since 2024-04-09 15:15
 */
@RestController
@RequestMapping("/api/banner")
public class BannerApiController extends BaseApiController{

    @Resource
    private BannerApiService bannerApiService;

    /**
     * api:001:banner列表
     */
    @GetMapping("/list")
    public BaseResponse<List<BannerVo>> list(){
        List<BannerVo> list = bannerApiService.list();
        return BaseResponse.success(list);
    }
}
