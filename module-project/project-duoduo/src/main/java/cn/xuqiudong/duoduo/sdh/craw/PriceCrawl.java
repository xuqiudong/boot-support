package cn.xuqiudong.duoduo.sdh.craw;

import cn.xuqiudong.common.base.craw.BaseCrawl;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/5/11/0011 16:15
 */
public class PriceCrawl extends BaseCrawl {

    @Override
    protected int getTimeout() {
        return 30000;
    }
}
