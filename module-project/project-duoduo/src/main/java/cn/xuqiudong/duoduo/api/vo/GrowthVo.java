package cn.xuqiudong.duoduo.api.vo;

import cn.xuqiudong.duoduo.base.vo.BaseVo;

/**
 * 描述: 长成记返回的vo
 * @author Vic.xu
 * @since 2024-04-08 17:18
 */
public class GrowthVo extends BaseVo {

    private static final long serialVersionUID = 1L;

    private String age;

    private String date;

    private String img;

    private String content;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
