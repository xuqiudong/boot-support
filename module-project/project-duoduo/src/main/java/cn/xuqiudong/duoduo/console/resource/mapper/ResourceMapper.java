package cn.xuqiudong.duoduo.console.resource.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.duoduo.console.resource.model.VicResource;

/**
 *功能: :资源数据 Mapper
 * @author Vic.xu
 * @since  2024-04-08 14:46
 */
public interface ResourceMapper extends BaseMapper<VicResource> {
	
}
