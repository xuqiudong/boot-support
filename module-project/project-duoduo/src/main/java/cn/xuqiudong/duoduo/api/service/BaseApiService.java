package cn.xuqiudong.duoduo.api.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述: BaseApiService
 * @author Vic.xu
 * @since 2024-04-08 17:25
 */
public abstract class BaseApiService {

    protected Logger logger = LoggerFactory.getLogger(getClass());
}
