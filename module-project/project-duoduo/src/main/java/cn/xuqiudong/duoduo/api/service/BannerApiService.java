package cn.xuqiudong.duoduo.api.service;

import cn.xuqiudong.duoduo.api.vo.BannerVo;
import cn.xuqiudong.duoduo.console.banner.model.Banner;
import cn.xuqiudong.duoduo.console.banner.service.BannerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 描述: banner api service
 * @author Vic.xu
 * @since 2024-04-09 15:15
 */
@Service
public class BannerApiService {

    @Resource
    private BannerService bannerService;

    @Resource
    private ResourceApiService resourceApiService;

    /**
     * 获取banner图
     */
    public List<BannerVo> list() {
        Banner lookup = new Banner();
        List<Banner> list = bannerService.list(lookup);

        return list.stream().map(banner -> {
            BannerVo vo = new BannerVo();
            vo.setId(banner.getId());
            vo.setName(banner.getName());
            vo.setImage(resourceApiService.getResourcePreviewUrl(banner.getImage()));
            return vo;
        }).collect(Collectors.toList());
    }

}
