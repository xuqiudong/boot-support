package cn.xuqiudong.duoduo;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StopWatch;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-11-08 14:50
 */
@SpringBootApplication
@MapperScan(value = "cn.xuqiudong.duoduo.**.mapper")
public class DuoduoApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuoduoApplication.class);

    public static void main(String[] args) {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ConfigurableApplicationContext cax = SpringApplication.run(DuoduoApplication.class, args);
        stopWatch.stop();
        ServerProperties serverProperties = cax.getBean(ServerProperties.class);
        Integer port = serverProperties.getPort();
        ServerProperties.Servlet servlet = serverProperties.getServlet();
        String contextPath = servlet.getContextPath();
        String urlSuffix = StringUtils.isBlank(contextPath) ? String.valueOf(port) : port + contextPath;
        LOGGER.info("duoduo application 启动完成, 耗时{}ms, 请访问: http://127.0.0.1:{}", stopWatch.getTotalTimeMillis(), urlSuffix);

    }
}
