package cn.xuqiudong.duoduo.console.growth.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.duoduo.console.growth.model.Growth;
import cn.xuqiudong.duoduo.console.growth.service.GrowthService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *功能: :身高体重记录 控制层
 * @author Vic.xu
 * @since  2024-04-08 15:32
 */
@RestController
@RequestMapping("/growth/growth")
public class GrowthController extends BaseController<GrowthService, Growth>{

}
