package cn.xuqiudong.duoduo.console.resource.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.duoduo.console.resource.model.ResourceCategory;

/**
 *功能: :资源分类 Mapper
 * @author Vic.xu
 * @since  2024-04-08 14:46
 */
public interface ResourceCategoryMapper extends BaseMapper<ResourceCategory> {
	
}
