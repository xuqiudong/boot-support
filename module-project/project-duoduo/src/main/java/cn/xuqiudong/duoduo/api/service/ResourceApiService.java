package cn.xuqiudong.duoduo.api.service;

import cn.xuqiudong.duoduo.constant.CommonConstant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-08 17:37
 */
@Service
public class ResourceApiService {

    @Value("${vic.duoduo.resource.host}")
    private String resourceHost;

    /**
     * 获取资源预览地址
     * @param resourceId resource id
     * @return url
     */
    public String getResourcePreviewUrl(Integer resourceId) {
        String uri = CommonConstant.RESOURCE_PREVIEW_URI.replace("{id}",String.valueOf(resourceId));
        return resourceHost + uri;
    }
}
