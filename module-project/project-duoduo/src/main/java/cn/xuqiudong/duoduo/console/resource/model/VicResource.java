package cn.xuqiudong.duoduo.console.resource.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import cn.xuqiudong.duoduo.enums.ResourceOriginEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


/**
 * 资源数据 实体类
 *
 * @author Vic.xu
 */
public class VicResource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private String name;

    /**
     *
     */
    private Integer categoryId;

    /**
     * 备注说明
     */
    private String note;

    /**
     * 资源类型:image,video.
     */
    private String type;

    /**
     * 资源来源：local,network，attachment
     */
    private ResourceOriginEnum origin;

    /**
     * 那个瞬间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date moment;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 文件名:a.png
     */
    private String fileName;

    /**
     * 文件的路径:本地路径,网络url,附件id
     */
    private String filePath;

    /**
     * 缩略图(若有的话)
     */
    private String thumbnailPath;

    /**
     * 资源标签
     */
    private String tags;

    /**
     * 资源编码
     */
    private String code;


    /***************** set|get  start **************************************/
    /**
     * set：
     */
    public VicResource setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：
     */
    public String getName() {
        return name;
    }

    /**
     * set：
     */
    public VicResource setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }

    /**
     * get：
     */
    public Integer getCategoryId() {
        return categoryId;
    }

    /**
     * set：备注说明
     */
    public VicResource setNote(String note) {
        this.note = note;
        return this;
    }

    /**
     * get：备注说明
     */
    public String getNote() {
        return note;
    }

    /**
     * set：资源类型:imgage,video.
     */
    public VicResource setType(String type) {
        this.type = type;
        return this;
    }

    /**
     * get：资源类型:imgage,video.
     */
    public String getType() {
        return type;
    }

    /**
     * set：资源来源：local,network，attachment
     */
    public VicResource setOrigin(ResourceOriginEnum origin) {
        this.origin = origin;
        return this;
    }

    /**
     * get：资源来源：local,network，attachment
     */
    public ResourceOriginEnum getOrigin() {
        return origin;
    }

    /**
     * set：那个瞬间
     */
    public VicResource setMoment(Date moment) {
        this.moment = moment;
        return this;
    }

    /**
     * get：那个瞬间
     */
    public Date getMoment() {
        return moment;
    }

    /**
     * set：文件大小
     */
    public VicResource setFileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    /**
     * get：文件大小
     */
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * set：文件名:a.png
     */
    public VicResource setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    /**
     * get：文件名:a.png
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * set：文件的路径:本地路径,网络url,附件id
     */
    public VicResource setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    /**
     * get：文件的路径:本地路径,网络url,附件id
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * set：缩略图(若有的话)
     */
    public VicResource setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
        return this;
    }

    /**
     * get：缩略图(若有的话)
     */
    public String getThumbnailPath() {
        return thumbnailPath;
    }

    /**
     * set：资源标签
     */
    public VicResource setTags(String tags) {
        this.tags = tags;
        return this;
    }

    /**
     * get：资源标签
     */
    public String getTags() {
        return tags;
    }

    /**
     * set：资源编码
     */
    public VicResource setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * get：资源编码
     */
    public String getCode() {
        return code;
    }
    /***************** set|get  end **************************************/
}
