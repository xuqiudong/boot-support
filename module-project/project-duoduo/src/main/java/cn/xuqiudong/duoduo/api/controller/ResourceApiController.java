package cn.xuqiudong.duoduo.api.controller;

import cn.xuqiudong.duoduo.console.resource.service.ResourceService;
import cn.xuqiudong.duoduo.constant.CommonConstant;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 描述: 资源的一些api接口
 * @author Vic.xu
 * @since 2024-04-08 18:01
 */
@RestController
public class ResourceApiController extends BaseApiController{

    @Resource
    private ResourceService resourceService;


    /**
     * api:000:预览一个资源
     * @param id resource id
     */
    @GetMapping(CommonConstant.RESOURCE_PREVIEW_URI)
    public void previewResource(@PathVariable Integer id) throws IOException {
        resourceService.previewResource(id, request, response);
    }
}
