package cn.xuqiudong.duoduo.console.growth.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;


/**
 * 身高体重记录 实体类
 *
 * @author Vic.xu
 */
public class Growth extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 身高
     */
    private BigDecimal height;

    /**
     * 体重
     */
    private BigDecimal weight;

    /**
     * 测量日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date measureDate;

    /**
     * 测量地点
     */
    private String measurePlace;

    /**
     * 备注
     */
    private String note;

    /**
     * 图标
     */
    private Integer icon;


    /***************** set|get  start **************************************/
    /**
     * set：身高
     */
    public Growth setHeight(BigDecimal height) {
        this.height = height;
        return this;
    }

    /**
     * get：身高
     */
    public BigDecimal getHeight() {
        return height;
    }

    /**
     * set：体重
     */
    public Growth setWeight(BigDecimal weight) {
        this.weight = weight;
        return this;
    }

    /**
     * get：体重
     */
    public BigDecimal getWeight() {
        return weight;
    }

    /**
     * set：测量日期
     */
    public Growth setMeasureDate(Date measureDate) {
        this.measureDate = measureDate;
        return this;
    }

    /**
     * get：测量日期
     */
    public Date getMeasureDate() {
        return measureDate;
    }

    /**
     * set：测量地点
     */
    public Growth setMeasurePlace(String measurePlace) {
        this.measurePlace = measurePlace;
        return this;
    }

    /**
     * get：测量地点
     */
    public String getMeasurePlace() {
        return measurePlace;
    }

    /**
     * set：备注
     */
    public Growth setNote(String note) {
        this.note = note;
        return this;
    }

    /**
     * get：备注
     */
    public String getNote() {
        return note;
    }

    /**
     * set：图标
     */
    public Growth setIcon(Integer icon) {
        this.icon = icon;
        return this;
    }

    /**
     * get：图标
     */
    public Integer getIcon() {
        return icon;
    }
    /***************** set|get  end **************************************/
}
