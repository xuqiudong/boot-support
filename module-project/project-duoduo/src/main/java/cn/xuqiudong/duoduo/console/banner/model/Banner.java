package cn.xuqiudong.duoduo.console.banner.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 * banner图 实体类
 *
 * @author Vic.xu
 */
public class Banner extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * resource id
     */
    private Integer image;

    /**
     * name
     */
    private String name;

    /**
     * 备注
     */
    private Integer sort;

    /**
     *
     */
    private String note;


    /* **************** set|get  start **************************************/
    /**
     * set：resource id
     */
    public Banner setImage(Integer image) {
        this.image = image;
        return this;
    }

    /**
     * get：resource id
     */
    public Integer getImage() {
        return image;
    }

    /**
     * set：name
     */
    public Banner setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：name
     */
    public String getName() {
        return name;
    }

    /**
     * set：备注
     */
    public Banner setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    /**
     * get：备注
     */
    public Integer getSort() {
        return sort;
    }

    /**
     * set：
     */
    public Banner setNote(String note) {
        this.note = note;
        return this;
    }

    /**
     * get：
     */
    public String getNote() {
        return note;
    }
    /***************** set|get  end **************************************/
}
