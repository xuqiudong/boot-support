package cn.xuqiudong.duoduo.console.resource.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 * 资源分类 实体类
 *
 * @author Vic.xu
 */
public class ResourceCategory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 所属父类id
     */
    private Integer pid;

    /**
     * 备注说明
     */
    private String note;


    /***************** set|get  start **************************************/
    /**
     * set：名称
     */
    public ResourceCategory setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：所属父类id
     */
    public ResourceCategory setPid(Integer pid) {
        this.pid = pid;
        return this;
    }

    /**
     * get：所属父类id
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * set：备注说明
     */
    public ResourceCategory setNote(String note) {
        this.note = note;
        return this;
    }

    /**
     * get：备注说明
     */
    public String getNote() {
        return note;
    }
    /***************** set|get  end **************************************/
}
