package cn.xuqiudong.duoduo.constant;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.util.Date;

/**
 * 描述: 一些常量
 * @author Vic.xu
 * @since 2024-03-25 10:50
 */
public final class CommonConstant {

    /**
     * 生日
     */
    private static final String BIRTHDAY_STR = "2021-01-27";

    /**
     * 生日
     */
    @SuppressFBWarnings("MS_SHOULD_BE_FINAL")
    public static Date birthday = parseDate();

    /**
     * 访问资源的请求地址
     */
    public static final String RESOURCE_PREVIEW_URI = "/api/resource/preview/{id}";



    public static Date parseDate(){
        try {
            return DateUtils.parseDate(BIRTHDAY_STR, "yyyy-MM-dd");
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }



    public static void main(String[] args) {
        System.out.println(birthday);
    }
}
