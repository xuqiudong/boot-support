package cn.xuqiudong.duoduo.console.banner.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.duoduo.console.banner.model.Banner;
/**
 *功能: :banner图 Mapper
 * @author Vic.xu
 * @since  2024-04-09 11:06
 */
public interface BannerMapper extends BaseMapper<Banner> {
	
}
