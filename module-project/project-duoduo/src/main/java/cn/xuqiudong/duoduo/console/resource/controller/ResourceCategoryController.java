package cn.xuqiudong.duoduo.console.resource.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.duoduo.console.resource.model.ResourceCategory;
import cn.xuqiudong.duoduo.console.resource.service.ResourceCategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :资源分类 控制层
 * @author Vic.xu
 * @since  2024-04-08 14:46
 */
@RestController
@RequestMapping("/resource/resourceCategory")
public class ResourceCategoryController extends BaseController<ResourceCategoryService, ResourceCategory>{

}
