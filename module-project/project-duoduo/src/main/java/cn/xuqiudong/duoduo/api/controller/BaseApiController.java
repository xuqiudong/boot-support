package cn.xuqiudong.duoduo.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-08 18:02
 */
public class BaseApiController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    protected HttpServletRequest request;
    @Resource
    protected HttpServletResponse response;

}
