package cn.xuqiudong.duoduo.console.banner.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.duoduo.console.banner.model.Banner;
import cn.xuqiudong.duoduo.console.banner.service.BannerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :banner图 控制层
 * @author Vic.xu
 * @since  2024-04-09 11:06
 */
@RestController
@RequestMapping("/banner/banner")
public class BannerController extends BaseController<BannerService, Banner>{

}
