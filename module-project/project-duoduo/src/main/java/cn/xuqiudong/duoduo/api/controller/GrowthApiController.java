package cn.xuqiudong.duoduo.api.controller;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.duoduo.api.service.GrowthApiService;
import cn.xuqiudong.duoduo.api.vo.GrowthVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述: 长成记
 * @author Vic.xu
 * @since 2024-04-09 15:15
 */
@RestController
@RequestMapping("/api/growth")
public class GrowthApiController extends BaseApiController{

    @Resource
    private GrowthApiService growthApiService;

    /**
     * api:002:growth列表
     */
    @GetMapping("/list")
    public BaseResponse<List<GrowthVo>> list(){
        List<GrowthVo> list = growthApiService.list();
        return BaseResponse.success(list);
    }
}
