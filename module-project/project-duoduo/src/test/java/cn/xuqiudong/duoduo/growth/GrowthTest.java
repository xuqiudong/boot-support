package cn.xuqiudong.duoduo.growth;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.duoduo.BaseTest;
import cn.xuqiudong.duoduo.console.growth.model.Growth;
import cn.xuqiudong.duoduo.console.growth.service.GrowthService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-08 15:38
 */
public class GrowthTest extends BaseTest {

    @Resource
    private GrowthService growthService;

    @Test
    public void list(){
        List<Growth> list = growthService.list(new Growth());
        JsonUtil.printJson(list);
    }
}
