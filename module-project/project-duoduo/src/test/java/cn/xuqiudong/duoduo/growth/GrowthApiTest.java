package cn.xuqiudong.duoduo.growth;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.duoduo.BaseTest;
import cn.xuqiudong.duoduo.api.service.GrowthApiService;
import cn.xuqiudong.duoduo.api.vo.GrowthVo;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-09 10:34
 */
public class GrowthApiTest extends BaseTest {

    @Resource
    private GrowthApiService growthApiService;

    @Test
    public void list() {
        List<GrowthVo> growthVos = growthApiService.list();
        JsonUtil.printJson(growthVos);
    }
}
