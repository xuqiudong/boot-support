package cn.xuqiudong.duoduo.resource;

import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.duoduo.BaseTest;
import cn.xuqiudong.duoduo.console.resource.model.ResourceCategory;
import cn.xuqiudong.duoduo.console.resource.model.VicResource;
import cn.xuqiudong.duoduo.console.resource.service.ResourceCategoryService;
import cn.xuqiudong.duoduo.console.resource.service.ResourceService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-08 15:01
 */
public class ResourceTest extends BaseTest {

    @Resource
    private ResourceService resourceService;

    @Resource
    private ResourceCategoryService resourceCategoryService;

    @Test
    public void page(){
        PageInfo<VicResource> page = resourceService.page(new VicResource());
        JsonUtil.printJson(page);
    }

    @Test
    public void categoryPage(){
        PageInfo<ResourceCategory> page = resourceCategoryService.page(new ResourceCategory());
        JsonUtil.printJson(page);
    }
}
