package cn.xuqiudong.duoduo;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-08 14:57
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseTest {

}
