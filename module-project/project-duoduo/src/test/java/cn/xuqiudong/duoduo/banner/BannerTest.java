package cn.xuqiudong.duoduo.banner;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.duoduo.BaseTest;
import cn.xuqiudong.duoduo.api.service.BannerApiService;
import cn.xuqiudong.duoduo.api.vo.BannerVo;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-04-09 15:24
 */
public class BannerTest extends BaseTest {

    @Resource
    private BannerApiService bannerApiService;


    @Test
    public void list(){
        List<BannerVo> list = bannerApiService.list();
        JsonUtil.printJson(list);
    }
}
