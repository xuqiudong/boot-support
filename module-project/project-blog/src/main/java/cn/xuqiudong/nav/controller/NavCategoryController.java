package cn.xuqiudong.nav.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.nav.model.NavCategory;
import cn.xuqiudong.nav.service.NavCategoryService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *功能: :导航分类 控制层
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@RestController
@RequestMapping("/nav/category")
public class NavCategoryController extends BaseController<NavCategoryService, NavCategory>{


    @CrossOrigin
    @GetMapping("/all")
    public BaseResponse<?> allCategories(){
        List<NavCategory> navCategories = service.allCategories();
        return BaseResponse.success(navCategories);
    }
}
