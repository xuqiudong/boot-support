package cn.xuqiudong.nav.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.nav.mapper.NavCategoryMapper;
import cn.xuqiudong.common.nav.mapper.NavSiteMapper;
import cn.xuqiudong.common.nav.model.NavCategory;
import cn.xuqiudong.common.nav.model.NavSite;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *功能: :导航分类 Service
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@Service
public class NavCategoryService extends BaseService<NavCategoryMapper, NavCategory>{

    @Resource
    private NavSiteMapper navSiteMapper;
    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 全部的导航地址
     */
    public List<NavCategory> allCategories() {
        NavCategory lookup = new NavCategory();
        lookup.setEnable(true);
        List<NavCategory> list = mapper.list(lookup);
        NavSite siteLookup = new NavSite();
        siteLookup.setEnable(true);
        LinkedHashMap<String, List<NavSite>> collect = navSiteMapper.list(siteLookup).stream().collect(Collectors.groupingBy(NavSite::getCategoryCode, LinkedHashMap::new, Collectors.toList()));
        list.forEach(category ->{
            category.setSites(collect.get(category.getCode()));
        });
        return list;

    }
}
