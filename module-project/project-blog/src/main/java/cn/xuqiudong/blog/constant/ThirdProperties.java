package cn.xuqiudong.blog.constant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Vic.xu
 * 说明 :  第三方授权配置文件
 * @since  2020/7/22 0022 10:36
 */
@Configuration
@ConfigurationProperties(prefix = "third")
public class ThirdProperties {

    private Logger logger = LoggerFactory.getLogger(ThirdProperties.class);

    /**
     * 第三方登陆配置列表
     */
    private List<ThirdAuthConfig> configs = new ArrayList<>();


    private final Map<String, ThirdAuthConfig> configMap = new HashMap<>();

    /**
     * 初始化后把list转程map，key为flag
     */
    @PostConstruct
    public void post() {
        configs.forEach(c -> {
            configMap.put(c.getType(), c);
        });
        logger.info("第三方授权配置项：{}", configMap);
    }

    public List<ThirdAuthConfig> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ThirdAuthConfig> configs) {
        this.configs = configs;
    }

    /**
     * 获得当前第三方的配置
     * @param flag
     * @return
     */
    public ThirdAuthConfig getConfigByType(String flag) {
        return configMap.get(flag);
    }

    public static class ThirdAuthConfig {

        /**
         * 第三方标志  如 gitee,github,qq等
         */
        public String type;

        private String clientId;

        private String clientSecret;

        /**
         * 回调地址  用以返回code
         */
        private String callback;

        /**
         * 前往授权的地址
         */
        private String renderUrl;
        /**
         * 获取accessToken的地址
         */
        private String accessTokenUrl;

        private String userUrl;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getClientId() {
            return clientId;
        }

        public void setClientId(String clientId) {
            this.clientId = clientId;
        }

        public String getClientSecret() {
            return clientSecret;
        }

        public void setClientSecret(String clientSecret) {
            this.clientSecret = clientSecret;
        }

        public String getCallback() {
            return callback;
        }

        public void setCallback(String callback) {
            this.callback = callback;
        }

        public String getRenderUrl() {
            return renderUrl;
        }

        public void setRenderUrl(String renderUrl) {
            this.renderUrl = renderUrl;
        }

        public String getAccessTokenUrl() {
            return accessTokenUrl;
        }

        public void setAccessTokenUrl(String accessTokenUrl) {
            this.accessTokenUrl = accessTokenUrl;
        }

        public String getUserUrl() {
            return userUrl;
        }

        public void setUserUrl(String userUrl) {
            this.userUrl = userUrl;
        }

        @Override
        public String toString() {
            return "ThirdAuthConfig{" +
                    "type='" + type + '\'' +
                    ", clientId='" + clientId + '\'' +
                    ", clientSecret='" + clientSecret + '\'' +
                    ", callback='" + callback + '\'' +
                    ", renderUrl='" + renderUrl + '\'' +
                    ", accessTokenUrl='" + accessTokenUrl + '\'' +
                    ", userUrl='" + userUrl + '\'' +
                    '}';
        }
    }
}
