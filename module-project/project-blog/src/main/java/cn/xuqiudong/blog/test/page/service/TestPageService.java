package cn.xuqiudong.blog.test.page.service;

import cn.xuqiudong.blog.test.page.mapper.TestPageMapper;
import cn.xuqiudong.common.base.lookup.Lookup;
import com.github.pagehelper.PageHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-08-28 14:48
 */
@Service
public class TestPageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestPageService.class);

    private TestPageMapper testPageMapper;

    public TestPageService(TestPageMapper testPageMapper) {
        this.testPageMapper = testPageMapper;
    }


    public List<Map<String, Object>> topicList(Lookup lookup){
        LOGGER.info("进入分页查询topicList"  );
        PageHelper.startPage(lookup.getPage(), lookup.getSize());
        PageHelper.clearPage();
        if (lookup.getSize() == 2) {
            LOGGER.info("进入分页查询topicList, 但是只分页没查询"  );
            return null;
        }
        return testPageMapper.topicList();
    }

    public List<Map<String, Object>> categoryList(){
        LOGGER.info("进入不分页查询categoryList"  );
        return testPageMapper.categoryList();
    }




}
