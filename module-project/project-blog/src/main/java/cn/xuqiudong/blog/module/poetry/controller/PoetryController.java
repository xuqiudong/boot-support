package cn.xuqiudong.blog.module.poetry.controller;

import cn.xuqiudong.blog.base.BaseBlogController;
import cn.xuqiudong.blog.module.other.model.TimelineModel;
import cn.xuqiudong.blog.module.poetry.service.BlogPoetryService;
import cn.xuqiudong.common.blog.model.BlogPoetry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author Vic.xu
 * 说明 :  诗歌
 * @since  2020/8/19 0019 15:28
 */
@Controller
public class PoetryController extends BaseBlogController {

    @Autowired
    private BlogPoetryService blogPoetryService;

    /**
     * 不分页的诗歌
     * @return
     */
    @GetMapping("/poetry")
    public String poetry(Model model) {
        List<TimelineModel> timeline = blogPoetryService.timeline();
        model.addAttribute("list", timeline);
        return "poetry";
    }

    @GetMapping("/poetry/{id}")
    public String detail(@PathVariable int id, Model model) {
        BlogPoetry entity = blogPoetryService.findById(id);
        model.addAttribute("entity", entity);
        return "poetry-detail";
    }

}
