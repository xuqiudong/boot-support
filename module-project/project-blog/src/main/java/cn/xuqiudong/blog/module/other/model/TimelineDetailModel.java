package cn.xuqiudong.blog.module.other.model;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 * 说明 :  时间轴（归档） 列表对象
 * @author Vic.xu
 * @since  2020/7/9 0009 17:24
 */
public class TimelineDetailModel implements Serializable {

    private static final long serialVersionUID = 884428701620816528L;
    /**
     * id
     */
    private Integer id;
    /**
     * 年
     */
    private Integer year;

    /**
     * 月
     */
    private Integer month;

    /**
     * 日
     */
    private Integer day;

    /**
     * 标题
     */
    private String title;


    private Date createTime;


    /**
     * 获取年月
     * @return
     */
    public String getDate() {
        if (createTime == null) {
            createTime = new Date();
        }
        return DateFormatUtils.format(createTime, "yyyy-MM");
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建日期的时候 设置年月日
     * @param createTime
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
        handTime();
    }

    /**
     * 分解创建时间
     */
    public void handTime() {
        if (createTime != null) {
            Calendar calender = Calendar.getInstance();
            calender.setTime(createTime);
            this.year = calender.get(Calendar.YEAR);
            this.month = calender.get(Calendar.MONTH) + 1;
            this.day = calender.get(Calendar.DAY_OF_MONTH);
        }
    }
}
