package cn.xuqiudong.blog.module.blog.controller;

import cn.xuqiudong.blog.base.BaseBlogController;
import cn.xuqiudong.blog.module.blog.model.BlogHomeModel;
import cn.xuqiudong.blog.module.blog.service.BlogArticleService;
import cn.xuqiudong.blog.module.blog.service.BlogHomeService;
import cn.xuqiudong.blog.module.other.service.OtherService;
import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.blog.config.es.model.EsBlogArticleModel;
import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.common.blog.model.BlogComment;
import cn.xuqiudong.elasticsearch.model.EsLookup;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

/**
 * @author Vic.xu
 * 说明 :  首页
 * @since 2020/5/25 0025 8:52
 */
@Controller
public class BlogHomeController extends BaseBlogController {


    private final BlogHomeService blogHomeService;

    private final BlogArticleService blogArticleService;

    private final OtherService otherService;

    public BlogHomeController(BlogHomeService blogHomeService, BlogArticleService blogArticleService, OtherService otherService) {
        this.blogHomeService = blogHomeService;
        this.blogArticleService = blogArticleService;
        this.otherService = otherService;
    }

    /*
      1. 最新文章 N 条
      2. 首页的专题,各N条
      3. TODO
      4. 功能展示
      5. 杂感
      6. 侧边归档:只做展示使用吗?还是切到列表页
          默认10条?

     */

    /*
      1. 列表页
         需要带查询条件吗?分类下:标签/专题  专题下:标签/分类?
         需要排序吗:时间/点击次数
         分类:
         专题:
      2. 详情页:
          评论先不管: 接第三方登录
     */


    /**
     * 首页
     */
    @GetMapping(value = {"", "/"})
    public String home(Model model) {
        BlogHomeModel blogHomeModel = blogHomeService.homeQuery();
        model.addAttribute("model", blogHomeModel);
        return "index";
    }



    /* ***************************************************************************************************/

    /**
     * 专题下的文章列表页
     * @param tid: m某个专题id
     */
    @GetMapping(value = "topics")
    public String topics(Model model, Integer tid, @ModelAttribute(LOOKUP) BlogArticle lookup) {
        if (tid != null) {
            lookup.setTopicId(tid);
        }
        PageInfo<BlogArticle> pageInfo = blogHomeService.page(lookup);
        model.addAttribute(PAGER, pageInfo);
        model.addAttribute("topics", blogHomeService.enableTopicList());
        // 可用标签
        blogHomeService.putTagToModel(model);
        //当前专题详情
        blogHomeService.putCurrentTopicToModel(model, lookup);
        return "list";
    }

    /**
     * 专题文章列表查询 :  使用RedirectAttributes.addFlashAttribute 通过session传递查询参数(传递后会在session中擦除),使用@ModelAttribute在控制类中接收参数
     * @param lookup BlogArticle
     */
    @PostMapping(value = "topics")
    public String topics(BlogArticle lookup, RedirectAttributes attributes) {
        attributes.addFlashAttribute(LOOKUP, lookup);
        return "redirect:topics";
    }
    /* ***************************************************************************************************/

    /**
     * 分类下的文章列表
     * @param cid category id
     */
    @GetMapping(value = "categories")
    public String categories(Model model, Integer cid, @ModelAttribute(LOOKUP) BlogArticle lookup) {
        if (cid != null) {
            lookup.setCategoryId(cid);
        }
        PageInfo<BlogArticle> pageInfo = blogHomeService.page(lookup);
        model.addAttribute(PAGER, pageInfo);
        model.addAttribute("categories", blogHomeService.enableCategoryList());

        blogHomeService.putTagToModel(model);
        //当前分类详情
        blogHomeService.putCurrentCategoryToModel(model, lookup);
        return "list";
    }

    /**
     * 分类下的文章列表查询
     * @param lookup BlogArticle
     */
    @PostMapping(value = "categories")
    public String categories(BlogArticle lookup, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("lookup", lookup);
        return "redirect:categories";
    }

    /* ***************************************************************************************************/

    /**
     * 详情页
     */
    @GetMapping(value = "/detail/{id}")
    public String detail(@PathVariable int id, Model model) {
        BlogArticle entity = blogHomeService.findById(id);
        model.addAttribute("entity", entity);
        BlogComment lookup = new BlogComment();
        lookup.setSize(Integer.MAX_VALUE);
        lookup.setArticleId(id);
        PageInfo<BlogComment> pager = otherService.comments(lookup);
        model.addAttribute("pager", pager);
        return "detail";
    }

    /**
     *根据关键词搜索
     */
    @GetMapping(value = "/search")
    public String search(@ModelAttribute(value = "lookup") EsLookup<EsBlogArticleModel> lookup, Model model) throws IOException {
        try {
            PageInfo<EsBlogArticleModel> pageInfo = blogHomeService.search(lookup);
            model.addAttribute("pager", pageInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "search";
    }

    /**
     * 根据关键词搜索 翻页
     */
    @PostMapping(value = "search")
    public String search(EsLookup<EsBlogArticleModel> lookup, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("lookup", lookup);
        return "redirect:search";
    }
}
