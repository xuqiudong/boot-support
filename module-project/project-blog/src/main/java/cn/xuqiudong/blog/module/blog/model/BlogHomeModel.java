package cn.xuqiudong.blog.module.blog.model;

import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.common.blog.model.BlogCategory;
import cn.xuqiudong.common.blog.model.BlogTag;
import cn.xuqiudong.common.blog.model.BlogTopic;

import java.util.List;

/**
 * @author Vic.xu
 * 说明 :  博客首页聚合 MODEL
 *  1.最新文章 取10条
 *  2. 各个在首页展示的专题，每个专题下10条
 * @since  2020/5/25 0025 17:24
 */
public class BlogHomeModel {

    /**
     * 最新文章
     */
    private List<BlogArticle> newest;

    /**
     * 首页展示的专题
     */
    private List<ShowTopicModel> showTopics;

    /**
     *全部分类
     */
    private List<BlogCategory> blogCategories;

    /**
     * 全部专题
     */
    private List<BlogTopic> blogTopics;

    /**
     * 全部标签
     */
    private List<BlogTag> blogTags;

    public List<BlogArticle> getNewest() {
        return newest;
    }

    public void setNewest(List<BlogArticle> newest) {
        this.newest = newest;
    }

    public List<ShowTopicModel> getShowTopics() {
        return showTopics;
    }

    public void setShowTopics(List<ShowTopicModel> showTopics) {
        this.showTopics = showTopics;
    }

    public List<BlogCategory> getBlogCategories() {
        return blogCategories;
    }

    public void setBlogCategories(List<BlogCategory> blogCategories) {
        this.blogCategories = blogCategories;
    }

    public List<BlogTopic> getBlogTopics() {
        return blogTopics;
    }

    public void setBlogTopics(List<BlogTopic> blogTopics) {
        this.blogTopics = blogTopics;
    }

    public List<BlogTag> getBlogTags() {
        return blogTags;
    }

    public void setBlogTags(List<BlogTag> blogTags) {
        this.blogTags = blogTags;
    }
}
