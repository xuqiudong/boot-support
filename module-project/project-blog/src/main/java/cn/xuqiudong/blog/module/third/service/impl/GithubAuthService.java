package cn.xuqiudong.blog.module.third.service.impl;

import cn.xuqiudong.blog.constant.GlobalFieldParams;
import cn.xuqiudong.blog.constant.ThirdProperties;
import cn.xuqiudong.blog.constant.ThirdUserType;
import cn.xuqiudong.blog.module.third.service.AbstractAuthService;
import cn.xuqiudong.common.base.craw.CrawlConnect;
import cn.xuqiudong.common.base.exception.CommonException;
import cn.xuqiudong.common.blog.model.BlogThirdUser;
import cn.xuqiudong.common.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * @author Vic.xu
 * 说明 :  github 授权实现
 * @since  2020/7/21 0021 17:57
 */
@Service
public class GithubAuthService extends AbstractAuthService {


    @Override
    public GlobalFieldParams.ThirdTypeEnum type() {
        return GlobalFieldParams.ThirdTypeEnum.github;
    }

    /**
     * 获取AccessToken   @link https://docs.github.com/en/developers/apps/authorizing-oauth-apps
     * @param code
     *
     * hub.com/login/oauth/access_token?client_id=${third.github.clientId}&client_secret=${third.github.clientSecret}&code=${code}&redirect_uri=${third.github.callback}&state=vic_${random.int[10000,20000]}
     * @return Access Token
     */
    @Override
    public String getAccessToken(String code) {
        ThirdProperties.ThirdAuthConfig config = config();
        CrawlConnect crawlConnect = CrawlConnect.build(config.getAccessTokenUrl()).validateTlsCertificates();
        crawlConnect.data("client_secret", config.getClientSecret());
        crawlConnect.data("code", code);
        crawlConnect.header("Accept", "application/json");
        try {
            /*
            text 的数据格式
           {"access_token":"e72e16c7e42f292c6912e7710c838347ae178b4a", "scope":"repo,gist", "token_type":"bearer"}
             */
            String text = crawlConnect.postBodyText();
            logger.info("通过code[{}]获取授权码返回数据[{}]", code, text);
            return JsonUtil.jsonToObject(text, new TypeReference<Map<String, String>>() {
            }).get("access_token");
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            throw new CommonException("获取授权码出了点问题");
        }
    }

    /**
     * 根据授权码获取用户信息
     * @param accessToken
     * @return BlogThirdUser
     */
    @Override
    public BlogThirdUser getUserInfo(String accessToken) {
        CrawlConnect crawlConnect = CrawlConnect.build(config().getUserUrl()).validateTlsCertificates();
        crawlConnect.data("access_token", accessToken);
        crawlConnect.header("Accept", "application/json");
        //Authorization: token OAUTH-TOKEN
        crawlConnect.header("Authorization", "token " + accessToken);
        try {
             /*
            {
            "login": "xuduochoua",
            "id": 13197198,
            "node_id": "MDQ6VXNlcjEzMTk3MTk4",
            "avatar_url": "https://avatars3.githubusercontent.com/u/13197198?v=4",
            "gravatar_id": "",
            "url": "https://api.github.com/users/xuduochoua",
            "html_url": "https://github.com/xuduochoua",
            "followers_url": "https://api.github.com/users/xuduochoua/followers",
            "following_url": "https://api.github.com/users/xuduochoua/following{/other_user}",
            "gists_url": "https://api.github.com/users/xuduochoua/gists{/gist_id}",
            "starred_url": "https://api.github.com/users/xuduochoua/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/xuduochoua/subscriptions",
            "organizations_url": "https://api.github.com/users/xuduochoua/orgs",
            "repos_url": "https://api.github.com/users/xuduochoua/repos",
            "events_url": "https://api.github.com/users/xuduochoua/events{/privacy}",
            "received_events_url": "https://api.github.com/users/xuduochoua/received_events",
            "type": "User",
            "site_admin": false,
            "name": null,
            "company": null,
            "blog": "",
            "location": null,
            "email": null,
            "hireable": null,
            "bio": null,
            "twitter_username": null,
            "public_repos": 21,
            "public_gists": 0,
            "followers": 0,
            "following": 2,
            "created_at": "2015-07-06T08:53:36Z",
            "updated_at": "2020-07-28T08:11:10Z",
            "private_gists": 0,
            "total_private_repos": 0,
            "owned_private_repos": 0,
            "disk_usage": 152,
            "collaborators": 0,
            "two_factor_authentication": false,
            "plan": {
                "name": "free",
                "space": 976562499,
                "collaborators": 0,
                "private_repos": 10000
            }
        }

             */
            String text = crawlConnect.getBodyText();
            Map<String, Object> result = JsonUtil.jsonToObject(text, new TypeReference<Map<String, Object>>() {
            });
            BlogThirdUser user = new BlogThirdUser();
            user.setAvatar(String.valueOf(result.get("avatar_url")));
            user.setType(ThirdUserType.github.name());
            user.setIdentifier(String.valueOf(result.get("login")));
            user.setNickname(String.valueOf(result.get("name")));
            logger.info("accessToken[{}]获取用户信息返回数据[{}]", accessToken, text);
            return user;
        } catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            throw new CommonException("获取账户信息出了点问题");
        }
    }

}
