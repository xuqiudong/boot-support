package cn.xuqiudong.blog.module.blog.model;

import cn.xuqiudong.common.blog.model.BlogArticleFile;

import java.util.List;

/**
 * 描述: 归档按年汇总
 * @author Vic.xu
 * @date 2023-12-19 16:12
 */
public class YearBlogFileModel {

    /**
     * 年份
     */
    private Integer year;

    /**
     * 本年度总数量
     */
    private Integer num;

    /**
     * 本年度每月汇总
     */
    private List<BlogArticleFile> files;

    public YearBlogFileModel() {
    }

    public YearBlogFileModel(Integer year, Integer num, List<BlogArticleFile> files) {
        this.year = year;
        this.num = num;
        this.files = files;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public List<BlogArticleFile> getFiles() {
        return files;
    }

    public void setFiles(List<BlogArticleFile> files) {
        this.files = files;
    }
}
