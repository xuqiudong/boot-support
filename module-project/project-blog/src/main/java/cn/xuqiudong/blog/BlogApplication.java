package cn.xuqiudong.blog;

import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


/**
 *  说明 :  博客启动类
 *  @author Vic.xu
 * @since  2020年5月6日下午3:12:43
 */

@EnableScheduling
@SpringBootApplication(scanBasePackages = {"cn.xuqiudong.blog", "cn.xuqiudong.common.blog", "cn.xuqiudong.nav"})
@MapperScan(value = {"cn.xuqiudong.blog.**.mapper", "cn.xuqiudong.common.blog.**.mapper", "cn.xuqiudong.common.nav.**.mapper"})
public class BlogApplication {
    private static Logger logger = LoggerFactory.getLogger(BlogApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
        logger.debug("BlogApplication has started.....");
    }
}
