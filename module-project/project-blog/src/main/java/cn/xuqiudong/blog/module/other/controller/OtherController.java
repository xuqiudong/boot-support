package cn.xuqiudong.blog.module.other.controller;

import cn.xuqiudong.blog.base.BaseBlogController;
import cn.xuqiudong.blog.constant.GlobalFieldParams;
import cn.xuqiudong.blog.module.blog.service.BlogHomeService;
import cn.xuqiudong.blog.module.other.model.TimelineModel;
import cn.xuqiudong.blog.module.other.service.GlobalConfigService;
import cn.xuqiudong.blog.module.other.service.OtherService;
import cn.xuqiudong.common.base.lookup.Lookup;
import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.blog.model.GlobalConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Properties;

/**
 * 说明 :  一些其他的页面的访问
 * @author Vic.xu
 * @since 2020/6/11 0011 9:14
 */
@Controller
public class OtherController extends BaseBlogController {

    @Autowired
    private OtherService otherService;

    @Autowired
    private BlogHomeService blogHomeService;

    @Autowired
    private GlobalConfigService globalConfigService;

    @Value("${server.name}")
    private String serverName;

    /**
     * 关于我
     */
    @GetMapping(value = "about")
    public String about(Model model) {
        GlobalConfig about = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.about);
        model.addAttribute("about", about.getContent());
        model.addAttribute("ip", getComputerInfo().toLowerCase());
        return "about";
    }

    private String getComputerInfo() {
        Properties props = System.getProperties();
        return "[system: " + props.getProperty("os.name") + "-" + props.getProperty("os.version") + "]; " +
                "[jdk: " + props.getProperty("java.version") + "]; " +
                "[user: " + props.getProperty("user.name") + "]; " +
                "[location: "+ serverName + "]";
    }


    /**
     * 日志归档 get
     */
    @GetMapping("/timeline")
    public String timeline(@ModelAttribute("lookup") Lookup lookup, Model model) {
        PageInfo<TimelineModel> pageInfo = otherService.timelinePage(lookup);
        model.addAttribute("pager", pageInfo);
        return "timeline";
    }

    /**
     * 日志归档 post
     * @param lookup lookup
     * @param attributes RedirectAttributes
     */
    @PostMapping("timeline")
    public String searchTimeline(Lookup lookup, RedirectAttributes attributes) {
        attributes.addFlashAttribute("lookup", lookup);
        return "redirect:timeline";
    }

    /* ****************************************************************************************** */


}
