package cn.xuqiudong.blog.config.web;

import cn.xuqiudong.common.base.web.intercept.SaveRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author Vic.xu
 * 说明 :  注册一些web级别的配置
 * @since  2020/7/22 0022 17:26
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 注册个最后的GET请求拦截器
     */
    @Bean
    public SaveRequestInterceptor saveRequestInterceptor() {
        return new SaveRequestInterceptor();
    }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //最后一次get请求url保存到session
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(saveRequestInterceptor());
        interceptorRegistration.excludePathPatterns("/blog/**");
        interceptorRegistration.addPathPatterns("/", "/comment", "/about", "/topics", "/categories", "/detail/**", "/search", "/timeline", "/poetry/**");
    }

}
