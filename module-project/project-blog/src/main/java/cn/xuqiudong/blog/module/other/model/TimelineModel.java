package cn.xuqiudong.blog.module.other.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 说明 :  时间轴（归档） 列表对象
 * @author Vic.xu
 * @since  2020/7/9 0009 17:24
 */
public class TimelineModel {

    /**
     * 时间： year + month
     */
    private String date;


    /**
     * 当前年月里的详情
     */
    private List<TimelineDetailModel> details = new ArrayList<>();

    public TimelineModel() {
    }

    public TimelineModel(String date, List<TimelineDetailModel> details) {
        this.date = date;
        this.details = details;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<TimelineDetailModel> getDetails() {
        return details;
    }

    public void setDetails(List<TimelineDetailModel> details) {
        this.details = details;
    }
}
