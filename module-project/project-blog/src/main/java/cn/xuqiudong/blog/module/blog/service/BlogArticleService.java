package cn.xuqiudong.blog.module.blog.service;

import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.config.es.model.EsBlogArticleModel;
import cn.xuqiudong.common.blog.config.es.service.EsBlogArticleService;
import cn.xuqiudong.common.blog.mapper.BlogArticleMapper;
import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.common.util.NumberUtils;
import cn.xuqiudong.elasticsearch.model.EsLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;

/**
 * @author Vic.xu
 * 描述:博客表 Service
 * @since  2020-05-08 10:53
 */
@Service
public class BlogArticleService extends BaseService<BlogArticleMapper, BlogArticle> {

    protected static Logger logger = LoggerFactory.getLogger(BlogArticleService.class);

    @Autowired
    private EsBlogArticleService esBlogArticleService;

    @Autowired
    private BlogCategoryService blogCategoryService;

    @Autowired
    private BlogTopicService blogTopicService;

    @Override
    protected boolean hasAttachment() {
        return true;
    }

    /**
     * 保存的时候同时操作ES
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int save(BlogArticle entity) {
        int num = super.save(entity);
        esBlogArticleService.save(entity);
        return num;
    }

    /**
     * 新增的时候更新分类和专题的文章数量
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insert(BlogArticle entity) {
        int num = super.insert(entity);
        updateArticleNum(entity);
        return num;
    }

    /**
     * 新增的时候更新分类和专题的文章数量
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int update(BlogArticle entity) {
        BlogArticle old = findById(entity.getId());
        int num = super.update(entity);
        boolean hasChanged =
                !NumberUtils.equals(entity.getCategoryId(), old.getCategoryId()) || !NumberUtils.equals(entity.getTopicId(), old.getTopicId());
        if (hasChanged) {
            updateArticleNum(entity);
            updateArticleNum(old);
        }
        return num;
    }

    /**
     * 更新分类/主题中文章数量
     */
    public void updateArticleNum(BlogArticle entity) {
        if (entity == null) {
            return;
        }
        Integer cId = entity.getCategoryId();
        Integer tId = entity.getTopicId();
        if (cId != null) {
            blogCategoryService.updateArticleNum(cId);
        }
        if (tId != null) {
            blogTopicService.updateArticleNum(tId);
        }
    }

    /**
     * 删除的时候同时操作es 和更新分类与专题的文章数量
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delete(int id) {
        int num = super.delete(id);
        esBlogArticleService.delete(id);
        updateArticleNum(findById(id));
        return num;
    }

    /**
     * 删除的时候同时操作es 更新分类和专题的文章数量
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int delete(int[] ids) {
        int num = super.delete(ids);
        esBlogArticleService.deleteBatch(ids);
        findByIds(ids).forEach(this::updateArticleNum);
        return num;
    }

    /**
     * 查询并高亮显示
     *
     * @param esLookup
     * @return
     * @throws IOException
     */
    public PageInfo<EsBlogArticleModel> search(EsLookup<EsBlogArticleModel> esLookup) throws IOException {
        PageInfo<EsBlogArticleModel> pager = esBlogArticleService.search(esLookup);
        return pager;
    }

}
