package cn.xuqiudong.blog.module.other.service;

import cn.xuqiudong.blog.constant.GlobalFieldParams;
import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.GlobalConfigMapper;
import cn.xuqiudong.common.blog.model.GlobalConfig;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * @author Vic.xu
 * 描述:全局配置 Service
 * @since  2020-06-10 14:37
 */
@Service
public class GlobalConfigService extends BaseService<GlobalConfigMapper, GlobalConfig> {

    @Override
    protected boolean hasAttachment() {
        return true;
    }

    /**
     * 根据code 返回配置
     * FIXME ： 缓存
     * @param code
     * @param <T>
     * @return
     */
    @NotNull
    public GlobalConfig getByCode(GlobalFieldParams.GlobalConfigCodeEnum code) {
        GlobalConfig config = mapper.getByCode(code.name());
        return config == null ? new GlobalConfig() : config;
    }


}
