package cn.xuqiudong.blog.module.blog.model;


import cn.xuqiudong.common.blog.model.BlogArticle;

import java.util.List;

/**
 * 说明 :  首页展示的专题
 * @author Vic.xu
 * @since  2020/5/25 0025 17:27
 */
public class ShowTopicModel {

    /**
     * 专题id
     */
    private Integer id;

    /**
     * 专题名称
     */
    private String name;

    /**
     * 专题下的N篇文章
     */
    private List<BlogArticle> articles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<BlogArticle> getArticles() {
        return articles;
    }

    public void setArticles(List<BlogArticle> articles) {
        this.articles = articles;
    }
}
