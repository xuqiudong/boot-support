package cn.xuqiudong.blog.module.blog.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogTopicMapper;
import cn.xuqiudong.common.blog.model.BlogTopic;
import org.springframework.stereotype.Service;

/**
 * 描述:博客专题表 Service
 * @author Vic.xu
 * @since  2020-05-08 09:36
 */
@Service
public class BlogTopicService extends BaseService<BlogTopicMapper, BlogTopic> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 更新文章数量
     */
    public int updateArticleNum(Integer id) {
        return mapper.updateArticleNum(id);
    }
}
