package cn.xuqiudong.blog.module.poetry.service;

import cn.xuqiudong.blog.module.other.model.TimelineDetailModel;
import cn.xuqiudong.blog.module.other.model.TimelineModel;
import cn.xuqiudong.common.base.lookup.Lookup;
import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogPoetryMapper;
import cn.xuqiudong.common.blog.model.BlogPoetry;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vic.xu
 * 描述:诗歌 Service
 * @since  2020-08-19 14:26
 */
@Service
public class BlogPoetryService extends BaseService<BlogPoetryMapper, BlogPoetry> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 我的诗歌  时间轴
     *
     * @return
     */
    public List<TimelineModel> timeline() {
        Lookup lookup = new BlogPoetry();
        lookup.setSize(Integer.MAX_VALUE);
        List<BlogPoetry> list = list(lookup);
        //1 先转换对象为TimelineDetailModel
        List<TimelineModel> result = list.stream().map(p -> {
                    TimelineDetailModel model = new TimelineDetailModel();
                    model.setId(p.getId());
                    model.setCreateTime(p.getWriteDate());
                    model.setTitle(p.getTitle());
                    return model;
                    //2根据年月分组 key 为年月   value为详情list
                }).collect(Collectors.groupingBy(TimelineDetailModel::getDate))
                .entrySet().stream().map(e -> new TimelineModel(e.getKey(),
                                // 先把value按照day进行排序
                                e.getValue()
                                        .stream().sorted(Comparator.comparing(TimelineDetailModel::getDay).reversed()).collect(Collectors.toList()))
                        //对结果list进行排序
                ).sorted(Comparator.comparing(TimelineModel::getDate).reversed())
                .collect(Collectors.toList());


        return result;
    }
}
