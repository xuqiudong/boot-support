package cn.xuqiudong.blog.module.blog.mapper;

import cn.xuqiudong.blog.module.blog.model.ShowTopicModel;
import cn.xuqiudong.blog.module.other.model.TimelineDetailModel;
import cn.xuqiudong.common.base.lookup.Lookup;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Vic.xu
 * 说明 :  首页的mapper
 * @since  2020/5/26 0026 10:49
 */
public interface BlogHomeMapper {

    /**
     * 查询首页展示的专题,以及专题下的前N条文章
     * @param num 展示数量
     * @return 展示的专题list
     */
    List<ShowTopicModel> topicShows(@Param("num") int num);

    /**
     * 时间轴分页查询-
     * @param lookup 查询条件
     * @return列表
     */
    List<TimelineDetailModel> timelineList(Lookup lookup);
}
