package cn.xuqiudong.blog.module.third.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogThirdUserMapper;
import cn.xuqiudong.common.blog.model.BlogThirdUser;
import org.springframework.stereotype.Service;

/**
 * 描述:第三方用户 Service
 * @author Vic.xu
 * @since  2020-07-21 17:33
 */
@Service
public class BlogThirdUserService extends BaseService<BlogThirdUserMapper, BlogThirdUser> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 查看用户是否存在，不存在则创建
     * 又忍不住使用ON DUPLICATE KEY UPDATE ，想了想再次删除， 省得id不连续，看着不舒服
     * @param user
     */
    public BlogThirdUser checkUser(BlogThirdUser user) {
        BlogThirdUser existed = mapper.checkByTypeAndIdentifier(user);
        // 存在的话更新 相关信息
        if (existed != null) {
            user.setId(existed.getId());
            super.update(user);
            return findById(existed.getId());
        }
        super.insert(user);
        return user;

    }
}
