package cn.xuqiudong.blog.module.blog.model;


import cn.xuqiudong.common.blog.model.BlogArticleFile;
import cn.xuqiudong.common.blog.model.BlogCategory;
import cn.xuqiudong.common.blog.model.BlogTag;
import cn.xuqiudong.common.blog.model.BlogTopic;

import java.util.List;
import java.util.Map;

/**
 * 说明 :  页面右侧需要数据
 * @author Vic.xu
 * @since  2020/7/10 0010 11:48
 */
public class RightAsideModel {
    /**
     *全部分类
     */
    private List<BlogCategory> blogCategories;

    /**
     * 全部专题
     */
    private List<BlogTopic> blogTopics;

    /**
     * 全部标签
     */
    private List<BlogTag> blogTags;

    /**
     * 友情链接
     * name=url
     */
    private Map<String, String> links;

    /**
     * 全部归档
     */
    private List<BlogArticleFile> blogFiles;

    /**
     * 把全部归档 blogFiles 按照年份汇总
     */
    private List<YearBlogFileModel> yearBlogFiles;


    public List<BlogCategory> getBlogCategories() {
        return blogCategories;
    }

    public void setBlogCategories(List<BlogCategory> blogCategories) {
        this.blogCategories = blogCategories;
    }

    public List<BlogTopic> getBlogTopics() {
        return blogTopics;
    }

    public void setBlogTopics(List<BlogTopic> blogTopics) {
        this.blogTopics = blogTopics;
    }

    public List<BlogTag> getBlogTags() {
        return blogTags;
    }

    public void setBlogTags(List<BlogTag> blogTags) {
        this.blogTags = blogTags;
    }

    public List<BlogArticleFile> getBlogFiles() {
        return blogFiles;
    }

    public void setBlogFiles(List<BlogArticleFile> blogFiles) {
        this.blogFiles = blogFiles;
    }

    public Map<String, String> getLinks() {
        return links;
    }

    public void setLinks(Map<String, String> links) {
        this.links = links;
    }


    public List<YearBlogFileModel> getYearBlogFiles() {
        return yearBlogFiles;
    }

    public void setYearBlogFiles(List<YearBlogFileModel> yearBlogFiles) {
        this.yearBlogFiles = yearBlogFiles;
    }
}
