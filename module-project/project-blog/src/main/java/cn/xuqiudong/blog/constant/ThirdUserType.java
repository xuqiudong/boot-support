package cn.xuqiudong.blog.constant;

/**
 * @author Vic.xu
 * 说明 :  第三方用户类型
 * @since  2020/7/22 0022 14:54
 */
public enum ThirdUserType {
    /**
     * gitee
     */
    gitee,
    /**
     * github
     */
    github,
    /**
     * qq
     */
    qq
}
