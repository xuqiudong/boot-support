package cn.xuqiudong.blog.test.page.mapper;

import java.util.List;
import java.util.Map;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-08-28 14:39
 */
public interface TestPageMapper {

    List<Map<String, Object>> topicList();

    List<Map<String, Object>> categoryList();

}
