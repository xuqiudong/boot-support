package cn.xuqiudong.blog.module.other.controller;

import cn.xuqiudong.blog.base.BaseBlogController;
import cn.xuqiudong.blog.module.other.service.OtherService;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.blog.model.BlogComment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.annotation.Resource;

/**
 * 描述: 留言
 * @author Vic.xu
 * @since 2022-08-11 9:53
 */
@Controller
public class CommentController extends BaseBlogController {

    @Resource
    private OtherService otherService;

    /**
     * 前往留言板页面
     */
    @GetMapping("/comment")
    public String comment(@ModelAttribute("lookup") BlogComment lookup, Model model) {
        PageInfo<BlogComment> pager = otherService.comments(lookup);
        model.addAttribute("pager", pager);
        return "comment";
    }

    /**
     * 留言板页面分页
     */
    @PostMapping("/comment")
    @ResponseBody
    public String comment(BlogComment lookup, RedirectAttributes attributes) {
        attributes.addFlashAttribute("lookup", lookup);
        return "redirect:comment";
    }

    /**
     * 异步加载留言板数据
     * @param lookup
     * @return pager
     */
    @PostMapping("/commentMore")
    @ResponseBody
    public BaseResponse<PageInfo<BlogComment>> commentMore(BlogComment lookup) {
        PageInfo<BlogComment> pager = otherService.comments(lookup);
        return BaseResponse.success(pager);
    }

    /**
     * 保存留言
     * @param entity
     * @return success
     */
    @PostMapping("saveComment")
    @ResponseBody
    public BaseResponse<Integer> saveComment(BlogComment entity) {
        int num = otherService.saveComment(entity);
        return BaseResponse.success(num);

    }

}
