package cn.xuqiudong.blog.test.page.controller;

import cn.xuqiudong.blog.test.page.service.TestPageService;
import cn.xuqiudong.common.base.lookup.Lookup;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-08-28 14:48
 */
@RestController
@RequestMapping("/test/page")
public class TestPageController {

    private TestPageService testPageService;

    public TestPageController(TestPageService testPageService) {
        this.testPageService = testPageService;
    }


    @RequestMapping("/topics")
    public List<Map<String, Object>> topicList(Lookup lookup){
        if (lookup.getSize() > 5) {

            lookup.setSize(new Random().nextInt(5));
        }
        return testPageService.topicList(lookup);
    }

    @RequestMapping("/categories")
    public List<Map<String, Object>> categoryList() {
        return testPageService.categoryList();
    }




}
