package cn.xuqiudong.blog.module.blog.service;

import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogCommentMapper;
import cn.xuqiudong.common.blog.model.BlogComment;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Vic.xu
 * 描述:评论 Service
 * @since  2020-07-21 14:59
 */
@Service
public class BlogCommentService extends BaseService<BlogCommentMapper, BlogComment> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 楼中楼列表
     * @param lookup
     * @return
     */
    public PageInfo<BlogComment> commentsPage(BlogComment lookup) {
        startPage(lookup.getPage(), lookup.getSize());

        List<BlogComment> list = mapper.commentsList(lookup);
        PageInfo<BlogComment> pager = PageInfo.instance(list, lookup);
        return pager;

    }
}
