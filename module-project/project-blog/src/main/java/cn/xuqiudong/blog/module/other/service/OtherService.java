package cn.xuqiudong.blog.module.other.service;

import cn.xuqiudong.blog.constant.GlobalFieldParams;
import cn.xuqiudong.blog.module.blog.mapper.BlogHomeMapper;
import cn.xuqiudong.blog.module.blog.service.BlogCommentService;
import cn.xuqiudong.blog.module.other.model.TimelineDetailModel;
import cn.xuqiudong.blog.module.other.model.TimelineModel;
import cn.xuqiudong.common.base.lookup.Lookup;
import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.blog.model.BlogComment;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Vic.xu
 * 说明 :  博客的一些其他的service处理
 * @since  2020/7/9 0009 17:44
 */
@Service
public class OtherService {


    @Autowired
    private BlogHomeMapper blogHomeMapper;

    @Autowired
    private BlogCommentService blogCommentService;

    /**
     * 查询时间轴分页数据
     * @param lookup Lookup
     * @return PageInfo
     */
    public PageInfo<TimelineModel> timelinePage(Lookup lookup) {
        lookup.setSize(GlobalFieldParams.TIMELINE_SIZE);
        PageHelper.startPage(lookup.getPage(), lookup.getSize());
        List<TimelineDetailModel> detailslist = blogHomeMapper.timelineList(lookup);
        int total = (int) ((Page) detailslist).getTotal();
        /*
            1. 先查询出TimelineDetailModel列表
            2. 根据date聚合成map
            3. 把map转化成list（key + value共同组成对象TimelineModel）
            4. 组外组内都按照时间倒序排列
         */
        List<TimelineModel> list = detailslist
                .stream().collect(Collectors.groupingBy(TimelineDetailModel::getDate))
                .entrySet().stream().map(e -> new TimelineModel(e.getKey(), e.getValue()
                        .stream().sorted(Comparator.comparing(TimelineDetailModel::getDay).reversed())
                        .collect(Collectors.toList()
                        )))
                .sorted(Comparator.comparing(TimelineModel::getDate).reversed())
                .collect(Collectors.toList());
        return PageInfo.instance(total, list, lookup);
    }

    /* ********************************************************************************************* */

    /**
     * 留言板分页列表
     * 1. 查询某个博客的评论 或者留言板的评论
     * 2. 只查询主评论
     * 3. 查询出评论中的回复：只查前10条
     * @param lookup BlogComment
     * @return PageInfo of BlogComment
     */
    public PageInfo<BlogComment> comments(BlogComment lookup) {
        lookup.setSortOrder("asc");
        lookup.setSortColumn("id");
        PageInfo<BlogComment> pager = blogCommentService.commentsPage(lookup);
        // 表示查询的是主评论， 此时应该把主评论的子回复查询出来
        if (lookup.getId() == null) {
            pager.getDatas().forEach(c -> {
                c.setSortOrder("asc");
                c.setSortColumn("id");
                c.setSubComments(blogCommentService.commentsPage(c));
            });
        }
        return pager;
    }

    public int saveComment(BlogComment entity) {
        return blogCommentService.save(entity);
    }
}
