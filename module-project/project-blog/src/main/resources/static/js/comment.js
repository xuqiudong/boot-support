/*评论页面的js*/
$(function () {
    loadMoreEvent();
    replyEvent();
    commentsEvent();

});

// 加载更多子回复事件 针对某个回复  加载它的子回复
function loadMoreEvent() {
    $("div.load-more").on("click", function () {
        var data = $(this).data();
        var $more = $(this)
        //找到列表所在div
        var $box = $more.closest(".comments-sub-box");
        //找到模版
        var $temp = $more.prev("div");
        var url = ctx + "commentMore"
        $.ajax(url, {
            type: "post",
            dataType: "json",
            data: data
        }).done(function (res) {
            if (res.code != 0) {
                console.info(res.msg);
                return;
            }
            var pager = res.data;
            pager.datas.forEach(function (item) {
                var $comment = $temp.clone(true);
                $comment.find("img").attr("src", item.avatar);
                $comment.find(".comments-item-right-header strong").text(item.nickname);
                $comment.find(".comments-item-right-header span").text(item.createTime);
                $comment.find("p").text(item.content);
                // data-reply=${sub.replyId},data-atid   (replyId 原来模版中就存在)
                $comment.find(".replay").data("atid", item.id);
                $more.before($comment);

            });
            if (pager.hasMore) {
                $more.data("page", (pager.page + 1));
            } else {
                $more.remove();
            }
        });
    })
}

// 点击回复按钮事件绑定
function replyEvent() {
    //点击回复按钮
    $("a.replay").on("click", function () {
        var className = "relation-replay";
        var $replayBtn = $(this);
        var $commentsForm = $replayBtn.closest(".comments-sub-item,.comments-item").children("." + className);
        if ($commentsForm.length == 0) {
            $commentsForm = $("#main-comment").clone(true).attr("id", "");
            var $textArea = $commentsForm.find("textarea").val("");
            //绑定相关数据
            $textArea.data("reply", $replayBtn.data("reply")).data("atid", $replayBtn.data("atid"));
            $("img", $commentsForm).attr("src", user.avatar);
            $commentsForm.addClass(className);
            $replayBtn.closest(".comments-sub-item,.comments-item").children(".clearfix").after($commentsForm);
            $replayBtn.text("收起")
        } else {
            $commentsForm.toggle("fast", "linear", function () {
                $commentsForm.find("textarea").val("");
                $replayBtn.text("收起" == $replayBtn.text() ? "回复" : "收起")
            })
        }
        $commentsForm.find("textarea").focus();

    });
}

//提交评论按钮事件绑定
function commentsEvent() {
    $("button.comments-submit").on("click", function () {
        $(this).prop("disabled", true);
        var $textArea = $(this).closest(".comments-form").find("textarea");
        if ($textArea.length == 0 || !$textArea.val()) {
            return;
        }
        var articleId = $("#mainTextArea").data("id");
        if (!articleId) {
            articleId = 0;
        }
        var data = {
            content: $textArea.val(),
            articleId:articleId,
            replyId: $textArea.data("reply"),
            atId: $textArea.data("atid"),
            userId: user.id
        };
        var url = ctx + "saveComment";
        $.ajax(url, {
            type: "post",
            dataType: "json",
            data: data
        }).done(function (res) {
            window.location.reload();
        });
    });
}