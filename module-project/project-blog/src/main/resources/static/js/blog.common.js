/*
博客公共的代码
 */

$(function () {
    initTagCloud();
});

//初始化标签云
function initTagCloud() {
    //标签云
    var $tagDiv = $(".widget.tag");
    if ($tagDiv.length == 0) {
        return;
    }
    var tags = [];
    $("ul li a", $tagDiv).each(function () {
        tags.push($(this).text());
    });
    $tagDiv.empty();
    TagCloud($tagDiv[0], tags, {});
    $tagDiv.on("click", ".tagcloud&#45;&#45;item", function () {
        console.info($(this).text());
    });
}

$(function () {
    console.info("bind click for collapsed")

    $("ul li.collapsed").on("click", function () {
        var collapsedClass = "caret";
        debugger;
        var $li = $(this);
        var $others = $li.siblings("li");
        var $caret = $(".collapsedFlag", $li);
        var flag = $caret.hasClass(collapsedClass);
        if (!flag) {
            $others.hide();
            $caret.addClass(collapsedClass)
        } else {
            $others.show();
            $caret.removeClass(collapsedClass)
        }
    });
    $("ul li.collapsed").trigger("click");
});