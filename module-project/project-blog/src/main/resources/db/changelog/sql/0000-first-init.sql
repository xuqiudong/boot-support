DROP TABLE IF EXISTS `blog_article`;

CREATE TABLE `blog_article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标题',
  `summary` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '简介',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '内容',
  `category_id` int DEFAULT NULL COMMENT '所属分类id',
  `author` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '作者',
  `originated` tinyint(1) DEFAULT '1' COMMENT '是否原创',
  `source` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '来源',
  `create_id` int DEFAULT '1' COMMENT '创建人id',
  `read_num` int DEFAULT '0' COMMENT '阅读次数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `tag_ids` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签id,逗号分隔',
  `tag_names` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '标签名称 逗号分隔',
  `topic_id` int DEFAULT NULL COMMENT '专题id',
  `authority` tinyint(1) DEFAULT '0' COMMENT '权限 0-公开 1-好友 2-自己',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用/发布',
  `icon` int DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='博客表';

/*Table structure for table `blog_category` */

DROP TABLE IF EXISTS `blog_category`;

CREATE TABLE `blog_category` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `click_num` int DEFAULT '0' COMMENT '点击次数',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `introduce` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '分类介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='博客分类表';

/*Table structure for table `blog_tag` */

DROP TABLE IF EXISTS `blog_tag`;

CREATE TABLE `blog_tag` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(200) DEFAULT NULL COMMENT '名称',
  `click_num` int DEFAULT '0' COMMENT '点击次数',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='标签表';

/*Table structure for table `blog_topic` */

DROP TABLE IF EXISTS `blog_topic`;

CREATE TABLE `blog_topic` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '名称',
  `click_num` int DEFAULT '0' COMMENT '点击次数',
  `show_home` tinyint(1) DEFAULT '0' COMMENT '是否首页展示',
  `sort` int DEFAULT NULL COMMENT '排序',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  `introduce` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '专题介绍',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='博客专题表';

/*Table structure for table `global_config` */

DROP TABLE IF EXISTS `global_config`;

CREATE TABLE `global_config` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `code` varchar(64) NOT NULL COMMENT '配置code',
  `name` varchar(64) DEFAULT NULL COMMENT '配置名称',
  `show_type` tinyint DEFAULT '1' COMMENT '展示类型:1-input;2-textarea;3-图片;4-富文本',
  `content` text COMMENT '内容',
  `validate` varchar(64) DEFAULT NULL COMMENT '校验规则:正则',
  `validate_msg` varchar(64) DEFAULT NULL COMMENT '校验提示',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` tinyint(1) DEFAULT '0' COMMENT '是否删除',
  `is_enable` tinyint(1) DEFAULT '1' COMMENT '是否启用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='全局配置';