-- 博客定时任务
CREATE TABLE `blog_task_job` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务code',
  `name` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务名称',
  `group` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务组',
  `clazz` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '任务类全路径',
  `cron` VARCHAR(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'cron表达式',
  `status` TINYINT(1) DEFAULT '1' COMMENT '1执行中 2暂停 0删除',
  `remark` VARCHAR(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其他说明',
  `create_time` DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `is_enable` TINYINT(1) DEFAULT '1' COMMENT '是否启用',
  `is_delete` TINYINT(1) DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`),
  UNIQUE KEY `blog_task_job_unique_index` (`code`)
) ENGINE=INNODB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='博客定时任务';

insert into `blog_task_job` (`id`, `code`, `name`, `group`, `clazz`, `cron`, `status`, `remark`, `create_time`, `update_time`, `is_enable`, `is_delete`) values('1','first-job','第一个job','first-group','pers.vic.blog.module.api.task.job.TestJob','0/30 * * * * ?','1','第一个job remark','2020-09-03 15:55:16','2020-09-03 15:55:16','1','0');
insert into `blog_task_job` (`id`, `code`, `name`, `group`, `clazz`, `cron`, `status`, `remark`, `create_time`, `update_time`, `is_enable`, `is_delete`) values('2','article-file-job','文章归档','article-file-job-group','pers.vic.blog.module.api.task.job.ArticleFileJob','0 0 1 * * ?','1','凌晨一点统计当月归档数量，如果表数据为空，则插入全部数据','2020-09-07 11:00:55','2020-09-07 11:00:55','1','0');


