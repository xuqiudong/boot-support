package pers.vic;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-08-08 17:03
 */
public class Some {
    public static void main(String[] args) {
        //System.out.println(798);
        //int[] a = {25, 4, 256, 9, -38, 47, 128, -256, 64};
        //test(a, a.length);
        //System.out.println(Arrays.toString(a));
        //System.out.println(123);

        f32(Tree.build(), 14, 50);
    }


    public static void f32(Tree t, int k1, int k2) {
        if (t != null) {
            f32(t.left, k1, k2);
            if (t.data >= k1 && t.data <= k2) {
                System.out.println(t.data);
            }
            f32(t.right, k1, k2);
        }
    }

    public static void test(int[] a, int n) {
        int k, m = 0, temp;

        while (a[m] < 0 && m < n) {
            m++;
        }
        k = m;
        System.out.println(m);
        while (k < n) {

            while (a[k] >= 0 && k < n) {
                k++;
                if (k < n) {
                    temp = a[k];
                    a[k] = a[m];
                    a[m] = temp;
                    m++;
                }

            }
        }
    }


    static class Tree {
        int data;
        Tree left;
        Tree right;

        public Tree(int data) {
            this.data = data;
        }


        public static Tree build() {
            Tree root = new Tree(50);
            Tree t18 = new Tree(18);
            Tree t36 = new Tree(36);
            Tree t10 = new Tree(10);
            Tree t25 = new Tree(25);
            Tree t16 = new Tree(16);
            Tree t66 = new Tree(66);

            root.left = t16;
            root.right = t66;

            t16.left = t10;
            t16.right = t25;

            t25.left = t18;
            t25.right = t36;

            return root;
        }
    }
}
