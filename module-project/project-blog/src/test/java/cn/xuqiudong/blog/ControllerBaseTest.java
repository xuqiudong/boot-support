package cn.xuqiudong.blog;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.annotation.Rollback;

//import pers.vic.boot.base.model.BaseResponse;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Rollback(true)
public class ControllerBaseTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testIndex() {
		/*
		BaseResponse result = restTemplate.getForObject("/test/mybatis/list",BaseResponse.class);
		System.out.println(result.toJson());
		*/
    }

    @Test
    public void contextLoads() {
    }
}