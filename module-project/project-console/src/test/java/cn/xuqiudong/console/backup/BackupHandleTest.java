package cn.xuqiudong.console.backup;

import cn.xuqiudong.console.BaseTest;
import cn.xuqiudong.console.module.backup.handler.BackupHandler;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
import cn.xuqiudong.console.module.backup.service.BackupConfigService;
import cn.xuqiudong.console.module.timer.schedule.backup.BackupSchedule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-03-14 11:48
 */
public class BackupHandleTest extends BaseTest {

    @Resource
    private BackupHandler backupHandler;

    @Resource
    private BackupConfigService backupConfigService;


    @Resource
    private BackupSchedule backupSchedule;


    @Test
    public void uploadDir(){
        Integer id = 1;
        BackupConfig config = backupConfigService.findById(id);
        if (config == null) {
            System.out.println("none config found for " + id);
            return;
        }
        backupHandler.handle(config);

    }


    @Test
    public void  backupSchedule(){
        backupSchedule.backup();
        Assertions.assertTrue(true);
    }
}
