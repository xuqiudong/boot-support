package cn.xuqiudong.console.backup;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.console.BaseTest;
import cn.xuqiudong.console.module.backup.constant.BackupCategory;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
import cn.xuqiudong.console.module.backup.model.BackupRecord;
import cn.xuqiudong.console.module.backup.service.BackupConfigService;
import cn.xuqiudong.console.module.backup.service.BackupRecordService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-02-07 13:43
 */
public class BackupServiceTest extends BaseTest {

    @Resource
    private BackupRecordService backupRecordService;

    @Resource
    private BackupConfigService backupConfigService;

    @Test
    public void insert() {
        BackupRecord record = new BackupRecord();
        record.setCategory(BackupCategory.shell);
        record.setFilename("test.md");
        record.setFilePath("/xqd/data/backup/shell/test.md");
        record.setFileSize(200L);
        record.setBaiduPath("/xqd/data/backup/shell/test.md");
        record.setResult("success");
        record.setSuccessful(Boolean.TRUE);
        backupRecordService.save(record);
        System.out.println(record.getId());
        Assertions.assertNotNull(record.getId());
    }

    @Test
    public void update(){
        BackupRecord record = new BackupRecord();
        record.setId(1);
        record.setBaiduPath("/apps/linux/backup/shell/test.md");
        backupRecordService.save(record);
        BackupRecord backupRecord = backupRecordService.findById(1);
        JsonUtil.printJson(backupRecord);


    }

    @Test
    public void query(){
        Integer id = 1;
        BackupRecord backupRecord = backupRecordService.findById(id);
        JsonUtil.printJson(backupRecord);
    }

    @Test
    public void listConfigs(){
        BackupConfig lookup = new BackupConfig();
        lookup.setEnable(true);
        List<BackupConfig> list = backupConfigService.list(lookup);
        JsonUtil.printJson(list);
    }
}
