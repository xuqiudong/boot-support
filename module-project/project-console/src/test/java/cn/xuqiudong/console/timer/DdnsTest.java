package cn.xuqiudong.console.timer;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.console.BaseTest;
import cn.xuqiudong.console.module.timer.schedule.ddns.handler.AliDdnsService;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-28 10:28
 */
public class DdnsTest extends BaseTest {
    @Resource
    private AliDdnsService aliDdnsService;

    @Test
    public void list() {
        List<String> strings = aliDdnsService.domainList();
        JsonUtil.printJson(strings);
        aliDdnsService.ddns();
    }

    @Test
    public void log() {
        aliDdnsService.insertLog("nav", " 152.202 .55 .1 ", " 192.168 .1 .5 ");
    }
}
