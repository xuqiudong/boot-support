package cn.xuqiudong.console;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-28 10:26
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class BaseTest {



}
