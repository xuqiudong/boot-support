package cn.xuqiudong.console.common;

import cn.xuqiudong.common.base.service.CommonBaseMapperService;
import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.console.BaseTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-10-16 15:38
 */
public class CommonBaseMapperServiceTest extends BaseTest {

    @Autowired
    private CommonBaseMapperService commonBaseMapperService;


    @Test
    public void list() {
        Class<BlogArticle> clazz = BlogArticle.class;
        List<BlogArticle> list = commonBaseMapperService.list(new BlogArticle(), clazz);
        JsonUtil.printJson(list);

    }

}
