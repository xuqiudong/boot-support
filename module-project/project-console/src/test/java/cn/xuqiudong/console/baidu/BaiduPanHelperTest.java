package cn.xuqiudong.console.baidu;

import cn.xuqiudong.common.base.vo.BooleanWithMsg;
import cn.xuqiudong.console.BaseTest;
import cn.xuqiudong.console.module.backup.helper.baidu.BaiduPanHelper;
import cn.xuqiudong.console.module.backup.helper.baidu.model.FileListQuery;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-01-31 16:52
 */
public class BaiduPanHelperTest extends BaseTest {

    @Autowired
    private BaiduPanHelper baiduPanHelper;

    @Test
    public void refreshToken(){
        try {
            baiduPanHelper.refreshToken();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void userInfo(){
        try {
            baiduPanHelper.userInfo();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void capacity(){
        try {
            baiduPanHelper.capacity();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void fileList(){
        try {
            FileListQuery query = new FileListQuery();
            query.setDir("/apps/home-linux/");
            baiduPanHelper.fileList(query);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void upload(){
        try {
            String path = "/temp/test002.txt";
            File file = new File("D:/desk/temp/aa.txt");
            System.out.println(file.exists());
            BooleanWithMsg upload = baiduPanHelper.upload(path, file);
            System.out.println("111111");
            BooleanWithMsg upload2 = baiduPanHelper.upload(path, file);
            System.out.println("222222");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
