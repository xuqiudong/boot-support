/**
 * https://github.com/chris-peng/wangEditor-fullscreen-plugin
 */
window.wangEditor.fullscreen = {
	// editor create之后调用
	init: function(selector){
		$(selector).siblings(".w-e-toolbar").append('<div class="w-e-menu"><a class="_wangEditor_btn_fullscreen" href="###" onclick="window.wangEditor.fullscreen.toggleFullscreen(\'' + selector + '\')">全屏</a></div>');
	},
	toggleFullscreen: function(selector){
		//当前的selector 为textarea 是平级的   ，需要先找到它的父级
		var editorSelector = $(selector).parent("div");
		$(editorSelector).toggleClass('fullscreen-editor');
		if($('._wangEditor_btn_fullscreen',editorSelector).text() == '全屏'){
			$('._wangEditor_btn_fullscreen',editorSelector).text('退出全屏');
		}else{
			$('._wangEditor_btn_fullscreen',editorSelector).text('全屏');
		}
	}
	
};
//预览  Vic.xu
window.wangEditor.preview = {
	// editor create之后调用
	init: function(selector){
		$(selector).siblings(".w-e-toolbar").append('<div class="w-e-menu"><a class="_wangEditor_btn_preview" href="###" onclick="window.wangEditor.preview.togglePreview(\'' + selector + '\')">预览</a></div>');	
	},
	togglePreview: function(editorSelector){
		var editor = $(editorSelector).data("nojsWangEditor");
		if(editor){
			var html = editor.txt.html();
			$.alert({
				columnClass: 'col-md-12 col-sm-12',
				title:"预览",
				content:html
			});
		}
		
	}
};

//查看源码   Vic.xu
window.wangEditor.source = {
	// editor create之后调用
	init: function(selector){
		$(selector).siblings(".w-e-toolbar").append('<div class="w-e-menu"><a class="_wangEditor_btn_viewsource" href="###" onclick="window.wangEditor.source.toggleSource(\'' + selector + '\')">源码</a></div>');	
	},
	
	toggleSource: function(selector){
		var editor = $(selector).data("nojsWangEditor");
		debugger;
		if(!editor) return;
		//当前的selector 为textarea 是平级的   ，需要先找到它的父级
		var editorSelector = $(selector).parent("div");
		editorHtml = editor.txt.html();
		if($('._wangEditor_btn_viewsource',editorSelector).text() == '源码'){
			editorHtml = editorHtml.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/ /g, "&nbsp;");
			$('._wangEditor_btn_viewsource', editorSelector).text('返回');
		}else{
			editorHtml = editor.txt.text().replace(/&lt;/ig, "<").replace(/&gt;/ig, ">").replace(/&nbsp;/ig, " ");
			$('._wangEditor_btn_viewsource',editorSelector).text('源码');
		}
		editor.txt.html(editorHtml);
		editor.change && editor.change();	//更新编辑器的内容

	}
};

 