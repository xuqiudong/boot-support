var contractTableCount;
var delSupplierContractIdArray = new Array();
var supplierForm={
	init:function(){
		supplierForm.bindEvent();
		supplierForm.initVariable();
	},
	bindEvent:function () {
		$("#addRelationContract").on("click",supplierForm.addRelationContract);
		$("#delRelationContract").on("click",supplierForm.delRelationContract);
	},
	addRelationContract:function () {//关联合同
		var url = base_url + "/business/contract/contractModal";
    	common.confirm("url:" + url, function() {
    		var rowscontent = $("#contractModalTable").bootstrapTable('getSelections');
    		if(rowscontent.length==0){
    			$.alert('请选择数据！');
    			return false;
    		}
    		var index = contractTableCount;
    	    var idArr = new Array();
    	    for (var j = 0; j < index; j++) {
    	    	var id = $("#relatedContract_id_" + j).val();
    	        idArr.push(id);
    	    }
    	    for (var i = 0; i < rowscontent.length; i++) {
    	    	var id = rowscontent[i].id == null ? "" : rowscontent[i].id;
    	        //判断对象是否已存在
    	        if ($.inArray(id, idArr) >= 0) {
    	        	continue;
    	        } else {
    	            idArr.push(id);
    	        }
    	        var id = rowscontent[i].id;
    	        var name = rowscontent[i].name;
    	        var type = rowscontent[i].contractType;
    	        var obj = $('#contractTable4Copy').find('tr').clone();

    	        obj.html(obj.html().replace(/@fkContractId@/g, id));
    	        obj.html(obj.html().replace(/@name@/g, name).replace("null", "").replace("undefined", ""));
    	        obj.html(obj.html().replace(/@index@/g, index++).replace("null", ""));
    	        $('#contractTable').append(obj);
    	     }
    	     contractTableCount = index;
    	}, {title: '我方', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
	},
	delRelationContract:function(){
		 var checkBoxs = $("#contractTable tr input[type=checkbox][name=contractSelect]:checked");
		 if (checkBoxs.length <= 0) {
			 $.alert('请选择数据！');
		      return false;
		  }
		  common.confirm("确认删除？", function () {
		      checkBoxs.each(function () {
		            $(this).parent().parent().remove();
		            var id = $(this).val() || "";
		            if(id){
		            	delSupplierContractIdArray.push(id);	            	
		            }
		     });
		     $("#delteSupplierContractIds").val(delSupplierContractIdArray.join(","));
		 });
	},
	initVariable:function(){
		contractTableCount = $('#contractTable tr').length - 1;
	}
	
};

$(function () {
	supplierForm.init();
});