var readFlag;
var taskI;
var actionName;
var comCode=top.companyCode;
$(function() {
	if($('.applyComment').length>0&&$('.applyComment').val()!=""){
		$('#approvecomment').text($('.applyComment').val());
	    $('#comment').val($('.applyComment').val());
	}else{
		$('#approvecomment').text("提交审核");
		$('#comment').val("提交审核");
	}

	$("#approvecomment").bind('blur',function(){
		if($('#creatorComment').length>0){//同步提交意见到经办人意见
        	$('#creatorComment').val($('#approvecomment').val());
        }
		if($('.applyComment').length>0){
			$('.applyComment').val($('#approvecomment').val());
		}
	});
	
	if($('.submitForm').length>0){
		var $forms = $("#submitForm.validate", $('#submitForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});

	};
	if($('.submitFormPull').length>0){
		var $forms = $("#submitFormPull.validate", $('#submitFormPull').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	if($('.submitFormJointly').length>0){
		var $forms = $("#submitFormJointly.validate", $('#submitFormJointly').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	var jointlyAssign=$('#jointlyAssign').val();
	if(jointlyAssign=='1'){
		selectpicker(true);
	}
	
	//默认选中处理人 项目组成员
	var $options = $("select#assignee").find("option");
	if($options.length>0){
		$options.each(function(){
			var teamMenberFlag = $(this).attr("teamMenberFlag");
			if(teamMenberFlag == "1-1"){
				$(this).prop("selected","selected");
			}
		});
	}
});

function confirmExecution() {
	var validate = $('#submitForm').data("validator");
	if (validate && !$('#submitForm').valid()) { //如果有验证 但是验证不通过
		return false;
	}
	if ($('#approvecomment').val().length > 4000) {
		$.alert("提交意见超长！");
		return false;
	}
    common.confirm("确定要提交审核吗？",
	    function() {
	        disableBtn();
	    	assemblyFlowAssigneeVo("submit");//---对应hidden中的assigneeList替换块
	        var assignee = $("#assignee").val();
	        $(".wangEditor-textarea-container").children().each(function(){//删除富文本中的style属性，防止因为复制自带的样式干扰
	        	removeFont(this);
	    	});
	        var id = $("#id").val();
	        var objId = $("#objId").val();
	        var carId = $("#carId").val();
	        $("#approveDate").val($("#appDate").val());
	        var options = {
	            success: function(res) {
	                enableBtn();
	            	common.alert(res, function() { 
	            		submitSuccessConfirm(id, objId);
	            		common.closeModelDialog('submitDialog');
	            	},true);
	            }
	        };
	        // 流程抄送环节需要知道表单保存的参数项,拼装表单中所有的pcode和value,提交后台保存
	        var notifyParameter = [];
	        var $pcodeElems = $("#lcForm select[pcode]");
	        $pcodeElems.each(function(){
	        	var notifyParam = {};
	        	var pcode = $(this).attr("pcode");
	        	notifyParam.pcode = pcode;
	        	var notifyParamValue = [];
	        	$(this).find("option:selected").each(function(){
	        		notifyParamValue.push($(this).val());
	        	});
	        	notifyParam.value = notifyParamValue;
	        	notifyParameter.push(notifyParam);
			});
	        var notifyParameterStr = JSON.stringify(notifyParameter).replace(/\"/g,"'");
	        $("#notifyParameter").val(notifyParameterStr);
	        $("#lcForm").attr("action", rcContextPath + "/" + key.split("_")[0] + "/startExecution?processDefinitionId=" + processId + "&assignee=" + assignee+ "&carId=" + carId);
	        disableBtn();
	        $("#lcForm").ajaxSubmit(options);
	        return false;
	    });
}

function selectpicker(isInit){
    $('#assignee').selectpicker('val','');
    $('#assignee').selectpicker('refresh');
    $('#assignee').parent().attr('style','width:100%;');
    $(this).on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) { 
        $('#assignee').selectpicker('refresh'); 
        $('#assignee').parent().attr('style','width:100%;');
    });
}

function confirmExecution_pull(){
	 var assignee = $('#assignee').val();
	 if(assignee==null||assignee==''||assignee==undefined){
		 $.alert(common.i18n('cp.js.t04'));
			return false;
	 }
	if($('#comment').val().length>4000){
		$.alert(common.i18n('cp.js._i0'));
		return false;
	}
	var userIds = new Array();
	var ffrId = $('#ffrId').val();
	var ifForceDistribute = $('#ifForceDistribute').val();
	$('.userIds').each(function(index){
		if(userIds.indexOf($(this).val())==-1){
			userIds.push($(this).val());
		}else{
			$('#userId'+index+'Assignee').remove();
			$('#processDelegationId'+index+'Assignee').remove();
			$('#delegateeId'+index+'Assignee').remove();
		}
	});
	assemblyFlowAssigneeVo("submit_pull");//---对应hidden中的assigneeList替换块
	 $(".wangEditor-textarea-container").children().each(function(){//删除富文本中的style属性，防止因为复制自带的样式干扰
	     	removeFont(this);
	 	});
	var hasText = $('#hasText').val();
	if(hasText=="1"){
		doStartExec('&ownerCreateType=PULL&userIds='+userIds+'&ffrId='+ffrId+'&ifForceDistribute='+ifForceDistribute);
	}else{
		common.confirm(common.i18n('cp.js.sure__to_submit_to_review'),function(){
			disableBtn();
			$('#assignee').removeAttr("disabled");
			if(!validForm(key.split("_")[0])){
				enableBtn();
				return false;
			}
			var id=$("#id").val();
			var objId=$("#objId").val();
			var entityType=$("#entityType").val();
			$("#approveDate").val($("#appDate").val());
			 var options = {
			    success : function(data) {
				    	if(key.split("_")[0]=="LeasecheckformGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeasecheckformGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                     }else if(key.split("_")[0]=="LeaseCheckEquipmentGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeaseCheckEquipmentGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}else if(key.split("_")[0]=="LeaseCheckShippingGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeaseCheckShippingGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}else if(key.split("_")[0]=="PaymentApprovalNewGY"){
	                		if(data.split("-")[0]=='true'){
	                			savePaymentApprovalNewGYpdf(data);
	                		}
	                		data = data.split("-")[0];
	                	}else if(key.split("_")[0]=="PaymentApprovalNewHY"){
	                		if(data.split("-")[0]=='true'){
	                			savePaymentApprovalNewGYpdf(data);
	                		}
	                		data = data.split("-")[0];
	                	}else if(key.split("_")[0]=="CustomerCreditApprovalGYHK"){
	                		if(data.split("_")[0]=='true'){
	                			saveCustomerCreditApprovalGYHKpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}
	                	data = data.replace("<pre>","").replace("</pre>","");
				    enableBtn();
				    debugger
					if (data == 'true') {
						//主线页面发起审批流程
						queryDetailIdByObjSubmit();
						 common.success(common.i18n('cp.js.submit_successfully'),function(){
							 if(key.split("_")[0]=='LeasecheckformGY'||key.split("_")[0]=='LeaseCheckEquipmentGY'||key.split("_")[0]=='LeaseCheckShippingGY'||key.split("_")[0]=='LeaseCheckHKGY'){
									var creatSign = $("#creatSign").val();
							    	if(creatSign=="1"){
							    		afterSaveF(key,"",objId,entityType);
									}else{
										afterSaveF(key,id,objId,entityType);
									}
								 }else{
									 afterSaveF(key,id,objId,entityType);
								 }
						 });
					} else if (data = 'false') {
						$.alert(common.i18n('cp.js.sw70'));
					} 
					common.closeModelDialog('submitDialog');
				}
			 };
			 submitOp(key.split("_")[0]);
			 if($("#isDefinite").length>0)
				 $("#lcForm").attr("action", rcContextPath + "/flowForm/startExecution?processDefinitionId="+processId+"&assignee="+assignee+'&ownerCreateType=PULL&userIds='+userIds+'&ffrId='+ffrId+'&ifForceDistribute='+ifForceDistribute);
			 else
				 $("#lcForm").attr("action", rcContextPath+"/"+key.split("_")[0]+"/startExecution?processDefinitionId="+processId+"&assignee="+assignee+'&ownerCreateType=PULL&userIds='+userIds+'&ffrId='+ffrId+'&ifForceDistribute='+ifForceDistribute);
			 disableBtn();
			 $("#lcForm").ajaxSubmit(options);
			 return false;
		});
	}
}

function confirmExecution_jointly(){
	if($('#comment').val().length>4000){
		$.alert(common.i18n('cp.js._i0'));
		return false;
	}
	var userIds = new Array();
	var positions = new Array();
	if($('#assignee').prop('disabled')==false){//会签 支持选择场景
		$('#assignee>option').each(function(index){ 
			if($(this).prop("selected")){
				if(userIds.indexOf($(this).val())==-1){
					userIds.push($(this).val()); 
					positions.push($(this).attr("positionId"));
				}else{
					$('#userId'+index+'Assignee').remove();
					$('#processDelegationId'+index+'Assignee').remove();
					$('#delegateeId'+index+'Assignee').remove();
				}
			}else{
				$('#userId'+index+'Assignee').remove();
				$('#processDelegationId'+index+'Assignee').remove();
				$('#delegateeId'+index+'Assignee').remove();
			} 
		});
	}else{//会签  不支持选择场景
		$('.userIds').each(function(index){
			if(userIds.indexOf($(this).val())==-1){
				userIds.push($(this).val()); 
				positions.push($(this).attr("positionId"));
			}else{
				$('#userId'+index+'Assignee').remove();
				$('#processDelegationId'+index+'Assignee').remove();
				$('#delegateeId'+index+'Assignee').remove();
			}
		});
	}
	assemblyFlowAssigneeVo("submit_jointly");//---对应hidden中的assigneeList替换块
	 $(".wangEditor-textarea-container").children().each(function(){//删除富文本中的style属性，防止因为复制自带的样式干扰
	     	removeFont(this);
	 	});
	var hasText = $('#hasText').val();
	if(hasText=="1"){
		doStartExec('&ownerCreateType=JOINTLY&userIds='+userIds+'&positionIds='+positions);
	}else{
		common.confirm(common.i18n('cp.js.sure__to_submit_to_review'),function(){
			if($('#assignee').prop('disabled')==false){//会签 支持选择场景 默认处理人传一个
				$('#assignee').val($('#assignee').find('option:selected').eq(0).val());
			}
			disableBtn();
			$('#assignee').removeAttr("disabled");
			var assignee=$("#assignee").val();
			if(!validForm(key.split("_")[0])){
				enableBtn();
				return false;
			}
			var id=$("#id").val();
			var objId=$("#objId").val();
			var entityType=$("#entityType").val();
			$("#approveDate").val($("#appDate").val());
			 var options = {
			    success : function(data) {
				    	if(key.split("_")[0]=="LeasecheckformGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeasecheckformGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                     }else if(key.split("_")[0]=="LeaseCheckEquipmentGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeaseCheckEquipmentGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}else if(key.split("_")[0]=="LeaseCheckShippingGY"){
	                		if(data.split("_")[0]=='true'){
	                			saveLeaseCheckShippingGYpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}else if(key.split("_")[0]=="PaymentApprovalNewGY"){
	                		if(data.split("-")[0]=='true'){
	                			savePaymentApprovalNewGYpdf(data);
	                		}
	                		data = data.split("-")[0];
	                	}else if(key.split("_")[0]=="PaymentApprovalNewHY"){
	                		if(data.split("-")[0]=='true'){
	                			savePaymentApprovalNewGYpdf(data);
	                		}
	                		data = data.split("-")[0];
	                	}else if(key.split("_")[0]=="CustomerCreditApprovalGYHK"){
	                		if(data.split("_")[0]=='true'){
	                			saveCustomerCreditApprovalGYHKpdf(data);
	                		}
	                		data = data.split("_")[0];
	                	}
	                	data = data.replace("<pre>","").replace("</pre>","");
				    enableBtn();
					if (data == 'true') {
						 common.success(common.i18n('cp.js.submit_successfully'),function(){
							 if(key.split("_")[0]=='LeasecheckformGY'||key.split("_")[0]=='LeaseCheckEquipmentGY'||key.split("_")[0]=='LeaseCheckShippingGY'||key.split("_")[0]=='LeaseCheckHKGY'){
								var creatSign = $("#creatSign").val();
						    	if(creatSign=="1"){
						    		afterSaveF(key,"",objId,entityType);
								}else{
									afterSaveF(key,id,objId,entityType);
								}
							 }else{
								 afterSaveF(key,id,objId,entityType);
							 }
							 
						 });
					} else if (data == 'false') {
						$.alert(common.i18n('cp.js.sw70'));
					} else if (data == 'repeatInitate') {//流程重复发起
						$.alert(common.i18n('cp.js.Apr21.7th.j29'));
					} 
					common.closeModelDialog('submitDialog');
				}
			 };
			 submitOp(key.split("_")[0]);
			 if($("#isDefinite").length>0)
				 $("#lcForm").attr("action", rcContextPath + "/flowForm/startExecution?processDefinitionId="+processId+"&assignee="+assignee+'&ownerCreateType=JOINTLY&userIds='+userIds+'&positionIds='+positions);
			 else
				 $("#lcForm").attr("action", rcContextPath+"/"+key.split("_")[0]+"/startExecution?processDefinitionId="+processId+"&assignee="+assignee+'&ownerCreateType=JOINTLY&userIds='+userIds+'&positionIds='+positions);
			 disableBtn();
			 $("#lcForm").ajaxSubmit(options);
			 return false;
		});
	}
}
function assemblyFlowAssigneeVo(name){
	$('#assigneeList').html("");
	if(name=='submit'){
		//单处理人 提交审批
		var assignee=$('#assignee').find("option:selected");
		var userId=assignee.attr("userId");
		var processDelegationId=assignee.attr("processDelegationId");
		var delegateeId=assignee.attr("delegateeId");
		var html ='<input type="hidden" name="assigneeList[0].userId" value="'+userId+'"></input>';
		html +=('<input type="hidden" name="assigneeList[0].processDelegationId" value="'+processDelegationId+'"></input>');
		html +=('<input type="hidden" name="assigneeList[0].delegateeId" value="'+delegateeId+'"></input>');
		$('#assigneeList').html(html);
		
	}else if(name=='submit_pull'){
		$('#assigneeList').html($('#assigneeListPull').html()+$('#ffrAssigneeVo').html()); 
	}else if(name=='submit_jointly'){
		$('#assigneeList').html($('#assigneeListJointly').html());
	}
}
