var assigneeForm={};
var commentArray =['已阅','不同意','弃权','同意','不同意'];
var nodeListIndex = $('#nodeListTable').find('tr').length-1;
var notifyListIndex = $('#notifyListTable').find('tr').length-1;
$(function() {
	var msg = $('#message').val();
	var readFlag = $('#readFlag').val();
	if($('#approvecomment').length>0)	{
		if($('#comment').val()!=""&&$.inArray($('#comment').val(), commentArray)==-1){
			$('#approvecomment').text($('#comment').val());
		}else{
			if(msg=='Y'&&readFlag=='1'){
				$('#approvecomment').text('已阅');
				$('#comment').val('已阅');
			}else if(msg=='Y'&&readFlag=='2'){
				$('#approvecomment').text('不同意');
				$('#comment').val('不同意');
			}else if(msg=='Y'&&readFlag=='3'){
				$('#approvecomment').text('弃权');
				$('#comment').val('弃权');
			}else if(msg=='Y'){
				$('#approvecomment').text('同意');
				$('#comment').val('同意');
			}else if(msg=='N'||msg=='R'){
				$('#approvecomment').text('不同意');
				$('#comment').val('不同意');
			}
		}
	}
	var lastStepTime = "";
	if($('#lastStepTime').length>0){
		lastStepTime = $('#lastStepTime').val();
	}
	
	if($('#approveForm').length>0){
		var $forms = $("#approveForm.validate", $('#approveForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#distributeForm').length>0){
		var $forms = $("#distributeForm.validate", $('#distributeForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#approveFormPull').length>0){
		var $forms = $("#approveFormPull.validate", $('#approveFormPull').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#approveFormJointly').length>0){
		var $forms = $("#approveFormJointly.validate", $('#approveFormJointly').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#batchApproveForm').length>0){
		var $forms = $("#batchApproveForm.validate", $('#batchApproveForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#batchApproveFormPull').length>0){
		var $forms = $("#batchApproveFormPull.validate", $('#batchApproveFormPull').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
	
	if($('#batchApproveFormJointly').length>0){
		var $forms = $("#batchApproveFormJointly.validate", $('#batchApproveFormJointly').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}

	if($('#nodeForm').length>0){
		var $forms = $("#nodeForm.validate", $('#nodeForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	};
	
	if($('#assigneeForm').length>0){
		var $forms = $("#assigneeForm.validate", $('#assigneeForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	};
	
	if($('#delegationForm').length>0){
		var $forms = $("#delegationForm.validate", $('#delegationForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	};
	
	if($('#notifyForm').length>0){
		var $forms = $("#notifyForm.validate", $('#notifyForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	};
	
	$("#queryUserBtn").on("click", function (){
		$('#userName').val($('#userName_query').val());
		$('#name').val($('#name_query').val());
		$('#enName').val($('#name_query').val());
	});
	$("#resetUserBtn").on("click", function (){
		$('#userName_query').val('');
		$('#name_query').val('');
		$('#userName').val('');
		$('#name').val('');
		$('#enName').val('');
	});
	
	var jointlyAssign=$('#jointlyAssign').val();
	if(jointlyAssign=='1'){
		selectpicker(true);
	}
	
	//默认选中处理人 项目组成员
	var $options = $("select#assignee").find("option");
	if($options.length>0){
		$options.each(function(){
			var teamMenberFlag = $(this).attr("teamMenberFlag");
			if(teamMenberFlag == "1-1"){
				$(this).prop("selected","selected");
			}
		});
	}
});
function selectpicker(isInit){
	$('#assignee').selectpicker('val','');
    $('#assignee').selectpicker('refresh');
    $('#assignee').parent().attr('style','width:100%;');
    $(this).on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) { 
        $('#assignee').selectpicker('refresh'); 
        $('#assignee').parent().attr('style','width:100%;');
    });
}
function saveAssignee(){
//	$('#assigneeForm').bootstrapValidator('validate');
}

function submitAssignee(){
	$("#assignee").val($("#userId").val());
	$("#username").val($("#userId").find("option:selected").text());
	 common.closeModelDialog('assigneeDialog');
}

function changePosition(){
	var positionId = $('#posId').val();
	if(positionId !=''){
		$.ajax({  
			type: "post",
			url: 'flow/baseFlow/getUserL?positionId='+positionId,
			success: function (data) {
				$("#userId option").remove();
				$("#userId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
				$.each(data, function (i, item) {
					$("#userId").append("<option value='" + item[0] +  "'>" + item[1] + "</option>"); 
				});  
			}
		});  
	}else{
		$("#userId option").remove();
		$("#userId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
	}
}
function changePosition1(){
	var positionId = $('#posId').val();
	if(positionId !=''){
		$.ajax({  
			type: "post",
			url: 'flow/baseFlow/getUserL?positionId='+positionId,
			success: function (data) {
				$("#userIds option").remove();
				$("#userIds").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
				$.each(data, function (i, item) {
					$("#userIds").append("<option value='" + item[0] +  "'>" + item[1] + "</option>"); 
				});  
			}
		});  
	}else{
		$("#userIds option").remove();
		$("#userIds").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
	}
}

function changeOrgan(){
	   var organId = $('#organizationId').val();
	   if(organId !=''){
		   $.ajax({  
		       type: "post",
		       url: 'flow/baseFlow/getPositionList?organId='+organId,
		       success: function (data) {
		           $("#posId option").remove();
		           $("#posId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
		           $.each(data, function (i, item) {
		        	   $("#posId").append("<option value='" + item[0] +  "'>" + item[2] + "</option>"); 
		           });
		           $("#userId option").remove();
				   $("#userId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
		       }
		   });  
	   }else{
		   $("#positionId option").remove();
		   $("#positionId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
		   $("#userId option").remove();
		   $("#userId").append("<option value=''>"+common.i18n('cp.js.please_select')+"</option>");
	   }
}

function saveNotify(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#notifyForm').bootstrapValidator('validate');
}
function submitNotify(){
	$("#okBtn").attr("disabled",true);
	$("#cancelBtn").attr("disabled",true);
	if($('#notifyListTable').find('tr').length<2){
		$.alert(common.i18n('cp.js.please_add_node_list')); 
		$("#okBtn").removeAttr('disabled');
		return false;
	}
	var processKey =$("#processKey").val();
	var taskId =$("#taskId").val();
	$.ajax({
		type: "post",  
		url: 'flow/baseFlow/saveNotify?processKey='+processKey,
		data: $("#notifyForm").serialize(), 
		dataType :  'json',
		success: function(data) {
			if(data.msg =='0'){
				common.closeModelDialog('notifyDialog');
				refleshNotifyList();
				common.getTab('main').timeTask_refresh_proceeding();
			}else{
				$.alert(common.i18n('cp.js.sw70')+data.msg);
				$("#okBtn").removeAttr('disabled');
			}
		}
	});
}
function submitNotifyOrg(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
	$("#okBtn").attr("disabled",true);
	if($('#userSelectedNotify option').size()<1){
		$.alert(common.i18n('cp.js.p17')); 
		$("#okBtn").removeAttr('disabled');
		return false;
	}
	var userId = "";
	$("#userSelectedNotify option").each(function (i) {
			if(i < $("#userSelectedNotify option").length -1 )
				userId = userId+$(this).val()+",";
			else
				userId = userId+$(this).val();
	});
	$("#notifyDialog #userId").val(userId);
	var processKey =$("#processKey").val();
	var taskId =$("#taskId").val();
	$.ajax({
		type: "post",  
		url: 'flow/baseFlow/saveNotify?processKey='+processKey,
		data: $("#notifyForm").serialize(), 
	    dataType :  'json',
		success: function(data) {
			if(data.msg =='0'){
				common.closeModelDialog('notifyDialog');
				refleshNotifyList();
				common.getTab('main').timeTask_refresh_proceeding();
			}else{
				$.alert(common.i18n('cp.js.sw70')+data.msg);
				$("#okBtn").removeAttr('disabled');
			}
		}
	});
}

function saveNode(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#nodeForm').bootstrapValidator('validate');
}

function submitNode(){
	$("#okBtn").attr("disabled",true);
	if($('#nodeListTable').find('tr').length<2){
		$.alert(common.i18n('cp.js.please_add_node_list')); 
		$("#okBtn").removeAttr('disabled');
		return false;
	}
	var userIds = new Array();
	var userValid = true;
	$("#nodeListTable .userId").each(function(){
		var userId=$(this).find("option:selected").val();
		if($.inArray(userId,userIds)>=0){
			$.alert(common.i18n('cp.js.aA.613'));
			$("#okBtn").removeAttr('disabled');
			userValid = false;
			return;
		}
		userIds.push($(this).find("option:selected").val());
	  });
	if(!userValid)return false;
	var processKey =$("#processKey").val();
	var taskId =$("#taskId").val();
	$("#nodeType1").removeAttr("disabled");
	$.ajax({
		type: "post",  
		url: 'flow/baseFlow/saveNodeList', 
		data: $("#nodeForm").serialize(), 
		dataType :  'json',
		success: function(data) {
			if(data.msg =='0'){
				common.closeModelDialog('nodeDialog');
				 $('#addNode').hide();
				 refleshNodeList();
			}else if(data.msg =='1'){
				$.alert(common.i18n('cp.js.aA.636'));
				$("#okBtn").removeAttr('disabled');
			}else{
				$.alert(common.i18n('cp.js.sw70')+data.msg);
				$("#okBtn").removeAttr('disabled');
			}
		}
	});
}

function submitNodeOrg(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
	$("#okBtn").attr("disabled",true);
	if($('#userSelectedNode option').size()<1){
		$.alert(common.i18n('cp.js.p17')); 
		$("#okBtn").removeAttr('disabled');
		return false;
	}
	var checkBoxs=$("input[name='approveType']:checked");
	if(checkBoxs.size()<1){
		$.alert('请选择审批方式'); 
		$("#okBtn").removeAttr('disabled');
		return false;
	}
	var userId = "";
	$("#userSelectedNode option").each(function (i) {
			if(i < $("#userSelectedNode option").length -1 )
				userId = userId+$(this).val()+",";
			else
				userId = userId+$(this).val();
	});
	$("#nodeForm #userId").val(userId);
	var processKey =$("#processKey").val();
	var taskId =$("#taskId").val();
	$("#nodeType1").removeAttr("disabled");
	$.ajax({
		type: "post",  
	      url: 'flow/baseFlow/saveNodeList?', 
	      data: $("#nodeForm").serialize(), 
	      dataType :  'json',
	      success: function(data) {
	    if(data.msg =='0'){
	       common.closeModelDialog('nodeDialog');
	       $('#addNode').hide();
	       refleshNodeList();
	    }else{
	       $.alert(common.i18n('cp.js.sw70')+data.msg);
	       $("#okBtn").removeAttr('disabled');
	     }
	    }
	  });
}

function refleshNodeList(){
	var instId =$("#instId").val();
	$.ajax({
		async:false,
		type: "post",  
		url: 'flow/baseFlow/showNodeList?instId='+instId,
		success: function(data) {
			$('#addNodeList').html(data);
		}
	})
}

function refleshNotifyList(){
	var instId =$("#instId").val();
	$.ajax({
		async:false,
		type: "post",  
		url: 'flow/baseFlow/showNotifyList?instId='+instId,
		success: function(data) {
			$('#notifyList').html(data);
		}
	})
}
function saveDelegation(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#delegationForm').bootstrapValidator('validate');
}
function saveDelegationOrg(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#delegationForm').bootstrapValidator('validate');
	if(ids.length<1){
		$.alert(common.i18n('cp.js.aA.697'));
		return false;
	}else if(ids.length>1){
		$.alert(common.i18n('cp.js.aA.700'));
		return false;
	}else{
		$("#userIds").val(ids);
		submitDelegation();
	}
}

function submitDelegation(){
	$("#okBtn").attr("disabled",true);
	var taskId =$("#taskId").val();
	 var objId = $('#objId').val();
	 var id = $('#id').val();
	 var key = $('#key').val();
	$.ajax({
		type: "post",  
		url: 'flow/baseFlow/saveDelegation1?taskId='+taskId,
		data: $("#delegationForm").serialize(), 
	    dataType :  'json',
		success : function(data) {
			if (data.msg == '0') {
				common.closeModelDialog('assigneeDialog');
				common.success(common.i18n('cp.js.submit_successfully'), afterSaveF(key, id, objId));
			} else {
				$.alert(common.i18n('cp.js.sw70') + data.msg);
				$("#okBtn").removeAttr('disabled');
			}
		}
	});
}

function changeNodeType(){
	var nodeType = $('#nodeType1').val();
	if(nodeType=='1'){
		$('#nodeNum').hide();
		$('.nodeNum').hide();
	}else{
		$('#nodeNum').show();
		$('.nodeNum').show();
	}
}

function confirmApprove() {
	var validate = $('#approveForm').data("validator");
	if (validate && !$('#approveForm').valid()) { //如果有验证 但是验证不通过
		return false;
	}
	if ($('#comment').val().length > 4000) {
		$.alert("审批意见超长！");
		return false;
	}
	var taskId = $('#taskId').val();
	if (approveValid(taskId)){
	    var optMsg = $('#optMsg').val();
	    var message = '确定同意吗？';
	    if (optMsg == 'R') message = '确定退回上一级吗？';
	    else if (optMsg == 'N') message = '确定退回申请人吗？';
	    disableBtn();
	    common.confirm(message,
	    	function() {
	    		$('#cycleNodeList').html("");
	    		$('#cycleNodeList').html($('#cycleNodeShow').html());
	    		assemblyFlowAssigneeVo('approve');
    	        var assignee = $("#assignee").val();
    	        var key = $('#processKey').val();
    	        $("#approveDate").val($("#appDate").val());
    	        var options = {
    	        	dataType : 'json',
    	            success: function(data) {
    	            	common.alert(data, function(){
	                    	afterSaveF(key, id, $('#objId').val(),$('entityType').val());
	  	            		common.closeModelDialog('approveDialog');
    	            	},true);
    	                enableBtn();
    	            }
    	        };
    	        $("#lcForm").attr("action", "/flow/" + key.split('_')[0] + "/approve?assignee=" + assignee + "&msg=" + optMsg);
    	        $("#lcForm").ajaxSubmit(options);
    	        return false;
	    });
	}
}

function submitApprove_pull(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#approveFormPull').bootstrapValidator('validate');	       
}

function submitApprove_jointly(){
	var taskId = $('#taskId').val();
	if(!approveValid(taskId))return false;
//	$('#approveFormJointly').bootstrapValidator('validate');	       
}

function selectAssignee_pull(){
	var id = $('#id').val();
	if(!approveValid(id)){
		common.closeModelDialog('assigneeDialog');
		return false;
	}
//	$('#batchApproveFormPull').bootstrapValidator('validate');	       	
}
function selectAssignee_jointly(){
	var id = $('#id').val();
	if(!approveValid(id)){
		common.closeModelDialog('assigneeDialog');
		return false;
	}
//	$('#batchApproveFormJointly').bootstrapValidator('validate');	       	
}

function confirmAssignee(){
	var validate = $('#batchApproveForm').data("validator");
	if (validate && !$('#batchApproveForm').valid()) { //如果有验证 但是验证不通过
		return false;
	}
	if ($('#comment').val().length > 4000) {
		$.alert("提交意见超长！");
		return false;
	}
    disableBtn();
	var id = $('#id').val();
	var processKey=$('#batchApproveForm #processKey').val();
	processKey=processKey.split(":")[0].split("_")[0];
	if(approveValid(id)){
		common.confirm("确定审核吗？",
		    function() {
		        $.ajax({
			        type:"post",
			        url:'/flow/'+ processKey+ '/batchApprove?msg=Y',
			        dataType : 'json',
			        data: $("#batchApproveForm").serialize(), 
			        success:function(res){
			        	common.alert(res, function() { 
			        		common.closeModelDialog('assigneeDialog');
			        		$("#grid-pending").bootstrapTable('refresh')
			        		query();
		            	},true);
			        	enableBtn();
			        }
			    });	
		    });
	}
}


function confirmApprove_pull(msg){
	var assignee = $('#assignee').val();
	 if(assignee==null||assignee==''||assignee==undefined){
		 $.alert(common.i18n('cp.js.t04'));
			return false;
	 }
	 var readFlag = $('#readFlag').val();
	if($('#comment').val().length>4000){
		$.alert(common.i18n('cp.js.alsl'));
		return false;
	}
	var userIds = new Array();
	var key = $('#processKey').val();
	var ffrId = $('#ffrId').val();
	var ifForceDistribute = $('#ifForceDistribute').val();
	$('.userIds').each(function(index){
		if(userIds.indexOf($(this).val())==-1){
			userIds.push($(this).val());
		}else{
			$('#userId'+index+'Assignee').remove();
			$('#processDelegationId'+index+'Assignee').remove();
			$('#delegateeId'+index+'Assignee').remove();
		}
	});
	assemblyFlowAssigneeVo('approve_pull');
	common.confirm(common.i18n('cp.js.sure_to_agree'),function(){
		if (!approveValid(taskId)) return false;
		disableBtn();
		var objId = $('#objId').val();
		var id = $('#id').val();
		var ownerCreateType = $('#ownerCreateType').val();
		$("#approveDate").val($("#appDate").val());
		
		 var options = {
				dataType : 'json',
			    success : function(data) {
				if (data.code == 'true') {
					common.success(common.i18n('cp.js.rq18'),function(){
						afterSaveF(key,id,objId);
					});
				} else if (data.code == 'false') {
                    $.alert(common.i18n('cp.js.rq16'));
                    enableBtn();
                }else if (data.code == 'validflag') {
                    $.alert(common.i18n('cp.js.rq16')+"<br/>"+data.msg);
                    enableBtn();
                }
				common.closeModelDialog('approveDialog');
				
			}
		 };
		 var key = $('#processKey').val();
		 if(fnGetApproveUrlPC(key.split('_')[0])!=undefined)
	        	pathName=fnGetApproveUrlPC(key.split('_')[0]);
	    $("#lcForm").attr("action", 'flow+/'+key.split('_')[0]+"/approve?msg="+msg+'&ownerCreateType='+ownerCreateType+'&userIds='+userIds+'&ffrId='+ffrId+'&ifForceDistribute='+ifForceDistribute+'&assignee='+assignee+'&readFlag='+readFlag);
		 $("#lcForm").ajaxSubmit(options);
		 return false;
	});
}

function confirmApprove_jointly(msg){
	var taskId = $('#taskId').val();
	var readFlag = $('#readFlag').val();
	var userIds = new Array();
	var positions = new Array();
	var key = $('#processKey').val();
	if($('#assignee').prop('disabled')==false){//会签 支持选择场景
		$('#assignee>option').each(function(index){ 
			if($(this).prop("selected")){
				if(userIds.indexOf($(this).val())==-1){
					userIds.push($(this).val());
					positions.push($(this).attr("positionId"));
				}else{
					$('#userId'+index+'Assignee').remove();
					$('#processDelegationId'+index+'Assignee').remove();
					$('#delegateeId'+index+'Assignee').remove();
				}
			}else{
				$('#userId'+index+'Assignee').remove();
				$('#processDelegationId'+index+'Assignee').remove();
				$('#delegateeId'+index+'Assignee').remove();
			} 
		});
	}else{//会签  不支持选择场景
		$('.userIds').each(function(index){
			if(userIds.indexOf($(this).val())==-1){
				userIds.push($(this).val());
				positions.push($(this).attr("positionId"));
			}else{
				$('#userId'+index+'Assignee').remove();
				$('#processDelegationId'+index+'Assignee').remove();
				$('#delegateeId'+index+'Assignee').remove();
			}
		});
	}
	assemblyFlowAssigneeVo('approve_jointly');
	if($('#comment').val().length>4000){
		$.alert(common.i18n('cp.js.alsl'));
		return false;
	}
	common.confirm(common.i18n('cp.js.sure_to_agree'),function(){
		var assignee = $("#assignee").val();
		if($('#assignee').prop('disabled')==false){//会签 支持选择场景 默认处理人传一个
			assignee=$('#assignee').find('option:selected').eq(0).val();
		}
		if (!approveValid(taskId)) return false;
		disableBtn();
		var objId = $('#objId').val();
		var id = $('#id').val();
		var ownerCreateType = $('#ownerCreateType').val();
		$("#approveDate").val($("#appDate").val());
		
		 var options = {
				dataType : 'json',
			    success : function(data) {
				if (data.code == 'true') {
					 common.success(common.i18n('cp.js.rq18'),function(){
						 afterSaveF(key,id,objId);
					 });
				} else if (data.code == 'false') {
                   $.alert(common.i18n('cp.js.rq16'));
                   enableBtn();
               }else if (data.code == 'validflag') {
                   $.alert(common.i18n('cp.js.rq16')+"<br/>"+data.msg);
                   enableBtn();
               }
				common.closeModelDialog('approveDialog');
				
			}
		 };
		 var key = $('#processKey').val();
		 if(fnGetApproveUrlPC(key.split('_')[0])!=undefined)
	        	pathName=fnGetApproveUrlPC(key.split('_')[0]);
	     $("#lcForm").attr("action", 'flow/'+key.split('_')[0]+"/approve?msg="+msg+'&ownerCreateType='+ownerCreateType+'&userIds='+userIds+'&assignee='+assignee+'&positionIds='+positions+'&readFlag='+readFlag);
		 $("#lcForm").ajaxSubmit(options);
		 return false;
	});
}

function submitDistribute(){
//	$('#distributeForm').bootstrapValidator('validate');
}

function confirmDistribute(){
	if($('#comment').val().length>4000){
		$.alert(common.i18n('cp.js.alsl'));
		return false;
	}
	common.confirm(common.i18n('cp.js.aA.1062'),function(){
		disableBtn();
		var objId = $('#objId').val();
		var id = $('#id').val();
		var assignee = $("#assignee").val();
		var key = $('#processKey').val();
		$("#approveDate").val($("#appDate").val());

		 var options = {
			    success : function(data) {
				if (data == 'true') {
					 common.success(common.i18n('cp.js.handed_out_successfully'),function(){
						 afterSaveF(key,id,objId);
					 });
				} else{
					$.alert(common.i18n('cp.js.aA.1077'));
				} 
				enableBtn();
				common.closeModelDialog('distributeDialog');
				
			}
		 };
		 $("#lcForm").attr("action", "flow/baseFlow/saveDistribute?assignee="+assignee);
		 $("#lcForm").ajaxSubmit(options);
		 return false;
	});
}

function revalidNum(){
	$('#nodeListTable .numValid').each(function(i){
//			$('#nodeForm').bootstrapValidator('revalidateField', $('#'+$(this).attr('id')));
	});
}

function assemblyFlowAssigneeVo(name){
	$('#assigneeList').html("");
	if(name=='approve' || name=='distribute' || name=='batchApprove'){
		//单处理人 表单内同意
		var assignee=$('#assignee').find("option:selected");
		var userId=assignee.attr("userId") || $('#assignee').val();
		var processDelegationId=assignee.attr("processDelegationId");
		var delegateeId=assignee.attr("delegateeId");
		$('#nodeId').val($('#nextNodeId').val());
		var html ='<input type="hidden" name="assigneeList[0].userId" value="'+userId+'"></input>';
		html +=('<input type="hidden" name="assigneeList[0].processDelegationId" value="'+processDelegationId+'"></input>');
		html +=('<input type="hidden" name="assigneeList[0].delegateeId" value="'+delegateeId+'"></input>');
		$('#assigneeList').html(html);
		
	}else if(name=='approve_pull'){
		$('#assigneeList').html($('#assigneeListPull').html()+$('#ffrAssigneeVo').html()); 
	}else if(name=='approve_jointly'){
		$('#assigneeList').html($('#assigneeListJointly').html());
	}else if(name=='batchApprove_pull'){
		$('#assigneeList').html($('#assigneeListPull').html()+$('#ffrAssigneeVo').html()); 
	}else if(name=='batchApprove_jointly'){
		$('#assigneeList').html($('#assigneeListJointly').html());
	}
}
