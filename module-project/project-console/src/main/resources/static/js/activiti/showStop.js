var readFlag;
var taskI;
var actionName;
var commentArray =['不同意'];
$(function() {
	if($('#comment1').length>0)	{
		if($('#comment').val()!=""&&$.inArray($('#comment').val(), commentArray)==-1){
			$('#comment1').text($('#comment').val());
		}else{
			$('#comment1').text('不同意');
			$('#comment').val('不同意');
		}
	}
	actionName = $('#actionName').val();
	if($('#stopForm').length>0){
		var $forms = $("#stopForm.validate", $('#stopForm').context);
		var option = { // 验证的一些设置
			ignore: ".novalidate", // 对某些元素不验证
			errorClass: "help-inline", // 默认 "error"。指定错误提示的 css 类名
			errorElement: "label", // 指定使用什么标签标记错误。
			highlight: function(element) { // 可以给未通过验证的元素加效果、闪烁等。
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error');
			}
		};
		$forms.each(function() {
			var $form = $(this);
			var opts = $.extend(true, {}, option, $form.data());
			$form.validate(opts);
		});
	}
});

function confirmStop(){
	debugger
	if($('#comment').val().length>4000){
		$.alert("审批意见超长！");
		return false;
	}
	var id = $("#id").val();
	var key = $("#actionName").val();
	disableBtn();
	var options = {
		dataType : 'json',
		success : function(data) {
			common.alert(data,
				function(){
					submitSuccessConfirm(id, $('#objId').val());
			},false);
		}
	};
	$("#stopForm").attr("action", "/flow/"+key+"/stop?id="+id+"&key="+key);
	$("#stopForm").ajaxSubmit(options);
}

function enableBtn(){
	$("#btns").find("button").each(function(i){
		$(this).removeAttr("disabled");
	 });
	$("input[type='button']").each(function(i){
		$(this).removeAttr("disabled");
	});
 }
function disableBtn(){
	$("#btns").find("button").each(function(i){
		$(this).attr("disabled",true);
	});
	$("input[type='button']").each(function(i){
		$(this).attr("disabled",true);
	});
}