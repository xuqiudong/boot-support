var targetKey_;
var entityId_;
var entityType_;
var definitionId_;
var actionName_;
var flowName_;
function queryProcessFormList(entityId,sourcekey,targetKey,entityType,definitionId,actionName, flowName){
	targetKey_ = targetKey;
	entityId_ = entityId;
	entityType_ = entityType;
	definitionId_ = definitionId;
	actionName_ = actionName;
	flowName_=flowName;
	$.ajax({
		type : "post",
		url: rcContextPath + '/baseFlow/queryProcessFormListMult?entityId='+entityId+"&key="+sourcekey, 
		success : function(data) {
			$('#processFormDialog').html(data);
			$('#processFormDialog').modal('toggle');
		}
	});
}

function processStartValid(objId,key,type) {
	var valid = true;
	$.ajax({
		type: "post",
		url: rcContextPath + '/baseFlow/processStartValid?objId=' + objId+'&key='+key+'&type='+type,
		dataType: 'json',
		async: false,
		success: function(data) {
			if (data.msg != 'true') {
				if(type!='ExtLawyerEmploy')
					common.error(data.msg);
				valid = false;
			}
		}
	});
	return valid;
}

function confirmProcessForm(){
	var processFormListSize = $('#processFormListSize').val();
	var ids =  new Array();
	var processkey= new Array();
	var checkBoxs=$("input[name='formId']:checked");
	checkBoxs.each(function(i) {
		ids.push($(this).attr('id'));
		processkey.push($(this).attr('processkey'));
	});
	if(ids.length!=1&&processFormListSize!=0&&(targetKey_=='ProjFinanceTrialChgHK'||targetKey_=='ProjFinanceTrialChg')){
		common.error(common.i18n('cp.js.sw07'));
		return false;
	}else if(ids.length!=1&&targetKey_!='ProjFinanceTrialChgHK'&&targetKey_!='ProjFinanceTrialChg'){
		common.error(common.i18n('cp.js.sw07'));
		return false;
	}else{
		common.closeModelDialog('processFormDialog');
		if(definitionId_!='')
			top.addTab('tabs',"", targetKey_, rcContextPath+'/flowForm/apply?flowDefinitionId='+definitionId_+'&key='+targetKey_+'&flag=add&objId='+entityId_+"&strId="+ids+'&entityType='+entityType_, true); 	
		else
			top.addTab('tabs', flowName_, targetKey_, rcContextPath+'/'+actionName_+'/apply?key='+targetKey_+'&flag=add&objId='+entityId_+"&strId="+ids+'&entityType='+entityType_+'&preKey='+processkey, true);
	}
}

function generatePdfMultLanguage(url,url1,formId,fn,key){
	var className= 'lc-title02';
	var pdfFlag = $('#pdfFlag').val();
	var key1 = key;
	if(key!=undefined)key1=key.split('_')[0];
	if(key1=='BfeOrderApproval' || key1=='PurchaseOrderApproval'){
		className= 'lc-title03';
	}
	var twoBarCodeMarginTop = -55;
	if(key1=='CostPaymentApproveGY'){
		twoBarCodeMarginTop = twoBarCodeMarginTop+70;
	}
	var targetStr = 'class="'+className+'"><div class="twoBarCode" style="margin-top: '+twoBarCodeMarginTop+'px;"><img style="height: 100px;width: 100px;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /></div>';
	var companyCode = $('#companyCode').val();
	//归档生成双语pdf
	if('zh_CN'==sessionLocale){
		//生成文件顺序：先英文后中文
		if(companyCode=='gongyin'){
			$.ajax({
				async:false,
				type: "post",  
				url: url+"&local=en_US",    
				success: function(data) {
					var twoBarCodeNumber = $(data).find('#twoBarCodeNumber').val();
					if(twoBarCodeNumber!=null&&twoBarCodeNumber!=''&&twoBarCodeNumber!=undefined){
						targetStr =  'class="'+className+'"><div class="twoBarCode" style="margin-top: '+twoBarCodeMarginTop+'px;width: 100px;"><img style="margin-bottom: -10px;height: 100px;width: 100%;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /><span class="twoBarCodeNo" style="margin-left: 0px;">'+twoBarCodeNumber+'</span></div>';
					}
					data = data.replace('class="'+className+'">',targetStr);
					$('#xmlEnglist').val(data);
				}
			});
		}
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=zh_CN",    
			success: function(data) {
				var twoBarCodeNumber = $(data).find('#twoBarCodeNumber').val();
				if(twoBarCodeNumber!=null&&twoBarCodeNumber!=''&&twoBarCodeNumber!=undefined){
					targetStr =  'class="'+className+'"><div class="twoBarCode" style="margin-top: '+twoBarCodeMarginTop+'px;width: 100px;"><img style="margin-bottom: -10px;height: 100px;width: 100%;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /><span class="twoBarCodeNo" style="margin-left: 0px;">'+twoBarCodeNumber+'</span></div>';
				}
				data = data.replace('class="'+className+'">',targetStr);
				$('#xml').val(data);
			}
		});
	}else{
		//生成文件顺序：先中文后英文
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=zh_CN",    
			success: function(data) {
				data = data.replace('class="'+className+'">',targetStr);
				$('#xml').val(data);
			}
		});
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=en_US",    
			success: function(data) {
				data = data.replace('class="'+className+'">',targetStr);
				$('#xmlEnglist').val(data);
			}
		});
	}
	
	$.ajax({
		async:false,
		type: "post",  
		data: $("#xmlForm").serialize(), 
		url:  url1,  
		dataType :  'json',
		success: function(data) {
			if(data.msg =='0'){
				 common.success(common.i18n('cp.js.submit_successfully'),function(){
					 $('#xml').val("");
					 if (fn!=''&&jQuery.isFunction(fn)) {
							fn();
						}
					 common.getTab('main').query();
				 });
			}else{
				common.error(common.i18n('cp.js.achf'));
			}
		}
	});
}

// 流程表单失效
function invalidForm(targt,objType,objId){
	var curTable = $(targt).parents("div").filter("#entityPending").find("table");
	// 仅支持已完成、已生成状态的表单操作
	var ids = curTable.bootgrid("getSelectedRows");
	var current = curTable.bootgrid("getSelectedRowsContent");
	var instId = $(current[0]).prop('instId');
	var key = $(current[0]).prop('processkey');
	if(ids.length !=1){
		common.error(common.i18n('cp.js.sw07'));
		return false;
	}
	var statusFore  = $(current[0]).prop('status');
	var status = getFlowFormStatus($(current[0]).prop('id'),$(current[0]).prop('processkey'));
	if(status!=2&&status!=3){
		common.error(common.i18n('cp.js.Jul20.39th.j44'),function(){
			if(statusFore!=status){
				curTable.bootgrid("search","");
			}
		});
		return false;
	}else{
		// 是否可失效标志
		var flag = false;
		var key1 = key.split('_')[0];
		// 流程是否可失效校验
		$.ajax({
			async:false,
			type:"post",
			url: rcContextPath+"/flowgy/getProcessInvalidMsg?id="+ids+"&key="+key1, 
			dataType :  'json',
			success:function(data){
				// alert(data.status);
				if (data.result == 'false') {
					common.error(data.msg);
					flag = false;
				} else if (data.result == 'true') { // 指令未处理
					flag = true;
				}
			}
		});
		if(flag){
			common.confirm(common.i18n('cp.js.sure_to_invalidate'),function(){
				$.ajax({
					type:"post",
					url: rcContextPath+"/flowgy/invalidForm?instId="+instId+"&id="+ids+"&key="+key+"&status="+status, 
					dataType :  'json',
					success:function(data){
						if (data.msg =='0') {
							common.success(common.i18n('cp.js.operation_successfully'),function fn(){
								common.getTab('main').query();
								refreshTable(objType,objId,curTable);
							});
						} else if (data.msg =='1') {
							common.error(common.i18n('cp.js.operation_failed'));
						}
					}
				});
			});
		}
	}
}

// 流程表单生效
function validForm(targt,objType,objId){
	var curTable = $(targt).parents("div").filter("#entityPending").find("table");
	// 仅支持已失效状态的表单操作
	var ids = curTable.bootgrid("getSelectedRows");
	var current = curTable.bootgrid("getSelectedRowsContent");
	var instId = $(current[0]).prop('instId');
	var key = $(current[0]).prop('processkey');
	if(ids.length !=1){
		common.error(common.i18n('cp.js.sw07'));
		return false;
	}
	var statusFore  = $(current[0]).prop('status');
	var status = getFlowFormStatus($(current[0]).prop('id'),$(current[0]).prop('processkey'));
	if(status!=7){
		common.error(common.i18n('cp.js.Jul20.38th.j9'),function(){
			if(statusFore!=status){
				curTable.bootgrid("search","");
			}
		});
		return false;
	}else{
		// 流程是否可生效校验
		var key1 = key.split('_')[0];
		$.ajax({
			async:false,
			type:"post",
			url: rcContextPath+"/flowgy/getProcessValidMsg?id="+ids+"&key="+key1, 
			dataType :  'json',
			success:function(data){
				// alert(data.status);
				if (data.sign == '1') {
					common.error(data.msg);
					return false;
				} else if (data.sign == '0') {
					common.confirm(common.i18n('cp.js.Jul20.38th.j7'),function(){
						$.ajax({
							type:"post",
							url: rcContextPath+"/flowgy/validForm?instId="+instId+"&id="+ids+"&key="+key+"&status="+status, 
							dataType :  'json',
							success:function(data){
								if (data.msg =='0') {
									common.success(common.i18n('cp.js.operation_successfully'),function fn(){
										common.getTab('main').query();
										refreshTable(objType,objId,curTable);
									});
								} else if (data.msg =='1') {
									common.error(common.i18n('cp.js.operation_failed'));
								} 
							}
						});
					});
				}
			}
		});
	}
}

function refreshTable(objType,objId,curTable){
	//维修改装生效失效后 刷新页面 刷新金额。金额实时从流程中取得
	if("REPAIRREFITPROJECT" == objType && objId){
	    if (common.getTab1('editRepairRefitPro'+ objId) != null){
	    	top.addTab('tabs', common.i18n('cp.js.Oct20.46th.j13'), 'editRepairRefitPro' + objId,
	    			rcContextPath + '/repairRefitPro/repairRefitProForm?flag=edit&id=' + objId, true, false, 'visitRepairRefitPro');
		}else if (common.getTab1('detailRepairRefitPro'+ objId) != null){
	    	 top.addTab('tabs', common.i18n('cp.js.Oct20.46th.j12'), 'detailRepairRefitPro'+objId,
	    				rcContextPath+ '/repairRefitPro/repairRefitProForm?flag=detail&id=' + objId,true,false,'visitRepairRefitPro');
		}
	}else{
		$(curTable).bootgrid("search","");
	}
}
//ICBCSTL-51007
function getFlowFormStatus(formId,key){
	var flowStatus="";
	$.ajax({
		async:false,
		type:"post",
		url: rcContextPath+"/baseFlow/getFlowFormStatus?formId="+formId+"&key="+key, 
		dataType :  'json',
		success:function(data){
			flowStatus=data;
		}
	});
	return flowStatus;
}

//ICBCSTL-51007 待审列表 删除流程操作中公共提出来
function deleteFormCommon(gridName,myLineReflashFn){
	var ids = $("#"+gridName).bootgrid("getSelectedRows");
	if(ids.length <1){
		common.error(common.i18n('cp.js.p40'));
		return false;
	}else{
		var current = $("#"+gridName).bootgrid("getSelectedRowsContent");
		var keys = new Array();
		var actionNames = new Array();
		var ifSame = true;
		for(var i=0;i<ids.length;i++){
			var statusFore = $(current[i]).prop('status');
			var status = getFlowFormStatus($(current[i]).prop('id'),$(current[i]).prop('processkey'));
			if(status!=0 && status!=5){
				common.error(common.i18n('cp.js.nely'),function(){
					if(statusFore!=status){
						$("#"+gridName).bootgrid("search","");
					}
				});
				return false;
			}	
			var key =  $(current[i]).prop('processkey');
			keys.push(key);
			var actionName1 = $(current[i]).prop('actionName');
			var isDefinition = $(current[i]).prop('isDefinition');
			if(isDefinition=='1') actionName = "flowForm";
			else if(actionName1!='') actionName = actionName1;
			else actionName = fnGetApproveUrlPC(key);
			if($.inArray(actionName,actionNames)==-1&&i>0)//删除的表单在不同的action里
				ifSame=false;
			actionNames.push(actionName);
		}
		//common.confirm(common.i18n('cp.js.a1'),function(){
			if(!ifSame){
				for(var i=0;i<ids.length;i++){
					$.ajax({
						type:"post",
						async: false,
						url: rcContextPath + '/'+actionNames[i]+'/blankOut?id='+ids[i]+'&key='+keys[i],   
						success:function(msg){ }
					});
				}
				common.success(common.i18n('cp.js.delete_successfully'));
				//查询主线任务明细id，同时更新任务明细状态 //主线页面发起审批流程 
				if (myLineReflashFn!=''&&jQuery.isFunction(myLineReflashFn)) {
					 myLineReflashFn();
				}
				$("#"+gridName).bootgrid("search","");
			}else{
				$.ajax({
					type:"post",
					url: rcContextPath + '/'+actionNames[0]+'/blankOut?id='+ids+'&key='+keys,   
					success:function(msg){
						common.success(common.i18n('cp.js.delete_successfully'));
						//查询主线任务明细id，同时更新任务明细状态 //主线页面发起审批流程 
						if (myLineReflashFn!=''&&jQuery.isFunction(myLineReflashFn)) {
							 myLineReflashFn();
						}
						$("#"+gridName).bootgrid("search","");
					}
				});
			}
		//});
	}
}

//ICBCSTL-51007 待审列表 撤销流程操作中公共提出来
//撤回成功 fn通用的回调函数  eg：刷新主线
function cancelFormCommon(gridName,fn){
	var ids = $("#"+gridName).bootgrid("getSelectedRows");
	if(ids.length !=1){
		common.error(common.i18n('cp.js.p13'));
		return false;
	}
	var current = $("#"+gridName).bootgrid("getSelectedRowsContent");
	var keys = new Array();
	var statusS = new Array();
	var instIds = new Array();
	var actionNames = new Array();
	var ifSame = true;
	for(var i=0;i<ids.length;i++){
		var statusFore = $(current[i]).prop('status');
		var instId = $(current[i]).prop('instId');
		var key =  $(current[i]).prop('processkey');
		var status = getFlowFormStatus($(current[i]).prop('id'),key);
		if(status!=1&&status!=4){
			common.error(common.i18n('cp.js.only_reviewing_or_rollback'),function(){
				if(statusFore!=status){
					$("#"+gridName).bootgrid("search","");
				}
			});
			return false;
		}	
		statusS.push(status);
		instIds.push(instId);
		keys.push(key);
		var isDefinition = $(current[i]).prop('isDefinition');
		var actionName1 = $(current[i]).prop('actionName');
		
		if(isDefinition=='1') actionName = "flowForm";
		else if(actionName1!='') actionName = actionName1;
		else actionName = fnGetApproveUrlPC(key);
		actionNames.push(actionName);
		if($.inArray(actionName,actionNames)==-1&&i>0)//删除的表单在不同的action里
			ifSame=false;
	}
	common.confirm(common.i18n('cp.js.sure_to_cancel'),function(){
		if(!ifSame){
			for(var i=0;i<ids.length;i++){
				var instId = $(current[i]).prop('instId');
				$.ajax({
					type:"post",
					async: false,
					url: rcContextPath+"/"+actionNames[i]+"/cancel?instId="+instId+"&id="+ids[i]+"&key="+keys[i]+"&status="+statusS[i], 
					success:function(msg){ 
						if (data.msg =='0') {
							common.success(common.i18n('cp.js.operation_successfully'));
						} else if (data.msg =='1') {
							common.error(common.i18n('cp.js.operation_failed_the'));
						} 
					}
				});
			}
			if (fn!=''&&jQuery.isFunction(fn)) {
				 fn();
			}
			$("#"+gridName).bootgrid("search","");
			common.getTab('main').query();
		}else{
			$.ajax({
				type:"post",
				url: rcContextPath+"/"+actionNames[0]+"/batchCancel?instId="+instIds+"&id="+ids+"&key="+keys+"&status="+statusS, 
				dataType :  'json',
				success:function(data){
					if (data.msg =='0') {
						common.success(common.i18n('cp.js.operation_successfully'));
						if (fn!=''&&jQuery.isFunction(fn)) {
							 fn();
						}
						$("#"+gridName).bootgrid("search","");
						common.getTab('main').query();
					} else if (data.msg =='1') {
						common.error(common.i18n('cp.js.operation_failed_the'));
					} 
				}
			});
		}
	}); 
}

//ICBCSTL-51007 待审列表 编辑流程操作中公共提出来
function editFormCommon(gridName){
	var ids = $("#"+gridName).bootgrid("getSelectedRows");
	if(ids.length  !=1){
		common.error(common.i18n('cp.js.pdd'));
		return false;
	}
	var objId = $("#id").val();
	var current = $("#"+gridName).bootgrid("getSelectedRowsContent");
	var statusFore = $(current[0]).prop('status');
	var id = $(current[0]).prop('id');
	var processkey = $(current[0]).prop('processkey');
	var key = processkey.split('_')[0];
	var status = getFlowFormStatus(id,key);
	if(status!=0 && status!=5){
		common.error(common.i18n('cp.js.ow'),function(){
			if(statusFore!=status){
				$("#"+gridName).bootgrid("search","");
			}
		});
		return false;
	}else{
		var processName = $(current[0]).prop('processName');
		var entityType = $(current[0]).prop('entityType');
		var isDefinition = $(current[0]).prop('isDefinition');
		var actionName1 = $(current[0]).prop('actionName'); 
    	if(sessionLocale =='en_US')
    		processName = ' '+processName;
		var actionName="";
		if(isDefinition=='1') actionName = "flowForm";
		else if(actionName1!='') actionName = actionName1;
		else actionName = fnGetApproveUrlPC(key);
		
		top.addTab('tabs', common.i18n('cp.js.edit') + processName, processkey+id, rcContextPath+'/'+actionName+'/apply?key='+processkey+'&id='+id+'&objId='+objId+'&entityType='+entityType+'&flag=edit', true);
	}
}