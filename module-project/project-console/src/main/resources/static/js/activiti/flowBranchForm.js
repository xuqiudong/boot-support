var flowBranchForm = {};
var flowCondiIndex;

$(function(){

	flowCondiIndex = $('#flowBranchCondiTable').find('tr').length - 1;
	for(var i=0;i<flowCondiIndex;i++){
		flowBranchForm.switchItem(i);
	}
	common.pubilcMethodContainer("form");
});

flowBranchForm.addBranchCondi = function(){
	var index=flowCondiIndex;
	var obj = $('#flowCondiForCopy').find('tr').clone();
	obj.attr("id","condi-tr-"+index);
	obj.html(obj.html().replace(/@indexvalue@/g, flowCondiIndex++));
	$("#flowBranchCondiTable").append(obj);
}

flowBranchForm.delBranchCondi = function(){
	var $checkboxs = $("input[name='flowBranchChb']:checked");
	if($checkboxs.length == 0){
		$.alert("请选择分支条件!",'提示');
        return;
	}
	$checkboxs.each(function(){
		var indexNo = $(this).val();
		$("#condi-tr-"+indexNo).remove();
	});
	 common.repalceIndexForTable('flowBranchCondiTable');//下标重新排序
}

/**
 * 分支条件-分支元素 切换时，对应的值切换
 * @param index 列表中序号
 */
flowBranchForm.switchItem = function(index){
	var itemValue = $("#item_"+index).val();
	if(itemValue == "projectAmount" || itemValue == "batchAmount1" 
		|| itemValue == "batchAmount2" || itemValue == "batchAmount3" 
			|| itemValue == "singleAmount"){
		itemValue = "common";
	}
	$("#condi-tr-"+index+" div").css("display","none");
	$("#condi-tr-"+index+" div input,select").attr("disabled",true);
	$("#condi-"+itemValue + '-'+index).css("display","");
	$("#condi-"+itemValue + '-'+index+" input,select").attr("disabled",false);
}

//选择部门
flowBranchForm.addOrganization = function(index){
	var url = base_url + "/activiti/nodeOwner/organizationModal";
	common.confirm("url:" + url, function() {
		var treeObj = $(".ztree").data("ztree");
	 	var nodes = treeObj.getCheckedNodes();
	 	if (nodes.length != 1) {
	        common.confirm("请选择一个节点!");
	        return;
	 	}
	 	var orgId = nodes[0].id;
	 	var orgName = nodes[0].name;
	 	$('#flowBranchCondiTable #organizationId_'+index).val(orgId);
	 	$('#flowBranchCondiTable #organizationName_'+index).val(orgName);
	}, {title: '部门', closeIcon: true,columnClass: 'col-md-8 col-md-offset-0'});
}

//选择岗位
flowBranchForm.addPosition = function(index){
	var url = base_url + "/system/position/positionListModal";
	common.confirm("url:" + url, function() {
		var rows = $("#positionModalTable").bootstrapTable('getSelections');
		if (rows.length == 0) {
			$.alert("请至少选择一行数据", "提示");
			return false;
		}
		var positionId = rows[0].id;
	 	var positionName = rows[0].name;
	 	$('#flowBranchCondiTable #positionId'+index).val(positionId);
	 	$('#flowBranchCondiTable #positionName'+index).val(positionName);
	}, {title: '岗位', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
}


//选择用户
flowBranchForm.addUser = function(index){
	var url = base_url + "/system/user/userListModal";
	common.confirm("url:" + url, function() {
		var rows = $("#userModalTable").bootstrapTable('getSelections');
		if (rows.length == 0) {
			$.alert("请至少选择一行数据", "提示");
			return false;
		}
		var userId = rows[0].id;
	 	var userName = rows[0].name;
	 	$('#flowBranchCondiTable #userId'+index).val(userId);
	 	$('#flowBranchCondiTable #userName'+index).val(userName);
	}, {title: '用户', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
}


//选择参数
flowBranchForm.addParameter= function(index){
	var url = base_url + "/system/parameter/parameterListModal";
	common.confirm("url:" + url, function() {
		var rows = $("#parameterModalTable").bootstrapTable('getSelections');
		if (rows.length == 0) {
			$.alert("请至少选择一行数据", "提示");
			return false;
		}
		var parameterId = rows[0].id;
	 	var parameterName = rows[0].name;
	 	$('#flowBranchCondiTable #parameterId'+index).val(parameterId);
	 	$('#flowBranchCondiTable #parameterName'+index).val(parameterName);
	 	//回显参数项下拉
	 	$request.postSync(base_url + "/system/option/getByPid?pid="+parameterId, '', function(data) {
			console.info(data);
	 		if (!!data.data && data.data.length>0) {
		        for(var i=0;i<data.data.length;i++){
		        	var option = data.data[i];
		        	$("#parameterValue"+index).append("<option value='"+option.code+"'>"+option.text+"</option>");
		        }
			}
		});
	}, {title: '参数', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
}

//切换参数项
flowBranchForm.changeOption = function(index){
	var optionId = $("#parameterValue"+index).val();
	$('#flowBranchCondiTable #optionId'+index).val(optionId);
}