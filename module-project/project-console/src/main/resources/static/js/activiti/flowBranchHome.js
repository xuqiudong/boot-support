function yesOrNoFormat(value, row, index, field) {
	let html =	"";
	if(value=='1'){
		html = "是";
	}else{
		html= "否";
	
	}
	return html;
}

function branchOperationFormat(value, row, index, field) {
	let html =	"";
	if(value=='and'){
		html = "与";
	}else if(value=='or'){
		html= "或";
	
	}
	return html;
}

function viewForm(id,key,processName){
	common.addTab('/flow/'+key+'/pdfForm?id='+id+'&key='+key,processName+"预览", id);
}