var deletedAttachmentIds = new Array();
var rcContextPath=window.base_url+"/flow";
var optType;
var msg;
var processId;
var taskId;
var key;
var start;
var hasText;
var entityType='';
var formEntityId;
var readFlag;
var saveNoValidArray =["CoopOrganAdjustDebtApproval"];
$.fn.extend({
    txtaAutoHeight: function () {
       return this.each(function () {
            var $this = $(this);
            if (!$this.attr('initAttrH')) {
                $this.attr('initAttrH', $this.outerHeight());
            }
            setAutoHeight(this).on('input', function () {
               setAutoHeight(this);
           });
       });
       function setAutoHeight(elem) {
           var $obj = $(elem);
           if(elem.scrollHeight == 0){
        	   return $obj.css({ height: $obj.attr('initAttrH'), 'overflow-y': 'hidden' }).height($obj.attr('initAttrH'));
           }else{
        	   return $obj.css({ height: $obj.attr('initAttrH'), 'overflow-y': 'hidden' }).height(elem.scrollHeight);
           }
       }
   }
});
window.onload=function(){
	$("#lcForm textarea").txtaAutoHeight();
	$('#formContent').css("height", (top.frames[0].innerHeight) + "px");
}
$(function() {
	Number.prototype.toFixed = function(fractionDigits){
		var flag="";
		var s1=this+"";
		if(s1.indexOf("-")==0){
			s1=s1.substring(1,s1.length);
			flag="-";
		}
		var start =s1.indexOf(".");
		if(start==-1) return flag+s1;
		if(s1.substr(start+fractionDigits+1,1)==''){//整数或小数位不全
			return flag+s1;
		}
		if(s1.substr(start+fractionDigits+1,1)>=5){
			var temp=Math.pow(10,fractionDigits);
			var s=Math.floor(s1.substring(0,start+fractionDigits+1).replace(/\$|\./g,''))+1;
			return flag+s/temp;
		}else if(s1.substr(start+fractionDigits+1,1)==4 && s1.substr(start+fractionDigits+2,2)==99){//1101750*6.7193=7402988.774999999  精度一般都是 0.0000001 或者0.99999998等
			var temp=Math.pow(10,fractionDigits);
			var s=Math.floor(s1.substring(0,start+fractionDigits+1).replace(/\$|\./g,''))+1;
			return flag+s/temp;
		}else{
			return flag+s1.substr(0,start+fractionDigits+1);
		}
	}
	
	formEntityId = $('#formEntityId').val();
	$(".transformThousands").bind('blur',function(){
		if($(this).val()!=''){
			var amount =$(this).val();
			var entityType=$('#entityType').val();
			amount = parseFloat(parseFloat(amount.replace(/[^\d\.-]/g,"")).toFixed(2)); 
			$(this).val($.formatMoney(isNaN(amount)?'0.00':amount));  
		}
	 }) ;
	//输入正数
	$(".transformThousandsGT").bind('blur',function(){
		if($(this).val()!=''){
			var amount =$(this).val(); 
			amount = parseFloat(parseFloat(amount.replace(/[^\d\.]/g,"")).toFixed(2)); 
			$(this).val($.formatMoney(isNaN(amount)?'0.00':amount));  
		}
	 });
	
	$('input').keydown(function(){
		if(event.keyCode==13){
			event.keyCode==0;
			event.returnValue=false;
		}
	});
	if($('#creatorComment').val()==''){
		 $('#creatorComment').val('提交审批');
	}

	var start = $('#start').val();
    var actId = $('#actId').val();
    var flag = $('#flag').val();
    adminMode = $('#adminMode').val();
    key =  $('#processKey').val();
    status = $("#status").val();
    if (start != undefined && start == '0'){
    	$("form").find(".editForm").attr("disabled", true);
        $("form").find("input[type='checkbox']").attr("disabled", true);
        $(".wangEditor-textarea").css("background-color","#eee");
        $(".wangEditor-btn-containe").css("background-color","#eee");
    	$('button.con').hide();
    	$('.changeBtn').css('width','100%');
    	$('input.con').attr("disabled", true);
    	$('#formAbstractTop').attr("readonly", "readonly");
    }
    if(flag != undefined && flag == 'view'){
    	 $("form").find(".form-control").attr("disabled", true);
    	 $("form").find(".editForm").attr("disabled", true);
         $("form").find("input[type='checkbox']").attr("disabled", true);
         $(".wangEditor-textarea").css("background-color","#eee");
         $(".wangEditor-btn-container").css("background-color","#eee");
     	$('button.con').hide();
     	$('.changeBtn').css('width','100%');
    }
    if(status==undefined)status='';
    if(actId==undefined)actId='';
    if (status == '2' || status == '3' || (status == '1'&&actId != ''&&actId.indexOf("apply") < 0)) {
    	$("form").find(".editForm").attr("disabled", true);
        $("form").find("input[type='checkbox']").attr("disabled", true);
        $(".wangEditor-textarea").css("background-color","#eee");
        $(".wangEditor-btn-container").css("background-color","#eee");
    	$('button.con').hide();
    	$('.changeBtn').css('width','100%');
    	$('input.con').attr("disabled", true);
    }
    if(typeof pathName=='undefined')
    var pathName = "activiti";
    if(pathName.indexOf("flowds")==0){
    	 $("form").find("button.attr").removeAttr("disabled");
    }
    $(".wangE table").each(function(){
    	$(this).removeAttr('width');
    	$(this).removeAttr('style');
	});
    $(".wangEditor-textarea-container").children().each(function(){
    	removeFont(this);
	});
});

function removeFont(obj) {
   $(obj).children().each(function(){
   		$(this).css('font-family','');
   		$(this).removeAttr('face');
   		return removeFont(this);
   });
}

function read(taskId, flag) {
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/showRead?flag=' + flag + '&taskId=' + taskId,
        success: function(data) {
            $('#readDialog').html(data);
            $('#readDialog').modal('toggle');
        }
    });
}

function delegation() {
    var objId = $('#objId').val();
    var id = $('#id').val();
    var key = $('#processKey').val();
    var taskId = $('#taskId').val();
    if (!approveValid(taskId)) return false;
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/delegation?objId=' + objId + '&id=' + id + '&key=' + key,
        data: $("#flowForm").serialize(),
        success: function(data) {
            $('#assigneeDialog').html(data);
            inTree();
            $('#assigneeDialog').modal('toggle');
        }
    });
}

function addNode() {
    var taskId = $('#taskId').val();
    var nodeFlag = $('#nodeFlag').val();
    if (!approveValid(taskId)) return false;
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/addNode?nodeFlag'+nodeFlag,
        data: $("#flowForm").serialize(),
        success: function(data) {
            $('#nodeDialog').html(data);
            $('#nodeDialog').modal('toggle');
        }
    });
}

function addNotify(flag) {
    var taskId = $('#taskId').val();
    if (!approveValid(taskId)) return false;
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/addNotify?flag='+flag,
        data: $("#flowForm").serialize(),
        success: function(data) {
            $('#notifyDialog').html(data);
            $('#notifyDialog').modal('toggle');
        }
    });
}

function chooseAssignee() {
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/chooseAssignee',
        data: $("#flowForm").serialize(),
        success: function(data) {
            $('#assigneeDialog').html(data);
            $('#assigneeDialog').modal('toggle');
        }
    });
}

function stopForm(taskId) {
    var taskId = $('#taskId').val();
    var key = $('#processKey').val();
    var result = "";
    if (!approveValid(taskId,'stop')) return false;
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/showStop?taskId=' + taskId,
        success: function(data) {
            $('#stopDialog').html(data);
            $('#stopDialog').modal('toggle');
            if(result)
            	$("#stopForm .changeCondition").val(result);
        }
    });
}

function distribute() {
    var taskId = $('#taskId').val();
    var key = $('#processKey').val();
    var relevantOrgId = $('#relevantOrgId').val();
    if (!approveValid(taskId)) return false;
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/distribute?taskId=' + taskId + '&key=' + key + '&relevantOrgId=' + relevantOrgId,
        success: function(data) {
            $('#distributeDialog').html(data);
            $('#distributeDialog').modal('toggle');
        }
    });
}

function setCommit(obj) {
    $('#comment').val(obj.value);
}

function saveForm(key1) {
	if($.inArray(key1.split('_')[0], saveNoValidArray)>-1){
	}else{
    	var validate = $('#lcForm').data("validator");
		if (validate && !$('#lcForm').valid()) { //如果有验证 但是验证不通过
			return false;
		}
	}
	submitForm(key1);
}
function saveOrExcution(optType){
   if(optType=='approve'){
		var objId = "";
	    if($('#objId').length>0)
			objId = $('#objId').val();
		 var key = $('#processKey').val();
		 var taskId = $('#taskId').val();
		  $.ajax({
	        type: "post",
	        url: rcContextPath + '/baseFlow/showApprove?taskId=' + taskId + '&key=' + key + '&msg=' + msg + '&readFlag=' + readFlag+'&objId='+objId,
	        success: function(data) {
	            $('#approveDialog').html(data);
	            $('#approveDialog').modal('toggle');
	        }
	    });
	}
}

function submitForm(key){
    disableBtn();
    var status = $("#status").val();
    var id = $("#id").val();
    var objId = $("#objId").val();
    var processName = $("#processName").val();
    var pathName=key.split('_')[0];
	var tabUrl = 'flow/'+pathName + "/apply?flag=edit&key=" + key;
    var options = {
        success: function(res) {
            enableBtn();
            common.alert(res, function() {
				common.addTab(tabUrl + '&id='+res.data+'&objId='+objId+'&formEntityId='+formEntityId+'&entityType='+entityType, "编辑");
            	afterSaveF(key, id, objId,entityType,'save');
        	},true);
        }
    };
    $("#lcForm").attr("action", rcContextPath + "/" + key.split("_")[0] + "/saveOrUpdateForm");
    disableBtn();
    $("#lcForm").ajaxSubmit(options);
}

function getEditTabId(key,id){
	var tabId = key+id;
	return tabId;
}

function startExecution(key) {
	var validate = $('#lcForm').data("validator");
	if (validate && !$('#lcForm').valid()) { //如果有验证 但是验证不通过
		return false;
	}
	submitExecution(processId,taskId,key);
}

function submitExecution(processId,taskId,key) {
	var objId = $('#objId').val();
	var processKeyHaveBranch=$('#processKeyHaveBranch').val();
	if(processKeyHaveBranch=='No'){//无分支判断不用走 getProcessId
		showSubmit(key,key);
	}else{   
		$.ajax({
	        type: "post",
	        url: rcContextPath + '/baseFlow/getProcessId?key=' + key+'&entityId='+objId,
	        data: $("#lcForm").serialize(), 
	        dataType: 'json',
	        success: function(res) {
	        	var processKey = key;
	        	common.alert(res, function() {
	        		var procId = res.data.returnId;
	        		if(procId!=""){
	        			processKey = procId.split(":")[0];
	        			$('#processKey').val(processKey);
	        			$('#processId').val(procId);
	        			$('.processId').val(procId);
	        		}
	        		showSubmit(key,processKey);
	        	 },false);
	        }
	    }); 
	}
}

function showSubmit(key,processKey){
	var id = $("#id").val();
	var entityType = $("#entityType").val();
	var objId = $('#objId').val(); 
	var processId = $("#processId").val();
	var url=rcContextPath + '/baseFlow/showSubmit?key=' + processKey+'&processId='+processId+'&objId='+objId+'&entityType='+entityType+'&formId='+id;
	$.ajax({
		type: "post",
		url: url,
		success: function(data) {
//			common.confirm(data,  function(){
//				var validate = $('#submitForm').data("validator");
//				if (validate && !$('#submitForm').valid()) { //如果有验证 但是验证不通过
//					return false;
//				}
//				assemblyFlowAssigneeVo("submit");//---对应hidden中的assigneeList替换块
//				confirmExecution();
//			}, 
//			{ title:'提交审批',columnClass: 'col-md-8 col-md-offset-2'});
			$('#submitDialog').html(data);
			$('#submitDialog').modal('show');
		}
	});
}

function generatePdf(ids,actionName,formId){
	var url;var url1;var objId="";
	if(actionName==""||actionName==null){
		url = rcContextPath + top.processPath + '/pdfForm?id='+ids+"&pdfFlag=1&flowSign=1";
		url1 = rcContextPath + top.processPath + '/generatePdf?id='+ids;
	}
	else{
		url= rcContextPath + '/'+actionName+'/pdfForm?id='+ids+"&pdfFlag=1&flowSign=1";
		url1= rcContextPath + '/'+actionName+'/generatePdf?id='+ids;
	}
	generatePdfAddLanguage(url,url1,formId);
}
function generatePdfAddLanguage(url,url1,formId){
	//归档生成双语pdf
	var targetStr = 'class="lc-title02"><div class="twoBarCode" style="margin-top: -50px;"><img style="height: 100px;width: 100px;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /></div>';
	 var twoBarCodeNumber = $('#twoBarCodeNumber').val();
	 if(twoBarCodeNumber!=null&&twoBarCodeNumber!='')
		targetStr =  'class="lc-title02"><div class="twoBarCode" style="margin-top: -55px;"><img style="float: left;height: 100px;width: 100px;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /><br/><span class="twoBarCodeNo">'+twoBarCodeNumber+'</span></div>';
	
	if('zh_CN'==sessionLocale){
		//生成文件顺序：先英文后中文
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=en_US",    
			success: function(data) {
				data = data.replace('class="lc-title02">',targetStr);
				$('#xmlEnglist').val(data);
			}
		});
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=zh_CN",    
			success: function(data) {
				data = data.replace('class="lc-title02">',targetStr);
				$('#xml').val(data);
			}
		});
	}else{
		//生成文件顺序：先中文后英文
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=zh_CN",    
			success: function(data) {
				data = data.replace('class="lc-title02">',targetStr);
				$('#xml').val(data);
			}
		});
		$.ajax({
			async:false,
			type: "post",  
			url: url+"&local=en_US",    
			success: function(data) {
				data = data.replace('class="lc-title02">',targetStr);
				$('#xmlEnglist').val(data);
			}
		});
	}
	$.ajax({
		async:false,
		type: "post",  
		data: $("#xmlForm").serialize(), 
		url:  url1,  
		dataType :  'json',
		success: function(data) {
		}
	});
}

//提交审批 成功后刷新提款单状态 和 待审事项
function submitSuccessConfirm(id, objId){
	var objName = $('#objName').val();
	var key1=key.split("_")[0];
	if(entityType=='CONTRACT'){
		if (common.getTab1('editContract'+ objId) != null){
			if ($("#session-identity").val() != 'APLEASE') {//刷新合同
				common.addTab(rcContextPath + '/contract/contractForm?flag=edit&id=' + objId, '租赁合同');
			}
		}
		refreshMainTabAndClosingFlowform(key, id);
	}else{
		afterSaveF(key, id, objId,entityType);
    }	  
}

function refreshMainTabAndClosingFlowform(key, formId){
	common.getTab('main').query();
	if($('#oldProcessKey').val().length>0){//流程重新提交审批时可能流程分支变化了
		key=$('#oldProcessKey').val();
	}
	var key1 = key;
    var key2 = key.split("_")[0];
    if (formId != ""){
    	key1 = key + formId;
    	if(common.getTab1(key1) == null){
    		key1=key2+formId;
    	}
    }else if (common.getTab1(key1) == null){
    	if(key.split("_").length>2)
    		key1= key.split("_")[0]+"_"+key.split("_")[1];
    	else
    		key1 = key2; 
    }
    common.closeTab(key1);
}

/**点击同意，返回上一级，返回申请人，校验表单是否被处理过*/
function approveValid(taskId,instId) {
	var key = $('#processKey').val();
    var actId = $('#actId').val();
    var id = $('#id').val();
    var linkSize = $('#linkSize').val();
    var valid = false;
    $.ajax({
        type: "post",
        url: '/flow/baseFlow/approveValid?linkSize='+linkSize+'&taskId=' + taskId +"&instId="+instId,
        dataType: 'json',
        async: false,
        success: function(data) {
        	common.alert(data, function(){
        		valid = true;
        	},false);
        }
    });
    return valid;
}

function approve(optMsg, readFlag) {
    var taskId = $('#taskId').val();
    var isDefinition = $('#isDefinition').val();
    var relevantOrgId = $('#relevantOrgId').val();
    var instId = $('#instId').val();
    var objId = "";

    if($('#objId').length>0&&$('#objId').val()!='')
		objId = $('#objId').val();
    else   if($('#entityId').length>0)
        objId = $('#entityId').val();
    if (!approveValid(taskId,instId,optMsg)) return false;
    var key = $('#processKey').val();
    var formId=$('#formId').val();
    $.ajax({
        type: "post",
        url: rcContextPath + '/baseFlow/showApprove?taskId=' + taskId + '&key=' + key + '&optMsg=' + optMsg + '&readFlag=' + readFlag+'&objId='+objId+'&isDefinition='+isDefinition+'&formId='+formId,
        success: function(data) {
            $('#approveDialog').html(data);
            $('#approveDialog').modal('toggle');
            $(".modal-dialog").find("button[data-id='assignee']").on('click', function () {
            	var maxHeight = $('.modal-dialog').find('ul').css('max-height');
            	maxHeight = parseInt(maxHeight.substring(0,maxHeight.length-2));
            	maxHeight+=10;
            	$('.modal-dialog').find('ul').parent().css('max-height',maxHeight+'px');
            });
        }
    });
}


function cancelForm(instId, id, key,actionName) {
	var taskId = $('#taskId').val();
	if (!approveValid(taskId,'cancel')) return false;
    if (instId == '') {
        $.alert("该表单还没提交审核，不能撤销！");
        return false;
    }
    common.confirm("确定要撤销吗？",
    function() {
        var objId = $("#objId").val();
        var options = {
            dataType: 'json',
            success: function(data) {
                enableBtn();
                common.alert(data,
            		function() {
                    	afterSaveF(key, id, objId,entityType);
                });
            }
        };
        $("#lcForm").attr("action", rcContextPath + "/" + key.split("_")[0] + "/cancel?key=" + key);
        disableBtn();
        $("#lcForm").ajaxSubmit(options);
    });
}

function blankOut(id, key) {
    common.confirm("确定要作废吗？",
    function() {
        $("#blankOut").attr("disabled", true);
        var objId = $("#objId").val();
        var options = {
            dataType: 'json',
            success: function(data) {
                enableBtn();
                common.alert(data,
            		function() {
                    	afterSaveF(key, id, objId,entityType);
                });
            },
            dataType: 'json'
        };
        $("#lcForm").attr("action", rcContextPath + "/" + key.split("_")[0] + "/blankOut?key=" + key);
        disableBtn();
        $("#lcForm").ajaxSubmit(options);
    });
}

function pdfForm(ids, actionName) {
    common.addTab(rcContextPath + '/' + actionName + '/pdfForm?id=' + ids + '&flag=inst', "表单详情");
}
//预览表单，上一页、下一页
function pdfForm(instId,actionName,formId,closeInstId,processKey){
	common.addTab(rcContextPath +'/'+processKey+'/pdfForm?id='+instId+'&flag=inst', "表单详情"); 
	if (common.getTab1('viewPDF'+closeInstId) != null)
		common.closeTab('viewPDF'+closeInstId);
	else if (common.getTab1('pdfForm'+formId) != null)
		common.closeTab('pdfForm'+formId);
	else if (common.getTab1('viewPDF'+formId) != null)
		common.closeTab('viewPDF'+formId);
	else if (common.getTab1(processKey+'Pdf'+formId) != null)
		common.closeTab(processKey+'Pdf'+formId);
}

function disableBtn() {
    $("#flowBtns").find("button").each(function(i) {
        $(this).attr("disabled", true);
    });
    $(".assigneeForm").find("button").each(function(i) {
    	 $(this).attr("disabled", true);
    });
    $("input[type='button']").each(function(i) {
        $(this).attr("disabled", true);
    });
    $('.kj-modal-footer-confirm-btn').attr("disabled", true);
}

function enableBtn() {
    $("#flowBtns").find("button").each(function(i) {
        $(this).removeAttr("disabled");
    });
    $(".assigneeForm").find("button").each(function(i) {
    	$(this).removeAttr("disabled");
    });
    $("input[type='button']").each(function(i) {
        $(this).removeAttr("disabled");
    });
    $('.kj-modal-footer-confirm-btn').removeAttr("disabled");
}

function afterRead(id, actionName) {
    if (common.getTab1('main') != null) common.getTab1('main').timeTask_refresh_proceeding();
    if (actionName == "" || actionName == null) common.addTab(rcContextPath + top.processPath + '/pdfForm?id=' + id + '&flag=task', common.i18n('cp.js.v04'));
    else common.addTab(rcContextPath + '/' + actionName + '/pdfForm?id=' + id + '&flag=task', common.i18n('cp.js.v04'));
}

//金额小写转大写
function DX(num) {
    var strOutput = "";
    var strUnit = "仟佰拾亿仟佰拾万仟佰拾元角分";
    num += "00";
	var flag="正";
	if(num.indexOf("-")==0){
		num=num.substring(1,num.length);
		flag='负';
	}
    var intPos = num.indexOf('.');
    if (intPos >= 0) num = num.substring(0, intPos) + num.substr(intPos + 1, 2);
    strUnit = strUnit.substr(strUnit.length - num.length);
    for (var i = 0; i < num.length; i++) strOutput +="零壹贰叁肆伍陆柒捌玖".substr(num.substr(i, 1), 1) + strUnit.substr(i, 1);
    return (flag=="负"?"负":"")+strOutput.replace(/零角零分$/, '整').replace(/零[仟佰拾]/g, '零').replace(/零{2,}/g, '零').replace(/零([亿|万])/g, '$1').replace(/零+元/, '元').replace(/亿零{0,3}万/, '亿').replace(/^元/, "零元");
};

function relevProcess(id){
    if(id=='' || id==null || id == undefined){
    	id = $("#processDataRelId").val();
    }
	$.ajax({  
		type: "post",
		url: rcContextPath + '/baseFlow/queryRelevProcess?id='+ id,
		success: function (data) {
			$('#relateToFlowDialog').html(data);
			$('#relateToFlowDialog').modal('toggle');
		 }
	});
}

function formatDate(dateStr){
	var date  = dateStr;
	if(dateStr!='' && dateStr!=undefined){
		var dates = dateStr.split("-");
		if(dates.length==3)
			date = dates[0]+'年'+dates[1]+'月'+dates[2]+'日';
	}
	return date;
}

function projectData(objId, key, flag, entityType,formId,taskId) {
	var key2 = key.split("_")[0];
	if(formId==''||formId==undefined )formId = $("#id").val();
	var actId=$("#actId").val();
	if(actId==undefined){
		actId='';
	}else if(actId==''){
		actId='fqrsq_apply';
	}
	var projId = "";
	//协同流程实体ID等于FormID
	if(entityType == 'OA' && objId == ''){
		objId = formId;
	}
	common.addTab(rcContextPath + '/processExtension/processData?entityId=' + objId + '&flag=' + flag + '&mark='+entityType+'&key='+key+"&formId="+formId+"&nodeId="+actId+"&processDataRelId="+$("#processDataRelId").val()+"&projId="+projId, '资料清单');
};

function printForm(id,actionName,key,instId){
	var className= 'lc-title02';
	var pdfFlag = $('#pdfFlag').val();
	var key1 = key;
	if(key!=undefined)key1=key.split('_')[0];
	var twoBarCodeMarginTop = -50;
	var twoBarCodeMarginTop1 = -55;
	var url;
	if(actionName==""||actionName==null)
		url = rcContextPath + top.processPath + '/viewForm?id='+id+"&pdfFlag=2&key="+key;
	else
		url= rcContextPath + '/'+actionName+'/viewForm?id='+id+"&pdfFlag=2&key="+key;
	$.ajax({
		type: "post",  
		url: url,    
		success: function(data) {
			 var formId = $('#id').val();
			 var targetStr = 'class="'+className+'"><div class="twoBarCode" style="margin-top: '+twoBarCodeMarginTop+'px;"><img style="height: 100px;width: 100px;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /></div>';
			 var twoBarCodeNumber = $('#twoBarCodeNumber').val();
			 if(twoBarCodeNumber!=null&&twoBarCodeNumber!='')
				targetStr =  'class="'+className+'"><div class="twoBarCode" style="margin-top: '+twoBarCodeMarginTop1+'px;"><img style="float: left;height: 100px;width: 100px;" src="'+rcContextPath+'/images/temp/'+formId+'.png" /><span class="twoBarCodeNo">'+twoBarCodeNumber+'</span></div>';
			 data = data.replace('class="'+className+'">',targetStr);
			 data = data.replace('class="liucheng tab-pane fade"','class="liucheng"');//表单是多页签时全部显示出来
			 var formAbstractShow=$('#formAbstractShow').val();
			 var formAbstractCopy=$('#formAbstractCopy').html();
			 if(formAbstractShow=='1'){
				data = data.replace('<div class="'+className+'">',formAbstractCopy+'<div class="'+className+'">');
				data = data.replace('<div class="lc-title02-abstract">',formAbstractCopy+'<div class="lc-title02-abstract">');
			}
			 data = data.replace('style="overflow-y:auto"','').replace('style="overflow-y:auto"','');
			 document.body.innerHTML = data;
			window.print();
			common.addTab(rcContextPath + '/'+actionName+'/viewForm?id='+id+'&flag=inst&key='+key, '资料清单'); 
			if (common.getTab1(key+'Pdf'+id) != null){
				common.closeTab(key+'Pdf'+id); 
			}else if (common.getTab1('pdfForm'+id) != null){
				common.closeTab("pdfForm"+id);
			}else if (common.getTab1('process_pending_preview_'+instId) != null){
				common.closeTab("process_pending_preview_"+instId);
			}
		}
	});
}

function getExplorer() {
	var explorer = window.navigator.userAgent ;
	//ie 
	if (explorer.indexOf("MSIE") >= 0) { return "IE"; }
	//firefox 
	else if (explorer.indexOf("Firefox") >= 0) { return "Firefox"; }
	//Chrome
	else if(explorer.indexOf("Chrome") >= 0){ return "Chrome"; }
	//Opera
	else if(explorer.indexOf("Opera") >= 0){ return "Opera"; }
	//Safari
	else if(explorer.indexOf("Safari") >= 0){ return "Safari"; }
}

function viewFormAbstract(flag){
	debugger
	if($('#formAbstractBtn').text()=='查看摘要'){
		if(flag!='view'&&flag!='inst'&&flag!='task'){
			$('#formAbstractTr').show();
			if(getExplorer()=='Firefox' || getExplorer()=='Chrome'){
				 $('#formAbstractTr textarea').addClass('textareaChrome');
			}else{
				 $('#formAbstractTr textarea').addClass('textareaIE');
			}
			$('.topBtns').height('122');
			$('#formContent').css("height", ($('#formContent').height()-80) + "px");
		}else{
			var formAbstractCopy=$('#formAbstractCopy').html();
			$('.lc-title02').before(formAbstractCopy);
			$('.lc-title02-abstract').before(formAbstractCopy);//针对一些表单没有二维码，没有lc-title02标签的流程页面
			$('#formAbstractShow').val('1');
		}
		$('#formAbstractBtn').html("<i class=\"iconfont icon-yincang\"></i>" + '隐藏摘要');
	}else if($('#formAbstractBtn').text()=='隐藏摘要'){
		if(flag!='view'&&flag!='inst'&&flag!='task'){
			$('#formAbstractTr').hide();
			$('.topBtns').height('35');
			if($('#myTab').length==0){
				$('#lcForm').css('margin-top','0px');
			}
			$('#formContent').css("height", ($('#formContent').height()+80) + "px");
		}else{
			$('.lc-title02').prev().remove();
			$('.lc-title02-abstract').prev().remove();
			$('#formAbstractShow').val('0');
		}
		$('#formAbstractBtn').html("<i class=\"iconfont icon-abs\"></i>"+'查看摘要');
	}
}
function setFormAbstract(){
	$('#formAbstract').val($('#formAbstractTop').val());
}

//varType 区分符合分支条件的分支值。因判断逻辑不一致，导致返回的类型不一样
//eg:用户直接返回流程processId。其他流程还需要带其他指标比喻：归口部门等等
function graphView() {
    var actId = $('#actId').val(); 
    var key = $('#processKey').val();
    var objId=$('#objId').val(); 
	var smallClass ="";
	var project ="";
    var instId=$('#instId').val();
    var isStartNode="false";
    if(actId==undefined || actId=='' || actId.indexOf("fqrsq") > -1 || actId.indexOf("_apply") > -1){
    	isStartNode="true";
    }
    var varType="";
    var getSuitableFlowUrl=rcContextPath + "/baseFlow/getSuitableFlow?isStartNode="+isStartNode+"&key=" + key+'&smallClass='+smallClass+'&projectItem='+project;
    $.ajax({
	      type:"post",
  	      url:getSuitableFlowUrl,
  	      dataType : 'json',
  	      data: $("#lcForm").serialize(), 
  	      success:function(res){
  	    	common.alert(res, function() { 
  	    		debugger
  	    		var isOuterFlag = $('#isOuterFlag').val();
    	    	var id = $("#id").val();
    	    	var entityType = $("#entityType").val();
    	    	var relevantOrgId=$("#relevantOrgId").val()||"";
    	    	if(res.data.relevantOrgId!='' && res.data.relevantOrgId!=undefined)
    	    		relevantOrgId=data.relevantOrgId;
    	    	var relevantPosId=$("#relevantPos").val()||"";
   	    	  	if(res.data.relevantPosId!='' && res.data.relevantPosId!=undefined)
   	    			relevantPosId=data.relevantPosId;
    	    	var isDefinition=$("#isDefinition").val()||"";
    	    	var elString=$("#meeting").val()||"";
  	    	  	//用于传输流程跳转条件 发起则直接结束
      		  	var thirdParameter="";
	    		var url= rcContextPath +"/baseFlow/viewFlowInForm?procinstIds="+res.data.procinstIds+"&processKey="+key+"&isStartNode="+isStartNode+'&objId='+objId+'&entityType='+entityType+'&isOuterFlag='+isOuterFlag+'&formId='+id+'&isDefinition='+isDefinition+'&relevantOrgId='+relevantOrgId+"&elString="+elString+"&instId="+instId+thirdParameter;
	    		common.addTab(url, "跟踪流程"); 
  	    	},false);
  	      }
    });	 
}

function viewForm(ids,key,processName,isDefinition){
	common.addTab(rcContextPath+'/'+key+'/viewForm?id='+ids+'&key='+key, processName+"预览PDF"); 
}

function getLastComment(){
	 common.confirm("带入上一环节意见会覆盖掉当前已填写的意见，是否确定带入？",function(){
   	  var taskId = $('#taskId').val();
		$('#lcForm .liucheng table textarea').each(function(){
			var blurFun = "";
			var textDis="";
			var textRead="";
			if($(this).attr('onblur')!=undefined &&$(this).attr('onblur')!=null)
				blurFun = $(this).attr('onblur');
			if($(this).prop('readonly')!=undefined &&$(this).prop('readonly')!=null)
				textRead = $(this).prop('readonly');
			if($(this).prop('disabled')!=undefined &&$(this).prop('disabled')!=null)
				textDis = $(this).prop('disabled');
			if(blurFun!='')
			if(blurFun!=''&&blurFun.indexOf('setCommit')>-1&&!textRead&&!textDis){
				var obj = $(this);
				$.ajax({
			        type: "post",
			        dataType: 'json',
			        url: rcContextPath + '/baseFlow/getLastComment?taskId=' + taskId,
			        success: function(data) {
			        	if(data.msg=='已阅')data.msg='已阅 ';
			        	if(data.msg=='同意')data.msg='同意 ';
			        	var msg = data.msg.replaceAll('<br/>','\n');
			        	obj.val(msg);
			        	$('#comment').val(msg);
			        	obj.txtaAutoHeight();
			        }
			    });
			}
		})
	 });
}

function afterSaveF(key, id, objId, entityType,optType) {
	if(optType=='save'){
		if(id==undefined || id==null || id==''){
			common.closeTab('-flow-'+key+'-apply'); 
		}else{
			common.closeTab(); 
		}
	}else{
		if (entityType == 'PROJECT') {
//	    	common.addTab('editProject' + objId1);
//	    	common.addTab('projectDetail' + objId1);
//			common.addTab(tabUrl + '&id='+data+'&flowFormId=' + data+'&objId='+objId+'&formEntityId='+formEntityId+'&entityType='+entityType, "编辑" + processName,id);
	    }  
		common.closeTab(); 
	}
}