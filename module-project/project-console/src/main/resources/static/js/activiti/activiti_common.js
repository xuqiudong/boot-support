var activitiCommon = {};
$(function (){
	processId = $('#processId').val()||'';
	taskId = $('#taskId').val()||'';
	key = $('#processKey').val()||'';
	key_ = key.split('_')[0];
});

function showStart(key){
	// 表单验证
	var validate = $('#lcForm').data("validator");
	if (validate && !$('#lcForm').valid()) {
		return false;
	}
	// 确定流程分支
	var processKey = key;
	var processKeyHaveBranch=$('#processKeyHaveBranch').val();
	if(!(processKeyHaveBranch=='No')){//无分支判断不用走 getProcessId
		$request.postSync(base_url + '/flow/baseFlow/getProcessId?key=' + key+'&entityId='+objId, 
				{data: $("#lcForm").serialize()}, 
				function(res) {
					var procId = res.data.returnId;
	        		if(procId!=""){
	        			processKey = procId.split(":")[0];
	        			$('#processKey').val(processKey);
	        			$('#processId').val(procId);
	        			$('.processId').val(procId);
	        		}
			});
	}
	
	var id = $("#id").val();
	var entityType = $("#entityType").val();
	var objId = $('#objId').val(); 
	var processId = $("#processId").val();
	var url = base_url + '/process/showSubmit?key=' + processKey +'&processId=' 
		+ processId+'&objId='+objId+'&entityType='+entityType+'&formId='+id;
	common.confirm("url:" + url, function() {
		// 确定提交审批
    	activitiCommon.startConfirm();
    	return false;
	}, {title: '提交审批', closeIcon: true,columnClass: 'col-md-8 col-md-offset-2'});
}

activitiCommon.startConfirm = function(){
	// 表单验证
	var validate = $('#submitForm').data("validator");
	if (validate && !$('#submitForm').valid()) {
		return false;
	}
	// 拼接抄送的参数项
	activitiCommon.setNotifyParameter();
	disableBtn();
	var assignee = $("#assignee").val();
	var id = $("#id").val();
    var objId = $("#objId").val();
    $("#approveDate").val($("#appDate").val());
    // 提交审批
	var url = base_url + "/flow/" + key.split("_")[0] + "/startExecution?processDefinitionId=" + processId + "&assignee=" + assignee;
	$request.postSync(url, 
			{data: $("#lcForm").serialize()}, 
			function(data) {
				common.alert(data, function(){
					submitSuccessConfirm(id, objId);
		    	},true);
		});
}

activitiCommon.setNotifyParameter = function(){
	// 流程抄送环节需要知道表单保存的参数项,拼装表单中所有的pcode和value,提交后台保存
    var notifyParameter = [];
    var $pcodeElems = $("#lcForm select[pcode]");
    $pcodeElems.each(function(){
    	var notifyParam = {};
    	var pcode = $(this).attr("pcode");
    	notifyParam.pcode = pcode;
    	var notifyParamValue = [];
    	$(this).find("option:selected").each(function(){
    		notifyParamValue.push($(this).val());
    	});
    	notifyParam.value = notifyParamValue;
    	notifyParameter.push(notifyParam);
	});
    var notifyParameterStr = JSON.stringify(notifyParameter).replace(/\"/g,"'");
    $("#notifyParameter").val(notifyParameterStr);
}

function showApprove(optMsg, optText, readFlag) {
	// 审批校验
    var taskId = $('#taskId').val();
    var instId = $('#instId').val();
    if (!approveValid(taskId,instId,optMsg)) return false;
    
    // 显示审批弹框
    var objId = $('#objId').val();
    var formId=$('#formId').val();
    var isDefinition = $('#isDefinition').val();
    var url = base_url + '/flow/baseFlow/showApprove?taskId=' + taskId + '&key=' + key + '&optMsg=' + optMsg + '&readFlag=' + readFlag+'&objId='+objId+'&isDefinition='+isDefinition+'&formId='+formId;
    common.confirm("url:" + url, function() {
		// 确定同意
    	activitiCommon.approveConfirm(optMsg, optText);
    	return false;
	}, {title: optText, closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
}

activitiCommon.approveConfirm = function(optMsg, optText){
	// 表单验证
	var validate = $('#approveForm').data("validator");
	if (validate && !$('#approveForm').valid()) {
		return false;
	}
	var taskId = $('#taskId').val();
    if (!approveValid(taskId)){
    	return false;
    }
    var message = '确定' + optText + '吗？';
    // disableBtn();
    // 弹框会导致上一个弹框消失，暂时不弹
    common.confirm(message,
	    function() {
    		$('#cycleNodeList').html("");
    		$('#cycleNodeList').html($('#cycleNodeShow').html());
    		assemblyFlowAssigneeVo('approve');
	        var assignee = $("#assignee").val();
	        var key = $('#processKey').val();
	        $("#approveDate").val($("#appDate").val());
	        var options = {
	        	dataType : 'json',
	            success: function(data) {
	            	common.alert(data, function(){
                    	afterSaveF(key, id, $('#objId').val(),$('entityType').val());
  	            		common.closeModelDialog('approveDialog');
	            	},true);
	                enableBtn();
	            }
	        };
	        $("#lcForm").attr("action", "/flow/" + key.split('_')[0] + "/approve?assignee=" + assignee + "&msg=" + optMsg + "&optType=" + optText);
	        $("#lcForm").ajaxSubmit(options);
	        return false;
	    });
}

function showStop(){
    var taskId = $('#taskId').val();
    var key = $('#processKey').val();
    var result = "";
    if (!approveValid(taskId,'stop')) return false;
    var url = base_url + '/flow/baseFlow/showStop?taskId=' + taskId
    common.confirm("url:" + url, function() {
		// 确定不同意
    	activitiCommon.stopConfirm();
    	return false;
	}, {title: '不同意', closeIcon: true,columnClass: 'col-md-8 col-md-offset-0'});
}

activitiCommon.stopConfirm = function(){
    var id = $('#id').val();
	$request.postSync(base_url + "/flow/"+key+"/stop?id="+id+"&key="+key, 
		{data: $("#stopForm").serialize()}, 
		function(data) {
			common.alert(data, function(){
	        	afterSaveF(key, id, $('#objId').val(),$('entityType').val());
	      		common.closeModelDialog('approveDialog');
	    	},true);
	});
}