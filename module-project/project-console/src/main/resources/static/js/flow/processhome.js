function showEnbale(value, row, index, field){
	var text = value===true ? "启用" :"禁用";
	var labelClass = $nojs.randomLabel.getRandomLableClass(value);
	return '<span class="' + labelClass + '">' + text+'</span>';
}

function showIsSystem(value, row, index, field){
	if(value=='0')
		return '自定义';
	else
		return '系统';
}

function showIsCustomization(value, row, index, field){
	if(value=='1')
		return '是';
	else
		return '否';
}

function showStatus(value, row, index, field){
	var text = '';
	if(value=='1')
		text= '启用';
	else
		text= '禁用';
	var labelClass = $nojs.randomLabel.getRandomLableClass(value);
	return '<span class="' + labelClass + '">' + text+'</span>';
}
function showNewVersion(value, row, index, field){
	if(value=='1')
		return '是';
	else
		return '否';
}

function defaultLink(value, row, index, field) {
	var html =	
		"<a href=\"javascript:nodeListForm.showNotify('"+row.id+"','');\">抄送</a>";

	return html;
}


function testback(rows){
	console.info(rows);
	return true;
}




function currentVersion(){
	debugger
	var current = $("#table").bootstrapTable("getSelections");
	if(current.length != 1){
		common.info('只能选择一条记录。');
		return false;
	}else {
		var key = $(current[0]).prop('key');
		var id = $(current[0]).prop('id');
		$.ajax({
			type:"post",
			url:window.base_url+'/base/actReProcdef/currentVersion?id='+id+'&key='+key,
			dataType :  'json',
			success:function(data){
				$("#table").bootstrapTable('refresh');
			}
		});
	}
}

