var nodeListForm={};
var assigneeListIndex;
var deletedNodeOwnerIds = new Array();
var currNodeOwnerIndex;

/**
 * 设置节点处理人
 */
nodeListForm.setAssignee = function(procDefId, processDefinitonNodId, nodeKey, nodeName){
	debugger;
	var url="/activiti/nodeOwner/setAssigneeForm?procDefId="+procDefId
		+'&processDefinitonNodId='+processDefinitonNodId+'&nodeKey='+nodeKey;
	/*$request.post(url, {dataType: 'text',async: false, data:{'procDefId':procDefId,'processDefinitonNodId':processDefinitonNodId}}, 
		function(result){
		common.confirm(result, saveAssignee, {columnClass: 'col-md-12', title:'节点处理人（'+nodeName+'）'});
	});*/
	common.confirm("url:" + url, function() {
		// 确定
		saveAssignee();
	}, {title: '节点处理人（'+nodeName+'）', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
	assigneeListIndex = $('#assigneeListTable').find('tr').length-1;
}

function saveAssignee(){
	var flag=true;
	for(var i=0;i<assigneeListIndex;i++){
		var pattern = $("select#pattern_"+i).find("option:selected").val();
		if("SELECTED"==pattern){
			// 检查有没有选择部门
		}
	}
	if(!flag){
		$.alert('有直接选定部门未选择部门的行，请确认！');
		return false;
	}
	 
	var len = $("#assigneeListTable tr").length;
	if(len<2){
		$.alert('请添加节点处理人!');
		return false;
	}
	
	$request.postSync(base_url + '/activiti/nodeOwner/saveAssigneeList', 
			{data: $("#assigneeForm").serialize()}, 
			function(data) {
				common.alert(data, function(){
		    	},true);
		});
}

nodeListForm.addAssigneeList = function(){
	var obj = $('#addAssigneeCopy').find('tr').clone();
	var index=assigneeListIndex;
	obj.html(obj.html().replace(/@namevalue@/g, assigneeListIndex++));
	$("#assigneeListTable").append(obj);
	/*$("#posId_"+index).select2({
				allowClear: true,
				placeholder: common.i18n('cp.js.please_select'),
				language: "zh-CN"
			});
	$("#userId_"+index).select2({
		allowClear: true,
		placeholder: common.i18n('cp.js.please_select'),
		language: "zh-CN"
	});*/
};

nodeListForm.delAssigneeList = function (name){
	var checkedlen= $("#assigneeListTable tr").find("input[type=checkbox]:checked").length;
	if(checkedlen < 1){
		common.info('请选择一条记录！');
		return;
	}
	var checkBoxs=$("input[name=" + name + "]:checked");
	common.confirm('确定删除吗？',function(){
		checkBoxs.each(function(i) {
			if($(this).attr('id')) {
				deletedNodeOwnerIds.push($(this).attr('id'));
			}
			$(this).parent().parent().remove();
		});
	});
}

nodeListForm.changePattern = function(index){
	var pattern = $("#addAssignee_pattern_"+index).val();
	if("SELECTED"==pattern){
		// 直接选定部门时，部门可选、处理人可选
		$("#addAssignee_orgBtn_"+index).prop("disabled",false);
		$("#addAssignee_userId_"+index).prop("disabled",false);
	}else {
		// 部门、处理人不可选
		$("#addAssignee_orgBtn_"+index).prop("disabled",true);//部门不可选
		$("#addAssignee_userId_"+index).prop("disabled",true); //处理人不可选
		
		$("#addAssignee_orgId_"+index).val(''); //部门
		$("#addAssignee_orgName_"+index).val('');//部门id
	}
	nodeListForm.loadPositionList(index);
	nodeListForm.loadUserList(index);
}

nodeListForm.addOrganization = function(index){
	currNodeOwnerIndex = index;
	$.ajax({
		type : "get",
		url: '/activiti/nodeOwner/organizationModal', 
		data: { }, 
		success : function(data) {
			common.confirm(data, nodeListForm.confirmOrganization, {columnClass: 'col-md-8 col-md-offset-2', title:'选择部门'});
		}
	});
}

nodeListForm.confirmOrganization = function(){
	var treeObj = $(".ztree").data("ztree");
 	var nodes = treeObj.getCheckedNodes();
 	if (nodes.length != 1) {
        common.confirm("请选择一个节点!");
        return;
 	}
 	var orgId = nodes[0].id;
 	var orgName = nodes[0].name;
 	$('#addAssignee_orgId_'+currNodeOwnerIndex).val(orgId);
 	$('#addAssignee_orgName_'+currNodeOwnerIndex).val(orgName);
 	// 更新岗位下拉
	nodeListForm.loadPositionList(currNodeOwnerIndex);
	nodeListForm.loadUserList(currNodeOwnerIndex);
}

nodeListForm.loadPositionList = function (index){
	var orgId = $('#addAssignee_orgId_' + index).val();
	// 将岗位下拉清空
	$('#addAssignee_positionId_' + index).val('').html('<option value="">请选择</option>');
	$.ajax({
		type: "post", 
		url: '/activiti/nodeOwner/getPositionList', 
		data: {'orgId':orgId}, 
		success: function(data) {
			if(data.code =='0'){
				var positionList = data.data;
				// 更新人员下拉
				for(var i=0; i<positionList.length;i++){
					var position = positionList[i];
					$('#addAssignee_positionId_' + index).append('<option value="' 
						+ position.id + '">' + position.name + '</option>');
				}
			}
		}
	});
}

nodeListForm.loadUserList = function (index){
	var posId = $('#addAssignee_positionId_' + index).val();
	var orgId = $('#addAssignee_orgId_' + index).val();
	// 将人员下拉清空
	$('#addAssignee_userId_' + index).val('').html('<option value="">请选择</option>');
	if(!posId && !orgId){
		return;
	}
	$.ajax({
		type: "post", 
		url: '/activiti/nodeOwner/getUserList', 
		data: {'posId':posId, 'orgId':orgId}, 
		success: function(data) {
			if(data.code =='0'){
				var userList = data.data;
				// 更新人员下拉
				for(var i=0; i<userList.length;i++){
					var user = userList[i];
					$('#addAssignee_userId_' + index).append('<option value="' 
						+ user.id + '">' + user.name + '</option>');
				}
			}
		}
	});
}

/**
 * 抄送
 */
nodeListForm.showNotify = function(procDefId,nodeKey){
	var url="/activiti/nodeOwner/showNotify?procDefId="+procDefId+'&nodeKey='+nodeKey;
	common.confirm("url:" + url, function() {
		// 确定
		nodeListForm.saveNotifyList();
	}, {title: '抄送', closeIcon: true, columnClass: 'col-md-12 col-md-offset-0'});
};

nodeListForm.saveNotifyList = function(){
	// 保存抄送配置
	$request.postSync(base_url + '/activiti/nodeOwner/saveNotifyList', 
			{data: $("#assigneeForm").serialize()}, 
			function(data) {
				common.alert(data, function(){
		    	},true);
		});
}

//选择参数
nodeListForm.addParameter= function(index){
	var url = base_url + "/system/parameter/parameterListModal";
	common.confirm("url:" + url, function() {
		var rows = $("#parameterModalTable").bootstrapTable('getSelections');
		if (rows.length == 0) {
			$.alert("请至少选择一行数据", "提示");
			return false;
		}
		var parameterId = rows[0].id;
	 	var parameterName = rows[0].name;
	 	$('#showNotify_paramId_'+index).val(parameterId);
	 	$('#showNotify_paramName_'+index).val(parameterName);
	 	$("#showNotify_optionIds_"+index).html('<option value="">请选择</option>');
	 	//回显参数项下拉
	 	$request.postSync(base_url + "/system/option/getByPid?pid="+parameterId, '', function(data) {
			console.info(data);
	 		if (!!data.data && data.data.length>0) {
		        for(var i=0;i<data.data.length;i++){
		        	var option = data.data[i];
		        	$("#showNotify_optionIds_"+index).append("<option value='"+option.code+"'>"+option.text+"</option>");
		        }
			}
		});
	}, {title: '参数', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
}