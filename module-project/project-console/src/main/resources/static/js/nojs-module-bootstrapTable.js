/*
 * nojs的关于bootstrap-table插件的 子模块
 */
;
(function ($, $nojs) {
    if (!$ || !$nojs) {
        try {
            console.warn("jQuery or $nojs is not define ");
        } catch (e) {
            console.warn("what happend ");
        }
    }
    var boottabPriority = 95;//这个nojs的默认优先级
    /**
     * 001. 初始化一个bootstrap-table表选项在jQuery.fn.bootstrapTable.defaults中定义.
     */
    $nojs.bootstrapTable = {
        enable: function () {
            return !!$.fn.bootstrapTable;
        },
        selector: {
            table: "table.bootstrapTable", // 需要初始化的table
            reset: " .reset", //重置按钮
            query: " .query" //查询按钮
        },
        priority: boottabPriority,
        //默认一些参数    参见源码中的var DEFAULTS (列的默认参数 参见源码中的COLUMN_DEFAULTS)
        options: {
            idField: 'id', // 每行的唯一标识字段
            undefinedText: '',
            pagination: true, // 是否显示分页
            pageNumber: 1,
            pageSize: 10, // 每页的记录行数
            pageList: [10, 25, 50, 100], // 可供选择的每页的行数
            dataField: 'datas', //数据列表的传入json
            sidePagination: "server", // 分页方式：client客户端分页，server服务端分页
            queryParams: function (params) { // 修改默认的分页参数和后台配合使用
                var temp = {
                    size: params.limit || 10, // 每页数据量
                    page: ((params.offset / params.limit) + 1) || 1,
                    sortColumn: params.sort, // 排序的列名
                    sortOrder: params.order // 排序方式'asc' 'desc'
                };
                return temp;
            },

            responseHandler: function (res) {
                // 后台返回的数据类型同一类型的 BaseResponse,  列表页的数据  :baseResponse.data 此处的data在后台表现为PageInfo
                //如果后台返回的json格式不是{rows:[{...},{...}],total:100},可以在这块处理成这样的格式
                if (res.code != 0) {
                    $.alert({
                        title: "请求失败",
                        content: res.msg,
                        onClose: function () {
                            if (res.code == 401) {
                                top.location.href = project_prefix + "lcxm_login.html";
                            }
                        }
                    });
                    return {
                        rows: [],
                        total: 0
                    }
                }
                return res.data;
            }
        },
        specialHandler: function (opts, $table) { // 对参数做一些其他处理
            var module = this;
            // 新增 查询
            if (opts.toolbar !== undefined) {
                //opts.queryParams = $.extend(true, opts.queryParams, $(opts.toolbar).formJson());
                var queryParams = opts.queryParams;
                opts.queryParams = function (params) {
                    return $.extend(true, {}, queryParams(params), $(opts.toolbar).formJson());
                }
            }
            //补充完整url
            var url = opts.url || opts.listUrl; // data-list-url=""
            var isFragmentUrl = !(/^(http|https|ftp):\/\/.*/.test(opts.url));
            if (isFragmentUrl && url) {
                opts.url = htx + url;
            }
            //携带token
            opts.ajaxOptions = {};
            $request.addToken(opts.ajaxOptions);
            opts.ajaxOptions.complete = $request.complete;
            //console.log(opts);
            module.bindTool(opts, $table);
        },
        bindTool: function (opts, $table) {
            var module = this;
            var toolbar = opts.toolbar;
            if (toolbar !== undefined) {
                //绑定查询按钮
                $(document).on('click', toolbar + module.selector.query, function () {
                    //触发jquery.validate
                    var $form = $(toolbar);
                    var validate = $form.data("validator");
                    if (validate && !$form.valid()) { //如果有验证 但是验证不通过
                        return false;
                    }
                    $table.bootstrapTable('refresh');
                });
                //绑定重置按钮
                $(document).on('click', toolbar + module.selector.reset, function () {
                    $(toolbar + " :input", document).val('');
                    $table.bootstrapTable('refresh');
                });

            }
        },
        init: function (context) {
            var module = this;
            $(module.selector.table, context).each(function () {
                var $table = $(this);
                var opts = $.extend({}, module.options, $table.data());
                module.specialHandler(opts, $table);
                //初始化table
                $table.bootstrapTable(opts);
                //onPostBody 在表体呈现并在DOM中可用之后触发
                $table.on("post-body.bs.table", function (data) {
                    postTableDomLoaded($table);
                })

            });
        }
    };

    /**
     * 002. 初始化一个bootstrap-tablegrid
     * https://www.bootstrap-table.com.cn/doc/extensions/treegrid/
     */
    $nojs.bootstrapTableGrid = {
        enable: function () {
            return !!$.fn.bootstrapTable && !!$.fn.treegrid;
        },
        selector: "table.bootstrapTableGrid", // 需要初始化的table
        priority: boottabPriority,
        options: $.extend(true, {}, $nojs.bootstrapTable.options, {
            treeEnable: true,
            treeShowField: 'name', //在哪一列展开树形
            parentIdField: 'pid',////指定父id列
            pagination: false, // 是否显示分页
        }),
        specialHandler: function (opts, $table) {
            var otherOperation = {
                onCheck: function (row) {
                    return false;
                    var datas = $table.bootstrapTable('getData');
                    // 勾选子类
                    selectChilds(datas, row, "id", "pid", true);
                    // 勾选父类
                    selectParentChecked(datas, row, "id", "pid")
                    // 刷新数据
                    $table.bootstrapTable('load', datas);
                },
                onUncheck: function (row) {
                    return false;
                    var datas = $table.bootstrapTable('getData');
                    selectChilds(datas, row, "id", "pid", false);
                    $table.bootstrapTable('load', datas);
                },
                onPostBody: function (data) { //在表体呈现并在DOM中可用之后触发，参数包含：data: 渲染数据
                    var columns = $table.bootstrapTable('getOptions').columns
                    if (columns && columns[0][1].visible) {
                        $table.treegrid({
                            treeColumn: 1,
                            initialState: 'collapsed', // 所有节点都折叠
                            onChange: function () {
                                $table.bootstrapTable('resetWidth')
                            }
                        });
                        //只展开树形的第一级节点
                        // $table.treegrid('getRootNodes').treegrid('expand');
                    }
                },
                /* 	onResetView: function() {
                        $table.treegrid({
                            initialState: 'collapsed', // 所有节点都折叠
                            // initialState: 'expanded',// 所有节点都展开，默认展开
                            treeColumn: 1,
                            // expanderExpandedClass: 'glyphicon glyphicon-minus',  //图标样式
                            // expanderCollapsedClass: 'glyphicon glyphicon-plus',
                            onChange: function() {
                                $table.bootstrapTable('resetWidth');
                            }
                        });
                        //只展开树形的第一级节点
                        $table.treegrid('getRootNodes').treegrid('expand');
                    }, */
            };
            return $.extend(true, {}, opts, otherOperation);
        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $table = $(this);
                var opts = $.extend({}, module.options, $table.data());
                $nojs.bootstrapTable.specialHandler(opts, $table);
                opts = module.specialHandler(opts, $table);
                //初始化table gridtree
                $table.bootstrapTable(opts);

            });
        }
    };

    /**
     * 003. 对bootstrap-table数据的一些操作
     */
    $nojs.tableActions = {
        enable: function () {
            return !!$.fn.bootstrapTable;
        },
        init: function (context) {
            for (var name in this) {
                var action = this[name];
                if (action && action.init) {
                    action.init(context);
                }
            }
        },
        // 操作选中的bootstrap-table
        operateSelections: {
            selector: 'button.bootable-action-post,a.bootable-action-post',
            //选择器上绑定的data的key
            data: {
                table: 'table', // data-table="" 要操作的目标table
                href: 'href', //data-href="" 要操作的url
                url: 'url',//同data-href,兼容     data-url="" 要操作的url
                confirm: 'confirm', // data-confirm="" 操作前的提示
                colunm: 'column', // 获得被select的行中的列数据 data-column="id", 默认为id, 多个参数英文逗号分隔data-column="id,status"
            },
            init: function (context) {
                var module = this;
                $(context).on("click", module.selector, function () {
                    var $btn = $(this);
                    var url = $btn.data(module.data.href); //对应的url
                    var tableSelector = $btn.data(module.data.table); //需要操作的table
                    var $table = $(tableSelector, context);
                    if (!url || !$table.length) {
                        console.info("未找到对应的url或table")
                        return;
                    }
                    var rows = $table.bootstrapTable('getSelections');
                    if (rows.length == 0) {
                        $.alert("请至少选中一行数据", "提示");
                        return false;
                    }
                    var column = $btn.data(module.data.colunm) || "id";
                    var data;
                    //如果只有一个id，则发送到后台的数据  统一为ids数组
                    if ("id" == column) {
                        data = {
                            ids: rows.map(function (val) {
                                return val[column];
                            })
                        };
                    } else {
                        var paramArray = common.tableSelectValues(rows, column);
                        //发送到后台的数据  统一为数组对象[{k1:"v1", k2:"v2"},{k1:"v3", k2:"v4"}]
                        data = JSON.stringify(paramArray)
                    }

                    var confirm = $btn.data(module.data.confirm);
                    if (confirm) {
                        common.confirm(confirm, function () {
                            request();
                        });
                    } else {
                        request();
                    }

                    function request() {
                        $request.post(url, {data: data}, function (result) {
                            //操作完成刷新表格
                            $table.bootstrapTable('refresh');
                        });
                    }
                })
            }
        }
    };
})(jQuery, $nojs);

/**
 * 在表格渲染之后做的一些通用操作
 */
function postTableDomLoaded($table) {
    var $tbody = $("tbody", $table);

    $nojs.attachment.init($tbody);//附件展示
    $nojs.popPicture.init($tbody);//附件弹出
}

/**
 * 选中父项时，同时选中子项
 * @param datas 所有的数据
 * @param row 当前数据
 * @param id id 字段名
 * @param pid 父id字段名
 */
function selectChilds(datas, row, id, pid, checked) {
    for (var i in datas) {
        if (datas[i][pid] == row[id]) {
            datas[i].check = checked;
            selectChilds(datas, datas[i], id, pid, checked);
        }
        ;
    }
}

function selectParentChecked(datas, row, id, pid) {
    for (var i in datas) {
        if (datas[i][id] == row[pid]) {
            datas[i].check = true;
            selectParentChecked(datas, datas[i], id, pid);
        }
        ;
    }
}

/**
 * 默认的bootstrap-table 行内的操作按钮  | 编辑 | 删除
 */
function defaultOperate(value, row, index, field) {
    let html =
        '<a href="#!" data-id="' + row.id +
        '" class="btn btn-xs btn-default edit-btn add-multitab in-table" data-title="编辑" title="编辑" data-toggle="tooltip"><i class="mdi mdi-tooltip-edit"></i></a>' +
        '<a  href="#!" data-id="' + row.id +
        '" class="btn btn-xs btn-default del-btn in-table" title="删除" data-toggle="tooltip" data-confirm="确认删除本地记录吗?"><i class="mdi mdi-window-close"></i></a>';
    return html;
}


/**
 * 对应 defaultOperate中定义的按钮的默认事件
 */
var defaultOperateEvent = {
    //删除事件
    'click .del-btn': function (event, value, row, index) {
        var $this = $(event.target);
        if (!$this.hasClass('del-btn')) {
            $this = $this.closest('.del-btn');
        }
        var id = $this.data("id");
        var confirm = $this.data("confirm");
        if (confirm) {
            common.confirm(confirm, doRequest);
        } else {
            doRequest();
        }

        function doRequest() {
            var inTable = $this.hasClass("in-table");
            var url = inTable ? $this.closest("table").data("delUrl") : $this.data("href");
            if (!url || !id) return;
            $request.post(url, {
                data: {
                    id: id
                },
                confirm: true
            }, function (result) {
                if (result.code == 0 && inTable) {
                    $this.closest("table").bootstrapTable('refresh');
                }
            });
        }
    }
};

/*把列表的数据变成icon*/
function showIcon(value, row, index, field) {
    return '<i class="' + value + '"></i>';
}

/**
 * 把字典的id替换成字典的值
 */
function showDict(value, row, index, field) {
    var dictNameUrl = "/system/dict/findDictName?id=" + value;
    var dictName = "";
    $request.getSync(dictNameUrl, {}, function (result) {
        dictName = result.data;
    });
    return dictName;
}

function showDictLable(value, row, index, field) {
    var dictName = $cache.getDict(value);
    return '<span class="' + $nojs.randomLabel.getRandomLableClass(value) + '">' + dictName + '</span>';
    ;
}

/**
 * 启用/禁用
 */
function showEnable(value, row, index, field) {
    var text = (value || value === 1) ? "启用" : "禁用";
    var labelClass = $nojs.randomLabel.getRandomLableClass(value);
    return '<span class="' + labelClass + '">' + text + '</span>';
}

/**
 * 展示进度条
 */
function showProgress(value, row, index, field) {
    var html = '<div class="progress progress-striped progress-sm">'
        + '<div class="progress-bar progress-bar-success" style="width: ' + value + '%;">' + value + '</div>'
        + '</div>'
    return html;
}

function showAttachment(value, row, index, field) {
    var html = '<img data-attachment="' + value + '" class="pop" />';
    return html;
}