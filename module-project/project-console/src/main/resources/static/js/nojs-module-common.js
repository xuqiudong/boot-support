/*
 * nojs的关于本项目的通用的一些封装
 */
;
(function ($, $nojs) {
    if (!$ || !$nojs) {
        try {
            console.warn("jQuery or $nojs is not define ");
        } catch (e) {
            console.warn("what happened ");
        }
    }
    /**
     * 001. 在iframe内触发父document的Multitabs方法新建tab
     * https://gitee.com/edwinhuish/multi-tabs/tree/master
     * url: 1-绑定在当前link上面 data-url="url"
     *      2-绑定在父级table上面:hasClass('in-table') data-form-url="url"
     *
     * 20220228： 选在一个选项，并传参到tab 的url上面， 且支持检验参数  ————Vic.xu
     *
     *
     */
    $nojs.addMultitab = {
        enable: function () {
            return !!$.fn.multitabs;
        },
        selector: "a.add-multitab,button.add-multitab",
        // 请求参数 放在按钮中的<i class="action-param" data-key="" data-value="">
        option: {
            param: ".action-param" // 直接绑定在当前url上的参数，

        },
        withTable: {
            table: 'table', // data-table="" 要操作的目标table
            colunms: 'columns', // 获得被select的行中的列数据 data-columns="id,name",
            callBack: 'callback' //data-callback="" 回调函数，参数为当前row
        },

        init: function (context) {
            var module = this;
            $(context).on('click', module.selector, function () {
                var $link = $(this);
                var url = $link.data("url");
                //如果这个按钮在table里面
                var inTable = $link.hasClass('in-table');
                if (inTable) {
                    url = $link.closest("table").data("formUrl") + "?id=" + $link.data("id");
                }
                if (!url && !inTable) {
                    console.info("未找到对应的url或table");
                    return true;
                }

                var title = $link.data("title") || $link.text();
                //url 追加请求参数
                $(module.option.param, $link).each(function (index, element) {
                    var $param = $(element);

                    if (!inTable && index == 0) {
                        url = url + "?" + $param.data("key") + "=" + $param.data("value");
                    } else {
                        url = url + "&" + $param.data("key") + "=" + $param.data("value");
                    }
                });

                //如果是通过table上传参：
                var tableSelector = $link.data(module.withTable.table); //需要操作的table
                var $table = $(tableSelector, context);
                if ($table.size() == 1) {
                    var rows = $table.bootstrapTable('getSelections');
                    if (rows.length == 0) {
                        $.alert("请选中一行数据", "提示");
                        return false;
                    }
                    if (url.indexOf("?") < 0) {
                        url = url + "?1=1";
                    }
                    var columns = $link.data(module.withTable.colunms);
                    if (columns) {
                        for (var column of columns.split(",")) {
                            url = url + "&" + column + "=" + rows[0][column];
                        }
                    }

                    //需要的回调
                    var callBack = $link.data(module.withTable.callBack);
                    if (callBack) {
                        var result = common.callFunction(callBack, rows);
                        if (false === result) {
                            return false;
                        }
                    }

                }

                //在iframe内触发父document的Multitabs方法新建tab
                common.addTab(url, title);

            });
        }
    };

    /**
     * 002. 为按钮绑定异步提交表单事件
     */
    $nojs.ajaxSubmitForm = {
        enable: function () {
            return !!$.fn.ajaxSubmit;
        },
        selector: "form button.action-ajax-form",
        options: {
            dataType: 'json',
            type: 'post',
            success: function (data) {
                common.alert(data, function () {
                    top.common.closeTab();
                });
            },
            error: function (jqXHR, textStatus, errorMsg) { // 出错时默认的处理函数
                $.alert("请求失败:" + jqXHR.status + ":" + errorMsg, "请求失败");
            }
        },
        init: function (context) {
            var module = this;
            $(context).on('click', module.selector, function () {
                var $btn = $(this);
                var $form = $btn.closest("form");
                var url = $form.attr("action");
                if (!url) {
                    console.info("当前form表单没有指定提交路径");
                    return true;
                }
                //
                var beforeFn = $btn.data("before");
                if (beforeFn) {
                    let before = common.callFunction(beforeFn);
                    if (false === before) {
                        console.info("before function return false");
                        return false;
                    }
                }
                let options = module.options;
                options.url = base_url + url;
                $request.addToken(options);
                //触发jquery.validate
                var validate = $form.data("validator");
                if (validate && !$form.valid()) { //如果有验证 但是验证不通过
                    return false;
                }
                //提交成功后的回调
                var afterFn = $btn.data("after");
                if (afterFn) {
                    options.success = function (data) {
                        common.callFunction(afterFn, data);
                    };
                }
                $form.ajaxSubmit(options);
            });
        }
    };

    /**
     *004. form 表单验证
     */
    $nojs.validation = {
        enable: function () {
            return !!$.fn.validate;
        },
        selector: "form.validate",

        _tooltipHandler: function (element, add, error) {//错误提示展示/消除
            var $element = $(element);
            //特殊判断select2
            if ($element.hasClass("select2-hidden-accessible")) {
                $element = $element.next("span.select2-container");
            }

            // 只要进入此方法就先删除tooltip
            if ($element.data("error")) {
                if (null != $element) {
                    $element.tooltip("destroy");
                } else {
                    console.log('element is null when destroy ');
                }
                $element.data("error", false);
            }
            if (add) {// 追加
                $element.tooltip({
                    html: true,
                    title: error.html()

                }).data("error", true);
            }
        },
        _highlightHandler: function (element, add) { // 高亮/取消高亮显示
            var $div = $(element).closest(".validateDiv");
            if ($div.size() == 0) {
                $div = $(element).parent();
            }
            if (add) {
                $div.addClass('has-error');
            } else {
                $div.removeClass('has-error');
            }
        },
        init: function (context) {
            var module = this;
            var $forms = $(this.selector, context);
            var option = { // 验证的一些设置
                ignore: ".novalidate", // 对某些元素不验证
                errorClass: "text-danger", // 默认 "error"。指定错误提示的 css
                errorElement: "label", // 指定使用什么标签标记错误。label
                errorPlacement: function (error, element) { // 跟一个函数，可以自定义错误放到哪里。
                    module._tooltipHandler(element, true, error);
                },
                highlight: function (element) { // 可以给未通过验证的元素加效果、闪烁等。
                    module._highlightHandler(element, true);
                },
                unhighlight: function (element) {
                    module._highlightHandler(element, false);
                    module._tooltipHandler(element, false);
                }
            };

            $forms.each(function () {
                var $form = $(this);
                var opts = $.extend(true, {}, option, $form.data())
                $form.validate(opts);
            });

        }
    };

    /**
     * 005. 一些操作按钮
     */
    $nojs.commonActions = {
        enable: true,
        init: function (context) {
            for (var name in this) {
                var action = this[name];
                if (action && action.init) {
                    action.init(context);
                }
            }
        },
        closeTab: { //关闭当前tab
            selector: '.action-close-tab',
            init: function (context) {
                $(context).on('click', this.selector, function () {
                    // 关闭后激活哪个tab
                    var goTab = $(this).data("goTab")
                    common.closeTab(false, false, goTab);
                });
            }
        }
    };

    /**
     * 006. 加载字典下拉框或者其他下拉框
     * //TODO 前端缓存字典
     */
    $nojs.loadDictSelect = {
        enable: true,
        selector: 'select[data-pcode],select[data-url]',
        $template: $('<option></option>'),
        priority: -1,//在nojs中的执行顺序 越小越先执行
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $select = $(this);
                var url = $select.data("url");
                var pcode = $select.data("pcode");
                if (!url && !pcode) return true;
                url = url || "/system/common/dictSelect"
                $request.post(url, {async: false, data: {pcode: pcode}}, function (result) {
                    $.each(result.data, function (index, opt) {
                        var $opt = module.$template.clone().text(opt.text).val(opt.id);
                        $select.append($opt);
                    });
                })


            });
        }
    };

    /***007. -随机标签的颜色***/

    $nojs.randomLabel = {
        enable: true,
        selector: "span[data-random-label]",
        labelClass: ['label label-primary', 'label label-success', 'label label-warning', 'label label-info', 'label label-danger', 'label label-default'],
        getRandomLableClass: function (lableNumer) {
            var module = this;
            if (isNaN(lableNumer)) {//此处不再做额外的处理
                lableNumer = 0;
            }
            if (lableNumer === true) {
                lableNumer == 1;
            }
            var index = lableNumer % 6;
            return module.labelClass[index];

        },
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $label = $(this);
                var lableNumer = $label.data("randomLabel");
                $label.addClass(module.getRandomLableClass(lableNumer));

            });
        }
    };

})(jQuery, $nojs);
