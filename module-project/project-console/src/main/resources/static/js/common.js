var common = {};

/**
 * 001.关闭某个tab (通过id或者关闭当前) 并刷新上一个tab 默认会刷新
 * @param {Object} id: 即打开的标签上绑定的data-id="id"
 * @param {String} goId : 关闭tab后 激活哪个tab
 * 子页面 使用top.common.closeTab  或者parent.common.closeTab
 */
common.closeTab = function(id, refresh, goId) {
	var $tabs = $(document).data("multitabs") || parent.$(parent.document).data("multitabs");;
	if (!$tabs) return;
	refresh = refresh === false ? false : true;
	var $navTab;
	var $navPanelList = $tabs.$element.navPanelList;
	if (id) {
		//参考源码中的 _exist 方法
		$navTab = $navPanelList.find('a[data-id="' + id + '"]:first');
	} else { //没有传入id则关闭当前激活的tab
		//参见源码中的active方法
		$navTab = $navPanelList.find('li.active:first a');
	}
	if(goId) { //激活某个id
		$navPanelList.find('a[data-id="' + goId + '"]:first').trigger("click");
	}
	$tabs.close($navTab);
	
	_refresh($navTab);
	
	//刷新某个li(激活的) 对应的tab
	function _refresh() {
		if (!refresh) return; // 不刷新则不管
		var $navTabRefresh = $("li.active a", $navPanelList);//获得当前激活的tab
		var iframeId = $navTabRefresh.attr("data-id");
		var param = $navTabRefresh.length ? $tabs._getParam($navTabRefresh) : {};
		var $tempTabPane = $(parent.document.getElementById(iframeId));
		if ($tempTabPane.is('iframe')) {
			$tempTabPane.attr('src', param.url);
		} else {
			$.ajax({
				url: param.url,
				dataType: "html",
				success: function(callback) {
					$tempTabPane.html($navTabRefresh.options.content.ajax.success(callback));
				},
				error: function(callback) {
					$tempTabPane.html($navTabRefresh.options.content.ajax.error(callback));
				}
			});
		}
	}
};
/**
 * 002. 在iframe内触发父document的Multitabs方法新建tab
 */
common.addTab = function(url, title, did) {
	did = did || common.convertUrl(url);
	parent.$(parent.document).data('multitabs').create({
		iframe: true, //指定为iframe模式，当值为false的时候，为智能模式，自动判断（内网用ajax，外网用iframe）。缺省为false。
		title: title, //标题（可选），没有则显示网址
		url: url, //链接（必须），如为外链，强制为info页
		did: did //指定生成的tab的id
	}, true);
}
/**
 * 003. 获取当前url中的全部参数
 */
common.getUrlParams = function() {
	var url = location.search; //获取url中"?"符后的字串
	var params = new Object();
	if (url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for (var i = 0; i < strs.length; i++) {
			params[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
		}
	}
	return params;
}
/**
 * 004. 获取当前url中的指定的参数
 */
common.getUrlParam = function(key) {
	//构造一个含有目标参数的正则表达式对象
	var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)");
	//匹配目标参数
	var r = window.location.search.substr(1).match(reg);
	//返回参数
	if (r != null) {
		return unescape(r[2]);
	} else {
		return null;
	}
}
/**
 * 005. 确认提示的按钮
 */
common.confirm = function(content, successFn, opts) {
	opts = opts || {};
	var isAjax = false;
	if (!$.isFunction(content)){
		isAjax = content.indexOf("url:") > -1;
	}

	var options = {
		title: '确定',
		content: content,
		titleClass: '',
		type: 'green', //'default'' 'blue, green, red, orange, purple & dark'
		theme: 'bootstrap', //对话框的颜色主题，：'white', 'black', 'material' , 'bootstrap'
		buttons: {
			confirm: {
				text: '确定',
				btnClass: 'btn-red',
				action: successFn || $.noop()
			},
			cancle: {
				text: '取消',
				btnClass: 'btn-info',
				action: function() {}
			}
		}
	};
	
	if(isAjax ){
		//如果是ajax请求，且没有定义onContentReady事件则定义一个onContentReady触发nojs
		if(!opts.onContentReady){
			//ajax 请求结束后触发nojs
			options.onContentReady = function(){
				var self = this;
				$nojs(self.$content);
			}
		}
		
	}else {
		//非ajax请求触发nojs
		try{
			var $content = $(content);
			$nojs($content);
		}catch(ex){
			console.warn(ex);
		}
		
	}

	$.confirm($.extend(true,{},  options, opts));
};

/**
 * 006. 是否是一个完整url
 */
common.isWholeUrl = function(url) {
	return url && /^(http|https|ftp):\/\/.*/.test(url);
};

/**
 * 007. 根据方法名调用方法
 */
common.callFunction = function(fnName) {
	try {
		var fn = eval(fnName);
		var args = Array.prototype.slice.call(arguments, 1);
		if (typeof fn == 'function') {
			//fn.call(this, args);
			return fn.apply(null, args);
		}
	} catch (e) {
		$.alert("根据方法名" + fnName + "调用方法错误:" + e);
		console.trace(e);
	}
};

common.getRandomLableClass = function(lableNumer) {
	if(!lableNumer) return;
	var labelClass = ['label label-primary', 'label label-success', 'label label-warning', 'label label-info',
		'label label-danger', 'label label-default'
	];
	if (isNaN(lableNumer)) { //此处不再做额外的处理
		lableNumer = 0;
	}
	if (lableNumer === true) {
		lableNumer == 1;
	}
	if(lableNumer === false) {
		lableNumer == 0;
	}
	var index = lableNumer % 6;
	return labelClass[index];

}
/**
 * 009-根据后台返回的基本对象 弹出提示
 * @param {Object} result: BaseResponse
 */
common.alert = function(result, successFn, confirm){
	if(!result || result.code === undefined) return;
	successFn = successFn || $.noop();
	if(result.code != 0) {
		$.alert({
			title:"请求失败",
			content:result.msg,
			onClose: function(){
				if(result.code == 401){
					top.location.href = htx + "/";
				}
			}
		});
	}else {
		if(confirm){
			$.confirm({
				title: '',
			    content: '操作成功',
			    buttons: {
			        确认: function () {
			           successFn(result);
			        }
			    }
			});
		}else {
			successFn(result);
		}
		
		
	}
};
/**
 * 去掉url中的特殊字符,把url转换成一个唯一标识id
 */
common.convertUrl = function(url){
	if(!!url){
		//deprecate 去掉参数 保证打开一个窗口url = url.substring(0, url.indexOf('?'));
		var u = url.replace(/[$#@&\\/\\?=.:<>\s]/g,'-');
		if(u && u.length > 64){
			//保留前64位2
			u = u.substring(0, 64);
		}
		return u;
	}
	return  null;
}

/**
 * 提取bootstrap table的选中的行的数据
 * rows：选中的行
 * column： 列（多个列逗号分隔）
 */
common.tableSelectValues = function(rows, column){
	if(rows.length == 0 || !column) {
		return [];
	}
	var columns = column.split(",");
	var paramArray = rows.map(function(val) {
		var obj = {};
		for(var c of columns) {
			obj[c] = val[c];
		}
		return obj;
	});
	return paramArray;
}
	
/*
sessionStorage：仅在当前浏览器窗口关闭前有效，自然也就不可能持久保持；
localStorage：始终有效，窗口或浏览器关闭也一直保存，
*/
var storage = {};
storage.set =  function(key, data) {
	sessionStorage.setItem(key, JSON.stringify(data));
};
storage.get =  function(key) {
	var data = sessionStorage.getItem(key);
	if(data){
		return JSON.parse(data)
	}
	return "";
}
storage.remove =  function(key){
	sessionStorage.removeItem(key);
};
storage.clear = function(){
	sessionStorage.clear() ;
};

var $cache = {};
//根据id获取字典的值
$cache.getDict = function(id){
	if(!id) return '';
	var key = "dict-" + id;
	var dictName = storage.get(key);
	if(dictName) return dictName;
	
	var dictNameUrl = "/system/dict/findDictName?id=" + id;
	$request.getSync(dictNameUrl,{},function(result){
		dictName =  result.data;
		storage.set(key, dictName);
	});
	return dictName;
};

common.closeModelDialog = function(dialogId) {
	$('#' + dialogId).modal('hide');
};


/**
 * 收放列表页面查询区域(列表页通用方法)
 * type： true (收起或展开根据页面状态自动判断) 不传值默认为true
 */
 common.slideSearchDiv = function (type) {

	var _queryFormId = ".kj-list-queryFrom-container"; // 搜索条件区域
	var _slideBtnId = ".kj-packUpdiv-icon"; // 收放按钮区域
	var _tableId = ".kj-list-container-only-table"; // 列表区域
	var _kjContainerPadding = 30; // 页面上下固定边距 
	var _listTBodyHeight = 520; // 页面最小高度
	if(type != false){
		type = true;
	}

	if (type) {
		if ($(_slideBtnId).hasClass('kj-icon-fang-by-listpage')) {
			/* 展开查询区域 */
			$(_slideBtnId).removeClass('kj-icon-fang-by-listpage');
			$(_slideBtnId).addClass('kj-icon-shou-by-listpage');
			$(_queryFormId).parent().parent().removeClass("kj-bs-bars-shouqi");
			$(_queryFormId).removeClass('kj-display-none');
		} else {
			/* 收起查询区域 */
			$(_slideBtnId).removeClass('kj-icon-shou-by-listpage');
			$(_slideBtnId).addClass('kj-icon-fang-by-listpage');
			$(_queryFormId).removeClass('kj-display-none');
			$(_queryFormId).parent().parent().addClass("kj-bs-bars-shouqi");
			$(_queryFormId).addClass('kj-display-none');
		}
	}

	var tableHeight = $('body').outerHeight(true) - _kjContainerPadding;
	/* 判断是否低于最低高度，使表格在低分辨率下仍然保持可读性 */
	if (tableHeight < _listTBodyHeight) {
		$(_tableId).bootstrapTable("resetView", {
			height: _listTBodyHeight,
			width:'100%'
		});
	} else {
		$(_tableId).bootstrapTable("resetView", {
			height: tableHeight,
			width:'100%'
		});
	}

};

/**
 * 公用方法容器 
 * 页面初始化完成后调用方法
 * pageType ：list 列表页、form 详情页
 * （没有所需类型请根据需要自行定义类型，有过定义的类型请按照定义使用。）
 *  */
common.pubilcMethodContainer = function (pageType) {
	switch (pageType) {
		case "list":
			// 列表页面
			common.slideSearchDiv(false); // 进入页面先调整列表高度
			break;
		case "form":
			// 根据页面值或者新的pageType类型判断是否是详情，如果是可以调用 common.disableElement 方法禁用页面元素。
			break;
		default:
			break;
	}


	// 窗口大小改变回调
	$(window).resize(function () {
		switch (pageType) {
			case "list":
				// 列表页面
				common.slideSearchDiv(false); // 进入页面先调整列表高度
				break;
			case "form":

				break;
			default:
				break;
		}
	}).resize();

}



/**
 * 替换table下tbody中表单元素动态操作时候的id和name对应的下标
 * 注意：id 和name应符合 id="xxx_数字" name="xxx[数字].name"
 * 入参：common.repalceIndex(tableId) 或者common.repalceIndex(tableId1-dependTab2,tableId2,....)
 */
common.repalceIndexForTable = function() {
	Array.prototype.slice.call(arguments).forEach(function(tableIds) {
		var tableArr = tableIds.split("-"); //表示下标修正值跟在此table后面,比如第一个table有下标到2,则当前table下标从3开始
		var afterTableId = null;
		var afterTableId1 = null;
		var tableId = tableIds;
		if(tableArr.length == 2) {
			afterTableId = tableArr[1];
			tableId = tableArr[0];
		} else if(tableArr.length == 3) {
			afterTableId1 = tableArr[2];
			afterTableId = tableArr[1];
			tableId = tableArr[0];
		}
		$("#" + tableId + " tbody tr").each(function(i) {
			var plu = common.findReplaceTargetIndexPlus(afterTableId) + common.findReplaceTargetIndexPlus(afterTableId1);
			var $tr = $(this);
			$("td :input", $tr).each(function() {
				var $input = $(this);
				var id = $input.attr("id");
				var name = $input.attr("name");
				var dataField = $input.attr("data-bv-field");
				var onclick = $input.attr("onclick");
				var onchange = $input.attr("onchange");
				var onblur = $input.attr("onblur");
				if(id) {
					$input.attr("id", id.replace(/_(\d+)/g, function() {
						return '_' + (i + plu);
					}));
				}
				if(name) {
					$input.attr("name", name.replace(/\[(\d+)\]/g, function() {
						return '[' + (i + plu) + ']';
					}));
				}
				if(dataField) {
					$input.attr("data-bv-field", dataField.replace(/\[(\d+)\]/g, function() {
						return '[' + (i + plu) + ']';
					}));
				}
				if(onclick) { //onclick事件中的参数同步修改
					$input.attr("onclick", onclick.replace(/(\d+)/g, function() {
						return(i + plu);
					}));
				}
				if(onchange) { //onchange事件中的参数同步修改
					$input.attr("onchange", onchange.replace(/(\d+)/g, function() {
						return(i + plu);
					}));
				}
				if(onblur) { //onblur事件中的参数同步修改
					$input.attr("onblur", onblur.replace(/(\d+)/g, function() {
						return(i + plu);
					}));
				}
			});
			$("td>div", $tr).each(function() {
				var $div = $(this); //div下面的id下标值也同步修改
				var id = $div.attr("id");
				if(id) {
					$div.attr("id", id.replace(/_(\d+)/g, function() {
						return '_' + (i + plu);
					}));
				}
				//valdate下面的校验元素同步修改下标值
				$div.find("i").each(function() {
					var icon = $(this).attr("data-bv-icon-for"); //由于日期按钮也用了i标签，所以会有空值出现，过滤
					if(icon != null && icon != undefined) {
						$(this).attr("data-bv-icon-for", icon.replace(/(\d+)/g, function() {
							return(i + plu);
						}));
					}
					// var icon = $(this).attr("data-bv-icon-for");
					// $(this).attr("data-bv-icon-for", icon.replace(/(\d+)/g, function() {
					// 	return(i + plu);
					// }));
				});
				$div.find("small").each(function() {
					var small = $(this).attr("data-bv-for");
					$(this).attr("data-bv-for", small.replace(/(\d+)/g, function() {
						return(i + plu);
					}));
				});
			});
		});
	});
};

/**
 * 获取目标table或者div 里的列表长度
 */
common.findReplaceTargetIndexPlus = function(targetId) {
	var plu = 0;
	if(targetId) {
		var istable = $("#" + targetId).has("tbody").size() > 0;
		if(istable) {
			plu = $("#" + targetId + " tbody tr").size();
		} else {
			plu = $("#" + targetId + " .panel-default").size();
		}
	}
	return plu;
};

/**
 * 替换table下tbody中表单元素动态操作时候的id和name对应的下标
 * 注意：id 和name应符合 id="xxx_数字" name="xxx[数字].name"
 * 入参：common.repalceIndex(tableId) 或者common.repalceIndex(tableId1,tableId2,....)
 */
common.repalceIndexForDiv = function() {
	Array.prototype.slice.call(arguments).forEach(function(divIds) {
		var tableArr = divIds.split("-"); //表示下标修正值跟在此table后面,比如第一个table有下标到2,则当前table下标从3开始
		var afterTableId = null;
		var divId = divIds;
		if(tableArr.length == 2) {
			afterTableId = tableArr[1];
			tableId = tableArr[0];
		}
		$("#" + divId + " .panel-default").each(function(i) {
			var $panelBody = $(this);
			$(".panel-body :input", $panelBody).each(function() {
				var plu = common.findReplaceTargetIndexPlus(afterTableId);
				var $input = $(this);
				var id = $input.attr("id");
				var name = $input.attr("name");
				if(id) {
					$input.attr("id", id.replace(/_(\d+)/g, function() {
						return '_' + (i + plu);
					}));
				}
				if(name) {
					$input.attr("name", name.replace(/\[(\d+)\]/g, function() {
						return '[' + (i + plu) + ']';
					}));
				}
			});
		});
	});
};


/**
 * 禁用页面元素
 * 适用于详情页面等
 */ 
common.disableElement = function () {
	// 输入框
	var inputList = $('input');
	for (let index = 0; index < inputList.length; index++) {
		const element = inputList[index];
		element.setAttribute("readonly", true);
	}

	// 选择框
	var selectList = $('select');
	for (let index = 0; index < selectList.length; index++) {
		const element = selectList[index];
		element.setAttribute("readonly", true);
		element.setAttribute("onfocus", "this.defaultIndex=selectedIndex;");
		element.setAttribute("onchange", "this.selectedIndex=this.defaultIndex;");
	}

	// 多行文本
	var textareaList = $('textarea');
	for (let index = 0; index < textareaList.length; index++) {
		const element = textareaList[index];
		element.setAttribute("readonly", true);
	}

	// 单选
	var radioList = $('radio');
	for (let index = 0; index < radioList.length; index++) {
		const element = radioList[index];
		element.setAttribute("readonly", true);
		element.setAttribute("onfocus", "this.defaultIndex=selectedIndex;");
		element.setAttribute("onchange", "this.selectedIndex=this.defaultIndex;");
	}

	// 多选
	var checkboxList = $('checkbox');
	for (let index = 0; index < checkboxList.length; index++) {
		const element = checkboxList[index];
		element.setAttribute("readonly", true);
		element.setAttribute("onfocus", "this.defaultIndex=selectedIndex;");
		element.setAttribute("onchange", "this.selectedIndex=this.defaultIndex;");
	}

	// 按钮
	var buttonList= $('button');
	for (let index = 0; index < buttonList.length; index++) {
		const element = buttonList[index];
		element.setAttribute("disabled", true);
	}

}