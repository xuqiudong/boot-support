var contractForm={
	init:function(){
		contractForm.bindEvent();
	},
	bindEvent:function () {
		 $("#queryLessee").on("click",contractForm.queryLessee);
		 $("#queryProject").on("click",contractForm.queryProject);
	},
	queryLessee:function () {
		var url = base_url + "/business/customer/customerModal";
    	common.confirm("url:" + url, function() {
    		var rows = $("#customerModalTable").bootstrapTable('getSelections');
    		if(rows.length!=1){
    			$.alert('请选择一条数据！');
    		}
    		$("#lesseeName").val(rows[0].name);
    		$("#fkLesseeCustomerId").val(rows[0].id);
    	}, {title: '我方', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
	},
	queryProject:function () {
		var url = base_url + "/business/project/projectModal";
    	common.confirm("url:" + url, function() {
    		var rows = $("#projectModalTable").bootstrapTable('getSelections');
    		if(rows.length!=1){
    			$.alert('请选择一条数据！');
    		}
    		$("#projectName").val(rows[0].name);
    		$("#fkProjectId").val(rows[0].id);
    	}, {title: '项目', closeIcon: true,columnClass: 'col-md-12 col-md-offset-0'});
	}
};

$(function () {
	contractForm.init();
});