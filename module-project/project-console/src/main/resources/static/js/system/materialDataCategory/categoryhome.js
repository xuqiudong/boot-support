function filter(treeId, parentNode, childNodes) {
        	if (!childNodes) return null;
        	childNodes = childNodes.data;
        	for (let i=0, l=childNodes.length; i<l; i++) {
        		if(childNodes[i].name){
        			childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
        		}
        	}
        	return childNodes;
        }
        
		let categoryTreeSetting = {
			"async": {
					"enable": true,
					"url": "/indusPlanMaterialData/category/getTree",
					"autoParam": [
					              "id",
					              "name=n",
					              "level=lv"
								],
        			"otherParam": {
        				"otherParam": "zTreeAsyncTest"
        			},
        			"dataFilter": filter
        	},
        	"check": {
        		"enable": false,
        		"chkStyle": "checkbox",
        		"chkboxType": {
        			"Y": "ps",
        			"N": "ps"
        		}
        	}
}
        
function onAsyncSuccess(event, treeId, treeNode, msg) {
			debugger;
			let treeObj = $(".ztree").data("ztree");
			let nodes = treeObj.getNodes();
			if (nodes.length > 0) {
				for (let i = 0; i < nodes.length; i++) {
					let isParent = nodes[i].isParent;
					if (isParent) {
						treeObj.expandNode(nodes[i], true, false, false);
					}
				}
			}						
}
		
function addNode() {
        	let treeObj = $(".ztree").data("ztree");
        	let nodes = treeObj.getSelectedNodes();
        	let isError = false;
        	let pid;
        	
        	if (nodes.length == 1) {
        		if (nodes[0].parentId != null || $.trim(nodes[0].parentId).length > 0) {
        			isError = true;
        		}
        	} else {
        		isError = true;
        	}
        	
        	if (isError) {
        		common.confirm("请先选择根节点，再进行添加!", null, {title: "提示"});
    	        return false;
        	}       	
        	pid = nodes[0].id;
        	var url = "url:" + base_url + "/indusPlanMaterialData/category/showEdit?parentId=" + pid;
        	common.confirm(url, function() {
      			if (!$("#categoryForm").valid()) {
      				return false;
      			}
      			
      			var resultFlag = false;
      			$("#categoryForm").ajaxSubmit({
      				async: false,
      				type: 'post',
      				url: base_url + "/indusPlanMaterialData/category/saveOrUpdate",
      				dataType: "json",
      				success: function(data) {
      					if (data.code == 0) {
      						resultFlag = true;
      					}
      					common.alert(data, function() {
      						treeObj.reAsyncChildNodes(nodes[0], "refresh", null);
      					}, true);
      				}
      			});
      			return resultFlag;
        	}, {title: '添加资料类别', closeIcon: true,columnClass: 'col-md-10 col-md-offset-0'});
}
        
function editNode() {
        	let treeObj = $(".ztree").data("ztree");
        	let nodes = treeObj.getSelectedNodes();
        	if (nodes.length != 1) {
        		common.confirm("请选择一个分类进行编辑!", null, {title: "提示"});
		        return;
        	}
        	let id = nodes[0].id;
        	
        	let url = "url:" + base_url + "/indusPlanMaterialData/category/showEdit?id=" + id;
        	common.confirm(url, function() {
        		if (!$("#categoryForm").valid()) {
    				return false;
    			}
    			
    			var resultFlag = false;
    			$("#categoryForm").ajaxSubmit({
    				async: false,
    				type: 'post',
    				url: base_url + "/indusPlanMaterialData/category/saveOrUpdate",
    				dataType: "json",
    				success: function(data) {
    					if (data.code == 0) {
    						resultFlag = true;
    					}
    					common.alert(data, function() {
    						treeObj.reAsyncChildNodes(nodes[0].getParentNode(), "refresh", null);
    					}, true);
    				}
    			});
    			return resultFlag;
        	}, {title: '编辑资料类别', closeIcon: true,columnClass: 'col-md-10 col-md-offset-0'})
        }
        
        function removeNode() {
        	let treeObj = $(".ztree").data("ztree");
        	let nodes = treeObj.getSelectedNodes();
        	if (nodes.length != 1) {
        		common.confirm("请选择一个资料类别!", null, {title: "提示"});
		        return;
        	}
        	
        	common.confirm("确认删除吗？", function() {
	        	$request.post(base_url + "/indusPlanMaterialData/category/delete", {
	        		data: {
	        			id: nodes[0].id
	        		},
	        		confirm: common.confirm
	        	}, function() {
	        		treeObj.reAsyncChildNodes(nodes[0].getParentNode(), "refresh", null);
	        	});
        	}, {title: "确认"});
}
        
function expandAll(treeObj) {
       		if (typeof(treeObj) != "undefined") {
       			let nodes = treeObj.getNodes();
       			if (nodes.length > 0) {
       				for (let i=0; i<nodes.length; i++) {
       					let isParent = nodes[i].isParent;
       					if (isParent) {
       						treeObj.expandNode(nodes[i], true, false, false);
       					}
       				}
       			}
       		}
}
        
function addAll() {
       		$.each($("#userSelectNode option"), function (i, v) {
    	 		if($("#userSelectedNode option[value="+$(this).val()+"]").size()<1)
    	 			$("#userSelectedNode").append("<option title='" + $(this).attr('title') + "' selected='selected' value='" + $(this).val() +  "'>" + $(this).text() + "</option>"); 
    	 	});
}
       	
function addOne() {
       		$.each($("#userSelectNode option:checked"), function (i, v) {
    	 		if($("#userSelectedNode option[value="+$(this).val()+"]").size()<1){
    	 			$("#userSelectedNode").append("<option title='" + $(this).attr('title') + "' selected='selected' value='" + $(this).val() +  "'>" + $(this).text() + "</option>"); 
    	 		}	
    	 	}); 
}
       	
function removeOne() {
       		$("#userSelectedNode option:checked").remove();
}
       	
function removeAll() {
       		$("#userSelectedNode option").remove();
}