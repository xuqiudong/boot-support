//已关联的权限
let
resourceIdArr = new Array();

// 预删除权限ID
let
deleteResourceIds = new Array();

var roleTreeSetting = {
	"async" : {
		"enable" : true,
		"url" : "/system/resource/resourceForm",
		"autoParam" : [ "id", "name=n", "level=lv" ],
		"otherParam" : {
			"otherParam" : "zTreeAsyncTest"
		},
		"dataFilter" : filter
	},
	"check" : {
		"enable" : true,
		"chkStyle" : "checkbox",
		"chkboxType" : {
			"Y" : "s",
			"N" : "ps"
		}
	}
};

function filter(treeId, parentNode, childNodes) {
	if (!childNodes)
		return null;
	childNodes = childNodes.data;
	for (var i = 0, l = childNodes.length; i < l; i++) {
		if (childNodes[i].name) {
			childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		}
	}
	return childNodes;
}

// 勾选/取消勾选
function checked(event, treeId, treeNode) {
	if (treeNode.checked) {
		// 判断id是否在数组之中是否存在, 若不在添加
		if ($.inArray(treeNode.id, resourceIdArr) < 0) {
			resourceIdArr.push(treeNode.id);
		}
		deleteResourceId(treeNode.id);

	} else {
		// 判断id是否在数组中是否存在,若在移除
		if ($.inArray(treeNode.id, resourceIdArr) >= 0) {
			for (var i = 0; i < resourceIdArr.length; i++) {
				if (resourceIdArr[i] == treeNode.id) {
					resourceIdArr.splice(i, 1);
					i--;
				}
			}
		}
		// 保存需要删除的资源Id by limj
		if ($.inArray(treeNode.id, deleteResourceIds) < 0) {
			deleteResourceIds.push(treeNode.id);
		}
	}
}

// 初始化resourceTree
function initTree() {
	$.fn.zTree.init($("#treeDemo"), setting);
}

$(function() {
	let
	idStr = $('#resourceIdArr').val();
	if ($.trim(idStr).length > 0) {
		resourceIdArr = idStr.split(',');
	}
});