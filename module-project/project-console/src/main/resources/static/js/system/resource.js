function filter(treeId, parentNode, childNodes) {
	if (!childNodes) return null;
	childNodes = childNodes.data;
	for (var i=0, l=childNodes.length; i<l; i++) {
		if(childNodes[i].name){
			childNodes[i].name = childNodes[i].name.replace(/\.n/g, '.');
		}
	}
	return childNodes;
}

function addNode() {
	//debugger;
	$.ajax({
		type: 'get',
		url: base_url + '/system/resource/showEdit',
		data:{id:''},
		dataType: 'html',
		success: function(data){
			common.confirm(data, saveNode, {title: '添加权限', closeIcon: true,columnClass: 'col-md-8 col-md-offset-2'});
		}
	});
}

function delNode() {
	
}

function editNode() {
	let treeObj = $(".ztree").data("ztree");
	let nodes = treeObj.getCheckedNodes();
	if(nodes.length != 1){
		$.alert({title: '提示', content: '请选中(非勾选)要编辑的节点!'});
		return false;
	}
	let id = nodes[0].id;
	$.ajax({
		url: base_url + '/system/resource/showEdit',
		data:{id:id},
		dataType: 'html',
		success: function(data){
			common.confirm(data, saveNode, {title: '编辑权限', closeIcon: true,columnClass: 'col-md-8 col-md-offset-2'});
		}
	});
}

function saveNode() {
	let validate = $('#resourceForm').data("validator");
	if (validate && !$('#resourceForm').valid()) { //如果有验证 但是验证不通过
		return false;
	}
	//let treeObj = $.fn.zTree.getZTreeObj("resourceTree");
	//let node = treeObj.getSelectedNodes();
	let resultFlag = true;
	let options = {
		async: false,	
		url: base_url + '/system/resource/saveOrUpdate',
		dataType : 'json',
		success : function(data) {
			if (data.repeat == '1') {
				common.info('相同权限代码已存在，无法保存！');
				resultFlag = false;
			} else {
				
			}
		}
	};
	$("#resourceForm").ajaxSubmit(options);
	
	return resultFlag;
}

function bindEvent() {
	//绑定添加
	$('#addNode').off('click').on('click', addNode);
	//绑定删除
	$('#delNode').off('click').on('click', delNode);
	//绑定编辑
	$('#updateNode').off('click').on('click', editNode);
}

$(function(){
	bindEvent();
});