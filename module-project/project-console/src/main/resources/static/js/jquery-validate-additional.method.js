/**
 * 为jquery.validate  追加一些验证
 */
jQuery.validator.addMethod("accept", function (value, element, param) {
    // Split mime on commas in case we have multiple types we can accept
    var typeParam = typeof param === "string" ? param.replace(/\s/g, '').replace(/,/g, '|') : "image/*",
        optionalValue = this.optional(element),
        i, file;

    // Element is optional
    if (optionalValue) {
        return optionalValue;
    }

    if ($(element).attr("type") === "file") {
        // If we are using a wildcard, make it regex friendly
        typeParam = typeParam.replace(/\*/g, ".*");

        // Check if the element has a FileList before checking each file
        if (element.files && element.files.length) {
            for (i = 0; i < element.files.length; i++) {
                file = element.files[i];
                // Grab the mimetype from the loaded file, verify it matches
                if (!file.type.match(new RegExp(".?(" + typeParam + ")$", "i"))) {
                    return false;
                }
            }
        }
    }

    // Either return true because we've validated each file, or because the
    // browser does not support element.files and the FileList feature
    return true;
}, "Please enter a value with a valid mimetype.");


//Older "accept" file extension method. Old docs: http://docs.jquery.com/Plugins/Validation/Methods/accept
jQuery.validator.addMethod("extension", function (value, element, param) {
    param = typeof param === "string" ? param.replace(/,/g, '|') : "png|jpe?g|gif";
    return this.optional(element) || value.match(new RegExp(".(" + param + ")$", "i"));
}, "Please enter a value with a valid extension.");


// 手机号码验证
jQuery.validator.addMethod("mobile", function (value, element) {
    var length = value.length;
    var mobile = /^(13[0-9]{9})|(18[0-9]{9})|(14[0-9]{9})|(17[0-9]{9})|(15[0-9]{9})$/;
    return this.optional(element) || (length == 11 && mobile.test(value));
}, "请正确填写您的手机号码");

$.validator.addMethod("mobile", function (value, element) {
    return this.optional(element) || /^1[3|4|5|7|8][0-9]\d{8}$/.test(value);
}, "Please specify a valid mobile number");

$.validator.addMethod("longitudeString", function (value, element) {
    return this.optional(element) || /^(([0-9]+\.[0-9]{6}))$/.test(value);
}, "请输入正确的经纬度");

$.validator.addMethod("tencentqq", function (value, element) {
    return this.optional(element) || /^[1-9]\d{4,8}$/.test(value);
}, "Please specify a valid tencent-qq number");
$.validator.addMethod("phone", function (value, element) {
    return this.optional(element) || /^(0[0-9]{2,3}\-)?([2-9][0-9]{6,7})+(\-[0-9]{1,4})?$/.test(value);
}, "Please specify a valid phone number");

/**
 * 正则表达式验证 Vic.xu
 */
jQuery.validator.addMethod("reg", function (value, element, param) {
    var reg = new RegExp(param);
    return reg.test(value);
}, "请按要求输入正确的内容");
/**
 * 判断是否重复(类似原来的remote 但是会携带上当前页面的id属性) Vic.xu
 */
$.validator.addMethod("repeat", function (value, element, param) {
    if (!value) return true;
    var data = {};
    data.id = $("[name=id]").val();
    data.value = value;
    var check = false;
    $request.post(param, {
        data: data,
        async: false
    }, function (result) {
        check = result.data;
    });
    return check;
}, "数据重复");

//根据自定义函数进行校验 方法须返回boolean
$.validator.addMethod("customization", function (value, element, param) {
    //data-rule-customer="customization"
    var methedName = param;
    return common.callFunction(methedName, value, element);
}, "定制化校验失败");

(function ($) {
    $.extend($.validator.messages, {
        required: "不能为空",
        remote: "请修正该字段",
        email: "请输入正确格式的电子邮件",
        url: "请输入合法的网址",
        date: "请输入合法的日期",
        dateISO: "请输入合法的日期 (ISO).",
        number: "请输入合法的数字",
        digits: "只能输入整数",
        creditcard: "请输入合法的信用卡号",
        equalTo: "请再次输入相同的值",
        accept: "请上传合法的文件",
        extension: "请上传合法的文件",
        maxlength: $.validator.format("请输入一个长度最多是 {0} 的字符串"),
        minlength: $.validator.format("请输入一个长度最少是 {0} 的字符串"),
        rangelength: $.validator.format("请输入一个长度介于 {0} 和 {1} 之间的字符串"),
        range: $.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
        max: $.validator.format("请输入一个最大为 {0} 的值"),
        min: $.validator.format("请输入一个最小为 {0} 的值"),
        mobile: "手机号码格式不正确",
        tencentqq: "QQ号格式不正确",
        phone: "电话号码格式不正确"
    });
}(jQuery));
