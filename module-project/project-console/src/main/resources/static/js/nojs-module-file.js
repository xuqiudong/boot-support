;
(function ($, $nojs) {
    if (!$ || !$nojs) {
        try {
            console.warn("jQuery or $nojs is not define ");
        } catch (e) {
            console.warn("what happend ");
        }
    }
    //附件前缀:  完整路径=前缀/id
    var fileprefix = file_prefix;
    $nojs.attachment = { //更换img源
        enable: function () {
            return !!fileprefix;
        },
        selector: "img[data-attachment]",
        init: function (context) {
            var module = this;
            $(module.selector, context).each(function () {
                var $img = $(this);
                var src = $img.attr("data-attachment");
                if (src) {
                    if (/^(http|https|ftp):\/\/.*/.test(src)) {
                        $img.attr("src", src);
                    } else {
                        $img.attr("src", fileprefix + src);
                    }
                }
            });
        }
    };

    /*
       2 弹出图片原图在窗口中间img.pop或者.pop[data-url]
    */
    $nojs.popPicture = {
        enable: true,
        selector: "img.pop,.pop[data-url]",
        $popHtml: $(
            '<div class="pop-outer-div" style="position:fixed;top:0;left:0;background:rgba(0,0,0,0.7);z-index:2;width:100%;height:100%;display:none;"><div class="inner-div" style="position:absolute;"><img style="border:5px solid #fff;" src="" /></div></div>'
        ),
        popEvent: function ($img, src) { // 图片点击事件
            var module = this;
            var $popOuter = $img.data("vic-pop-img");
            if (!$popOuter || $popOuter.size() === 0) {
                $popOuter = module.$popHtml.clone();
                $img.data("vic-pop-img", $popOuter);
                $("body").append($popOuter);
            }
            var $popInner = $(".inner-div", $popOuter);
            var $popImg = $("img", $popOuter);

            $popImg.attr("src", src).load(function () {
                var windowW = $(window).width(); //获取当前窗口宽度
                var windowH = $(window).height(); //获取当前窗口高度
                var realWidth = this.width; //获取图片真实宽度
                var realHeight = this.height; //获取图片真实高度
                var imgWidth, imgHeight;
                var scale = 0.8; //缩放尺寸，当图片真实宽度和高度大于窗口宽度和高度时进行缩放

                if (realHeight > windowH * scale) { //判断图片高度
                    imgHeight = windowH * scale; //如大于窗口高度，图片高度进行缩放
                    imgWidth = imgHeight / realHeight * realWidth; //等比例缩放宽度
                    if (imgWidth > windowW * scale) { //如宽度扔大于窗口宽度
                        imgWidth = windowW * scale; //再对宽度进行缩放
                    }
                } else if (realWidth > windowW * scale) { //如图片高度合适，判断图片宽度
                    imgWidth = windowW * scale; //如大于窗口宽度，图片宽度进行缩放
                    imgHeight = imgWidth / realWidth * realHeight; //等比例缩放高度
                } else { //如果图片真实高度和宽度都符合要求，高宽不变
                    imgWidth = realWidth;
                    imgHeight = realHeight;
                }

                $popImg.css("width", imgWidth); //以最终的宽度对图片缩放

                var w = (windowW - imgWidth) / 2; //计算图片与窗口左边距
                var h = (windowH - imgHeight) / 2; //计算图片与窗口上边距
                $popInner.css({
                    "top": h,
                    "left": w
                }); //设置innerdiv的top和left属性
                $popOuter.fadeIn("fast"); //淡入显示

            });
            $popOuter.click(function () { //再次点击淡出消失弹出层
                $(this).fadeOut("fast");
            });
        },
        init: function (context) {
            var module = this;
            //修改为支持未来元素
            $("body").on("click", module.selector, function () {
                var $img = $(this);
                var src = $img.attr("src") || $img.data("url");
                if (src) {
                    module.popEvent($img, src);
                }
            });
        }

    };
    //单个文件上传
    /*
     * 修改页面的回显
     * add：显示出进度条-label   是否需要验证-->submit
     * done:提示error||success  preview  bind-value hide progress and label
     *
     */
    $nojs.singleFileupload = {
        enable: function () {
            return !!$.fn.fileupload;
        },
        selector: ".fileupload:file:not([multiple])",
        progress: {
            $progress: null,
            $progressBar: null,
            $progressText: null,
            setValue: function (value) {
                this.$progressBar.css("width", value + "%");
                this.$progressText.text(value + "%");
            }
        },
        init: function (context) {
            var module = this;
            $(this.selector, context).each(function (i, fileInput) {
                var $progress = $(
                    '<div class="progress progress-striped"><div class="progress-bar" style="width:0%"><span class="sr-only">0%</span></div></div>'
                );
                var $label = $('<p class="label"></p>');
                var $fileInput = $(fileInput);
                $fileInput.attr("name", "upfile");
                $fileInput.after($progress);
                $fileInput.after($label);
                $progress.hide();
                var progress = {
                    $progress: $progress,
                    $progressBar: $progress.children(),
                    $progressText: $progress.children().children()
                };
                $fileInput.data("nojs.fileupload.progress", $.extend({}, module.progress, progress));
                $fileInput.data("nojs.fileupload.$statusBar", $label);

                var $form = $fileInput.parents("form");

                var $displayId = $("[name=" + $fileInput.data("displayId") + "]", $form);
                $fileInput.data("nojs.fileupload.display.$id", $displayId);
                var id = $displayId.val();
                //回显
                var needPreview = $fileInput.data("preview");
                if (needPreview === true) {
                    // var $preview = $('<img class="preview"/>');
                    var $preview = $('<img class="previewSingle pop"/>');
                    $fileInput.after($preview);

                    $fileInput.data("nojs.fileupload.$preview", $preview);

                    if (id) {
                        $preview.attr("src", fileprefix + id);
                    } else {
                        $preview.hide();
                    }
                }

                $fileInput.fileupload({
                    url: $fileInput.data("url") || (attachment_server + "/attachment/upfile"),
                    replaceFileInput: false,
                    dataType: 'json',
                    formData: null,
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $(this).data("nojs.fileupload.progress").setValue(progress);
                    },
                    add: function (e, data) {
                        data.fileInput.data("nojs.fileupload.$statusBar").text('Uploading...');
                        data.fileInput.data("nojs.fileupload.progress").setValue(0);
                        data.fileInput.data("nojs.fileupload.progress").$progress.show();
                        var validator = data.fileInput.parents("form").data("validator");
                        if (validator) {
                            if (!validator.element(data.fileInput[0])) {
                                return false;
                            }
                        }
                        data.submit();
                    },
                    done: function (e, data) {
                        var result = data.result;
                        if (result.code != 0) {
                            if (result.message) {
                                window.alert("文件上传失败！" + result.msg);
                            } else {
                                window.alert("文件上传出错了！");
                            }
                            return;
                        }
                        data.fileInput.trigger("uploadSuccess", result);
                        var $preview = data.fileInput.data("nojs.fileupload.$preview");
                        if ($preview) {
                            $preview.show();
                            $preview.attr("src", fileprefix + result.data.id);
                        }

                        var $displayId = data.fileInput.data("nojs.fileupload.display.$id");

                        $displayId.val(result.data.id);
                        var validator = $displayId.parents("form").data("validator");
                        if (validator) {
                            validator.element($displayId[0]);
                        }

                        data.fileInput.data("nojs.fileupload.$statusBar").hide();
                        data.fileInput.data("nojs.fileupload.progress").$progress.hide();
                    }
                });
            });
        }
    };
    /**
     * 多文件上传
     */
    $nojs.fileuploadMultiple = {
        enable: function () {
            return !!$.fn.fileupload;
        },
        selector: ".fileupload:file[multiple]",
        options: {
            showProgress: true,
            showLabel: true,
            showPreview: true,
            schema: "string",
            onlyShowPreview: false,
        },
        event: ["addfile.$nojs-fileupload", "removefile.$nojs-fileupload"],
        $template: {
            $progress: $(
                '<div class="progress progress-striped"><div class="progress-bar" style="width:0%"><span class="sr-only">0%</span></div></div>'
            ),
            $label: $('<p class="label"></p>'),
            $preview: $('<div class="clearfix preview"></div>'),
            $previewItem: $(
                '<div class="clearfix preview-item"><button type="button" class="close">&times;</button><img class="img-responsive pop"/></div>'
            ),
            $onlyPreviewItem: $('<div class="clearfix preview-item"><img class="img-responsive amplifier pop"/></div>')

        },
        progress: function ($progress) {
            this.$progress = $progress;
            this.setValue = function (value) {
                this.$progress.show();
                var $progressBar = this.$progress.find(".progress-bar");
                $progressBar.css("width", value + "%");
                $progressBar.children().text(value + "%");
            };
            this.show = function () {
                this.setValue(0);
                this.$progress.show();
            };
            this.hide = function () {
                this.$progress.hide();
            };
        },
        label: function ($label) {
            this.$label = $label;
            this.setText = function (txt) {
                this.$label.text(txt);
                this.$label.show();
            };
            this.show = function () {
                this.$label.show();
            };
            this.hide = function () {
                this.$label.hide();
            };
        },
        preview: function ($preview, options) {
            this.options = options;
            this.$preview = $preview;
            this.addImage = function (id) {
                var preview = this;
                var $previewItem = $nojs.fileuploadMultiple.$template.$previewItem.clone();

                /* 只是单纯的显示**/
                if (options.onlyShowPreview) {
                    $previewItem = $nojs.fileuploadMultiple.$template.$onlyPreviewItem.clone();
                }


                $previewItem.data("value", id);
                $previewItem.attr("id", id);
                var $img = $previewItem.find("img");
                $img.attr("src", this.getPreviewUrl(id));
                $img.attr("title", "attachment-" + id);
                if (preview.options.previewWidth) {
                    $img.css("width", preview.options.width);
                }
                if (preview.options.previewHeight) {
                    $img.css("height", preview.options.height);
                }
                $previewItem.find("button.close").click(function () {
                    preview.options.$element.trigger("removefile", id);
                });
                this.$preview.append($previewItem);
                return $previewItem;
            };
            this.removeImage = function (id) {
                this.$preview.find("#" + id).remove();
            };

            this.getPreviewUrl = function (id) {
                return fileprefix + id;
            };
        },
        schema: {
            string: {
                init: function ($value, preview) {
                    var originValue = $value.val();
                    if (originValue) {
                        var values = originValue.split(",");
                        for (var i = 0; i < values.length; i++) {
                            preview.addImage(values[i]);
                        }
                    }
                },
                addValue: function ($value, value, preview) {
                    var originValue = $value.val();
                    if (originValue) {
                        $value.val(originValue + "," + value);
                    } else {
                        $value.val(value);
                    }
                    if ($value.parents("form").data("validator")) {
                        $value.parents("form").validate().element($value[0]);
                    }
                    preview.addImage(value);
                },
                removeValue: function ($value, value, preview) {
                    var originValue = $value.val();
                    if (originValue) {
                        var pieces = originValue.split(",");
                        var newValue = "";
                        for (var i = 0; i < pieces.length; i++) {
                            var cur = pieces[i];
                            var valueStr = value + "";

                            if (cur === valueStr) {
                                continue;
                            }
                            newValue += (pieces[i] + ",");
                        }
                        if (newValue.length) {
                            newValue = newValue.substr(0, newValue.length - 1);
                        }
                        $value.val(newValue);
                    }
                    preview.removeImage(value);
                }
            }
        },
        init: function (context) {
            var module = this;
            $(this.selector, context).each(function (i, fileInput) {
                var $fileInput = $(fileInput);
                $fileInput.attr("name", "upfile");

                var opt = $.extend({}, module.options, $fileInput.data());
                opt.$element = $fileInput;
                var progress = {
                    setValue: function () {
                    },
                    show: function () {
                    },
                    hide: function () {
                    }
                };
                if (opt.showProgress) {
                    var $progress = module.$template.$progress.clone();
                    $fileInput.after($progress);
                    $progress.hide();
                    progress = new module.progress($progress);
                }
                $fileInput.data("nojs.fileupload.progress", progress);

                var label = {
                    setText: function () {
                    },
                    show: function () {
                    },
                    hide: function () {
                    }
                };
                if (opt.showLabel) {
                    var $label = module.$template.$label.clone();
                    $fileInput.after($label);
                    $label.hide();
                    label = new module.label($label);
                }
                $fileInput.data("nojs.fileupload.label", label);

                var preview = {
                    addImage: function () {
                    },
                    removeImage: function () {
                    }
                };
                if (opt.showPreview) {
                    var $preview = module.$template.$preview.clone();
                    $fileInput.after($preview);
                    preview = new module.preview($preview, opt);
                }
                $fileInput.data("nojs.fileupload.preview", preview);

                var $form = $fileInput.parents("form");
                var $displayId = $("[name=" + $fileInput.data("displayId") + "]", $form)

                var schema = module.schema[opt.schema];
                if (!schema) {
                    console.warn("nojs.fileupload dosn't recognize the schema:" + opt.schema);
                    return;
                }
                schema.init($displayId, preview);
                $fileInput.on("addfile", function (event, value) {
                    schema.addValue($("[name=" + $fileInput.data("displayId") + "]", $form), value, preview);
                });
                $fileInput.on("removefile", function (event, value) {
                    schema.removeValue($("[name=" + $fileInput.data("displayId") + "]", $form), value, preview);
                });

                $fileInput.fileupload({
                    url: $fileInput.data("url") || (base_url + "/attachment/upfile"),
                    replaceFileInput: false,
                    dataType: 'json',
                    sequentialUploads: true,
                    formData: null,
                    progressall: function (e, data) {
                        var progressValue = parseInt(data.loaded / data.total * 100, 10);
                        progress.setValue(progressValue);
                    },
                    add: function (e, data) {
                        var validator = data.fileInput.parents("form").data("validator");
                        if (validator) {
                            if (!validator.element(data.fileInput[0])) {
                                return false;
                            }
                        }
                        label.setText('Uploading...');
                        progress.show();
                        data.submit();
                    },
                    done: function (e, data) {
                        console.info(data);
                        var result = data.result;
                        if (result.code < 0) {
                            if (result.msg) {
                                window.alert("文件上传失败！" + result.msg);
                            } else {
                                window.alert("文件上传出错了！");
                            }
                            return;
                        }
                        data.fileInput.trigger("addfile", result.data.id);

                        label.hide();
                        progress.hide();
                    }
                });
            });
        }
    };

})(jQuery, $nojs);
