/**
 * 固定时间的定时器包
 * <br />
 * 1. 百度一键签到：启动后6s，然后每3小时
 * @see cn.xuqiudong.console.module.timer.schedule.baidu.BaiduSignInTask
 * 2. 博客定时统计: 23点55分统计 归档日志
 * @see cn.xuqiudong.console.module.timer.schedule.blog.BlogStatisticsSchedule
 * 3. 阿里云域名解析ddns: 每个小时判断一次是否需要重新解析域名
 * @see cn.xuqiudong.console.module.timer.schedule.ddns.AliDdnsSchedule
 * 4. 定时备份文件到百度云盘：
 * @see cn.xuqiudong.console.module.timer.schedule.backup.BackupSchedule
 */
package cn.xuqiudong.console.module.timer.schedule;