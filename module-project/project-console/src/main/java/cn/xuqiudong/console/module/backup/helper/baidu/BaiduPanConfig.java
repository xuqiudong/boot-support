package cn.xuqiudong.console.module.backup.helper.baidu;

import cn.xuqiudong.console.base.constant.GlobalFieldParams;
import cn.xuqiudong.console.module.blog.service.GlobalConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-01-31 14:52
 */
@Component
public class BaiduPanConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(BaiduPanConfig.class);

    @Autowired
    private GlobalConfigService globalConfigService;

    private BaiduPanProperties baiduPanProperties;


    @PostConstruct
    private void init(){
        initBaiduPanProperties();
        LOGGER.info("百度云盘相关配置: {}", baiduPanProperties);
    }

    public BaiduPanProperties getBaiduPanProperties(){
        return baiduPanProperties;
    }



    public void initBaiduPanProperties(){
        String accessToken = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduAccessToken).getContent();
        String refreshToken = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduRefreshToken).getContent();
        long expires = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduExpires).getLongContent();
        String appKey  = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduAppKey).getContent();
        String secretKey = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduSecretKey).getContent();
        String signKey = globalConfigService.getByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduSignKey).getContent();
        this.baiduPanProperties = new BaiduPanProperties(accessToken, refreshToken, expires, appKey, secretKey, signKey);
    }

    public void updateBaiduYunConfig(String refreshToken, String accessToken, long expires){
        String expiresStr = String.valueOf(expires * 1000 + System.currentTimeMillis());
        globalConfigService.updateContentByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduRefreshToken, refreshToken);
        globalConfigService.updateContentByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduAccessToken, accessToken);
        globalConfigService.updateContentByCode(GlobalFieldParams.GlobalConfigCodeEnum.baiduExpires, expiresStr);
        this.initBaiduPanProperties();
    }



}
