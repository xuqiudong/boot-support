package cn.xuqiudong.console.module.api;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.console.module.timer.schedule.ddns.handler.AliDdnsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 描述: aliddns
 * @author Vic.xu
 * @since 2022-08-01 11:16
 */
@RestController
@RequestMapping("/aliddns")
public class AliDnsApi {

    @Resource
    private AliDdnsService aliDdnsService;

    /**
     * 手动执行 aliddns
     * @return BaseResponse
     */
    @GetMapping(value = {"", "/"})
    public BaseResponse<?> aliddns(@RequestParam(defaultValue = "") String domain) {
        if (StringUtils.isBlank(domain)) {
            return aliDdnsService.ddns();
        }
        return aliDdnsService.ddns(domain);
    }

}
