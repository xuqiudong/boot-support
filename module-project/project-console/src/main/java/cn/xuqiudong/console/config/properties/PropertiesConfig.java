package cn.xuqiudong.console.config.properties;

import cn.xuqiudong.common.blog.constant.PropertiesParams;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-09-20 14:23
 */
@Configuration
public class PropertiesConfig {

    @Bean
    public PropertiesParams propertiesParams() {
        return new PropertiesParams();
    }
}
