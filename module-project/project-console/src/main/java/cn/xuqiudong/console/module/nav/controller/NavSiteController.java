package cn.xuqiudong.console.module.nav.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.nav.model.NavSite;
import cn.xuqiudong.console.module.nav.service.NavSiteService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :导航站点 控制层
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@RestController
@RequestMapping("/nav/site")
public class NavSiteController extends BaseController<NavSiteService, NavSite>{


}
