package cn.xuqiudong.console.module.timer.schedule.blog;

import cn.xuqiudong.console.module.blog.service.BlogArticleFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 描述:博客归档统计
 * @author Vic.xu
 * @date 2023-12-19 17:23
 */
@Component
public class BlogStatisticsSchedule {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlogStatisticsSchedule.class);

    @Autowired
    private BlogArticleFileService blogArticleFileService;

    /**
     * 23点55分统计 归档日志
     */
    @Scheduled(cron = "0 55 23 * * ?")
    public void entrance() {
        LOGGER.info("博客归档统计开始");
        long start = System.currentTimeMillis();
        blogArticleFileService.statisticsJob();
        long end = System.currentTimeMillis();
        LOGGER.info("博客归档统计结束, 耗时{}ms", (end - start));
    }
}
