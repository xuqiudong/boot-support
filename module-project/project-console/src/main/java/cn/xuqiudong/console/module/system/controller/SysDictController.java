package cn.xuqiudong.console.module.system.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.console.module.system.model.SysDict;
import cn.xuqiudong.console.module.system.service.SysDictService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述:数据字典表 控制层
 * @author Vic.xu
 * @since  2019-01-28 10:01
 */
@RestController
@RequestMapping("/system/dict")
public class SysDictController extends BaseController<SysDictService, SysDict> {

    /**
     * 获取一个节点的子节点
     * @return
     */
    @GetMapping(value = "/children")
    public BaseResponse<?> children(Integer pid) {
        List<SysDict> list = service.list(new SysDict().setPid(pid == null ? Integer.valueOf(0) : pid));
        return BaseResponse.success(list);
    }

    /** 检测code称是否重复 */
    @RequestMapping(value = "/check")
    public BaseResponse<?> checkName(Integer id, String code) {
        boolean isOk = service.checkCode(id, code);
        return BaseResponse.success(isOk);
    }

    /**
     * 根据id获取字典的名称
     */
    @GetMapping(value = "/findDictName")
    public BaseResponse<?> findDictName(Integer id) {
        SysDict entity = service.findById(id);
        return BaseResponse.success(entity == null ? "" : entity.getName());
    }


}
