package cn.xuqiudong.console.module.system.controller;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.base.vo.Select2VO;
import cn.xuqiudong.console.module.system.service.SysCommonService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 说明 :  系统模块的一些通用的请求
 * @author  Vic.xu
 * @since  2019年12月3日 上午11:26:30
 * TODO  走缓存
 */
@RestController
@RequestMapping("/system/common")
public class SysCommonContoller {

    @Resource
    private SysCommonService sysCommonService;

    /**
     * 角色下拉框
     * @return
     */
    @PostMapping(value = "roleSelect")
    public BaseResponse<?> roleSelect() {
        List<Select2VO> data = sysCommonService.roleSelect();
        return BaseResponse.success(data);
    }

    /**
     * 字典下拉框
     * 查看一个编码字典的子项
     */
    @PostMapping(value = "dictSelect")
    public BaseResponse<?> dictSelect(String pcode) {
        List<Select2VO> data = sysCommonService.dictSelect(pcode);
        return BaseResponse.success(data);
    }


}
