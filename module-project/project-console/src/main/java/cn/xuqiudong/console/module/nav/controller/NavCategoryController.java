package cn.xuqiudong.console.module.nav.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.nav.model.NavCategory;
import cn.xuqiudong.console.module.nav.service.NavCategoryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :导航分类 控制层
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@RestController
@RequestMapping("/nav/category")
public class NavCategoryController extends BaseController<NavCategoryService, NavCategory>{

}
