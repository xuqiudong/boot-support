package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.blog.model.GlobalConfig;
import cn.xuqiudong.console.module.blog.service.GlobalConfigService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Vic.xu
 * 描述:全局配置 控制层
 * @since  2020-06-10 14:37
 */
@RestController
@RequestMapping("/blog/globalConfig")
public class GlobalConfigController extends BaseController<GlobalConfigService, GlobalConfig> {

}
