package cn.xuqiudong.console.module.system.service;

import cn.xuqiudong.common.base.vo.Select2VO;
import cn.xuqiudong.console.module.system.mapper.SysCommonMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述:
 * @author  Vic.xu
 * @since  2019年12月3日 上午11:33:06
 */
@Service
public class SysCommonService {

    @Resource
    private SysCommonMapper sysCommonMapper;

    /**
     * 角色下拉框
     * @return
     */
    public List<Select2VO> roleSelect() {
        return sysCommonMapper.roleSelect();
    }

    /**
     * 字典下拉框
     * @return
     */
    public List<Select2VO> dictSelect(String pcode) {
        return sysCommonMapper.dictSelect(pcode);
    }

}
