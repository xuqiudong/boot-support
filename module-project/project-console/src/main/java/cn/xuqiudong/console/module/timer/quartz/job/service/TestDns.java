package cn.xuqiudong.console.module.timer.quartz.job.service;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.console.module.timer.schedule.ddns.handler.AliDnsUtils;
import com.aliyun.alidns20150109.models.DescribeSubDomainRecordsResponseBody;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-28 8:26
 */
public class TestDns {

    private static String accessKeyId = "LTAI5tMpNnGkUaCViw4iVdGV";
    private static String accessKeySecret = "5Jhqm7Wh0sHHTgYXZUcVHPnziOjUyY";

    public static void main(String[] args) throws Exception {
        boolean test = true;

        if (!test) {
            return;
        }

        // 1  测试wiki.xuqiudong.cn   获取当前ip地址
        String ip = "117.64.217.220";
        test("blog.xuqiudong.cn", ip);
        test("m.xuqiudong.cn", ip);
        test("wiki.xuqiudong.cn", ip);
        test("nav.xuqiudong.cn", ip);
        test("nextcloud.xuqiudong.cn", ip);
        test("sso.xuqiudong.cn", ip);
        test("home.xuqiudong.cn", ip);
        test("attachment.xuqiudong.cn", ip);

    }


    public static void test(String domain, String ip) throws Exception {
        DescribeSubDomainRecordsResponseBody.DescribeSubDomainRecordsResponseBodyDomainRecordsRecord record = subDomainRecords(domain);
        JsonUtil.printJson(record);
        if (!ip.equals(record.getValue())) {
            System.out.println("update " + domain + "  ->  " + ip);
            AliDnsUtils.getInstance(accessKeyId, accessKeySecret).update(record, ip);
        }
    }


    public static void describeDomains() throws Exception {
        AliDnsUtils.getInstance(accessKeyId, accessKeySecret).describeDomains();

    }

    public static DescribeSubDomainRecordsResponseBody.DescribeSubDomainRecordsResponseBodyDomainRecordsRecord subDomainRecords(String subDomain) throws Exception {
        DescribeSubDomainRecordsResponseBody.DescribeSubDomainRecordsResponseBodyDomainRecordsRecord
                record = AliDnsUtils.getInstance(accessKeyId, accessKeySecret).subDomainRecords(subDomain);
        return record;

    }
}
