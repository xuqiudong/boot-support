package cn.xuqiudong.console.module.backup.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
/**
 *功能: :备份配置表 Mapper
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
public interface BackupConfigMapper extends BaseMapper<BackupConfig> {
	
}
