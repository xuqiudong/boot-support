package cn.xuqiudong.console.module.system.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.system.model.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统用户表 Mapper
 *
 * @author Vic.xu
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

    /**
     * 判断username是否重复
     * @param id  id
     * @param username username
     * @return 是否重复
     */
    boolean checkUsername(@Param("id") Integer id, @Param("username") String username);

    /**
     *  用户已经拥有的角色
     * @param userId user id
     * @return role list
     */
    List<Integer> userRoleIds(@Param("userId") Integer userId);

    /**
     * 新增用户和角色关系
     * @param userId user id
     * @param needAdd role ids
     */
    void addUserRole(@Param("userId") int userId, @Param("ids") List<Integer> needAdd);

    /**
     * 删除用户和角色关系
     * @param userId user id
     * @param needDel role ids
     */
    void deleteUserRole(@Param("userId") int userId, @Param("ids") List<Integer> needDel);

    /**
     * 根据用户id删除全部和角色关系
     * @param userId user id
     */
    void deleteUserRoleByUserId(@Param("userId") int userId);

    /**
     * 根据用户名获取用户
     * @param username username
     * @return user info
     */
    SysUser findUserByUsername(@Param("username") String username);
}
