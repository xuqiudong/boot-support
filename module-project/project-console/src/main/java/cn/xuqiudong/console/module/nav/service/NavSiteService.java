package cn.xuqiudong.console.module.nav.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.nav.mapper.NavSiteMapper;
import cn.xuqiudong.common.nav.model.NavSite;
import org.springframework.stereotype.Service;

/**
 *功能: :导航站点 Service
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@Service
public class NavSiteService extends BaseService<NavSiteMapper, NavSite>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
