package cn.xuqiudong.console.module.system.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.system.model.SysMenu;
import cn.xuqiudong.console.module.system.model.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 描述:角色表 Mapper
 * @author Vic.xu
 * @since  2019-58-29 08:58
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    /**
     * 全部菜单:选中当前角色拥有的
     * @param roleId 角色id
     * @return menu list for role
     */
    List<SysMenu> roleMenus(@Param("roleId") Integer roleId);


    /**当前角色对应的菜单id集合
     *
     * @param roleId
     * @return 菜单id列表
     */
    List<Integer> roleMenuIds(@Param("roleId") Integer roleId);

    /**
     * 新建角色和菜单关系
     * @param roleId 角色id
     * @param needAdd 菜单id列表
     */
    void addRoleMenu(@Param("roleId") int roleId, @Param("ids") List<Integer> needAdd);

    /**
     * 删除角色和菜单的关系
     * @param roleId role id
     * @param needDelete menu ids
     */
    void deleteRoleMenu(@Param("roleId") int roleId, @Param("ids") List<Integer> needDelete);

    /**
     * 根据角色id删除全部关系
     * @param roleId role id
     */
    void deleteRoleMenuByRoleId(@Param("roleId") int roleId);


    /**
     * 用户有所的角色
     * @param userId user id
     * @return  role list
     */
    List<SysRole> findUserRoles(@Param("userId") Integer userId);

    /**
     * 用户的权限
     * @param userId user id
     * @return all permission
     */
    List<String> findUserPermissions(@Param("userId") Integer userId);

}
