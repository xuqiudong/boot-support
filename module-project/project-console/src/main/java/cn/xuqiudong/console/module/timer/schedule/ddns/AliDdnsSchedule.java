package cn.xuqiudong.console.module.timer.schedule.ddns;

import cn.xuqiudong.console.module.timer.schedule.ddns.handler.AliDdnsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-28 10:17
 */
@Component
public class AliDdnsSchedule {

    @Resource
    private AliDdnsService aliDdnsService;

    @Value("${schedule.ali.ddns: false}")
    private boolean execute;


    /**
     * 每个小时判断一次是否需要重新解析域名
     */
    @Scheduled(cron = "0 0 */1 * * ?")
    public void entrance() {
        if (execute) {
            aliDdnsService.ddns();
        }
    }
}
