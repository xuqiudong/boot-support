package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.blog.model.BlogThirdUser;
import cn.xuqiudong.console.module.blog.service.BlogThirdUserService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述:第三方用户 控制层
 * @author Vic.xu
 * @since  2020-07-21 17:33
 */
@RestController
@RequestMapping("/blog/blogThirdUser")
public class BlogThirdUserController extends BaseController<BlogThirdUserService, BlogThirdUser> {

}
