package cn.xuqiudong.console.base.constant;

/**
 * @author Vic.xu
 * 说明 :  全局的一些参数项配置
 * @since  2020/5/26 0026 10:02
 */
public final class GlobalFieldParams {

    /**
     * 时间轴归档 页size
     */
    public static Integer TIMELINE_SIZE = 100;


    /**
     * @see cn.xuqiudong.common.blog.model.GlobalConfig
     */
    public enum GlobalConfigCodeEnum {
        /**关于我*/
        about("about"),
        /**友情链接：百度=www.baidu.com;谷歌=www.google.com*/
        links("links"),
        /** 百度云盘 AccessToken*/
        baiduAccessToken("third.baidu.access_token"),

        /** 百度云盘 refresh token*/
        baiduRefreshToken("third.baidu.refresh_token"),

        /** 百度云盘过期时间 毫秒 long*/
        baiduExpires("third.baidu.expires"),
        baiduAppKey("third.baidu.appkey"),
        baiduSecretKey("third.baidu.SecretKey"),
        baiduSignKey("third.baidu.SignKey"),
        ;
        private String code;

        GlobalConfigCodeEnum(String code) {
            this.code = code;
        }


        public String getCode() {
            return code;
        }
    }


}
