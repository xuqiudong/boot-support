package cn.xuqiudong.console.module.system.mapper;

import cn.xuqiudong.common.base.vo.Select2VO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 描述:系统模块的一些通用 Mapper
 * @author Vic.xu
 * @since  2019-01-28 10:01
 */
public interface SysCommonMapper {

    /**
     * 全部的角色
     * @return 角色列表
     */
    List<Select2VO> roleSelect();

    /**
     * 查询一个字典编码的子项
     * @param pcode pcode
     * @return  下拉列表
     */
    List<Select2VO> dictSelect(@Param("pcode") String pcode);


}
