package cn.xuqiudong.console.module.timer.schedule.baidu;

import cn.xuqiudong.console.module.timer.schedule.baidu.service.TiebaSignInService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 百度贴吧自动签到
 * @author Vic.xu
 * @since  2021/7/12 0012 8:30
 */
@Service
public class BaiduSignInTask {

    @Value("${schedule.baidu.sign: false}")
    private boolean execute;

    @Resource
    private TiebaSignInService tiebaSignInService;

    /**
     * 3小时 /
     */
    public static final long FIXED_DELAY = 3 * 60 * 60 * 1000;

    /** 定时百度贴吧一键签到 */

    @Scheduled(initialDelay = 6000, fixedDelay = FIXED_DELAY)
    public void signIn() {
        if (execute) {
            tiebaSignInService.signIn();
        }

    }
}
