package cn.xuqiudong.console.module.backup.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import cn.xuqiudong.common.util.CnToSpellUtils;
import cn.xuqiudong.console.module.backup.constant.BackupCategory;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.File;
import java.util.Date;


/**
 * 备份配置表 实体类
 *
 * @author Vic.xu
 */
public class BackupConfig extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private BackupCategory category;

    /**
     * 是否需要转移原文件
     */
    private Boolean needTransfer;

    /**
     * 转移到的位置
     */
    private String transferDir;

    /**
     * 需要备份的位置
     */
    private String dirPath;

    /**
     * 最后备份时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date lastBackupTime;


    /***************** set|get  start **************************************/
    /**
     * set：名称
     */
    public BackupConfig setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：类型
     */
    public BackupConfig setCategory(BackupCategory category) {
        this.category = category;
        return this;
    }

    /**
     * get：类型
     */
    public BackupCategory getCategory() {
        return category;
    }

    /**
     * set：是否需要转移原文件
     */
    public BackupConfig setNeedTransfer(Boolean needTransfer) {
        this.needTransfer = needTransfer;
        return this;
    }

    /**
     * get：是否需要转移原文件
     */
    public Boolean getNeedTransfer() {
        return needTransfer;
    }

    /**
     * set：转移到的位置
     */
    public BackupConfig setTransferDir(String transferDir) {
        this.transferDir = transferDir;
        return this;
    }

    /**
     * get：转移到的位置
     */
    public String getTransferDir() {
        return transferDir;
    }

    /**
     * set：需要备份的位置
     */
    public BackupConfig setDirPath(String dirPath) {
        this.dirPath = dirPath;
        return this;
    }

    /**
     * get：需要备份的位置
     */
    public String getDirPath() {
        return dirPath;
    }

    /**
     * set：最后备份时间
     */
    public BackupConfig setLastBackupTime(Date lastBackupTime) {
        this.lastBackupTime = lastBackupTime;
        return this;
    }

    /**
     * get：最后备份时间
     */
    public Date getLastBackupTime() {
        return lastBackupTime;
    }

    public String getLastBackupTimeStr() {
        return lastBackupTime == null ? "开始" : DateFormatUtils.format(lastBackupTime, "yyyy-MM-dd HH:mm:ss");
    }
    /***************** set|get  end **************************************/

    /**
     * 存储的前缀：  分类/  名称(拼音)/
     */
    private String storePrefix;

    public String obtainStorePrefix() {
        if (storePrefix == null) {
            storePrefix = category.name() + File.separator + CnToSpellUtils.getFullSpell(name) + File.separator;
        }
        return storePrefix;
    }


}
