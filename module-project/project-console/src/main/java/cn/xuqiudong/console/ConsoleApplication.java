package cn.xuqiudong.console;

import org.apache.commons.lang3.StringUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StopWatch;

import javax.sql.DataSource;
import java.util.Map;
import java.util.stream.Stream;

/**
 * 描述:后台入口启动类
 * @author Vic.xu
 * @since 2022-06-21 10:02
 */
@SpringBootApplication(scanBasePackages = {"cn.xuqiudong.console", "cn.xuqiudong.common.blog"})
@MapperScan(value = {"cn.xuqiudong.console.**.mapper", "cn.xuqiudong.common.blog.**.mapper"})
public class ConsoleApplication {

    private static final Logger logger = LoggerFactory.getLogger(ConsoleApplication.class);

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ConfigurableApplicationContext cax = SpringApplication.run(ConsoleApplication.class, args);
        stopWatch.stop();
        ServerProperties serverProperties = cax.getBean(ServerProperties.class);
        Integer port = serverProperties.getPort();
        ServerProperties.Servlet servlet = serverProperties.getServlet();
        String contextPath = servlet.getContextPath();
        String urlSuffix = StringUtils.isBlank(contextPath) ? String.valueOf(port) : port + contextPath;
        logger.info("blog console 启动完成, 耗时{}ms, 请访问: http://127.0.0.1:{}", stopWatch.getTotalTimeMillis(), urlSuffix);
        promptIfInDevelopmentEnvironment(cax, urlSuffix);

    }

    /**
     * 如果是开发环境
     * @param cax ConfigurableApplicationContext
     */
    private static void promptIfInDevelopmentEnvironment(ConfigurableApplicationContext cax, String urlSuffix) {
        String[] activeProfiles = cax.getEnvironment().getActiveProfiles();
        logger.info("active profiles: {}",  StringUtils.join(activeProfiles, ", "));
        boolean isDevelopment = Stream.of(activeProfiles).anyMatch(p->"dev".equals(p));
        if (isDevelopment) {
            logger.info("代码生成访问地址： http://127.0.0.1:{}/generator", urlSuffix);
            Map<String, DataSource> databaseMap = cax.getBeansOfType(DataSource.class);
            logger.info("databaseMap size : {}", databaseMap.size());
            for (Map.Entry<String, DataSource> entry : databaseMap.entrySet()) {
                logger.info("Database:   {} = {}", entry.getKey(), entry.getValue());
            }
        }
    }

}
