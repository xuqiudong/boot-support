DROP TABLE IF EXISTS `baidu_cookie`;

CREATE TABLE `baidu_cookie`
(
    `id`       INT NOT NULL AUTO_INCREMENT,
    `name`     VARCHAR(64) DEFAULT NULL,
    `cookie`   TEXT,
    `sign_tbs` VARCHAR(64) DEFAULT NULL COMMENT '一键签到的tabs参数',
    PRIMARY KEY (`id`)
) ENGINE = INNODB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4 COMMENT ='百度cookie表';