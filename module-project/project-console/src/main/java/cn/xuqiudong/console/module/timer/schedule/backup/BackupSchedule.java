package cn.xuqiudong.console.module.timer.schedule.backup;

import cn.xuqiudong.console.module.backup.handler.BackupHandler;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
import cn.xuqiudong.console.module.backup.service.BackupConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述: 备份的定时任务
 * @author Vic.xu
 * @since 2024-03-18 17:20
 */
@Component
public class BackupSchedule {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackupSchedule.class);


    @Resource
    private BackupConfigService backupConfigService;

    @Resource
    private BackupHandler backupHandler;

    /**
     * 根据配置定时备份文件到百度云盘 零点5分
     */
    @Scheduled(cron = "0 5 0 * * ?")
    public void backup() {

        BackupConfig lookup = new BackupConfig();
        lookup.setEnable(true);
        List<BackupConfig> list = backupConfigService.list(lookup);
        LOGGER.info("当前需要上传到百度云盘的配置项数目为:{}", list.size());
        StopWatch  stopwatch = new StopWatch("backup");;
        for (BackupConfig config : list) {
            stopwatch.start();
            LOGGER.info( "开始备份配置项:{}", config );
            backupHandler.handle(config);
            stopwatch.stop();
            LOGGER.info( "备份配置项: {} 完成,耗时:{}s", config.getName(), stopwatch.getTotalTimeSeconds() );
        }
        LOGGER.info("备份任务完成，总耗时{}s", stopwatch.getTotalTimeSeconds());
    }


}
