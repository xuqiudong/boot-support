package cn.xuqiudong.console.module.timer.schedule.ddns.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述:和主域名通讯
 * @author Vic.xu
 * @since 2022-12-15 9:34
 */
public final class CommunicationWithMainDomain {

    private static final Logger logger = LoggerFactory.getLogger(CommunicationWithMainDomain.class);
    /**
     *restart api url
     */
    private static final String RESTART_NGINX_URL = "http://xuqiudong.cn:81/nginx/restart";

    /**
     * 调用主域名所在服务器的接口，重启nginx
     */
    public static void restartNginx() {
        try {
            HttpClient client = HttpClients.createDefault();
            HttpGet get = new HttpGet(RESTART_NGINX_URL);
            HttpResponse response = client.execute(get);
            String result = response.getEntity().toString();
            int code = response.getStatusLine().getStatusCode();
            logger.info("调用主机接口，重启nginx: code={}， result={}", code, result);
        } catch (Exception e) {
            logger.error("调用主机接口，重启nginx出错", e);
        }
    }
}
