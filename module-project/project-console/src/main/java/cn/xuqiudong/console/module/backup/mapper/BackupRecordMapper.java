package cn.xuqiudong.console.module.backup.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.backup.model.BackupRecord;
/**
 *功能: :备份记录表 Mapper
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
public interface BackupRecordMapper extends BaseMapper<BackupRecord> {
	
}
