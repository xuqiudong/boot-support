package cn.xuqiudong.console.module.backup.model;


import cn.xuqiudong.common.base.model.BaseEntity;
import cn.xuqiudong.console.module.backup.constant.BackupCategory;

import java.io.File;
import java.nio.file.Path;


/**
 * 备份记录表 实体类
 *
 * @author Vic.xu
 */
public class BackupRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 对应的配置
     */
    private Integer configId;

    /**
     * 类型
     */
    private BackupCategory category;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 文件路径
     */
    private String filePath;

    /**
     * 转移后的路径
     */
    private String transferPath;

    /**
     * 文件大小
     */
    private Long fileSize;

    /**
     * 上传到百度盘位置
     */
    private String baiduPath;

    /**
     * 上传结果
     */
    private String result;

    /**
     * 是否成功
     */
    private Boolean successful;

    /**
     * 配置名称
     */
    private String configName;

    /**
     * 是否需要转义原文件
     */
    private Boolean needTransfer;

    public BackupRecord() {
    }

    public BackupRecord(BackupConfig config, Path path) {
        this.configId = config.getId();
        this.configName = config.getName();
        this.needTransfer = config.getNeedTransfer();
        File file = path.toFile();
        this.filename = file.getName();
        this.filePath = file.getAbsolutePath();
        this.fileSize = file.length();
        this.category = config.getCategory();
    }


    /***************** set|get  start **************************************/
    /**
     * set：对应的配置
     */
    public BackupRecord setConfigId(Integer configId) {
        this.configId = configId;
        return this;
    }

    /**
     * get：对应的配置
     */
    public Integer getConfigId() {
        return configId;
    }

    /**
     * set：类型
     */
    public BackupRecord setCategory(BackupCategory category) {
        this.category = category;
        return this;
    }

    /**
     * get：类型
     */
    public BackupCategory getCategory() {
        return category;
    }

    /**
     * set：文件名
     */
    public BackupRecord setFilename(String filename) {
        this.filename = filename;
        return this;
    }

    /**
     * get：文件名
     */
    public String getFilename() {
        return filename;
    }

    /**
     * set：文件路径
     */
    public BackupRecord setFilePath(String filePath) {
        this.filePath = filePath;
        return this;
    }

    /**
     * get：文件路径
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * set：转移后的路径
     */
    public BackupRecord setTransferPath(String transferPath) {
        this.transferPath = transferPath;
        return this;
    }

    /**
     * get：转移后的路径
     */
    public String getTransferPath() {
        return transferPath;
    }

    /**
     * set：文件大小
     */
    public BackupRecord setFileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    /**
     * get：文件大小
     */
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * set：上传到百度盘位置
     */
    public BackupRecord setBaiduPath(String baiduPath) {
        this.baiduPath = baiduPath;
        return this;
    }

    /**
     * get：上传到百度盘位置
     */
    public String getBaiduPath() {
        return baiduPath;
    }

    /**
     * set：上传结果
     */
    public BackupRecord setResult(String result) {
        this.result = result;
        return this;
    }

    /**
     * get：上传结果
     */
    public String getResult() {
        return result;
    }

    /**
     * set：是否成功
     */
    public BackupRecord setSuccessful(Boolean successful) {
        this.successful = successful;
        return this;
    }

    /**
     * get：是否成功
     */
    public Boolean getSuccessful() {
        return successful;
    }

    /**
     * set：配置名称
     */
    public BackupRecord setConfigName(String configName) {
        this.configName = configName;
        return this;
    }

    /**
     * get：配置名称
     */
    public String getConfigName() {
        return configName;
    }

    /**
     * set：是否需要转义原文件
     */
    public BackupRecord setNeedTransfer(Boolean needTransfer) {
        this.needTransfer = needTransfer;
        return this;
    }

    /**
     * get：是否需要转义原文件
     */
    public Boolean getNeedTransfer() {
        return needTransfer;
    }
    /***************** set|get  end **************************************/
}
