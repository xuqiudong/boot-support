package cn.xuqiudong.console.module.backup.constant;

/**
 * 描述: 备份类型
 * @author Vic.xu
 * @since 2024-02-05 17:26
 */
public enum BackupCategory {
    /**
     * mysql 数据库/
     */
    mysql,
   /** 一些脚本文件*/
   shell,

   /**一些配置*/
   config,
    /**文件夹*/
    folder
    ;
}
