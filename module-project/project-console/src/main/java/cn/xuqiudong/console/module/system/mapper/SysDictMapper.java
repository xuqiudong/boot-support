package cn.xuqiudong.console.module.system.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.system.model.SysDict;
import org.apache.ibatis.annotations.Param;

/**
 * 描述:数据字典表 Mapper
 * @author Vic.xu
 * @since  2019-01-28 10:01
 */
public interface SysDictMapper extends BaseMapper<SysDict> {


    /**
     * 判断code是否重复
     * @param id id 如果存在的换
     * @param code code
     * @return 是否重复
     */
    boolean checkCode(@Param("id") Integer id, @Param("code") String code);

    /**
     * 根据id 删除自身以及子节点
     * @param id id
     * @return number
     */
    int deleteSelfAndSub(@Param("id") int id);

}
