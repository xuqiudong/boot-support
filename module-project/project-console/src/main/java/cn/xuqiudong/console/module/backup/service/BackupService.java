package cn.xuqiudong.console.module.backup.service;

import cn.xuqiudong.console.module.backup.helper.baidu.BaiduPanHelper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 描述: 备份处理类
 * @author Vic.xu
 * @since 2024-02-05 17:25
 */
@Service
public class BackupService {

    @Resource
    private BaiduPanHelper baiduPanHelper;
}
