package cn.xuqiudong.console.module.blog.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogTagMapper;
import cn.xuqiudong.common.blog.model.BlogTag;
import org.springframework.stereotype.Service;

/**
 * 描述:标签表 Service
 * @author Vic.xu
 * @since  2020-05-08 09:38
 */
@Service
public class BlogTagService extends BaseService<BlogTagMapper, BlogTag> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
