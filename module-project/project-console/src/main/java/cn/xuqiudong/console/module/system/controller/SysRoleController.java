package cn.xuqiudong.console.module.system.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.console.module.system.model.SysMenu;
import cn.xuqiudong.console.module.system.model.SysRole;
import cn.xuqiudong.console.module.system.service.SysRoleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 描述:角色表 控制层
 * @author Vic.xu
 * @since  2019-58-29 08:58
 */
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController<SysRoleService, SysRole> {

    /**
     * 角色对应的菜单关系:所有菜单,角色拥有的权限则选中
     * @return
     */
    @GetMapping(value = "/roleMenus")
    public BaseResponse<?> roleMenus(Integer id) {
        List<SysMenu> roleMenus = service.roleMenus(id);
        return BaseResponse.success(roleMenus);
    }

    /**
     * 保存
     * @param entity
     * @return
     */
    @PostMapping(value = "/save", params = {"menuIds"})
    public BaseResponse<?> save(SysRole entity, String menuIds) {
        service.save(entity, menuIds);
        return BaseResponse.success(entity);
    }

    /**
     * 保存
     * @param entity
     * @return
     */
    @Override
    @PostMapping(value = "/save")
    public BaseResponse<?> save(SysRole entity) {
        service.save(entity);
        return BaseResponse.success(entity);
    }

}
