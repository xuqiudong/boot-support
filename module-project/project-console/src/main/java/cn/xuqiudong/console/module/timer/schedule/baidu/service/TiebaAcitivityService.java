package cn.xuqiudong.console.module.timer.schedule.baidu.service;

import cn.xuqiudong.common.base.craw.CrawlConnect;
import cn.xuqiudong.common.util.RegexUtil;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 *功能: : 百度贴吧活动
 * @author Vic.xu
 * @since  2021/7/21 0021 8:13
 */
@Service
public class TiebaAcitivityService extends BaseBaiduService {

    /**
     * 航海王 强者之路帖子列表url
     */
    static String url =
            "https://tieba.baidu.com/f?kw=%E8%88%AA%E6%B5%B7%E7%8E%8B%E5%BC%BA%E8%80%85%E4%B9%8B%E8%B7%AF&ie=utf-8&tab=good";

    static String keyworlds = "【活动现场】强吧防暑小贴士活动";
    /**
     <a rel="noreferrer" href="/p/7435368062" title="【活动现场】强吧欢庆活动" target="_blank" class="j_th_tit ">【活动现场】强吧欢庆活动</a>
     */
    static String reg =
            "<a rel=\"noreferrer\" href=\"(.*?)\" title=\"" + keyworlds + "\" target=\"_blank\" class=\"j_th_tit \">";

    public static void list() throws IOException {
        CrawlConnect conn = con(url, null);
        Document document = conn.getDocument();
        Elements list = document.select("#thread_list");
        System.out.println(list.first().html());
        boolean flag = 1 < 2;
        if (flag) {
            return;
        }
        String html = conn.getHtml();
        System.out.println(reg);

        String href = RegexUtil.getFirstString(html, reg, 1);
        System.out.println(html);
        System.out.println("start");
        System.out.println(href);
        System.out.println("end");
    }

    public static void main(String[] args) throws IOException {
        list();
    }

}
