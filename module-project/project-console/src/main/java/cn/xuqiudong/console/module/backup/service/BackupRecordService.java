package cn.xuqiudong.console.module.backup.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.console.module.backup.mapper.BackupRecordMapper;
import cn.xuqiudong.console.module.backup.model.BackupRecord;
import org.springframework.stereotype.Service;

/**
 *功能: :备份记录表 Service
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
@Service
public class BackupRecordService extends BaseService<BackupRecordMapper, BackupRecord>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
