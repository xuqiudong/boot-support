package cn.xuqiudong.console.module.backup.helper.baidu.model;

import org.apache.commons.beanutils.BeanUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 描述: 获取文件列表的查询参数
 * @author Vic.xu
 * @since 2024-02-01 9:10
 */
public class FileListQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 需要list的目录，以/开头的绝对路径, 默认为/
     * 路径包含中文时需要UrlEncode编码
     * 给出的示例的路径是/测试目录的UrlEncode编码
     */
    private String dir = "/";

    /**
     * 排序字段：默认为name；
     * time表示先按文件类型排序，后按修改时间排序；
     * name表示先按文件类型排序，后按文件名称排序；
     * size表示先按文件类型排序，后按文件大小排序。
     */
    private String order;

    /**
     * 默认为升序，设置为1实现降序 （注：排序的对象是当前目录下所有文件，不是当前分页下的文件）
     */
    private int desc;

    /**
     * 起始位置，从0开始
     */
    private int start;

    /**
     * 查询数目，默认为1000，建议最大不超过1000
     */
    private int limit = 1000;

    /**
     * 值为1时，返回dir_empty属性和缩略图数据
     */
    private int web;

    /**
     * 是否只返回文件夹，0 返回所有，1 只返回文件夹，且属性只返回path字段
     */
    private int folder;

    /**
     * 是否返回dir_empty属性，0 不返回，1 返回
     */
    private int showempty;


    public Map<String, String> toMap() {

        Map<String, String> map = new HashMap<>(8);

        try {
            map = BeanUtils.describe(this);
        } catch (Exception e) {
            e.printStackTrace();
            map.put("dir", dir);
        }
        //移除null key和value
        return map.entrySet().stream().filter(entry -> entry.getKey() != null && entry.getValue() != null).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue()));
    }

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public int getDesc() {
        return desc;
    }

    public void setDesc(int desc) {
        this.desc = desc;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getWeb() {
        return web;
    }

    public void setWeb(int web) {
        this.web = web;
    }

    public int getFolder() {
        return folder;
    }

    public void setFolder(int folder) {
        this.folder = folder;
    }

    public int getShowempty() {
        return showempty;
    }

    public void setShowempty(int showempty) {
        this.showempty = showempty;
    }
}
