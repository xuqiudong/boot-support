package cn.xuqiudong.console.module.blog.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.GlobalConfigMapper;
import cn.xuqiudong.common.blog.model.GlobalConfig;
import cn.xuqiudong.console.base.constant.GlobalFieldParams;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

/**
 * @author Vic.xu
 * 描述:全局配置 Service
 * @since 2020-06-10 14:37
 */
@Service
public class GlobalConfigService extends BaseService<GlobalConfigMapper, GlobalConfig> {

    @Override
    protected boolean hasAttachment() {
        return true;
    }

    /**
     * 根据code 返回配置
     * FIXME ： 缓存
     * @return GlobalConfig
     */
    @NotNull
    public GlobalConfig getByCode(GlobalFieldParams.GlobalConfigCodeEnum code) {
        GlobalConfig config = mapper.getByCode(code.getCode());
        return config == null ? new GlobalConfig() : config;
    }

    /**
     * 根据code更新content
     * @param code
     * @param content
     */
    public boolean updateContentByCode(GlobalFieldParams.GlobalConfigCodeEnum code, String content) {
        return mapper.updateContentByCode(code.getCode(), content) > 0;
    }


}
