package cn.xuqiudong.console.module.backup.helper.baidu;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.util.Date;

/**
 * 描述: 云盘的一些配置信息
 * @author Vic.xu
 * @since 2024-01-31 14:52
 */
public class BaiduPanProperties {

    /**
     * token
     */
    private String accessToken;

    /**
     * refreshToken
     */
    private String refreshToken;

    /**
     * 到期时间 毫秒
     */
    private long expires;

    private String appKey;

    private String secretKey;

    private String signKey;


    public BaiduPanProperties() {
    }

    public BaiduPanProperties(String accessToken, String refreshToken, long expires, String appKey, String secretKey, String signKey) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expires = expires;
        this.appKey = appKey;
        this.secretKey = secretKey;
        this.signKey = signKey;
    }

    @Override
    public String toString() {
        return "{" +

                "accessToken='" + accessToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                ", expires=" + expires +
                ", appKey=" + appKey +
                ", secretKey=" + secretKey +
                ", signKey=" + signKey +
                "\n\t, 到期时间=" + DateFormatUtils.format(new Date(expires), "yyyy-MM-dd HH:mm:ss") +
                ", 是否合法=" + isValid() +
                ", 是否到期=" + isExpired() +
                '}';
    }


    /**
     * 是否合法的
     */
    public boolean isValid() {
        return !StringUtils.isAnyBlank(accessToken, refreshToken);
    }

    /**
     * 是否过期：提前5天让其过期
     */
    public boolean isExpired() {
        //当前时间加5天 是否大于到期时间
        return System.currentTimeMillis() + 5 * 24 * 60 * 60 * 1000 > expires;
    }


    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getSignKey() {
        return signKey;
    }

    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }
}
