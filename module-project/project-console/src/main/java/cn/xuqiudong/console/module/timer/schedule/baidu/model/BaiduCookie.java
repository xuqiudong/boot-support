package cn.xuqiudong.console.module.timer.schedule.baidu.model;

/**
 * @author Vic.xu
 *功能: : 百度Cookie相关信息
 * @since  2021/8/27 0027 8:42
 */
public class BaiduCookie {

    private Integer id;

    private String name;

    private String cookie;

    /**
     * 一键签到的tabs参数
     */
    private String signTbs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public String getSignTbs() {
        return signTbs;
    }

    public void setSignTbs(String signTbs) {
        this.signTbs = signTbs;
    }
}
