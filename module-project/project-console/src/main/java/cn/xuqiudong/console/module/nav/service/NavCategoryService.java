package cn.xuqiudong.console.module.nav.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.nav.mapper.NavCategoryMapper;
import cn.xuqiudong.common.nav.model.NavCategory;
import org.springframework.stereotype.Service;

/**
 *功能: :导航分类 Service
 * @author Vic.xu
 * @since  2024-03-04 14:35
 */
@Service
public class NavCategoryService extends BaseService<NavCategoryMapper, NavCategory>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
