package cn.xuqiudong.console.module.backup.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.console.module.backup.mapper.BackupConfigMapper;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 *功能: :备份配置表 Service
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
@Service
public class BackupConfigService extends BaseService<BackupConfigMapper, BackupConfig>{

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 更新最后上传时间
     */
    public int updateLastBackupTime(Integer id, Date date) {
        if (date == null) {
            date = new Date();
        }
        BackupConfig config = new BackupConfig();
        config.setId(id);
        config.setLastBackupTime(date);
        return super.update(config);
    }

}
