package cn.xuqiudong.console.module.system.controller;


import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.util.encrypt.PasswordUtils;
import cn.xuqiudong.console.module.system.model.SysUser;
import cn.xuqiudong.console.module.system.service.SysUserService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

/**
 * 说明 :  系统用户控制层
 * @author  Vic.xu
 * @since  2019年11月18日 下午2:12:59
 */
@RestController
@RequestMapping("/system/user")
public class SysUserController extends BaseController<SysUserService, SysUser> {

    /**
     * 用户列表页
     * @return
     */
    @GetMapping("home")
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView("system/user/home");
        return modelAndView;
    }

    /**
     * 判断用户名是否重复
     */
    @PostMapping(value = "/check", params = {"username"})
    public BaseResponse<?> checkUsername(Integer id, String username) {
        return BaseResponse.success(service.checkUsername(id, username));
    }

    /**
     * 保存
     * @param entity
     * @return
     */
    @PostMapping(value = "/save", params = {"roleIds"})
    public BaseResponse<?> save(SysUser entity, @RequestParam(required = false) Integer[] roleIds) {
        service.save(entity, roleIds);
        return BaseResponse.success(entity);
    }


    /**
     * 说明 :  修改密码
     * @author  Vic.xu
     * @since  2020年2月17日 下午4:15:27
     * @return
     */
    @PostMapping(value = "/password")
    public BaseResponse<?> password(String oldPassword, String password) {
        //TODO
        SysUser user = service.findById(1);
        if (user == null) {
            return BaseResponse.error("不存在的用户");
        }
        if (!PasswordUtils.validatePassword(oldPassword, user.getPassword())) {
            return BaseResponse.error("密码错误");
        }
        user.setPassword(PasswordUtils.entryptPassword(password));
        service.save(user);
        return BaseResponse.success();
    }


}
