package cn.xuqiudong.console.module.system.model;

import cn.xuqiudong.common.base.model.BaseEntity;
import cn.xuqiudong.common.util.NumberUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单表 实体类
 *
 * @author Vic.xu
 */
public class SysMenu extends BaseEntity implements Comparable<SysMenu> {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单编码,从100开始每级别占三位,同级别递增
     */
    private Long code;

    /**
     * 菜单名称
     */
    private String name;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 所属父类
     */
    private Integer pid;

    /**
     * 类型 1-目录 2-菜单 3-按钮
     */
    private Integer type;

    /**
     * 菜单url
     */
    private String url;

    /**
     * 权限标识
     */
    private String permission;

    /**
     * 排序
     */
    private Integer sort;

    /************** 其他属性 ******************************************/
    /**
     * 当前节点是否选择
     */
    private Boolean checked;

    /**
     * 菜单和角色关系表中对应的id
     */
    private Integer relId;

    /**
     * 父级菜单
     */
    @JsonIgnore
    private SysMenu parent;

    /**
     * 子菜单
     */
    private List<SysMenu> children = new ArrayList<SysMenu>();

    /********************************************************/

    /**
     * 添加子菜单
     */
    public void addChildren(SysMenu menu) {
        this.children.add(menu);
        menu.setParent(this);
    }

    /**
     * 当前菜单是根节点吗
     */
    public boolean isRoot() {
        return this.pid == null || this.pid == 0;
    }

    /**
     * 是否是父节点
     */
    public boolean isPnode() {
        return CollectionUtils.isNotEmpty(children);
    }

    /***************** set|get start **************************************/

    /**
     * set：按钮编码,从100开始每级别占三位,同级别递增
     */
    public SysMenu setCode(Long code) {
        this.code = code;
        return this;
    }

    /**
     * get：按钮编码,从100开始每级别占三位,同级别递增
     */
    public Long getCode() {
        return code;
    }

    /**
     * set：菜单名称
     */
    public SysMenu setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：菜单名称
     */
    public String getName() {
        return name;
    }

    /**
     * set：菜单图标
     */
    public SysMenu setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    /**
     * get：菜单图标
     */
    public String getIcon() {
        return icon;
    }

    /**
     * set：所属父类
     */
    public SysMenu setPid(Integer pid) {
        this.pid = pid;
        return this;
    }

    /**
     * get：所属父类
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * set：类型 1-目录 2-菜单 3-按钮
     */
    public SysMenu setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * get：类型 1-目录 2-菜单 3-按钮
     */
    public Integer getType() {
        return type;
    }

    /**
     * set：菜单url
     */
    public SysMenu setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * get：菜单url
     */
    public String getUrl() {
        return url;
    }

    /**
     * set：权限标识
     */
    public SysMenu setPermission(String permission) {
        this.permission = permission;
        return this;
    }

    /**
     * get：权限标识
     */
    public String getPermission() {
        return permission;
    }

    /**
     * set：排序
     */
    public SysMenu setSort(Integer sort) {
        this.sort = sort;
        return this;
    }

    /**
     * get：排序
     */
    public Integer getSort() {
        return sort;
    }

    public List<SysMenu> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenu> children) {
        this.children = children;
    }

    @Override
    public int compareTo(SysMenu o) {
        if (o == null) {
            return -1;
        }
        // 如果两个同属于一级则判断 sort
        if (o.getPid().equals(this.getPid())) {
            return this.sort - o.sort;
        }
        return this.getId() - o.getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SysMenu)) {
            return false;
        }
        SysMenu target = (SysMenu) obj;
        return NumberUtils.equals(this.getPid(), target.getPid()) && NumberUtils.equals(this.getId(), target.getId());
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pid).append(id).hashCode();
    }

    public SysMenu getParent() {
        return parent;
    }

    public void setParent(SysMenu parent) {
        this.parent = parent;
    }

    public Boolean getChecked() {
        return checked;
    }

    public Integer getRelId() {
        return relId;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public void setRelId(Integer relId) {
        this.relId = relId;
    }
    /***************** set|get end **************************************/

}
