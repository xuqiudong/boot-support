package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.blog.model.BlogComment;
import cn.xuqiudong.console.module.blog.service.BlogCommentService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Vic.xu
 * 描述:评论 控制层
 * @since  2020-07-21 14:59
 */
@RestController
@RequestMapping("/blog/blogComment")
public class BlogCommentController extends BaseController<BlogCommentService, BlogComment> {

}
