package cn.xuqiudong.console.index.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-06-22 11:36
 */
@Controller
public class MainController {

    @GetMapping("main")
    public String mainPage() {
        return "main";
    }

}
