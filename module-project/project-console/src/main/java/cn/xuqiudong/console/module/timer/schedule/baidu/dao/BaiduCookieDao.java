package cn.xuqiudong.console.module.timer.schedule.baidu.dao;

import cn.xuqiudong.console.module.timer.schedule.baidu.model.BaiduCookie;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Vic.xu
 *功能: :
 * @since  2021/8/27 0027 8:51
 */
@Repository
public class BaiduCookieDao {

    @Resource
    private JdbcTemplate jdbcTemplate;

    public List<BaiduCookie> cookieList() {
        String sql = "SELECT a.`id`, a.`name`, a.`cookie`, a.`sign_tbs` AS signTbs FROM baidu_cookie a WHERE 1=1 ";
        List<BaiduCookie> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<BaiduCookie>(BaiduCookie.class));
        return list;
    }
}
