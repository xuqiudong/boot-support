package cn.xuqiudong.console.module.system.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.console.module.system.model.SysMenu;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单表 Mapper
 *
 * @author Vic.xu
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 当前pid下的子菜单最大的code
     * @param pid
     * @return max
     */
    Long getMaxCode(@Param("pid") Integer pid);

    /**
     * 用作选择当前菜单父级菜单
     * @return  menu list
     */
    List<SysMenu> chooseParent();

}
