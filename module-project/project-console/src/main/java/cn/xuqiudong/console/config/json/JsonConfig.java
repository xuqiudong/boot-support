package cn.xuqiudong.console.config.json;

import cn.xuqiudong.common.base.handler.json.serializer.XssStringJsonSerializer;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-09-09 16:34
 */
@Configuration
public class JsonConfig {

    private final Logger logger = LoggerFactory.getLogger(JsonConfig.class);

    @Bean
    @Primary
    public ObjectMapper objectMapper(Jackson2ObjectMapperBuilder build) {
        logger.info("register xssObjectMapper");
        ObjectMapper objectMapper = build.createXmlMapper(false).build();
        SimpleModule simpleModule = new SimpleModule(XssStringJsonSerializer.class.getSimpleName());
        simpleModule.addSerializer(String.class, new XssStringJsonSerializer());
        objectMapper.registerModule(simpleModule);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }
}
