package cn.xuqiudong.console.module.timer.schedule.baidu.service;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.console.module.timer.schedule.baidu.dao.BaiduCookieDao;
import cn.xuqiudong.console.module.timer.schedule.baidu.model.BaiduCookie;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 贴吧签到service
 * @author Vic.xu
 * @since  2021/7/21 0021 8:45
 */
@Service
public class TiebaSignInService extends BaseBaiduService {

    @Resource
    private BaiduCookieDao baiduCookieDao;

    public BaseResponse<?> signIn() {
        Map<String, Object> result = new HashMap<>(16);

        logger.info("开始百度贴吧自动签到：****************************************");
        String url = "https://tieba.baidu.com/tbmall/onekeySignin1";
        List<BaiduCookie> cookies = baiduCookieDao.cookieList();
        if (CollectionUtils.isEmpty(cookies)) {
            logger.info("没有配置百度用户信息");
            return BaseResponse.error("没有配置百度用户信息");
        }
        for (BaiduCookie cookie : cookies) {
            try {
                Map<String, Object> response = requestForMap(url, cookie);
                logger.info("【{}】一键签到结果：\n {}", cookie.getName(), JsonUtil.toJsonPretty(response));
                result.put("【" + cookie.getName() + "】一键签到结果：", response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        logger.info("结束百度贴吧自动签到：-------------------------------------------");
        return BaseResponse.success(result);
    }

}
