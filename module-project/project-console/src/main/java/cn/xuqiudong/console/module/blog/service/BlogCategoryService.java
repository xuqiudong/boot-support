package cn.xuqiudong.console.module.blog.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogCategoryMapper;
import cn.xuqiudong.common.blog.model.BlogCategory;
import org.springframework.stereotype.Service;

/**
 * 描述:博客分类表 Service
 * @author Vic.xu
 * @since  2020-05-08 09:38
 */
@Service
public class BlogCategoryService extends BaseService<BlogCategoryMapper, BlogCategory> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

    /**
     * 更新文章数量
     */
    public int updateArticleNum(Integer id) {
        return mapper.updateArticleNum(id);
    }

}
