package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.blog.model.BlogArticle;
import cn.xuqiudong.console.module.blog.service.BlogArticleService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


/**
 * 描述:博客表 控制层
 * @author Vic.xu
 * @since  2020-05-08 10:53
 */
@RestController
@RequestMapping("/blog/article")
public class BlogArticleController extends BaseController<BlogArticleService, BlogArticle> {

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/article/home");
        return mv;
    }

    /**
     * 预览
     * @param id id
     * @return mv
     */
    @GetMapping("/preview")
    public ModelAndView preview(Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/article/preview");
        mv.addObject("entity", service.findById(id));
        return mv;
    }

    @GetMapping("form")
    public ModelAndView form(Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/article/form");
        if (id != null) {
            mv.addObject("entity", service.findById(id));
        }
        return mv;
    }

}
