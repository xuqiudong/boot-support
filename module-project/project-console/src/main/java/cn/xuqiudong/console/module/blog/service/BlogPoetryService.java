package cn.xuqiudong.console.module.blog.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.common.blog.mapper.BlogPoetryMapper;
import cn.xuqiudong.common.blog.model.BlogPoetry;
import org.springframework.stereotype.Service;

/**
 * @author Vic.xu
 * 描述:诗歌 Service
 * @since  2020-08-19 14:26
 */
@Service
public class BlogPoetryService extends BaseService<BlogPoetryMapper, BlogPoetry> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }
}
