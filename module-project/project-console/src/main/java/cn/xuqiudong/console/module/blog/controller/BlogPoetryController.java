package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.blog.model.BlogPoetry;
import cn.xuqiudong.console.module.blog.service.BlogPoetryService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述:诗歌 控制层
 * @author Vic.xu
 * @since  2020-08-19 14:26
 */
@RestController
@RequestMapping("/blog/blogPoetry")
public class BlogPoetryController extends BaseController<BlogPoetryService, BlogPoetry> {

}
