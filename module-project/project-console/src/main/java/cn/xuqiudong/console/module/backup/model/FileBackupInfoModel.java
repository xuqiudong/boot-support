package cn.xuqiudong.console.module.backup.model;

import java.io.Serializable;

/**
 * 描述: 待转移文件信息
 * @author Vic.xu
 * @since 2024-02-08 17:12
 */
public class FileBackupInfoModel implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设定的根路径
     */
    private String root;

    /**
     * 相对根路径的路径
     */
    private String relativePath;


}
