package cn.xuqiudong.console.module.system.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.console.module.system.model.SysMenu;
import cn.xuqiudong.console.module.system.service.SysMenuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 说明 :  系统菜单controller
 * @author  Vic.xu
 * @since  2019年11月19日 上午10:42:41
 */
@RestController
@RequestMapping(value = "/system/menu")
public class SysMenuController extends BaseController<SysMenuService, SysMenu> {

    /**
     * 全部的菜单-树形结构
     * @return
     */
    @GetMapping(value = "menuTree")
    public BaseResponse<?> menuTree() {
        List<SysMenu> tree = service.menuTree();
        return BaseResponse.success(tree);
    }

    /**
     * 菜单列表-全部  (重写积累的list)
     */
    @GetMapping(value = "list")
    @Override
    public BaseResponse<?> list(SysMenu lookup) {
        List<SysMenu> list = service.list(lookup);
        return BaseResponse.success(list);
    }

    /**
     * 用作选择父级菜单: 不包含按钮
     * @return
     */
    @GetMapping(value = "chooseParent")
    public BaseResponse<?> chooseParent() {
        List<SysMenu> list = service.chooseParent();
        return BaseResponse.success(list);
    }

}
