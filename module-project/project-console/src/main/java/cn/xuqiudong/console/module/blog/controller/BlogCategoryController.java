package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.base.vo.Select2VO;
import cn.xuqiudong.common.blog.model.BlogCategory;
import cn.xuqiudong.common.util.CommonUtils;
import cn.xuqiudong.console.module.blog.service.BlogCategoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 描述:博客分类表 控制层
 * @author Vic.xu
 * @since  2020-05-08 09:38
 */
@RestController
@RequestMapping("/blog/category")
public class BlogCategoryController extends BaseController<BlogCategoryService, BlogCategory> {

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/category/home");
        return mv;
    }

    @GetMapping("form")
    public ModelAndView form(Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/category/form");
        if (id != null) {
            mv.addObject("entity", service.findById(id));
        }
        return mv;
    }

    /**
     * 全部分类
     */
    @RequestMapping(value = "all")
    public BaseResponse<?> all() {
        BlogCategory lookup = new BlogCategory();
        lookup.setEnable(true);
        List<BlogCategory> list = service.list(lookup);
        List<Select2VO> result = CommonUtils.listFilter(list, category -> new Select2VO(category.getId(), category.getName()));
        return BaseResponse.success(result);
    }

}
