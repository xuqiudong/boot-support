package cn.xuqiudong.console.module.system.service;

import cn.xuqiudong.common.base.vo.BooleanWithMsg;
import cn.xuqiudong.console.module.system.model.SysUser;
import org.springframework.stereotype.Service;

/**
 * 描述: 授权信相关
 * @author Vic.xu
 * @since 2022-09-21 17:47
 */
@Service
public class AuthorityService {

    private SysUserService sysUserService;

    public AuthorityService(SysUserService sysUserService) {
        this.sysUserService = sysUserService;
    }

    public BooleanWithMsg afterLogin(String username) {
        try {
            SysUser user = sysUserService.findUserByUsername(username);
            if (user == null) {
                return BooleanWithMsg.fail("不存在的用户名:" + username);
            }
            
        } catch (Exception ex) {
            return BooleanWithMsg.fail(ex.getMessage());
        }
        return BooleanWithMsg.success();
    }

}
