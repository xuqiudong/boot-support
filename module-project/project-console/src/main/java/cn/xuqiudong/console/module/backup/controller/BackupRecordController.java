package cn.xuqiudong.console.module.backup.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.console.module.backup.model.BackupRecord;
import cn.xuqiudong.console.module.backup.service.BackupRecordService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *功能: :备份记录表 控制层
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
@RestController
@RequestMapping("/backup/record")
public class BackupRecordController extends BaseController<BackupRecordService, BackupRecord>{

}
