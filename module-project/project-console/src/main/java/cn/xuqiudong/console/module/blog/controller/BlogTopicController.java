package cn.xuqiudong.console.module.blog.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.base.vo.Select2VO;
import cn.xuqiudong.common.blog.model.BlogTopic;
import cn.xuqiudong.common.util.CommonUtils;
import cn.xuqiudong.console.module.blog.service.BlogTopicService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * 描述:博客专题表 控制层
 * @author Vic.xu
 * @since  2020-05-08 09:36
 */
@RestController
@RequestMapping("/blog/topic")
public class BlogTopicController extends BaseController<BlogTopicService, BlogTopic> {

    @GetMapping("/home")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/topic/home");
        return mv;
    }

    @GetMapping("form")
    public ModelAndView form(Integer id) {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("/blog/topic/form");
        if (id != null) {
            mv.addObject("entity", service.findById(id));
        }
        return mv;
    }

    /**
     * 全部专题
     * @return
     */
    @RequestMapping(value = "all")
    public BaseResponse all() {
        BlogTopic lookup = new BlogTopic();
        lookup.setEnable(true);
        List<BlogTopic> list = service.list(lookup);
        List<Select2VO> result = CommonUtils.listFilter(list, topic -> new Select2VO(topic.getId(), topic.getName()));
        return BaseResponse.success(result);
    }
}
