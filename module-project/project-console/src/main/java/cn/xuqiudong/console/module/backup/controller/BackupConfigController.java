package cn.xuqiudong.console.module.backup.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.console.module.backup.handler.BackupHandler;
import cn.xuqiudong.console.module.backup.model.BackupConfig;
import cn.xuqiudong.console.module.backup.service.BackupConfigService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 *功能: :备份配置表 控制层
 * @author Vic.xu
 * @since  2024-02-08 17:48
 */
@RestController
@RequestMapping("/backup/config")
public class BackupConfigController extends BaseController<BackupConfigService, BackupConfig>{

    @Resource
    private BackupHandler backupHandler;

    @GetMapping("/now")
    public BaseResponse<String> backup(Integer id){
        return backupHandler.handle(id);
    }

}
