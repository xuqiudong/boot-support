package cn.xuqiudong.console.config.sso;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 描述: cas sso的一些墨汁
 * @author Vic.xu
 * @since 2022-09-21 17:39
 */
@ConfigurationProperties(prefix = CasSsoProperties.CAS_SSO_PROPERTIES_PREFIX)
public class CasSsoProperties {
    public static final String CAS_SSO_PROPERTIES_PREFIX = "cas.sso";

    /**
     * appid
     */
    private String appId;

    /**
     * 密码
     */
    private String appSecret;

    /**
     * 所属组
     */
    private String group;

    /**
     * 服务端url
     */
    private String serverUrl;

    /**
     * 不拦截的url
     */
    private List<String> excludeUrls;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public List<String> getExcludeUrls() {
        return excludeUrls;
    }

    public void setExcludeUrls(List<String> excludeUrls) {
        this.excludeUrls = excludeUrls;
    }
}
