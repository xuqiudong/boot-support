package cn.xuqiudong.t.wechat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WechatProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(WechatProjectApplication.class, args);
    }

}
