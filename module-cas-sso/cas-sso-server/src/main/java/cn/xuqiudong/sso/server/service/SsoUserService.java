package cn.xuqiudong.sso.server.service;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.sso.common.model.SsoUser;

/**
 * 描述:
 *      用户登录
 * @author Vic.xu
 * @since 2021-10-29 17:39
 */
public interface SsoUserService {

    /**
     * 登录
     * @param group 组 可为空
     * @param username username
     * @param password  password
     * @return
     */
    BaseResponse<SsoUser> login(String group, String username, String password);
}
