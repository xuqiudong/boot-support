package cn.xuqiudong.sso.server.session;

import cn.xuqiudong.common.base.tool.Tools;
import cn.xuqiudong.sso.common.constant.SsoConstant;
import cn.xuqiudong.sso.common.model.AccessTokenContent;
import cn.xuqiudong.sso.common.util.SsoHttpUtil;

import java.io.IOException;

/**
 * 描述:
 *   调用凭证AccessToken管理器接口
 *
 * @author Vic.xu
 * @since 2021-11-02 11:19
 */
public interface AccessTokenManager extends Manager {

    /**
     * 生成AccessToken
     *
     * @param tokenInfo
     * @return
     */
    default String generate(AccessTokenContent tokenInfo) {
        String accessToken = "at-" + Tools.randomUuid();
        storage(accessToken, tokenInfo);
        return accessToken;
    }

    /**
     * 存储  accessToken，以及保存accessToken 和tgt的关系
     *
     * @param accessToken
     * @param tokenInfo
     */
    void storage(String accessToken, AccessTokenContent tokenInfo);

    /**
     * 刷新accessToken 延长其生命周期
     *
     * @param accessToken
     * @return
     */
    boolean refresh(String accessToken);

    /**
     * 查询
     *
     * @param accessToken
     * @return
     */
    AccessTokenContent get(String accessToken);

    /**
     * 根据TGT删除AccessToken,会向客户端同步发出退出请求
     *
     * @param tgt
     */
    void remove(String tgt);

    /**
     * 发起客户端登出请求，求请求会被client的LogoutFilter拦截，判断出请求头中存在退出标识，则做退出处理
     *
     * @param redirectUri
     * @param accessToken
     */
    default void sendLogoutRequest(String redirectUri, String accessToken) {
        try {
            SsoHttpUtil.connect(redirectUri).header(SsoConstant.LOGOUT_PARAMETER_NAME, accessToken).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
