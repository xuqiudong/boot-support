package cn.xuqiudong.sso.server.storage.model;

/**
 * 描述:
 *      用于保存在本地的数据
 * @author Vic.xu
 * @since 2021-11-02 11:28
 */
public class LocalData<T> {
    /**
     * 过期时间
     */
    private long expired;

    private T data;

    /**
     * 持续时间
     */
    private int duration;

    /**
     * @param duration 有效期  单位s
     * @param data  数据
     */
    public LocalData(T data, int duration) {
        this.duration = duration;
        this.expired = System.currentTimeMillis() + duration * 1000L;
        this.data = data;
    }

    /**
     * 是否过期
     * @return
     */
    public boolean isExpire() {
        return System.currentTimeMillis() > expired;
    }

    /**
     * 续期
     * @return
     */
    public void refresh() {
        this.expired = System.currentTimeMillis() + duration * 1000L;
    }

    public long getExpired() {
        return expired;
    }

    public void setExpired(long expired) {
        this.expired = expired;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
