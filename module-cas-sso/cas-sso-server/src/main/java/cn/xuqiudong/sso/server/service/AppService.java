package cn.xuqiudong.sso.server.service;

import cn.xuqiudong.common.base.vo.BooleanWithMsg;

/**
 * 应用服务相关接口
 * @author xuduo
 */
public interface AppService {

    /**
     * 检测appId是否存在
     * @param appId appId
     * @return boolean
     */
    boolean checkAppId(String appId);

    /**
     * 检测appId 和对应的秘钥是否正确
     * @param appId appId
     * @param appSecret  appSecret
     * @return BooleanWithMsg
     */
    BooleanWithMsg validate(String appId, String appSecret);
}
