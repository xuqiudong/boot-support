package cn.xuqiudong.sso.server.model;

/**
 * 描述: app信息，用户server端校验client端的合法性 包含id和secret
 * @author Vic.xu
 * @since 2022-05-16 17:42
 */
public class AppClient {

    /**
     * 客户端 id
     */
    private String id;

    /**
     * 密码
     */
    private String secret;

    /**
     * 所属组
     */
    private String group;

    /**
     * 当前app client退出登录的的url
     */
    String logoutUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    @Override
    public String toString() {
        return "AppClientModel{" +
                "id='" + id + '\'' +
                ", secret='" + secret + '\'' +
                ", group='" + group + '\'' +
                ", logoutUrl='" + logoutUrl + '\'' +
                '}';
    }
}
