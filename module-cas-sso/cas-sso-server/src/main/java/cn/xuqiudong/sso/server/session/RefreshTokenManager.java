package cn.xuqiudong.sso.server.session;

import cn.xuqiudong.common.base.tool.Tools;
import cn.xuqiudong.sso.common.model.AccessTokenContent;
import cn.xuqiudong.sso.common.model.RefreshTokenContent;

/**
 * 描述:
 *    刷新凭证refreshToken管理器接口
 * @author Vic.xu
 * @since 2021-11-02 14:34
 */
public interface RefreshTokenManager extends Manager {
    /**
     * 生成refreshToken
     *
     * @param accessTokenContent
     * @param accessToken
     * @return
     */
    default String generate(AccessTokenContent accessTokenContent, String accessToken) {
        String refreshToken = "rt-" + Tools.randomUuid();
        storage(refreshToken, new RefreshTokenContent(accessTokenContent, accessToken));
        return refreshToken;
    }

    /**
     * 存储 refreshToken
     * @param refreshToken key
     * @param refreshTokenContent value
     */
    void storage(String refreshToken, RefreshTokenContent refreshTokenContent);

    /**
     * 验证refreshToken有效性，无论有效性与否，都remove掉， 如果没获取到，则从暂存的 refreshToken中获取
     * @param refreshToken key
     * @return RefreshTokenContent
     */
    RefreshTokenContent validate(String refreshToken);

    /**
     * 暂存refreshToken 和新的 accessToken 的关联关系
     * @param refreshToken refreshToken
     * @param refreshTokenContent refreshTokenContent 其中包含了新的accessToken
     */
    void temporaryStorageRefreshToken(String refreshToken, RefreshTokenContent refreshTokenContent);

    /**
     * 获取暂存的 refreshToken信息
     * @param refreshToken key
     * @return RefreshTokenContent  包含新的accessToken
     */
    RefreshTokenContent getTemporaryStorageRefreshToken(String refreshToken);
}
