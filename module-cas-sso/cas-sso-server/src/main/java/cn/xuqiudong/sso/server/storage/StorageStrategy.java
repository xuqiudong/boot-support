package cn.xuqiudong.sso.server.storage;

import java.util.Set;

/**
 * 描述:
 *      存储策略
 * @author Vic.xu
 * @since 2021-11-08 8:28
 */
public interface StorageStrategy<T> {

    /**
     * 存储
     */
    void storage(String cacheName, String key, T data, int expire);

    /**
     * 获取数据
     */
    T getData(String cacheName, String key, Class<T> clazz);

    /**续期*/
    T refresh(String cacheName, String key, int expire, Class<T> clazz);

    /**删除*/
    T remove(String cacheName, String key, Class<T> clazz);

    /**保存到set集合*/
    void storage2SetData(String cacheName, String key, String data);

    Set<String> getSetData(String cacheName, String key);

    /**定时清除过期数据，如果需要的话*/
    void cleanSchedule();
}
