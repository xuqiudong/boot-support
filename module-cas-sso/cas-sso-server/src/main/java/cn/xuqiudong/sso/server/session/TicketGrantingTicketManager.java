package cn.xuqiudong.sso.server.session;

import cn.xuqiudong.common.base.tool.Tools;
import cn.xuqiudong.sso.common.model.SsoUser;

/**
 * 登录凭证（TGC）管理器接口
 * @author Vic.xu
 */
public interface TicketGrantingTicketManager extends Manager {

    /**
     * 配置文件中配置的全局票据管理器的 key,有效值local和redis，默认local <br />
     * sso.tgt.manager:redis
     *
     */
    public static final String SESSION_MANAGER_NAME_KEY = "sso.tgt.manager";

    /**
     * 登录成功后，根据用户信息生成令牌
     *
     * @param user
     * @return tgt
     */
    default String generate(SsoUser user) {
        String tgt = "tgc-" + Tools.randomUuid();
        storage(tgt, user);
        return tgt;

    }

    /**
     * 存储令牌：保存令牌和用户的关系
     *
     * @param tgt
     * @param user
     */
    void storage(String tgt, SsoUser user);


    /**
     *  验证tgt是否存在且在有效期内，并更新过期时间戳
     * @param tgt
     * @return
     */
    SsoUser getAndRefresh(String tgt);

    /**
     * 移除tgt 以及tgt对应的accessToken集合
     * @param tgt
     */
    void remove(String tgt);
}
