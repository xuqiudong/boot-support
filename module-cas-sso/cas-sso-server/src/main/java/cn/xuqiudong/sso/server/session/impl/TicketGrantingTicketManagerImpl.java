package cn.xuqiudong.sso.server.session.impl;

import cn.xuqiudong.sso.common.model.SsoUser;
import cn.xuqiudong.sso.server.config.SsoConfiguration;
import cn.xuqiudong.sso.server.session.TicketGrantingTicketManager;
import cn.xuqiudong.sso.server.storage.StorageStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 描述:
 *      登录凭证的管理
 * @author Vic.xu
 * @since 2021-11-01 10:07
 */
@Component
public class TicketGrantingTicketManagerImpl implements TicketGrantingTicketManager {

    //SsoUser
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private StorageStrategy<SsoUser> ssoUserStorageStrategy;

    @Resource
    private SsoConfiguration ssoConfiguration;


    @Override
    public int expire() {
        return ssoConfiguration.getTimeout();
    }

    @Override
    public String cacheName() {
        return "tgt";
    }


    @Override
    public void storage(String tgt, SsoUser user) {
        ssoUserStorageStrategy.storage(cacheName(), tgt, user, expire());
    }

    @Override
    public SsoUser getAndRefresh(String tgt) {
        return ssoUserStorageStrategy.refresh(cacheName(), tgt, expire(), SsoUser.class);
    }


    @Override
    public void remove(String tgt) {
        ssoUserStorageStrategy.remove(cacheName(), tgt, SsoUser.class);
        logger.debug("删除tgt：{}", tgt);
    }

}
