package cn.xuqiudong.sso.server.service.impl;

import cn.xuqiudong.common.base.vo.BooleanWithMsg;
import cn.xuqiudong.sso.server.config.SsoConfiguration;
import cn.xuqiudong.sso.server.model.AppClient;
import cn.xuqiudong.sso.server.service.AppService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 描述: 检测appid相关
 * @author Vic.xu
 * @since 2021-10-29 17:31
 */
@Service
public class AppServiceImpl implements AppService {

    @Resource
    private SsoConfiguration ssoConfiguration;


    /**
     *  检验appId
     */
    @Override
    public boolean checkAppId(String appId) {
        return ssoConfiguration.getClientMap().get(appId) != null;
    }

    /**
     * 检测appId 和对应的秘钥是否正确
     */
    @Override
    public BooleanWithMsg validate(String appId, String appSecret) {
        AppClient appClient = ssoConfiguration.getClientMap().get(appId);
        if (appClient == null) {
            return BooleanWithMsg.fail("不合法的sso client");
        }
        if (StringUtils.equals(appSecret, appClient.getSecret())) {
            return BooleanWithMsg.success();
        } else {
            return BooleanWithMsg.fail("appId对应的密码错误");
        }

    }
}
