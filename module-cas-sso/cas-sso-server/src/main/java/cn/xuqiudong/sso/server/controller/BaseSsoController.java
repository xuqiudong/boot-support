package cn.xuqiudong.sso.server.controller;

import cn.xuqiudong.common.base.model.Remind;
import cn.xuqiudong.sso.common.constant.Oauth2Constant;
import cn.xuqiudong.sso.common.constant.SsoConstant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.Model;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 描述:
 *  Sso Server端的baseController
 * @author Vic.xu
 * @since 2021-10-29 17:25
 */
public abstract class BaseSsoController {

    public static String PRINCIPAL = "_principal";

    @Resource
    protected HttpServletRequest servletRequest;

    @Resource
    protected HttpServletResponse servletResponse;


    /**
     * 携带信息跳转回到登录页面用以重新登录
     * @param redirectUri 跳转回客户端的url
     * @param appId  appId
     * @param model  存放数据
     * @return
     */
    protected String goLoginPath(String redirectUri, String appId, Model model, String msg) {
        model.addAttribute(Oauth2Constant.APP_ID, appId);
        model.addAttribute(SsoConstant.REDIRECT_URI, redirectUri);
        if (!StringUtils.isEmpty(msg)) {
            model.addAttribute(Remind.warning().setMessage(msg));
        }
        return SsoConstant.LOGIN_PATH;
    }

}
