package cn.xuqiudong.sso.server.session;

import cn.xuqiudong.common.base.tool.Tools;
import cn.xuqiudong.sso.common.model.AuthorizationCode;

/**
 * 描述:
 *      授权码管理器接口
 * @author Vic.xu
 * @since 2021-11-01 11:20
 */
public interface CodeManager extends Manager {

    default String generate(String tgt, boolean notifyLogout, String redirectUri) {
        String code = "code-" + Tools.randomUuid();
        storage(code, new AuthorizationCode(tgt, notifyLogout, redirectUri));
        return code;
    }

    /**
     * 存储授权码
     * @param code
     * @param authorizationCode
     */
    void storage(String code, AuthorizationCode authorizationCode);

    /**
     *  校验授权码，若存在则删除
     * @param code
     * @return
     */
    AuthorizationCode validateAndRemove(String code);
}
