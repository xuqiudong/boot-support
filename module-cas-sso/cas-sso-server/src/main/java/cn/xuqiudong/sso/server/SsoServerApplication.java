package cn.xuqiudong.sso.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * SSO服务端
 *
 * @author Vic.xu
 */
@SpringBootApplication
@EnableScheduling
public class SsoServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SsoServerApplication.class, args);

    }


}
