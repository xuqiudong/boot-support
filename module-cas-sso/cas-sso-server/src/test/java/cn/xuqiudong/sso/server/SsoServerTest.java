package cn.xuqiudong.sso.server;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.sso.common.model.AuthorizationCode;
import cn.xuqiudong.sso.server.config.SsoConfiguration;
import cn.xuqiudong.sso.server.model.AppClient;
import cn.xuqiudong.sso.server.service.ClientConfigService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Unit test for simple App.
 */
@Rollback(true)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SsoServerTest {

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Value("${sso.storageStrategy:local}")
    private String storageStrategy;

    @Resource
    private ClientConfigService clientConfigService;

    @Resource
    private SsoConfiguration ssoConfiguration;

    @Test
    public void storageStrategy() {
        System.out.println(ssoConfiguration);
    }


    @Test
    public void test() {
        String key = "test-123";
        AuthorizationCode code = new AuthorizationCode("1233", true, "baidu.com");
        String text = JsonUtil.toJson(code);
        stringRedisTemplate.opsForValue().set(key, text, 120, TimeUnit.SECONDS);

        String text2 = stringRedisTemplate.opsForValue().get(key);
        System.out.println(text2);
        AuthorizationCode code2 = JsonUtil.jsonToObject(text2, AuthorizationCode.class);
        System.out.println(code2);
    }

    @Test
    public void client() {
        List<AppClient> appClients = clientConfigService.allClients();
        Assert.notNull(appClients, "appClients from db is null");
    }
}
