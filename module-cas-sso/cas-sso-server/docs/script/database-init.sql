CREATE
    DATABASE sso_server DEFAULT CHARSET utf8;
-- GRANT ALL PRIVILEGES ON blog.* TO 'blog'@'localhost' IDENTIFIED BY '12345678';
-- mysql8 
CREATE USER
    'sso_server'@'localhost' IDENTIFIED BY '12345678';
GRANT ALL PRIVILEGES ON sso_server.* TO 'sso_server'@'localhost' WITH GRANT OPTION;

FLUSH
    PRIVILEGES;
