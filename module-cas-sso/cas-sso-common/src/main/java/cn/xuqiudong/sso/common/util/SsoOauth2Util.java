package cn.xuqiudong.sso.common.util;

import cn.xuqiudong.common.base.craw.CrawlConnect;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.sso.common.constant.Oauth2Constant;
import cn.xuqiudong.sso.common.enums.GrantTypeEnum;
import cn.xuqiudong.sso.common.model.RpcAccessToken;
import com.fasterxml.jackson.core.type.TypeReference;

import java.io.IOException;

/**
 * 描述:
 *      sso Oauth2 工具类
 * @author Vic.xu
 * @since 2021-11-03 11:50
 */
public class SsoOauth2Util {

    /**
     * 调用服务端刷新token接口
     * @param serverUrl so server 地址
     * @param appId  appid
     * @param refreshToken  登录时候返回的token时同时返回的refreshToken
     * @return RpcAccessToken RpcAccessToken
     */
    public static BaseResponse<RpcAccessToken> refreshToken(String serverUrl, String appId, String refreshToken) {
        CrawlConnect connect = SsoHttpUtil.connect(serverUrl + Oauth2Constant.REFRESH_TOKEN_URL);
        connect.data(Oauth2Constant.APP_ID, appId)
                .data(Oauth2Constant.REFRESH_TOKEN, refreshToken);
        return getRpcAccessTokenBaseResponse(connect);

    }


    /**
     *  通过code获取 accessToken
     * @param serverUrl sso server 地址
     * @param appId appid
     * @param appSecret appSecret
     * @param code 授权码
     * @return RpcAccessToken
     */
    public static BaseResponse<RpcAccessToken> getAccessToken(String serverUrl, String appId, String appSecret, String code) {
        CrawlConnect connect = SsoHttpUtil.connect(serverUrl + Oauth2Constant.ACCESS_TOKEN_URL);
        connect.data(Oauth2Constant.GRANT_TYPE, GrantTypeEnum.AUTHORIZATION_CODE.name())
                .data(Oauth2Constant.APP_ID, appId)
                .data(Oauth2Constant.APP_SECRET, appSecret)
                .data(Oauth2Constant.AUTH_CODE, code);
        return getRpcAccessTokenBaseResponse(connect);

    }

    /**
     *  通过账号密码获取 accessToken
     * @param serverUrl sso server 地址
     * @param appId appid
     * @param appSecret appSecret
     * @param username 用户名
     * @param  password 密码
     * @return RpcAccessToken
     */
    public static BaseResponse<RpcAccessToken> getAccessToken(String serverUrl, String appId, String appSecret, String username, String password) {
        CrawlConnect connect = SsoHttpUtil.connect(serverUrl + Oauth2Constant.ACCESS_TOKEN_URL);
        connect.data(Oauth2Constant.GRANT_TYPE, GrantTypeEnum.PASSWORD.name())
                .data(Oauth2Constant.APP_ID, appId)
                .data(Oauth2Constant.APP_SECRET, appSecret)
                .data(Oauth2Constant.USERNAME, username)
                .data(Oauth2Constant.PASSWORD, password);
        return getRpcAccessTokenBaseResponse(connect);

    }


    private static BaseResponse<RpcAccessToken> getRpcAccessTokenBaseResponse(CrawlConnect connect) {
        try {
            String text = connect.postBodyText();
            return JsonUtil.jsonToObject(text, new TypeReference<BaseResponse<RpcAccessToken>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
            return BaseResponse.error("发生了一些错误：" + e.getMessage());
        }
    }

}
