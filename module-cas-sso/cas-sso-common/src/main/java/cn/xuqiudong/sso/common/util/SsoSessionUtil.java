package cn.xuqiudong.sso.common.util;

import cn.xuqiudong.sso.common.constant.SsoConstant;
import cn.xuqiudong.sso.common.model.RpcAccessToken;
import cn.xuqiudong.sso.common.model.SessionAccessToken;

import javax.servlet.http.HttpServletRequest;

/**
 * 描述:
 *      Session工具类
 * @author Vic.xu
 * @since 2021-11-03 11:39
 */
public class SsoSessionUtil {

    /**
     * 从session或获取 SessionAccessToken
     */
    public static SessionAccessToken getAccessToken(HttpServletRequest request) {
        return (SessionAccessToken) request.getSession().getAttribute(SsoConstant.SESSION_ACCESS_TOKEN);
    }

    /**
     * 从session中删除 Session AccessToken
     */
    public static void removeAccessToken(HttpServletRequest request) {
        request.getSession().removeAttribute(SsoConstant.SESSION_ACCESS_TOKEN);
    }

    /**
     * SessionAccessToken存入session
     */
    public static void setAccessToken(HttpServletRequest request, RpcAccessToken rpcAccessToken) {
        SessionAccessToken sessionAccessToken = null;
        if (rpcAccessToken != null) {
            sessionAccessToken = createSessionAccessToken(rpcAccessToken);
        }
        request.getSession().setAttribute(SsoConstant.SESSION_ACCESS_TOKEN, sessionAccessToken);
    }

    /**
     * 根据RpcAccessToken  创建 SessionAccessToken
     */
    private static SessionAccessToken createSessionAccessToken(RpcAccessToken accessToken) {
        long expirationTime = System.currentTimeMillis() + accessToken.getExpiresIn() * 1000L;
        return new SessionAccessToken(accessToken.getAccessToken(), accessToken.getExpiresIn(),
                accessToken.getRefreshToken(), accessToken.getUser(), expirationTime);
    }
}
