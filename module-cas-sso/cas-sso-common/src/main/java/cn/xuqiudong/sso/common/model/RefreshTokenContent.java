package cn.xuqiudong.sso.common.model;

import java.io.Serializable;

/**
 * RefreshTokenContent
 * 设计了一个过渡方案，以适应在多并发更换token的情况
 * @author Vic.xu
 */
public class RefreshTokenContent implements Serializable {

    private static final long serialVersionUID = -1332598459045608781L;

    private AccessTokenContent accessTokenContent;

    private String accessToken;

    /**
     * 是否在更换token的期间：这个时间应该很短暂 比如60s
     */
    private boolean refreshDuration;

    /**
     * 在更换期间绑定新的accessToken信息
     */
    private RpcAccessToken newRpcAccessToken;

    public RefreshTokenContent() {
    }

    public RefreshTokenContent(AccessTokenContent accessTokenContent, String accessToken) {
        this.accessTokenContent = accessTokenContent;
        this.accessToken = accessToken;
    }

    public AccessTokenContent getAccessTokenContent() {
        return accessTokenContent;
    }

    public void setAccessTokenContent(AccessTokenContent accessTokenContent) {
        this.accessTokenContent = accessTokenContent;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public boolean isRefreshDuration() {
        return refreshDuration;
    }

    public void setRefreshDuration(boolean refreshDuration) {
        this.refreshDuration = refreshDuration;
    }

    public RpcAccessToken getNewRpcAccessToken() {
        return newRpcAccessToken;
    }

    public void setNewRpcAccessToken(RpcAccessToken newRpcAccessToken) {
        this.newRpcAccessToken = newRpcAccessToken;
        this.refreshDuration = true;
    }
}