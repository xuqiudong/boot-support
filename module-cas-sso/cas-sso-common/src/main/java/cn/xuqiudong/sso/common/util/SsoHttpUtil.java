package cn.xuqiudong.sso.common.util;

import cn.xuqiudong.common.base.craw.BaseCrawl;
import cn.xuqiudong.common.base.craw.CrawlConnect;

import java.io.IOException;
import java.time.LocalTime;

/**
 * 描述:
 *      sso的http工具类
 * @author Vic.xu
 * @since 2021-11-03 9:53
 */
public class SsoHttpUtil {

    public static CrawlConnect connect(String url) {
        CrawlConnect con = new SsoCrawl().con(url);
//        con.requestJson();
        return con;
    }

    public static void main(String[] args) throws IOException {
        String s = SsoHttpUtil.connect("http://localhost:10081/test").
                data("p","%2B").postBodyText();
        System.out.println(LocalTime.now() + "  "  + s);
    }


    static class SsoCrawl extends BaseCrawl {

        @Override
        public CrawlConnect con(String url) {
            return super.con(url);
        }

        @Override
        protected int getTimeout() {
            return 10000;
        }
    }
}
