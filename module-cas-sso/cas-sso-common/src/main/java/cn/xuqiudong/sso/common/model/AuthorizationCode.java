package cn.xuqiudong.sso.common.model;

/**
 * 描述:
 *      授权码信息
 * @author Vic.xu
 * @since 2021-11-01 11:30
 */
public class AuthorizationCode {

    private String tgt;

    /**
     * 退出的时候是否通知客户端
     */
    private boolean notifyLogout;

    private String redirectUri;

    public AuthorizationCode() {
    }

    public AuthorizationCode(String tgt, boolean notifyLogout, String redirectUri) {
        this.tgt = tgt;
        this.notifyLogout = notifyLogout;
        this.redirectUri = redirectUri;
    }

    public String getTgt() {
        return tgt;
    }

    public void setTgt(String tgt) {
        this.tgt = tgt;
    }


    public boolean isNotifyLogout() {
        return notifyLogout;
    }

    public void setNotifyLogout(boolean notifyLogout) {
        this.notifyLogout = notifyLogout;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }
}
