package cn.xuqiudong.sso.common.enums;

/**
 * 描述:
 *  Oauth2授权方式
 * @author Vic.xu
 * @since 2021-11-02 10:40
 */
public enum GrantTypeEnum {

    /**
     * 授权码模式
     */
    AUTHORIZATION_CODE,

    /**
     * 密码模式
     */
    PASSWORD;


}
