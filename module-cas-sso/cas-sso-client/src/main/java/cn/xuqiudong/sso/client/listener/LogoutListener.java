package cn.xuqiudong.sso.client.listener;

import cn.xuqiudong.sso.client.session.SessionMappingStorage;
import cn.xuqiudong.sso.client.session.local.LocalSessionMappingStorage;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * 描述:
 *    登出Listener，用于本地session过期，删除accessToken和session的映射关系
 *    默认为本地存储的方式，可配置不同SessionMappingStorage
 * @author Vic.xu
 * @since 2021-11-03 14:18
 */
public class LogoutListener implements HttpSessionListener {
    private static SessionMappingStorage sessionMappingStorage = new LocalSessionMappingStorage();

    @Override
    public void sessionCreated(final HttpSessionEvent event) {
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent event) {
        final HttpSession session = event.getSession();
        sessionMappingStorage.removeBySessionById(session.getId());
    }

    public void setSessionMappingStorage(SessionMappingStorage sms) {
        sessionMappingStorage = sms;
    }

    public static SessionMappingStorage getSessionMappingStorage() {
        return sessionMappingStorage;
    }
}
