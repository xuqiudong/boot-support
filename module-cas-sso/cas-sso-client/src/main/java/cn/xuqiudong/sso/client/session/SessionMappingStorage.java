package cn.xuqiudong.sso.client.session;

import javax.servlet.http.HttpSession;

/**
 * 描述:
 *   accessToken和session的映射关系
 * @author Vic.xu
 * @since 2021-11-03 14:16
 */
public interface SessionMappingStorage {
    /**
     * 根据accessToken 删除session
     * @param accessToken accessToken
     * @return removed session
     */
    HttpSession removeSessionByMappingId(String accessToken);

    /**
     * 根据 sessionId 删除session
     * @param sessionId sessionId
     * @return removed session
     */
    void removeBySessionById(String sessionId);

    /**
     * 保存session和accessToken 的相互关系，使之能互相查找到对方
     * @param accessToken accessToken
     * @param session session
     */
    void addSessionById(String accessToken, HttpSession session);
}
