package cn.xuqiudong.sso.client.filter;

import cn.xuqiudong.sso.common.constant.SsoConstant;
import org.apache.commons.lang3.StringUtils;

/**
 * 描述:
 *    存放Filter和SSO SERVER交互的参数
 * @author Vic.xu
 * @since 2021-11-02 9:36
 */
public class FilterParam {

    /**
     * 客户端标识
     */
    protected String appId;
    /**
     * 密码
     */
    protected String appSecret;

    /**
     * 所属组
     */
    protected String group;

    /**
     * sso server 地址
     */
    protected String serverUrl;

    /**
     * sso server 内网地址（用于Oauth2校验），如果不配置，默认取serverUrl
     */
    protected String internalServerUrl;

    /**
     * 客户端退出登录地址
     */
    protected String clientLogoutUrl = SsoConstant.LOGOUT_URL;


    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getClientLogoutUrl() {
        return clientLogoutUrl;
    }

    public void setClientLogoutUrl(String clientLogoutUrl) {
        this.clientLogoutUrl = clientLogoutUrl;
    }

    /**
     * 如果没有配置，则取serverUrl
     */
    public String getInternalServerUrl() {
        if (StringUtils.isBlank(internalServerUrl)) {
            return serverUrl;
        }
        return internalServerUrl;
    }

    public void setInternalServerUrl(String internalServerUrl) {
        this.internalServerUrl = internalServerUrl;
    }
}
