package cn.xuqiudong.sso.client.filter;

/**
 * 描述:基于接口的登录filter
 * <p>
 *     1. 使用与基于API接口的项目，比如移动端或者前后端分离
 *     2. 流程：
 *        2.1 进入拦截器，没有登录 则返回401  前端自行跳转到登录页面,输入账号密码
 *        2.2 后端进行基于账号密码的方式获取accessToken {@link cn.xuqiudong.sso.common.util.SsoOauth2Util#getAccessToken(String, String, String, String, String)} 并调用SsoSessionUtil.setAccessToken存入session
 *        2.3 前端在访问接口的时候携带 sessionId或者token
 * </p>
 *
 * 原本是通过此  SeparationLoginFilter 进行控制的，但是这种控制对后端耦合太强
 * @see SeparationLoginFilter
 * @author Vic.xu
 * @since 2022-08-15 11:40
 */
public class ApiLoginFilter {
}
