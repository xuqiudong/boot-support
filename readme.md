## 重新搭建一个springboot的支持框架基于2.x最新版本

|时间|本地version|springboot|
|---|---|---|
|2022-04-29|1.0.0|2.6.7|
|2022-08-12|1.0.1|2.7.2|

原来的基于： 2.1.7.RELEASE

####  changelog

1. starter 修改
>  spring.factories (org.springframework.boot.autoconfigure.EnableAutoConfiguration=xx.XxxAutoConfiguration)
> 修改为
> spring/org.springframework.boot.autoconfigure.AutoConfiguration.imports (xx.XxxAutoConfiguration)

参见：

- [boot)](https://gitee.com/xuqiudong/boot)
- [vic-environment](https://gitee.com/xuqiudong/vic-environment)



### 一些properties说明

- `rversion`: 版本占位符， 利用 `flatten-maven-plugin` 进行版本管理

  - [maven-ci-friendly](https://maven.apache.org/maven-ci-friendly.html )

  - [flatten-maven-plugin usage](https://www.mojohaus.org/flatten-maven-plugin/usage.html)
- `vic.support.version`： 依赖其他子项目的版本。默认为 **project.version**



#### 跳过一些插件的配置：

- `skip.pmd`：  跳过阿里巴巴 p3c-pmd

- `skip.spotbugs` ： 跳过spotbugs 静态代码检测

#### 模块说明

##### 01：模块:  lcxm-common-base： 

> 一个jar， spring web项目的一些基类  。

**backage说明：**

- aspect: 切面：
- authentication： 权限
- cache： 缓存
- captcha： 验证码
- controller： controller基类
- craw： 基于JSOUP的http请求封装
- enums: 常用的枚举，或枚举的父接口
- exception: 一些通用的异常
- handler：一些通用处理，如json序列相关
- lookup：通用查询条件
- mapper：mybatis mapper基类
- model：常用model
- service：service基类
- statistics：目的一些基本信息统计
- tool：一些系统级的工具类
- transmission：和其他系统的数据传输的封装，比如：数据上报
- vo： 常用vo
- web：如拦截器，全局异常捕捉

#### 02  lcxm-common-util

> 一个jar，一些常用的工具类

**package说明：**

- async:   批量异步操作
- collections： 集合比较，差集，并集，交集
- date：时间
- encrypt：加解密。Aes，RSA, Base62, Base62enhance等
- exception：异常
- lambda： LambdaIncrease-Lambda 中的自增
- poi:  excel 处理
- redis: jedis util
- reflect: util，以及基于反射的两个对象的比较
- sql: sql语句处理，jsqlparser
- thread:  线程池工具，线程工厂，批量操作
- web：web相关的一些工具类
- 其他：
  - 汉语转拼音
  - properties读取
  - 通用util
  - json util
  - cookie util
  - ListUtil
  - Number util
  - 正则util
  - mac地址
  - string util



### 感谢：JetBrains 

感谢[JetBrains](https://www.jetbrains.com/community/opensource/#support) <img src="assert/jb_beam.png" alt="jb_beam" />对本项目的支持，不胜感激！

