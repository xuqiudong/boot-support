package cn.xuqiudong.demo.oss.base;

import cn.xuqiudong.demo.oss.OssConfig;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;

/**
 * 描述: 放一下常量
 * @author Vic.xu
 * @since 2022-11-07 10:57
 */
public abstract class BaseDemo extends OssConfig {

    // 创建OSSClient实例。
    protected static OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
}
