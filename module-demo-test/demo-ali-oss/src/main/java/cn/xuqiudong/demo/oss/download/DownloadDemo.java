package cn.xuqiudong.demo.oss.download;

import cn.xuqiudong.demo.oss.base.BaseDemo;
import com.aliyun.oss.model.OSSObject;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 描述:deload
 * @author Vic.xu
 * @since 2022-12-30 15:06
 */
public class DownloadDemo extends BaseDemo {

    private static String objectName = "test/img/3.png";

    static {

        objectName = "12869/2fda06b9021547a4b9399021fca4f1aa-weixintupian7.png";
        objectName = "12889/eed478d205c845959ea147a1e60d252f-test20230110.docx";
    }

    public static void main(String[] args) throws IOException {

        boolean found = ossClient.doesObjectExist(bucketName, objectName);
        if (!found) {
            System.out.println(objectName + "文件不存在");
            return;
        }
        OSSObject object = ossClient.getObject(bucketName, objectName);
        InputStream inputStream = object.getObjectContent();
        File file = new File("d:/desk/22test20230110.docx");
        //FileUtils.copyInputStreamToFile(inputStream, file);
        FileCopyUtils.copy(inputStream, new FileOutputStream(file));
    }

}
