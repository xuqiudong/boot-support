package cn.xuqiudong.demo.oss.upload;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.demo.oss.base.BaseDemo;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import org.apache.commons.lang3.time.DateUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-11-07 10:45
 */
public class UploadDemo extends BaseDemo {

    private static String objectName = "test/img/1.png";

    public static void main(String[] args) {
        upload();
        //getUrl();

    }

    public static void upload() {

        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。


        try {
            // 填写字符串。
            String content = "D:\\document\\pic\\logo\\2.png";
            InputStream bi = new FileInputStream(new File(content));
            // 创建PutObjectRequest对象。
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, bi);

            // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
            // ObjectMetadata metadata = new ObjectMetadata();
            // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
            // metadata.setObjectAcl(CannedAccessControlList.Private);
            // putObjectRequest.setMetadata(metadata);

            // 上传字符串。
            PutObjectResult result = ossClient.putObject(putObjectRequest);
            JsonUtil.printJson(result);

            URL url = ossClient.generatePresignedUrl(bucketName, objectName, new Date());
            System.out.println(url);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }


    public static void getUrl() {
        Date date = DateUtils.addMinutes(new Date(), 1);
        URL url = ossClient.generatePresignedUrl(bucketName, objectName, date);
        System.out.println(url);
    }

}
