package cn.xuqiudong.demo.oss.bucket;

import cn.xuqiudong.demo.oss.base.BaseDemo;
import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.Bucket;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-11-07 10:56
 */
public class BucketDemo extends BaseDemo {

    public static void main(String[] args) {
        list();
    }

    public static void list() {


        try {
            // 列举当前账号所有地域下的存储空间。
            List<Bucket> buckets = ossClient.listBuckets();
            System.out.println("buckets size = " + buckets.size());
            for (Bucket bucket : buckets) {
                System.out.println(" - " + bucket.getName());
            }
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }
}
