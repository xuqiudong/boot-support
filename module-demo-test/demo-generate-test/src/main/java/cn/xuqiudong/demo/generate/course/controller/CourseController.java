package cn.xuqiudong.demo.generate.course.controller;

import cn.xuqiudong.common.base.controller.BaseController;
import cn.xuqiudong.demo.generate.course.model.Course;
import cn.xuqiudong.demo.generate.course.service.CourseService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 说明 :  控制层
 * @author Vic.xu
 * @since  2022-05-12 17:26
 */
@RestController
@RequestMapping("/course/course")
public class CourseController extends BaseController<CourseService, Course> {

}
