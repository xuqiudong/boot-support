package cn.xuqiudong.demo.generate.course.model;


import cn.xuqiudong.common.base.model.BaseEntity;


/**
 *  实体类
 *
 * @author Vic.xu
 */
public class Course extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    private String name;


    /***************** set|get  start **************************************/
    /**
     * set：
     */
    public Course setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * get：
     */
    public String getName() {
        return name;
    }
    /***************** set|get  end **************************************/
}
