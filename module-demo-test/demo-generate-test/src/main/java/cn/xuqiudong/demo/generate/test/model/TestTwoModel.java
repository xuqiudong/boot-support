package cn.xuqiudong.demo.generate.test.model;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-10-31 9:45
 */
public class TestTwoModel extends BaseTestModel {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String type() {
        return "two";
    }
}
