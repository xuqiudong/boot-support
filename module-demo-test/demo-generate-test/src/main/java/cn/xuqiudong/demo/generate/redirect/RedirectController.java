package cn.xuqiudong.demo.generate.redirect;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-01-13 11:45
 */
@Controller
@RequestMapping("/redirect")
public class RedirectController {


    @GetMapping("/go")
    public ModelAndView go(HttpServletRequest request, HttpServletResponse response, String test) throws IOException {
        if (StringUtils.isNotBlank(test)) {
            String url = "http://1233baidu.com";
            response.sendRedirect(url);
            System.out.println("after request " + LocalTime.now());
            return new ModelAndView("");

        }
        return new ModelAndView("index");
    }

    @GetMapping("/param")
    @ResponseBody
    public Object param(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> result = new HashMap<>();
        Map<String, String[]> parameterMap = request.getParameterMap();
        result.put("parameterMap", parameterMap);
        Map<String, Object> getParameter = new HashMap<>();
        String SSIAuth = request.getParameter("SSIAuth");
        String SSISign = request.getParameter("SSISign");
        getParameter.put("SSIAuth", SSIAuth);
        getParameter.put("SSISign", SSISign);
        result.put("getParameter", getParameter);


        return result;
    }

}
