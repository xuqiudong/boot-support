package cn.xuqiudong.demo.generate.deferred.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-17 13:57
 */
@Service
public class TestService {

    public String doSomething(){
        return RandomStringUtils.random(10,"qwertyuiop123456789asdfghjkl" );
    }
}
