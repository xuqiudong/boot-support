package cn.xuqiudong.demo.generate.filter;

import cn.xuqiudong.common.util.web.WebCommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-24 9:43
 */
@WebFilter
public class TestFilterOne implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(TestFilterOne.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("TestFilterOne");
        WebCommonUtils.storageRequestParameterToSession((HttpServletRequest) servletRequest);
        filterChain.doFilter(servletRequest, servletResponse);
    }


}
