package cn.xuqiudong.demo.generate.test.model;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-10-31 9:44
 */
public abstract class BaseTestModel {

    public abstract String type();
}
