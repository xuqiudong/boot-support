package cn.xuqiudong.demo.generate.dowanload;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-12-30 15:23
 */
@Controller
public class DownloadController {

    @GetMapping("/download")
    public String visit(HttpServletResponse response, int i) throws IOException {
        if (i > 0) {
            File file = new File("d:/desk/b.txt");
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment; filename=\"" + file.getName() + "\"");
            FileCopyUtils.copy(new FileInputStream(file), response.getOutputStream());
            return "";
        }
        return "redirect:a.txt";
    }
}
