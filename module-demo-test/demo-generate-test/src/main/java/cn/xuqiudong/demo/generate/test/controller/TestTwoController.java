package cn.xuqiudong.demo.generate.test.controller;

import cn.xuqiudong.demo.generate.test.model.TestTwoModel;
import cn.xuqiudong.demo.generate.test.service.TestTwoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-10-31 9:47
 */
@RestController
@RequestMapping("/two")
public class TestTwoController extends BaseTestController<TestTwoService, TestTwoModel> {
}
