package cn.xuqiudong.demo.generate.test.service;

import cn.xuqiudong.demo.generate.test.model.TestOneModel;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-10-31 9:46
 */
public class TestTwoService extends BaseTestService<TestOneModel> {
    @Override
    public String print() {
        return "I am two";
    }
}
