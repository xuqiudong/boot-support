package cn.xuqiudong.demo.generate.test.controller;

import cn.xuqiudong.demo.generate.test.model.BaseTestModel;
import cn.xuqiudong.demo.generate.test.service.BaseTestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 描述: test   @Resource AND  @Autowired
 * @author Vic.xu
 * @since 2022-10-31 9:43
 */
public abstract class BaseTestController<S extends BaseTestService, T extends BaseTestModel> {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    protected S service;

    @RequestMapping("/save")
    public String save() {
        return service.print();
    }

}
