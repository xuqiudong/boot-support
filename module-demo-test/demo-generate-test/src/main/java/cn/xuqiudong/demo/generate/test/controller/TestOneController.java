package cn.xuqiudong.demo.generate.test.controller;

import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.demo.generate.test.model.TestOneModel;
import cn.xuqiudong.demo.generate.test.service.TestOneService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-10-31 9:47
 */
@RestController
@RequestMapping("/one")
public class TestOneController extends BaseTestController<TestOneService, TestOneModel> {


    @RequestMapping("/cross")
    public BaseResponse<?> cross(HttpServletResponse response) {
        logger.info("into cross request");
        response.setHeader("Access-Control-Allow-Origin", "*");
        /* response.setHeader("Access-Control-Allow-Methods", "*");*/
        return BaseResponse.success(LocalTime.now());
    }
}
