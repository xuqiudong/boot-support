package cn.xuqiudong.demo.generate.course.mapper;

import cn.xuqiudong.common.base.mapper.BaseMapper;
import cn.xuqiudong.demo.generate.course.model.Course;

/**
 * 说明 :  Mapper
 * @author Vic.xu
 * @since  2022-05-12 17:26
 */
public interface CourseMapper extends BaseMapper<Course> {

}
