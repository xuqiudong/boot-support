package cn.xuqiudong.demo.generate.web;

import cn.xuqiudong.common.base.web.intercept.LogTraceIdInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 描述: web  configuration
 * @author Vic.xu
 * @since 2022-12-12 11:21
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebConfig.class);

    /**
     *  日志追踪拦截器
     * @param registry InterceptorRegistry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LOGGER.info("Adding LogTraceIdInterceptor interceptors");
        registry.addInterceptor(new LogTraceIdInterceptor()).addPathPatterns("/**");
    }
}
