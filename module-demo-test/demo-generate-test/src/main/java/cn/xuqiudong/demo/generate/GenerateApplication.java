package cn.xuqiudong.demo.generate;

import cn.xuqiudong.common.util.web.WebCommonUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.DispatcherServlet;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述: 测试代码生成
 * @author Vic.xu
 * @since 2022-05-12 16:38
 */
@SpringBootApplication
@MapperScan("cn.xuqiudong.demo.generate.**.mapper")
@ServletComponentScan
//@EnableAsync
public class GenerateApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(GenerateApplication.class);

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(GenerateApplication.class, args);
        //ProjectInfoStatistics.controller(context);
        // ProjectInfoStatistics.service(context);
    }

    //@Bean
    public ServletRegistrationBean<DispatcherServlet> dispatcherServlet() {
        ServletRegistrationBean<DispatcherServlet> registration = new ServletRegistrationBean(
                new DispatcherServlet(), "/");
        registration.setAsyncSupported(true);
        return registration;
    }


    @Value("${email.test}")
    private String email;

    @PostConstruct
    private void init() {
        System.out.println("email: " + email);
    }

    @RestController
    @RequestMapping
    static class Index {


        @RequestMapping("/")
        public Map<String, Object> index(HttpServletRequest request) {
            Map<String, Object> map = new HashMap<>();
            map.put("now", LocalTime.now());
            map.put("other", "test duplicate  json");
            map.put("test", WebCommonUtils.getAndRemoveSessionAttribute(request, "test"));
            map.put("aa", WebCommonUtils.getAndRemoveSessionAttribute(request, "aa"));
            LOGGER.info("into root");
            return map;
        }
    }
}
