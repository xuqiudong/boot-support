package cn.xuqiudong.demo.generate.email;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-12-14 11:26
 */
@Component
public class TestSendMail {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.from}")
    private String from;


    //@PostConstruct
    private void init() {
        System.err.println("TestSendMail send mail; from=" + from);
        //创建SimpleMailMessage对象
        SimpleMailMessage message = new SimpleMailMessage();
        //邮件发送人
        message.setFrom(from);
        //邮件接收人
        message.setTo("84597585@qq.com");
        //邮件主题
        message.setSubject("this is a test message " + LocalDate.now());
        //邮件内容
        message.setText("nothing test message " + LocalDate.now());
        //发送邮件
        mailSender.send(message);
    }
}
