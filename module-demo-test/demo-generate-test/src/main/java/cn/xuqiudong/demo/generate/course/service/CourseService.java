package cn.xuqiudong.demo.generate.course.service;

import cn.xuqiudong.common.base.service.BaseService;
import cn.xuqiudong.demo.generate.course.mapper.CourseMapper;
import cn.xuqiudong.demo.generate.course.model.Course;
import org.springframework.stereotype.Service;

/**
 * 说明 :  Service
 * @author Vic.xu
 * @since  2022-05-12 17:26
 */
@Service
public class CourseService extends BaseService<CourseMapper, Course> {

    @Override
    protected boolean hasAttachment() {
        return false;
    }

}
