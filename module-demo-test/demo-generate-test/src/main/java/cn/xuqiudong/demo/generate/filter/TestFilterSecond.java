package cn.xuqiudong.demo.generate.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-24 9:43
 */
@WebFilter
public class TestFilterSecond implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(TestFilterSecond.class);

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        logger.info("TestFilterSecond");
        filterChain.doFilter(servletRequest, servletResponse);
        /*filterChain.doFilter(servletRequest, servletResponse);
        filterChain.doFilter(servletRequest, servletResponse);
        */
    }
}
