package cn.xuqiudong.demo.generate;

import cn.xuqiudong.demo.generate.course.model.Course;
import cn.xuqiudong.demo.generate.course.service.CourseService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-05-12 17:30
 */
public class TestGenerate extends BaseTest {

    @Autowired
    private CourseService courseService;

    @Test
    public void insert() {
        Course course = new Course();
        course.setName("t-0");
        courseService.insert(course);
    }

    @Test
    public void batchInsert() {
        List<Course> list = new ArrayList<>();
        for (int i = 0; i < 1500; i++) {
            Course course = new Course();
            course.setName("t-" + (i + 1));
            list.add(course);
        }
        courseService.batchInsert(list);
    }
}
