package cn.xuqiudong.demo.generate.statistics;

import cn.xuqiudong.demo.generate.course.model.Course;
import org.apache.ibatis.javassist.ClassPool;
import org.apache.ibatis.javassist.CtClass;
import org.apache.ibatis.javassist.CtMethod;
import org.apache.ibatis.javassist.bytecode.CodeAttribute;
import org.apache.ibatis.javassist.bytecode.LineNumberAttribute;
import org.apache.ibatis.javassist.bytecode.MethodInfo;

import java.lang.reflect.Method;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-11-24 11:13
 */
public class StatisticsModel {

    private String id;

    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTest() {
        int num = ThreadLocalRandom.current().nextInt(0, 10);
        String result = "产生的随机数为：" + num;
        if (num > 5) {
            return result + " 大于5";
        }
        int i = 1 + 2;
        return result + " 不大于5";
    }


    public static void main(String[] args) throws Exception {
        StatisticsModel model = new StatisticsModel();
        model.setName("张三");

        Class<?> clazz = Course.class;
        Method method = Stream.of(clazz.getMethods()).
                filter(m -> "setName".equalsIgnoreCase(m.getName())).findFirst().get();
        ClassPool pool = ClassPool.getDefault();
        CtClass cc = pool.get(method.getDeclaringClass().getCanonicalName());
        CtMethod javassistMethod = cc.getDeclaredMethod(method.getName());
        int linenumber = javassistMethod.getMethodInfo().getLineNumber(0);
        MethodInfo methodInfo = javassistMethod.getMethodInfo();
        CodeAttribute ca = methodInfo.getCodeAttribute();
        LineNumberAttribute ainfo = (LineNumberAttribute) ca.getAttribute("LineNumberTable");
        int length = ainfo.tableLength();
        System.out.println(length);
        System.out.println(linenumber);
    }

}
