package cn.xuqiudong.demo.generate;

import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-05-12 17:30
 */
@SpringBootTest
public abstract class BaseTest {
}
