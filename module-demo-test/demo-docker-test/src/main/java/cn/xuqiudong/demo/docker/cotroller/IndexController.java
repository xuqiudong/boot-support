package cn.xuqiudong.demo.docker.cotroller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述: index
 * @author Vic.xu
 * @since 2024-02-21 13:54
 */
@RestController
@RequestMapping
public class IndexController {

    @Value("${spring.server.name}")
    private String name;

    @Resource
    private JdbcTemplate jdbcTemplate;

    @GetMapping(value = {"", "/"})
    public Object index(){
        String msg = "welcome to " + name;
        Map<String, Object> result = new HashMap<>();
        result.put("time", LocalTime.now());
        result.put("msg", msg);
        return result;
    }

    @GetMapping(value = {"/query"})
    public Object query(){
        String msg = "welcome to " + name;
        Map<String, Object> result = new HashMap<>();
        result.put("time", LocalTime.now());
        result.put("msg", msg);

        result.put("tags", tags());
        return result;
    }


    private List<Map<String, Object>> tags(){
        String sql = "SELECT a.id, a.`name` FROM blog_tag  a WHERE 1=1 LIMIT 5";
        List<Map<String, Object>> maps = jdbcTemplate.queryForList(sql);
        return maps;
    }
}
