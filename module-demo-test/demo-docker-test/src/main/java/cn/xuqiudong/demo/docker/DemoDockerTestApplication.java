package cn.xuqiudong.demo.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = {
})
public class DemoDockerTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoDockerTestApplication.class, args);
    }

}
