package cn.xuqiudong.demo.tongyilingma;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2024-03-15 10:01
 */
public class Demo001 {

    public static void main(String[] args) {

    }


    public static void buildTimeLines(List<Article> articles) {
        //articles.stream().collect(Collectors.groupingBy())
    }



    public static List<Article> buildArticles(String size) {
        //随机生成一个Article对象的List列表
        return null;
    }


    /**
     * 文章时间轴对象
     */
    @Data
    public static class TimeLine {

        private int year;

        private int month;

        private List<Article> articleList;


    }

    /**
     * 文章列表
     */
    @Data
    public static class Article {

        private Integer id;

        private String title;

        private String author;

        private String content;

        private String summary;

        private Date createTime;


    }

}
