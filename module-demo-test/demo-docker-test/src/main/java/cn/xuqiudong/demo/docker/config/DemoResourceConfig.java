package cn.xuqiudong.demo.docker.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.CacheControl;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 描述: demo for WebMvcConfigurer
 * @author Vic.xu
 * @since 2024-02-26 9:07
 */
@Configuration
public class DemoResourceConfig implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/js/**").addResourceLocations("classpath:/public/js/");
        registry.addResourceHandler("/css/**").addResourceLocations("classpath:/public/css/");
        registry.addResourceHandler("/html/**").addResourceLocations("classpath:/public/html/")
                .setCacheControl(CacheControl.noStore());
        registry.addResourceHandler("/js2/**").addResourceLocations("classpath:/static2/js/");
        registry.addResourceHandler("/css2/**").addResourceLocations("classpath:/static2/css/");
        registry.addResourceHandler("/html2/**").addResourceLocations("classpath:/static2/html/")
                .setCacheControl(CacheControl.noStore());
        //registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
    }
}
