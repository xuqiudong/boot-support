package cn.xuqiudong.demo.dubbo.common.model.service;

import cn.xuqiudong.demo.dubbo.common.model.OneModel;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-02 10:56
 */
public interface OneServiceInterface {

    /**
     * search
     * @param id id
     * @return OneModel
     */
    OneModel findOne(Integer id);

    /**
     * OneModel list
     * @return List<OneModel>
     */
    List<OneModel> list();

    /**
     * delete
     * @param id id
     * @return number
     */
    int delete(Integer id);
}
