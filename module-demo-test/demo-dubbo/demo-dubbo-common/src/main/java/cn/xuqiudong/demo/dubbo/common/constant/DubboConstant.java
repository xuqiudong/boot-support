package cn.xuqiudong.demo.dubbo.common.constant;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-01-25 11:06
 */
public final class DubboConstant {

    public static String SESSION_INFO = "session_info_key";
}
