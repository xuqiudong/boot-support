package cn.xuqiudong.demo.dubbo.common.filter;

import cn.xuqiudong.demo.dubbo.common.tools.DubboTools;
import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 描述: dubbo  消费端filter， 加入traceId 信息(用于log4j日志链路追加，tranceId里包含sessionId 用于获取session信息)
 *
 * @author Vic.xu
 * @since 2022-01-24 10:04
 */
@Activate(group = CommonConstants.CONSUMER)
public class ConsumerTraceFilter implements Filter {

    private final Logger logger = LoggerFactory.getLogger(ConsumerTraceFilter.class);

    @Override
    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        //RpcContext.getContext().setAttachment(DubboTools.TRACE_ID, tranceId);
        setAttachment(invoker, invocation);

        return invoker.invoke(invocation);
    }

    private void setAttachment(Invoker<?> invoker, Invocation invocation){
        invocation.getMethodName();
        String tranceId = DubboTools.generateTranceId();
        String url = invoker.getUrl().getAddress();
        url += "/" + invoker.getInterface().getSimpleName();
        logger.info("set tranceId = [{}], current url is [{}]", tranceId, url);
        invocation.setAttachment(DubboTools.TRACE_ID, tranceId);
    }
}
