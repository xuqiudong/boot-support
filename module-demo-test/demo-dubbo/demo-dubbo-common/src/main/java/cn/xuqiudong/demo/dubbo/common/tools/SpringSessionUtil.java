package cn.xuqiudong.demo.dubbo.common.tools;

import org.springframework.session.Session;
import org.springframework.session.SessionRepository;
import org.springframework.stereotype.Component;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-01-25 10:49
 */
@Component
public class SpringSessionUtil {

    private static SessionRepository<?> sessionRepository;

    /**
     * 构造方法注入
     * @param sessionRepository
     */
    public SpringSessionUtil(SessionRepository<?> sessionRepository) {
        SpringSessionUtil.sessionRepository = sessionRepository;
    }


    public static Session springSession(String sessionId){
        if (sessionRepository == null) {
            return null;
        }
        Session session = sessionRepository.findById(sessionId);
        return session;
    }
}
