package cn.xuqiudong.demo.dubbo.common.model;

import java.io.Serializable;
import java.util.Date;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-02 10:51
 */
public class OneModel implements Serializable {

    private static final long serialVersionUID = 2562732385050120930L;

    private Integer id;

    private String name;

    private Integer age;

    private Date birthDay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}
