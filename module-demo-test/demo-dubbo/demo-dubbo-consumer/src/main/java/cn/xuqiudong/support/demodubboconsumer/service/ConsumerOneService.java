package cn.xuqiudong.support.demodubboconsumer.service;

import cn.xuqiudong.demo.dubbo.common.model.OneModel;
import cn.xuqiudong.demo.dubbo.common.model.service.OneServiceInterface;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-03 10:31
 */
@Service
public class ConsumerOneService implements OneServiceInterface{

    @DubboReference
    private OneServiceInterface oneServiceInterface;

    @Override
    public OneModel findOne(Integer id) {
        return oneServiceInterface.findOne(id);
    }

    @Override
    public List<OneModel> list() {
        return oneServiceInterface.list();
    }

    @Override
    public int delete(Integer id) {
        return oneServiceInterface.delete(id);
    }

}
