package cn.xuqiudong.support.demodubboconsumer;

import cn.xuqiudong.demo.dubbo.common.model.OneModel;
import cn.xuqiudong.support.demodubboconsumer.service.ConsumerOneService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-03 10:33
 */
@SpringBootTest
public class TestConsumerOneService {

    @Autowired
    private ConsumerOneService consumerOneService;

    ObjectMapper mapper = new ObjectMapper();


    @Test
    public void findOne(){
        OneModel one = consumerOneService.findOne(12);
        printJson(one);
    }

    @Test
    public void list(){
        List<OneModel> list = consumerOneService.list();
        printJson(list);
    }

    @Test
    public void delete(){
        int num = consumerOneService.delete(43);
        printJson(num);
    }

    private void printJson(Object object){
        if (object == null) {
            System.out.println("object is null");
        }
        try {
            String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(object);
            System.out.println(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            System.out.println(object);
        }

    }
}
