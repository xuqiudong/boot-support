package cn.xuqiudong.demo.dubbo.provider.service;

import cn.xuqiudong.demo.dubbo.common.model.OneModel;
import cn.xuqiudong.demo.dubbo.common.model.service.OneServiceInterface;
import org.apache.dubbo.config.annotation.DubboService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-03-02 11:36
 */
@DubboService
public class OneServiceImpl implements OneServiceInterface {

    private static final Logger LOGGER = LoggerFactory.getLogger(OneServiceImpl.class);

    @Override
    public OneModel findOne(Integer id) {
        LOGGER.info("into findOne, id: {}", id);
        id = id == null ? 100 : id;
        OneModel one = new OneModel();
        one.setId(id);
        one.setName(id / 2 == 0 ? "张三" : "李四" + LocalTime.now());
        one.setAge(new Random().nextInt(60));
        one.setBirthDay(new Date());
        return one;
    }

    @Override
    public List<OneModel> list() {
        LOGGER.info("into list ");
        List<OneModel> list = new ArrayList<OneModel>();
        int k = new Random().nextInt(15);
        for (int i = 0; i < 5; i++) {
            list.add(findOne(k + i));
        }
        return list;
    }

    @Override
    public int delete(Integer id) {
        LOGGER.info("into delete, id: {}", id);
        id = id == null ? 0 : id;
        int k = new Random().nextInt(25);
        return id + k;
    }
}
