package cn.xuqiudong.demo.mvc;

import cn.xuqiudong.demo.mvc.base.ResponseWithTime;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 根据需要扫面不同的子模块，测试子模块
 * @author Vic.xu
 */
@SpringBootApplication(scanBasePackages = {
		"cn.xuqiudong.demo.mvc.base",
		/*i8n 模块*/
		"cn.xuqiudong.demo.mvc.i18n",
		/*redis 模块*/
		//"cn.xuqiudong.demo.mvc.redis",
		/*error 模块*/
		"cn.xuqiudong.demo.mvc.error",

})
public class DemoGenericMvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGenericMvcApplication.class, args);
	}


	@RestController
	static class Index {

		@RequestMapping(value = "/")
		public ResponseWithTime<String> index(HttpServletRequest request){
			String sid = request.getSession().getId();
			return new ResponseWithTime<>("sessionId = " + sid);
		}

	}
}
