package cn.xuqiudong.demo.mvc.base;

import java.time.LocalTime;

/**
 * 说明:包含当前时间的响应
 *
 * @author Vic.xu
 * @since 2023/4/19/0019 15:11
 */
public class ResponseWithTime <T>{

    private T data;

    private LocalTime now;

    public ResponseWithTime(){
        this.now = LocalTime.now();
    }

    public ResponseWithTime(T data){
        this();
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public LocalTime getNow() {
        return now;
    }

    public void setNow(LocalTime now) {
        this.now = now;
    }
}
