package cn.xuqiudong.demo.mvc.redis;

import cn.xuqiudong.demo.mvc.base.AbstractModuleStart;
import org.springframework.stereotype.Component;

/**
 * 说明: redis 模块
 *
 * @author Vic.xu
 * @since 2023/4/28/0028 16:00
 */
@Component
public class RedisStart extends AbstractModuleStart {


    @Override
    public String moduleName() {
        return "redis";
    }
}
