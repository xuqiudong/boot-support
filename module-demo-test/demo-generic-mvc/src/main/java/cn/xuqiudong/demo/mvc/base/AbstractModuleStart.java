package cn.xuqiudong.demo.mvc.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/4/28/0028 16:01
 */
public abstract class AbstractModuleStart {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    public abstract String moduleName();

    @PostConstruct
    public void init(){
        logger.warn("===============模块【{}】启用了！！！===============", moduleName());
    }

}
