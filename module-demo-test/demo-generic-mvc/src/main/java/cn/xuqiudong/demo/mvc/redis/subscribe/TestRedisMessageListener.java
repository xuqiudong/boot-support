package cn.xuqiudong.demo.mvc.redis.subscribe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;

/**
 * 说明: 测试redis 订阅者
 *
 * @author Vic.xu
 * @since 2023/4/28/0028 16:39
 */
@Component
public class TestRedisMessageListener implements MessageListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestRedisMessageListener.class);

    private static final StringRedisSerializer STRING_REDIS_SERIALIZER = new StringRedisSerializer();

    @Override
    public void onMessage(Message message, byte[] pattern) {
        StringBuilder stringBuilder = new StringBuilder("接收到消息了");
        byte[] channelBytes = message.getChannel();
        byte[] body = message.getBody();
        String content = STRING_REDIS_SERIALIZER.deserialize(body);
        String channel = STRING_REDIS_SERIALIZER.deserialize(channelBytes);
        String patternStr = new String(pattern);
        stringBuilder.append("\n\t pattern = ").append(patternStr);
        stringBuilder.append("\n\t channel = ").append(channel);
        stringBuilder.append("\n\t content = ").append(content);
        LOGGER.info(stringBuilder.toString());


    }
}
