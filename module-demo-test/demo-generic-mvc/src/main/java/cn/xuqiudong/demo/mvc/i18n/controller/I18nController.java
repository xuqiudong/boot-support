package cn.xuqiudong.demo.mvc.i18n.controller;

import cn.xuqiudong.demo.mvc.base.ResponseWithTime;
import cn.xuqiudong.demo.mvc.i18n.helper.I18nHelper;
import cn.xuqiudong.demo.mvc.i18n.service.I18nService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/4/19/0019 15:09
 */
@RestController
@RequestMapping("/i18n")
public class I18nController {

    @Autowired
    private I18nService i18nService;

    @GetMapping("/get")
    public ResponseWithTime<String> get(@RequestParam(defaultValue = "demo.name") String code) {
        String msg = i18nService.getMessage(code);
        return new ResponseWithTime<>(msg);
    }

    @GetMapping("/switch")
    public ResponseWithTime<String> switchLocal(HttpServletRequest request, HttpServletResponse response,
                                                String locale,
                                                @RequestParam(defaultValue = "demo.name") String code) {
        I18nHelper.switchLocale(request, response, StringUtils.parseLocale(locale));
        String msg = i18nService.getMessage(code);
        return new ResponseWithTime<>(i18nService.getMessage(msg));

    }

    public static void main(String[] args) {
        String localeValue = "zh_cN";
        System.out.println(localeValue);
        Locale locale1 = StringUtils.parseLocale(localeValue);
        System.out.println(locale1);
        System.out.println(Locale.SIMPLIFIED_CHINESE);
    }
}
