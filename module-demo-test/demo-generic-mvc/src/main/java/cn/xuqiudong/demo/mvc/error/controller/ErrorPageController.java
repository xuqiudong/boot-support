package cn.xuqiudong.demo.mvc.error.controller;

import cn.xuqiudong.demo.mvc.base.ResponseWithTime;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 描述:
 * @author Vic.xu
 * @date 2023-12-01 9:20
 */
@RestController
public class ErrorPageController {

    @RequestMapping("/error2")
    public ResponseWithTime<Object> error(){
        ResponseWithTime<Object> response = new ResponseWithTime<>();
        response.setData("this is error");
        return response;
    }
}
