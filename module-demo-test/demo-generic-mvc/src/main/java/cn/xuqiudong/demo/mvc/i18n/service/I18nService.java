package cn.xuqiudong.demo.mvc.i18n.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

/**
 * 说明:国际化测试
 *
 * @author Vic.xu
 * @since 2023/4/19/0019 14:28
 */
@Service
public class I18nService {
    @Autowired
    MessageSource messageSource;


    public String getMessage(String key, Object...args){
        Locale locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(key, args, locale);
    }

}
