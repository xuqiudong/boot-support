package cn.xuqiudong.demo.mvc.i18n;

import cn.xuqiudong.demo.mvc.base.AbstractModuleStart;
import org.springframework.stereotype.Component;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/4/28/0028 16:00
 */
@Component
public class I18nStart extends AbstractModuleStart {


    @Override
    public String moduleName() {
        return "国际化i18n";
    }
}
