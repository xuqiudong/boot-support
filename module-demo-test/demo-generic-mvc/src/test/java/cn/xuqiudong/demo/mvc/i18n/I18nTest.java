package cn.xuqiudong.demo.mvc.i18n;

import cn.xuqiudong.demo.mvc.i18n.service.I18nService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/4/19/0019 14:54
 */
@SpringBootTest
public class I18nTest {

    @Autowired
    private I18nService i18nService;


    @Test
    public void get(){
        String code = "demo.name";
        String message = i18nService.getMessage(code);
        System.out.println(message);
    }
}
