package pers.vic.attachment;

import cn.xuqiudong.attachment.AttachmentApplication;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-11-17 11:17
 */
@SpringBootTest(classes = AttachmentApplication.class)
public abstract class BaseTest {
}
