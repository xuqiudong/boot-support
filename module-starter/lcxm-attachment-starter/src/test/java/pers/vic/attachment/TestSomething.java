package pers.vic.attachment;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

/**
 * 描述:
 * @author Vic.xu
 * @since 2023-02-07 16:37
 */
public class TestSomething {

    public static void main(String[] args) {
        List<String> list = getList();
        System.out.println(String.join(",", list));
        Function<String, String> function = x -> x + " - " + new Random().nextInt(100);
       /* dosome(list, new Dosome() {

            @Override
            String doSome(String s) {
                return s + " - " + new Random().nextInt(100);
            }
        });*/
        dosome(list, x -> x + " - " + new Random().nextInt(100));
        System.out.println(String.join(",", list));
    }

    public static List<String> getList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            list.add(RandomStringUtils.random(10, "asddsfgfhghghg21hf645gf987654987"));
        }
        return list;
    }


    public static void dosome(List<String> list, Function<String, String> dosome) {
        for (int i = 0; i < list.size(); i++) {
            String s1 = dosome.apply(list.get(i));
            list.set(i, s1);
        }

    }

    public static void dosome(List<String> list, Dosome dosome) {
        for (int i = 0; i < list.size(); i++) {
            String s1 = dosome.doSome(list.get(i));
            list.set(i, s1);
        }

    }
}

abstract class Dosome {

    abstract String doSome(String s);
}
