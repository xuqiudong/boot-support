package pers.vic.attachment;

import cn.xuqiudong.attachment.dao.BaseAttachmentDao;
import cn.xuqiudong.attachment.model.Attachment;
import cn.xuqiudong.attachment.service.AttachmentService;
import cn.xuqiudong.common.util.JsonUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 说明 :  测试附件
 * @author  Vic.xu
 * @since  2019年12月11日 上午10:55:17
 */
public class AttachmentTest extends BaseTest {

    ObjectMapper mapper = new ObjectMapper();

    @Resource
    BaseAttachmentDao attachmentDao;

    @Resource
    private AttachmentService attachmentService;


    @Test
    public void add() throws IOException {
        File file = new File("D:\\desk\\default");
        MultipartFile multipartFile = new MockMultipartFile(file.getName(), new FileInputStream(file));
        Attachment test = attachmentService.upfile(multipartFile, "test");
        JsonUtil.printJson(test);
    }

    //1. insert
    @Test
    public void insert() throws JsonProcessingException {
        Attachment a = new Attachment();
        a.setAbsolutePath("4567");
        a.setRelativePath("8888");
        a.setContentType("xx888");
        a.setFilename("test88");
        a.setOriginalName("ooo88oo");
        attachmentDao.insert(a);
        System.out.println(mapper.writeValueAsString(a));
    }

    //2. query
    @Test
    public void query() throws JsonProcessingException {
        Attachment a = attachmentDao.selectAttachmentById(3);
        System.out.println(mapper.writeValueAsString(a));
    }

    //3. list
    @Test
    public void list() throws JsonProcessingException {
        List<Attachment> list = attachmentDao.selectTemporaryAttachments();
        System.out.println(mapper.writeValueAsString(list));
    }

    //4 update
    @Test
    public void update() {
        attachmentDao.updateTemporary(1, false);
        attachmentDao.updateTemporary("1,2,3", true);
    }

    @Test
    public void delete() {
        attachmentDao.delete(1);

        attachmentDao.delete(Arrays.asList("2,3".split(",")).stream().map(s -> Integer.parseInt((String) s)).collect(Collectors.toList()));
    }
}
