package pers.vic.attachment;

import cn.xuqiudong.attachment.dao.BaseAttachmentDao;
import cn.xuqiudong.attachment.dao.BaseAttachmentOssDao;
import cn.xuqiudong.attachment.helper.AliOssHelper;
import cn.xuqiudong.attachment.model.Attachment;
import cn.xuqiudong.attachment.schedule.OssUploadTask;
import cn.xuqiudong.common.util.JsonUtil;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述: tes  ali oss
 * @author Vic.xu
 * @since 2022-11-17 11:25
 */
public class AttachmentOssTest extends BaseTest {

    @Resource
    private BaseAttachmentOssDao attachmentOssDao;

    @Resource
    private AliOssHelper aliOssHelper;

    @Resource
    private OssUploadTask ossUploadTask;

    @Resource
    private BaseAttachmentDao attachmentDao;


    @Test
    public void list() {
        List<Attachment> notUploadedAttachmentList = attachmentOssDao.findNotUploadedAttachmentList(10);

        JsonUtil.printJson(notUploadedAttachmentList);
    }

    @Test
    public void upload() {
        ossUploadTask.upload();
    }


    @Test
    public void url() {
        Attachment attachment = attachmentDao.selectAttachmentById(1);
        String url = aliOssHelper.generatePreSignedUrl(attachment);
        System.out.println(url);
    }
}
