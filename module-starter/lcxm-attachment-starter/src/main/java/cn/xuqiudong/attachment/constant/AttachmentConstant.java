package cn.xuqiudong.attachment.constant;

/**
 * 描述:
 * @author Vic.xu
 * @since 2021-12-03 14:21
 */
public final class AttachmentConstant {

    /**
     * IDS  regex
     */
    public static final String IDS_REG = "(\\d+|\\d+-|\\d+-\\d+)+";

    /**
     * upfile  name
     */
    public static final String FILE_NAME = "upfile";

    /**
     * 附件表
     */
    public static final String ATTACHMENT_TABLE = "attachment";

    /**
     * Url: 前缀
     */
    public static final String PREFIX_API_URL = "/attachment";

    /**
     * URL： 修改一个文件的临时状态  /attachment/api/update
     */
    public static final String API_URL_ATTACHMENT_UPDATE = PREFIX_API_URL + "/api/update";
    /**
     * URL： 修改多个文件的临时状态 /attachment/api/batchUpdate
     */
    public static final String API_URL_ATTACHMENT_BATCH_UPDATE = PREFIX_API_URL + "/api/batchUpdate";
    /**
     * URL： 上传单个文件  /attachment/api/upfile
     */
    public static final String API_URL_ATTACHMENT_UP_FILE = PREFIX_API_URL + "/api/upfile";
    /**
     * URL：单个文件详情 /attachment/api/detail
     */
    public static final String API_URL_ATTACHMENT_DETAIL = PREFIX_API_URL + "/api/detail";

    /**
     * URL：多个个文件详情  /attachment/api/details
     */
    public static final String API_URL_ATTACHMENT_DETAILS = PREFIX_API_URL + "/api/details";

}
