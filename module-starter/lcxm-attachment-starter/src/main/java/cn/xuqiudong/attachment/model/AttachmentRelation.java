package cn.xuqiudong.attachment.model;

import cn.xuqiudong.common.base.model.BaseEntity;

/**
 * 描述:附件关系表， 满足某些情况不想使用
 * @author Vic.xu
 * @since 2022-07-07 11:08
 */
public class AttachmentRelation extends BaseEntity {

    private static final long serialVersionUID = 1611121786348772119L;
    /**
     * 附件id
     */
    private Integer attachmentId;

    /**
     * 业务表id
     */
    private String businessId;

    /**
     * 关联的业务表 表名
     */
    private String tableName;

    /**
     * 业务字段名
     */
    private String columnName;

    /**
     * 文件原始名 从附件表查出
     */
    protected String originalName;


    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }
}
