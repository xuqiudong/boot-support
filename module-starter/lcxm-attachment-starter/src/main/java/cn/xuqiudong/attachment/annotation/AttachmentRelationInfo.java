package cn.xuqiudong.attachment.annotation;

import cn.xuqiudong.attachment.model.AttachmentRelation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 附件表关系 标识注解 注解一个实体的说明， 如果对象不加此注解 则默认使用其className
 * @see AttachmentRelation 中的table字段
 * @author Vic.xu
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface AttachmentRelationInfo {

    /**
     * 当前实体的描述 默认取className  如果不加此注解的话
     */
    String value();

}
