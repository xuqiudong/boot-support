package cn.xuqiudong.attachment.model;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 描述: 表单附件提取结果
 *  1. 附件id 列表
 *  2. 关系列表 AttachmentRelation
 * @author Vic.xu
 * @since 2022-07-11 10:51
 */
public class AttachmentFetchResult {

    /**
     * 附件id列表, 包含了附件关系列表中的附件id
     */
    private List<Integer> idList;

    /**
     * 附件关系列表
     */
    private List<AttachmentRelation> relationList;

    public AttachmentFetchResult() {
        this.idList = new ArrayList<>();
        this.relationList = new ArrayList<>();
    }

    /**
     * 把关系中附件id提取到idList中
     */
    public void fetchRelation2IdList() {
        relationList.forEach(r -> idList.add(r.getAttachmentId()));
    }

    /**
     * 把id加到对象中
     * @param id attachment id
     */
    public void addId(Integer id) {
        this.idList.add(id);
    }

    /**
     * 把id加到对象中
     * @param ids attachment ids
     */
    public void addIds(List<Integer> ids) {
        this.idList.addAll(ids);
    }

    /**
     * 把AttachmentRelation  加当当前对象中， 且把其中的附件id提取出来加到idList中去
     * @param attachmentRelation 附件关系
     */
    public void addRelation(AttachmentRelation attachmentRelation) {
        if (attachmentRelation != null) {
            relationList.add(attachmentRelation);
            if (attachmentRelation.getAttachmentId() != null && attachmentRelation.getAttachmentId() > 0) {
                idList.add(attachmentRelation.getAttachmentId());
            }
        }
    }

    /**
     *  把AttachmentRelation 列表  加当当前对象中， 且把其中的附件id提取出来加到idList中去
     * @param attachmentRelations 附件关系 列表
     */
    public void addRelations(List<AttachmentRelation> attachmentRelations) {
        if (CollectionUtils.isNotEmpty(attachmentRelations)) {
            attachmentRelations.forEach(this::addRelation);
        }
    }

    /**
     * 全部附件id
     * @return
     */
    public List<Integer> getIdList() {
        return idList.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 全部附件关系
     * @return
     */
    public List<AttachmentRelation> getRelationList() {
        return relationList.stream().distinct().collect(Collectors.toList());
    }

    /**
     * 全部关系id
     */
    public List<Integer> getRelationIdList() {
        return getRelationList().stream().map(AttachmentRelation::getId).distinct().collect(Collectors.toList());
    }

}
