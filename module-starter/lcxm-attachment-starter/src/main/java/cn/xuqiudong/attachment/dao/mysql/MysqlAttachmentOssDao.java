package cn.xuqiudong.attachment.dao.mysql;

import cn.xuqiudong.attachment.constant.AttachmentConstant;
import cn.xuqiudong.attachment.dao.BaseAttachmentDao;
import cn.xuqiudong.attachment.dao.BaseAttachmentOssDao;
import cn.xuqiudong.attachment.model.Attachment;
import cn.xuqiudong.attachment.model.AttachmentOss;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * 描述: mysql  处理 attachment oss
 * @author Vic.xu
 * @since 2022-11-14 10:26
 */
public class MysqlAttachmentOssDao extends BaseAttachmentOssDao {

    @Override
    public AttachmentOss saveOrUpdate(AttachmentOss model) {
        AttachmentOss byAttachmentId = findByAttachmentId(model.getAttachmentId());
        if (byAttachmentId != null) {
            updateAttachmentOssByAttachmentId(model);
        } else {
            insert(model);
        }
        return model;
    }

    @Override
    public AttachmentOss insert(AttachmentOss model) {
        String sql = "INSERT INTO attachment_oss (attachment_id, object_name, origin_removed, result) " +
                " VALUE(?,?,?,?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, model.getAttachmentId());
                ps.setString(2, model.getObjectName());
                ps.setBoolean(3, model.getOriginRemoved());
                ps.setBoolean(4, model.getResult());
                return ps;
            }
        }, keyHolder);
        model.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
        return model;
    }

    @Override
    public List<Attachment> findNotUploadedAttachmentList(int size) {
        if (size == 0) {
            return Collections.emptyList();
        }
        String sql = "select " + BaseAttachmentDao.columns + " FROM " + AttachmentConstant.ATTACHMENT_TABLE +
                "  a LEFT JOIN attachment_oss b ON a.`id` = b.`attachment_id` " +
                " WHERE (b.id IS NULL  OR b.`result` != 1) and  a.temporary = 0 ";
        if (size > 0) {
            sql += " limit " + size;
        }
        return jdbcTemplate.query(sql, new BeanPropertyRowMapper<Attachment>(Attachment.class));

    }

    @Override
    public AttachmentOss findByAttachmentId(Integer attachmentId) {
        String sql = "SELECT a.id,  a.`object_name` AS objectName, a.`attachment_id` AS attachmentId,  " +
                "a.`origin_removed` AS originRemoved, a.`result`,  " +
                "a.`create_time` AS createTime, a.`update_time` AS updateTime " +
                "FROM attachment_oss a WHERE a.`attachment_id` = ?";
        List<AttachmentOss> list = jdbcTemplate.query(sql, new BeanPropertyRowMapper<AttachmentOss>(AttachmentOss.class), attachmentId);
        if (CollectionUtils.isNotEmpty(list)) {
            return list.get(0);
        }
        return null;
    }

    @Override
    public void updateAttachmentOssByAttachmentId(AttachmentOss model) {
        String sql = "UPDATE attachment_oss SET object_name = ?, original_name = ?, result = ? WHERE attachment_id = ?";
        Object[] args = {model.getObjectName(), model.getOriginRemoved(),
                model.getResult(), model.getAttachmentId()};
        jdbcTemplate.update(sql, args);
    }
}
