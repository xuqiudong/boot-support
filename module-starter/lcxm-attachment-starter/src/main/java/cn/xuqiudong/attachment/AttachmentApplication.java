package cn.xuqiudong.attachment;

import cn.xuqiudong.attachment.autoconfigure.AttachmentProperties;
import cn.xuqiudong.common.util.JsonUtil;
import org.springframework.boot.SpringApplication;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 一把情况下:
 * 启动器模块是一个空 JAR 文件，仅提供辅助性依赖管理，
 * 而自动配置模块应该再重新设计一个，然后启动器再去引用这个自动配置模块。
 * 也即:
 *  attachment-spring-boot-starter : 具体的代码
 *  应该依赖attachment-spring-boot-starter-autoconfigigure: 配置 辅助性依赖管理;
 *  本项目偷懒之处在于二者合并
 *
 * 描述 : 附件starter
 * @author VIC.xu
 * @since 2019年12月12日 上午11:00:02
 * @ SpringBootApplication
 */
//@SpringBootApplication
public class AttachmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(AttachmentApplication.class, args);
    }

    @Resource
    private AttachmentProperties attachmentProperties;

    @PostConstruct
    private void post() {
        JsonUtil.printJson(attachmentProperties.getAliOss());
    }

}
