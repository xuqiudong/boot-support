package cn.xuqiudong.attachment.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 附件标识注解
 * @author VIC
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface AttachmentFlag {

    //如果一个属性 尽量使用value  类型
    AttachmentType value() default AttachmentType.SIGN;

    /**
     * 当 为AttachmentType.RELATION 时候  标注的关系字段描述， 默认为filed name
     */
    String relationColumn() default "";


    public static enum AttachmentType {
        /** 1 单附件 */
        SIGN,
        /** 2 附件ids  形如1,2,3*/
        SIGNS,
        /** 3存在内容中的附件*/
        CONTENT,
        /**
         * 附件关系：多附件关系： List<AttachmentRelation> 或者 单附件关系：AttachmentRelation
         * 且必须追加注解 AttachmentRelationInfo  {@link AttachmentRelationInfo}
         * */
        RELATION
    }

}