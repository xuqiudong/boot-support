package cn.xuqiudong.attachment.model;

/**
 * 描述: 可用的客户端
 * table: client_config
 * @author Vic.xu
 * @since 2022-06-01 17:38
 */
public class ClientConfig {

    private Integer id;

    private String client;

    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
