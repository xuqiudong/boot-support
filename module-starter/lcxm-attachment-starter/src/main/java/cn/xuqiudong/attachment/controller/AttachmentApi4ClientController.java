/**
 *
 */
package cn.xuqiudong.attachment.controller;

import cn.xuqiudong.attachment.constant.AttachmentConstant;
import cn.xuqiudong.attachment.model.Attachment;
import cn.xuqiudong.attachment.service.AttachmentService;
import cn.xuqiudong.common.base.model.BaseResponse;
import cn.xuqiudong.common.util.CommonUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;


/**
 *  说明 :  附件服务端的controller为附件客户端 提供接口
 *  @author Vic.xu
 * @since  2020年9月18日下午3:59:49
 */
@RestController
public class AttachmentApi4ClientController {

    @Resource
    private AttachmentService attachmentService;


    /**
     * 修改一个文件的状态
     */
    @PostMapping(value = AttachmentConstant.API_URL_ATTACHMENT_UPDATE)
    public BaseResponse<?> update(int id, @RequestParam(defaultValue = "false") boolean temporary) {
        try {
            attachmentService.updateTemporary(temporary, id);
        } catch (Exception e) {
            return BaseResponse.error().setMsg(e + "");
        }
        return BaseResponse.success();
    }

    /**
     * 修改多个文件的状态
     *
     * @param ids       形如1-2-3-4
     * @param temporary true-临时 false-非临时的
     * @return
     */
    @PostMapping(value = AttachmentConstant.API_URL_ATTACHMENT_BATCH_UPDATE)
    public BaseResponse<?> batchUpdate(String ids, @RequestParam(defaultValue = "false") boolean temporary) {
        if (!ids.matches(AttachmentConstant.IDS_REG)) {
            return BaseResponse.error().setMsg("不合法的字符");
        }
        try {
            List<Integer> idList = CommonUtils.toIntList(ids.split("-"));
            attachmentService.updateTemporary(temporary, idList);
        } catch (Exception e) {
            return BaseResponse.error().setMsg(ExceptionUtils.getMessage(e));
        }
        return BaseResponse.success();
    }

    /**
     * 接收附件客户端上传的附件 尽量不要用这个接口
     */
    @PostMapping(value = AttachmentConstant.API_URL_ATTACHMENT_UP_FILE)
    public BaseResponse<?> upload(@RequestParam(AttachmentConstant.FILE_NAME) MultipartFile file,
                                  @RequestParam(required = false, defaultValue = "none") String module) {
        BaseResponse<Attachment> response = BaseResponse.error();
        if (file.isEmpty()) {
            // 204
            response.setCode(HttpStatus.NO_CONTENT.value());
            response.setMsg("没有上传任何文件");
            return response;
        }
        try {
            Attachment attachment = attachmentService.upfile(file, module);
            response.setData(attachment);
        } catch (IOException e) {
            e.printStackTrace();
            response.setCode(500);
            response.setMsg(e.getMessage());
        }
        return response;
    }


    /**
     * 附件详情
     */
    @RequestMapping(value = AttachmentConstant.API_URL_ATTACHMENT_DETAIL)
    public BaseResponse detail(int id) {
        Attachment attachment = attachmentService.selectAttachmentById(id);
        return BaseResponse.success(attachment);
    }

    /**
     * 附件列表
     * @param ids 如1-2-3-4
     * @return
     */
    @RequestMapping(value = AttachmentConstant.API_URL_ATTACHMENT_DETAILS)
    public BaseResponse details(String ids) {
        if (!ids.matches(AttachmentConstant.IDS_REG)) {
            return BaseResponse.error().setMsg("不合法的字符");
        }
        List<Integer> idList = CommonUtils.toIntList(ids.split("-"));
        attachmentService.findAttachmentsByIds(idList);
        return BaseResponse.success();
    }

}
