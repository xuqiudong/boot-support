package cn.xuqiudong.attachment.dao.oracle;

import cn.xuqiudong.attachment.dao.BaseAttachmentRelationDao;
import cn.xuqiudong.attachment.model.AttachmentRelation;

import java.util.List;

/**
 * 描述:
 * @author Vic.xu
 * @since 2022-07-07 11:32
 */
public class OracleAttachmentRelationDao extends BaseAttachmentRelationDao {

    @Override
    public void insert(AttachmentRelation relation) {

    }

    @Override
    public void batchInsert(List<AttachmentRelation> relationList) {

    }

    @Override
    public void delete(Integer... ids) {

    }

    @Override
    public List<AttachmentRelation> attachmentRelations(String businessId, String table) {
        return null;
    }

    @Override
    public List<AttachmentRelation> attachmentRelations(String businessId, String table, String column) {
        return null;
    }

    @Override
    public int delete(String businessId, String table) {
        return 0;
    }
}
