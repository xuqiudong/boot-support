package cn.xuqiudong.attachment.dao;

import cn.xuqiudong.attachment.model.Attachment;
import cn.xuqiudong.attachment.model.AttachmentOss;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * 描述: oss 附件表操作dao
 * @author Vic.xu
 * @since 2022-11-10 10:53
 */
public abstract class BaseAttachmentOssDao {

    @Resource
    protected JdbcTemplate jdbcTemplate;

    /**
     * 更新或新增
     * @param model AttachmentOss
     * @return AttachmentOss
     */
    public abstract AttachmentOss saveOrUpdate(AttachmentOss model);

    /**
     * 新增
     * @param model AttachmentOss
     * @return AttachmentOss
     */
    public abstract AttachmentOss insert(AttachmentOss model);

    /**
     * 获取所有没有上传到oss的或者上传失败的附件列表
     * @param size query size    0:  zero; negative number: unlimited; positive number:limit size
     * @return Attachment list
     */
    public abstract List<Attachment> findNotUploadedAttachmentList(int size);

    /**
     * 根据附件id获取上传oss的结果
     * @param attachmentId  attachmentId
     * @return AttachmentOss model
     */
    public abstract AttachmentOss findByAttachmentId(Integer attachmentId);

    /**
     * 更新上传结果
     * @param model  AttachmentOss
     */
    public abstract void updateAttachmentOssByAttachmentId(AttachmentOss model);


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

}
