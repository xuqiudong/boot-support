package cn.xuqiudong.attachment.model;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 描述: Oss附件上传记录 attachment_oss 表
 * @author Vic.xu
 * @since 2022-11-10 10:54
 */
public class AttachmentOss implements Serializable {

    private static final long serialVersionUID = -6797225554326284311L;

    private Integer id;

    /**
     *附件id
     */
    private Integer attachmentId;

    /**
     * Object完整路径
     */
    private String objectName;

    /**
     * 本地文件是否删除
     */
    private Boolean originRemoved;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     *更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 结果：1- success
     */
    private Boolean result;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Boolean getOriginRemoved() {
        return originRemoved;
    }

    public void setOriginRemoved(Boolean originRemoved) {
        this.originRemoved = originRemoved;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
