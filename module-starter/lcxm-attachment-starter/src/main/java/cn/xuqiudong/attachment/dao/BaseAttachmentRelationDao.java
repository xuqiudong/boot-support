package cn.xuqiudong.attachment.dao;

import cn.xuqiudong.attachment.model.AttachmentRelation;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.function.Supplier;

/**
 * 描述:附件关系 dao
 * @author Vic.xu
 * @since 2022-07-07 11:30
 */
public abstract class BaseAttachmentRelationDao {

    @Resource
    protected JdbcTemplate jdbcTemplate;

    /**
     * 当前创建着id
     */
    protected Supplier<Integer> currentUserIdSupplier;


    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 因为在starter中，具体项目可以考虑通过 BeanPostProcessor #postProcessAfterInitialization  方式set进来
     * @param currentUserIdSupplier
     */
    public void setCurrentUserIdSupplier(Supplier<Integer> currentUserIdSupplier) {
        this.currentUserIdSupplier = currentUserIdSupplier;
    }

    /**
     * 当前用户id
     * @return user id
     */
    protected Integer getCurrentUserId() {
        if (currentUserIdSupplier == null) {
            return 0;
        }
        return currentUserIdSupplier.get();
    }


    /**
     * 新增附件关系
     * @param relation 对象
     */
    public abstract void insert(AttachmentRelation relation);

    /**
     * 批量新增附件关系
     * @param relationList 关系列表
     */
    public abstract void batchInsert(List<AttachmentRelation> relationList);

    /**
     * 删除：单个或多个
     * @param ids
     */
    public abstract void delete(Integer... ids);

    /**
     * 删除某条业务数据的全部附件
     * @param businessId 业务id
     * @param table 业务表
     * @return number
     */
    public abstract int delete(String businessId, String table);

    /**
     * 某条业务数据的全部附件
     * @param businessId 业务id
     * @param table 业务表
     * @return 附件列表
     */
    public abstract List<AttachmentRelation> attachmentRelations(String businessId, String table);

    /**
     * 某条业务数据的某个字段的附件
     * @param businessId 业务id
     * @param table 业务表
     * @param column  业务字段
     * @return 附件列表
     */
    public abstract List<AttachmentRelation> attachmentRelations(String businessId, String table, String column);


}

