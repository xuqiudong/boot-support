package cn.xuqiudong.attachment.autoconfigure;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 说明 :  附件配置项
 * @author  Vic.xu
 * @since  2019年12月11日 上午9:33:21
 */
@Configuration
@ConfigurationProperties(prefix = AttachmentProperties.ATTACHMENT_PREFIX)
public class AttachmentProperties {

    public static final String ATTACHMENT_PREFIX = "attachment";

    public static final String DEFAULT_ATTACHMENT_REG = "/attachment/(download|visit)/(\\d+)\"";

    /**请求头检验name*/
    public static final String HEADER_CHECK_NAME = "sign";

    /**
     * 类型：
     * 1-服务端 ：开启接口 (默认)
     * 2-客户端 ：开启上传到服务端的sdk（AttachmentClientService）
     */
    private Integer type = 1;

    /**
     * 数据库类型，默认mysql
     */
    @Value("${attachment.database:mysql}")
    private String database;
    /**
     * 附件存放的位置
     */
    private String position;

    /**
     * 附件访问的地址的主机. 绝对地址：URL= host + 相对路径
     */
    private String host;

    /**
     *  附件访问地址(通过id访问项目)：相对于项目; 附件地址 = attachment.visit + /attachment/visit/{id}
     */
    private String visit;

    /**
     * type = 2(作为客户端的时候)时附件服务端的地址，调用接口的时候
     */
    private String server;

    /**
     * 空的附件的相对地址
     */
    private String broken;

    /**
     * 启动时是否检测table存在
     */
    private boolean checkTable;

    /**
     * 需要拦截的客户端请求:  如 /attachment/temporary/**
     */
    private String clientInterceptPattern;

    /**
     * 客户端header中sign加密key 16位
     */
    private String clientSignKey;

    /**
     *客户端header中加密内容（客户端标识，多个逗号分隔）
     */
    private String clientSignContent;

    /**
     * 从富文本的附件中获取附件对应的id的正则表达式
     */
    public static String textAttachmentReg = DEFAULT_ATTACHMENT_REG;

    /**
     * 是否开启定时清除临时态附件
     */
    private boolean cleaner = true;

    /**
     * ali oss 配置
     */
    private AliOss aliOss;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getBroken() {
        return broken;
    }

    public void setBroken(String broken) {
        this.broken = broken;
    }

    public boolean isCleaner() {
        return cleaner;
    }

    public void setCleaner(boolean cleaner) {
        this.cleaner = cleaner;
    }

    public String getTextAttachmentReg() {
        return textAttachmentReg;
    }

    @SuppressWarnings("static-access")
    public void setTextAttachmentReg(String textAttachmentReg) {
        if (StringUtils.isNotBlank(textAttachmentReg)) {
            this.textAttachmentReg = textAttachmentReg;
        }
    }

    public boolean isCheckTable() {
        return checkTable;
    }

    public void setCheckTable(boolean checkTable) {
        this.checkTable = checkTable;
    }

    public String getVisit() {
        return visit;
    }

    public void setVisit(String visit) {
        this.visit = visit;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    /**是否是客户端*/
    public boolean isClient() {
        return type != null && 2 == type;
    }


    public String getClientInterceptPattern() {
        return clientInterceptPattern;
    }

    public void setClientInterceptPattern(String clientInterceptPattern) {
        this.clientInterceptPattern = clientInterceptPattern;
    }

    public String getClientSignKey() {
        return clientSignKey;
    }

    public void setClientSignKey(String clientSignKey) {
        this.clientSignKey = clientSignKey;
    }

    public String getClientSignContent() {
        return clientSignContent;
    }

    public String[] getClientSignContents() {
        if (clientSignContent != null) {
            return clientSignContent.split(",");
        }
        return null;
    }

    public void setClientSignContent(String clientSignContent) {
        this.clientSignContent = clientSignContent;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public AliOss getAliOss() {
        return aliOss;
    }

    public void setAliOss(AliOss aliOss) {
        this.aliOss = aliOss;
    }

    /**
     * ali oss configuration
     */
    public static class AliOss {

        /**
         * 是否启用oss
         */
        private boolean enable;

        /**
         * endpoint
         */
        private String endpoint;

        /**
         * accessKeyId
         */
        private String accessKeyId;

        /**
         * accessKeySecret
         */
        private String accessKeySecret;
        /**
         * Bucket用来管理所存储Object的存储空间，详细描述请参看“开发人员指南 > 基本概念 > OSS基本概念介绍”。
         *  Bucket命名规范如下：只能包括小写字母，数字和短横线（-），必须以小写字母或者数字开头，长度必须在3-63字节之间。
         */
        private String bucketName;

        /**
         * 访问附件用
         */
        private String bucketDomain;

        /**
         * 上传到ali oss的时候是否删除本地
         */
        private boolean removeOrigin;

        /**
         * 一次性上传的数量(定时器用) 小于0 则为全部
         */
        private int onceUploadNumber;

        /**
         * 预签名url过期时间：秒 默认2小时
         */
        private long preSignedUrlExpiration;

        /**
         * 默认2小时过期
         */
        private static long defaultPreSignedUrlExpiration = 2 * 60 * 60;


        public boolean isEnable() {
            return enable;
        }

        public void setEnable(boolean enable) {
            this.enable = enable;
        }

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getAccessKeyId() {
            return accessKeyId;
        }

        public void setAccessKeyId(String accessKeyId) {
            this.accessKeyId = accessKeyId;
        }

        public String getAccessKeySecret() {
            return accessKeySecret;
        }

        public void setAccessKeySecret(String accessKeySecret) {
            this.accessKeySecret = accessKeySecret;
        }

        public String getBucketName() {
            return bucketName;
        }

        public void setBucketName(String bucketName) {
            this.bucketName = bucketName;
        }

        public String getBucketDomain() {
            return bucketDomain;
        }

        public void setBucketDomain(String bucketDomain) {
            this.bucketDomain = bucketDomain;
        }

        public boolean isRemoveOrigin() {
            return removeOrigin;
        }

        public void setRemoveOrigin(boolean removeOrigin) {
            this.removeOrigin = removeOrigin;
        }

        public int getOnceUploadNumber() {
            return onceUploadNumber;
        }

        public void setOnceUploadNumber(int onceUploadNumber) {
            this.onceUploadNumber = onceUploadNumber;
        }

        public long getPreSignedUrlExpiration() {
            return preSignedUrlExpiration > 0 ? preSignedUrlExpiration : defaultPreSignedUrlExpiration;
        }

        public void setPreSignedUrlExpiration(long preSignedUrlExpiration) {
            this.preSignedUrlExpiration = preSignedUrlExpiration;
        }
    }

}
