package cn.xuqiudong.attachment.constant;

/**
 * 数据库类型
 *
 * @author Vic.xu
 * @since 2021/10/22
 */
public enum DatabaseType {
    /**mysql*/
    mysql,
    /**oracle*/
    oracle;

    public static DatabaseType getByName(String name) {
        for (DatabaseType databaseType : DatabaseType.values()) {
            if (databaseType.name().equals(name)) {
                return databaseType;
            }
        }
        return null;
    }
}
