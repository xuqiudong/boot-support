-- mysql
CREATE TABLE `attachment`
(
    `id`            int(11) NOT NULL AUTO_INCREMENT COMMENT '附件表id',
    `absolute_path` varchar(255) DEFAULT NULL COMMENT '绝对地址',
    `relative_path` varchar(255) DEFAULT NULL COMMENT '相对地址',
    `temporary`     tinyint(1)   DEFAULT '0' COMMENT '是否是临时的',
    `module`        varchar(32)  DEFAULT NULL COMMENT '所属模块',
    `content_type`  varchar(128) DEFAULT NULL COMMENT 'mimetype',
    `file_size`     int(11)      DEFAULT NULL COMMENT '文件大小',
    `file_name`     varchar(128) DEFAULT NULL COMMENT '文件名',
    `original_name` varchar(128) DEFAULT NULL COMMENT '原文件名',
    `create_time`   datetime     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='附件表';

CREATE TABLE `client_config`
(
    `id`          int NOT NULL AUTO_INCREMENT COMMENT 'id',
    `client`      varchar(32) DEFAULT NULL COMMENT '客户端名称',
    `password`    varchar(64) DEFAULT NULL COMMENT '密码',
    `enabled`     tinyint     DEFAULT '1' COMMENT '是否可用',
    `create_time` datetime    DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='客户端配置表'

CREATE TABLE `attachment_relation`
(
    `id`            int NOT NULL AUTO_INCREMENT COMMENT 'id',
    `business_id`   varchar(32)  DEFAULT NULL COMMENT '业务表id',
    `attachment_id` int          DEFAULT NULL COMMENT '附件id',
    `table_name`    varchar(128) DEFAULT NULL COMMENT '表名',
    `column_name`   varchar(128) DEFAULT NULL COMMENT '业务字段名',
    `create_time`   datetime     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
    `is_enable`     tinyint(1)   DEFAULT '1' COMMENT '是否启用',
    `version`       int          DEFAULT '1' COMMENT '版本号',
    `is_delete`     tinyint(1)   DEFAULT '0' COMMENT '是否删除',
    `create_id`     int          DEFAULT NULL COMMENT '创建人id',
    `update_id`     int          DEFAULT NULL COMMENT '修改人id',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='附件关系表'
