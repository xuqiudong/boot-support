-- Create table
create table ATTACHMENT
(
    id            NUMBER(11) not null,
    absolute_path VARCHAR2(512),
    relative_path VARCHAR2(512),
    temporary     NUMBER(1),
    module        NUMBER(1),
    content_type  VARCHAR2(128),
    file_size     NUMBER(11),
    file_name     VARCHAR2(512),
    original_name VARCHAR2(512),
    create_time   TIMESTAMP(6)
)
;
-- Add comments to the table
comment
on table ATTACHMENT
  is '附件表';
-- Add comments to the columns
comment
on column ATTACHMENT.id
  is 'id';
comment
on column ATTACHMENT.absolute_path
  is '绝对地址';
comment
on column ATTACHMENT.relative_path
  is '相对地址';
comment
on column ATTACHMENT.temporary
  is '是否是临时的';
comment
on column ATTACHMENT.module
  is '所属模块';
comment
on column ATTACHMENT.content_type
  is 'mimetype';
comment
on column ATTACHMENT.file_size
  is '文件大小';
comment
on column ATTACHMENT.file_name
  is '文件名';
comment
on column ATTACHMENT.original_name
  is '原文件名';
comment
on column ATTACHMENT.create_time
  is '创建时间';
-- Create/Recreate primary, unique and foreign key constraints
alter table ATTACHMENT
    add constraint ATTACHMENT_PK_ID primary key (ID);