package cn.xuqiudong.generator;

import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.generator.dao.BaseGeneratorDao;
import cn.xuqiudong.generator.model.ColumnConfigVO;
import cn.xuqiudong.generator.model.TableConfigVO;
import cn.xuqiudong.generator.model.TableEntity;
import cn.xuqiudong.generator.service.GeneratorService;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author Vic.xu
 * @since 2021/10/13
 */
@SpringBootTest
@Rollback(true)
public class GeneratorTest {

    @Test
    public void contextLoads() {
    }

    @Resource
    BaseGeneratorDao generatorDao;

    @Resource
    GeneratorService generatorService;

    /**
     * 查询列表
     *
     * 描述:
     * @author  Vic.xu
     * @since  2020年3月13日 下午3:31:47
     */
    @Test
    public void list() {
        String tableName = "";
        TableEntity lookup = new TableEntity();
        lookup.setTableName(tableName);
        PageInfo<TableEntity> page = generatorService.list(lookup);
        System.err.println("总数据：" + page.getTotal());
        System.err.println("总页数：" + page.getPages());
        System.err.println("当前页：" + page.getPage());
        System.err.println("当前页数据：");
        page.getDatas().forEach(TableEntity::printLine);

    }

    @Test
    public void detail() {
        String tableName = "zbl_form_data";
        TableConfigVO vo = detail(tableName);
        JsonUtil.printJson(vo);
    }

    @Test
    public void export() {
        String path = "C:/Users/Administrator/Desktop/generator";
        String fileName = "generator.zip";
        String[] tableNames = {"t_auth_application", "t_auth_org_indentify", "t_auth_orgnization", "t_auth_position",
                "t_auth_position_indentify", "t_auth_user"};
        for (String tableName : tableNames) {
            TableConfigVO vo = detail(tableName);
            byte[] data = generatorService.exportByConfig(vo);
            try {
                FileUtils.writeByteArrayToFile(new File(path, tableName + "_" + fileName), data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public TableConfigVO detail(String tableName) {
        TableEntity entity = generatorService.buildTableDetail(tableName);
        JsonUtil.printJson(entity);
        String packageName = "com.kjlink";
        String moduleName = "auth";
        TableConfigVO vo = new TableConfigVO(tableName, packageName, moduleName);
        List<ColumnConfigVO> columns = new ArrayList<ColumnConfigVO>();
        Optional.ofNullable(entity.getColumns()).orElse(new ArrayList<>()).forEach(c -> {
            Integer condition = "string".equalsIgnoreCase(c.getAttrType()) ? 1 : 0;
            ColumnConfigVO columnConfigVO = new ColumnConfigVO(c.getColumnName(), condition, true);
            columns.add(columnConfigVO);
        });
        vo.setColumns(columns);
        JsonUtil.printJson(vo);
        return vo;
    }

}