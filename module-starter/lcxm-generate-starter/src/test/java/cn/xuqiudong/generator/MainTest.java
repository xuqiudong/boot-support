package cn.xuqiudong.generator;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * 描述:
 *
 * @author Vic.xu
 * @since 2024-04-24 14:43
 */
public class MainTest {

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        set.add("a");
        set.add("b");
        set.add("c");
        set.add("d");
        set.add("c");
        System.out.println(set);


        Map<String, Object> map = new LinkedHashMap<>();
        map.put("c", 3);
        map.put("d", 4);
        map.put("a", 1);
        map.put("b", 2);


        map.put("c", 111);

        map.keySet().forEach(key -> {
                    System.out.println(key + ":" + map.get(key));
        });
        System.out.println(map);
    }
}
