package cn.xuqiudong.generator.autoconfigure;

import cn.xuqiudong.generator.contant.DatabaseType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;

/**
 * 说明 :  代码自动生成配置项
 * @author  Vic.xu
 * @since  2019年12月11日 上午9:33:21
 */
@Configuration
@ConfigurationProperties(prefix = GeneratorProperties.GENERATOR_PREFIX)
@PropertySource("classpath:config/generator-config.properties")
public class GeneratorProperties implements Serializable {
    private static final long serialVersionUID = 1L;

    public static final String GENERATOR_PREFIX = "generator";

    /**
     * 数据库类型，默认mysql
     */
    @Value("${generator.database:mysql}")
    private String database;

    /**
     * 作者
     */
    private String author;

    /**
     * 表前缀 若存在则生成的实体名会去掉前缀
     */
    private String tablePrefix;

    /**
     * 实体中忽略的表字段
     */
    private Set<String> ignores;
    /**
     * 数据库中哪些数据类型表示大文本，(这样是数据不应出现在list页面)
     */
    private Set<String> textTypes;

    /**
     * 需要填充的数据模板
     */
    private List<String> templates;

    /**
     * 数据类型转换 数据库类型--> java 类型
     */
    private Map<String, String> dataTypeConvert;

    @PostConstruct
    private void post() {
        mergerDataType();
        ignorestoLowerCase();

    }

    /**
     * 忽略的字符串 转小写
     */
    private void ignorestoLowerCase() {
        if (ignores == null) {
            ignores = new HashSet<String>();
            return;
        }
        Set<String> set = new HashSet<String>();
        ignores.forEach(i -> set.add(i.toLowerCase()));
        ignores = set;
    }

    /**
     * 把默认的数据转换和配置的数据转换合并起来 并转小写
     */
    private void mergerDataType() {
        if (dataTypeConvert == null || dataTypeConvert.isEmpty()) {
            dataTypeConvert = new HashMap<String, String>();
            dataTypeConvert.putAll(DEFAULT_DATA_TYPE_CONVERT);
            return;
        }

        dataTypeConvert.forEach((k, v) -> {
            dataTypeConvert.put(k.toLowerCase(), v);
        });

        // 把没有配置的项放进来
        DEFAULT_DATA_TYPE_CONVERT.forEach((k, v) -> {
            if (!dataTypeConvert.containsKey(k)) {
                dataTypeConvert.put(k, v);
            }
        });
    }

    @Override
    public String toString() {
        List<String> dataTypeConverts = new ArrayList<String>();
        dataTypeConvert.forEach((k, v) -> {
            dataTypeConverts.add(k + "=" + v);
        });
        StringBuilder sb = new StringBuilder("\t【database=");
        sb.append(database).append(", author=").append(author).append(", tablePrefix=").append(tablePrefix)
                .append(",\n\t ignores=[").append(ignores).append("],\n\t textTypes=[").append(String.join(";", textTypes))
                .append("],\n\t templates=[\n\t\t").append(String.join("\n\t\t", templates))
                .append("],\n\t  dataTypeConvert=[").append(String.join(";", dataTypeConverts)).append("]\n\t】");
        return sb.toString();
    }

    public String getAuthor() {
        return author;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public Set<String> getIgnores() {
        return ignores;
    }

    public List<String> getTemplates() {
        return templates;
    }

    public Map<String, String> getDataTypeConvert() {
        return dataTypeConvert;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public void setIgnores(Set<String> ignores) {
        this.ignores = ignores;
    }

    public void setTemplates(List<String> templates) {
        this.templates = templates;
    }

    public void setDataTypeConvert(Map<String, String> dataTypeConvert) {
        this.dataTypeConvert = dataTypeConvert;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

    public DatabaseType getTaDatabaseType() {
        return DatabaseType.getByName(database);
    }

    /**
     * @param database
     *            the database to set
     */
    public void setDatabase(String database) {
        this.database = database;
    }

    /**
     * @return the textTypes
     */
    public Set<String> getTextTypes() {
        return textTypes;
    }

    /**
     * @param textTypes
     *            the textTypes to set
     */
    public void setTextTypes(Set<String> textTypes) {
        this.textTypes = textTypes;
    }

    /**
     * 默认的数据库类型和java类型的转换关系
     */
    public static Map<String, String> DEFAULT_DATA_TYPE_CONVERT = new HashMap<String, String>();

    static {
        DEFAULT_DATA_TYPE_CONVERT.put("tinyint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("smallint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("mediumint", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("int", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("integer", "Integer");
        DEFAULT_DATA_TYPE_CONVERT.put("number", "Integer");

        DEFAULT_DATA_TYPE_CONVERT.put("bigint", "Long");
        DEFAULT_DATA_TYPE_CONVERT.put("float", "Float");
        DEFAULT_DATA_TYPE_CONVERT.put("double", "Double");
        DEFAULT_DATA_TYPE_CONVERT.put("decimal", "BigDecimal");
        DEFAULT_DATA_TYPE_CONVERT.put("bit", "BigDecimal");
        DEFAULT_DATA_TYPE_CONVERT.put("enum", "String");

        DEFAULT_DATA_TYPE_CONVERT.put("char", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("varchar", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("varchar2", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("tinytext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("mediumtext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("longtext", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("blob", "String");
        DEFAULT_DATA_TYPE_CONVERT.put("clob", "String");

        DEFAULT_DATA_TYPE_CONVERT.put("date", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("time", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("datetime", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("timestamp", "Date");
        DEFAULT_DATA_TYPE_CONVERT.put("timestamp(6)", "Date");
    }

}
