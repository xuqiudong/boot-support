package cn.xuqiudong.generator.contant;

/**
 * 数据库类型
 *
 * @author Vic.xu
 * @since 2021/10/22
 */
public enum DatabaseType {
    /**mysql*/
    mysql("`"),
    /**oracle*/
    oracle("\"");
    /**
     * 列转义使用的引号， oracle:",  mysql:`
     */
    private String quotation;

    public String getQuotation() {
        return quotation;
    }

    DatabaseType(String quotation) {
        this.quotation = quotation;
    }

    public static DatabaseType getByName(String name) {
        for (DatabaseType databaseType : DatabaseType.values()) {
            if (databaseType.name().equals(name)) {
                return databaseType;
            }
        }
        return null;
    }
}
