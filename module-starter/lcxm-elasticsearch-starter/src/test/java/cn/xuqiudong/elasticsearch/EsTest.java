/**
 *
 */
package cn.xuqiudong.elasticsearch;

import cn.xuqiudong.common.base.model.PageInfo;
import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.elasticsearch.helper.EsSearchHelper;
import cn.xuqiudong.elasticsearch.model.EsLookup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

/**
 *  说明 :  测试ES  starter
 *  @author Vic.xu
 * @since  2020年9月11日下午2:13:47
 */
public class EsTest extends BaseTest {

    String index = "blog_index";

    @Autowired
    private EsSearchHelper esSearchHelper;

    @Test
    public void test() throws IOException {
        Assert.notNull(esSearchHelper, "esSearchHelper is null");
        String keyword = "java";
        EsLookup<BlogArticle> lookup = new EsLookup<BlogArticle>();
        lookup.setClazz(BlogArticle.class);
        lookup.setIndexName(index);
        lookup.setKeyword(keyword);
        lookup.setSize(5);
        Map<String, BiConsumer<BlogArticle, String>> highlightFieldAndSetFunctionMap = new HashMap<String, BiConsumer<BlogArticle, String>>();
        highlightFieldAndSetFunctionMap.put("title", (article, text) -> {
            article.setTitle(text);
        });
        highlightFieldAndSetFunctionMap.put("content", (article, text) -> {
            article.setContent(text);
        });
        highlightFieldAndSetFunctionMap.put("summary", (article, text) -> {
            article.setSummary(text);
        });
        lookup.setHighlightFieldAndSetFunctionMap(highlightFieldAndSetFunctionMap);
        PageInfo<BlogArticle> search = esSearchHelper.search(lookup);
        System.out.println(search.getTotal());
        System.out.println(search.getSize());
        search.getDatas().forEach(c -> {
            JsonUtil.printJson(c.getSummary());
            JsonUtil.printJson(c.getTitle());
            JsonUtil.printJson(c.getContent());
        });
    }


}
