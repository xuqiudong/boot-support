package cn.xuqiudong.elasticsearch;


import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

@SpringBootTest
@Rollback(true)
public class BaseTest {


    @Test
    public void contextLoads() {
    }
}