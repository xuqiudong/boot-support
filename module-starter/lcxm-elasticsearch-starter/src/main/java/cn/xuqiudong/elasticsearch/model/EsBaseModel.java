/**
 *
 */
package cn.xuqiudong.elasticsearch.model;

import java.io.Serializable;

/**
 *  说明 :  保存到ES中的实体的基类，id
 *  @author Vic.xu
 * @since  2020年9月16日上午10:35:09
 */
public class EsBaseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
