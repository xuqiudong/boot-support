/**
 *
 */
package cn.xuqiudong.elasticsearch.autoconfigure;

import java.util.List;

/**
 * 说明 :  配置是索引
 * @author Vic.xu
 * @since  2020年9月10日下午7:31:23
 */
public class IndexConfig {

    // 分片数
    private static final int DEFAULT_SHARDS = 3;
    // 备份数
    private static final int DEFAULT_REPLICAS = 0;
    // 分词器
    private static final String DEFAULT_ANALYZER = "ik_max_word";

    /** index name */
    String name;

    /** index 别名 */
    String alias;

    /** 是否默认的index */
    boolean defaults;

    /** 分片数 */
    private int shards = DEFAULT_SHARDS;

    /** 备份数 */
    private int replicas = DEFAULT_REPLICAS;

    /** 分词器 */
    private String analyzer = DEFAULT_ANALYZER;

    /**
     * index的mappings配置
     */
    private List<IndexProperty> properties;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public boolean isDefaults() {
        return defaults;
    }

    public void setDefaults(boolean defaults) {
        this.defaults = defaults;
    }

    public int getShards() {
        return shards;
    }

    public void setShards(int shards) {
        this.shards = shards;
    }

    public int getReplicas() {
        return replicas;
    }

    public void setReplicas(int replicas) {
        this.replicas = replicas;
    }

    public String getAnalyzer() {
        return analyzer;
    }

    public void setAnalyzer(String analyzer) {
        this.analyzer = analyzer;
    }

    public List<IndexProperty> getProperties() {
        return properties;
    }

    public void setProperties(List<IndexProperty> properties) {
        this.properties = properties;
    }

    /**
     * 说明 :  Index的mapping配置
     * @author Vic.xu
     * @since  2020年9月10日下午7:36:44
     */
    public static class IndexProperty {

        /** 字段名 */
        String field;

        /** 类型：text；keyword等 */
        String type;

        /**
         * @return the field
         */
        public String getField() {
            return field;
        }

        /**
         * @param field the field to set
         */
        public void setField(String field) {
            this.field = field;
        }

        /**
         * @return the type
         */
        public String getType() {
            return type;
        }

        /**
         * @param type the type to set
         */
        public void setType(String type) {
            this.type = type;
        }

    }

}
