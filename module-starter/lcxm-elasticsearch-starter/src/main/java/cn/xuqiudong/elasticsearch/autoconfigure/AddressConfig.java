/**
 *
 */
package cn.xuqiudong.elasticsearch.autoconfigure;

/**
 * 说明 :  ES 的address配置
 * @author Vic.xu
 * @since  2020年9月10日下午7:41:10
 */
public class AddressConfig {

    public String ip;

    private int port;

    public AddressConfig() {
        super();
    }

    public AddressConfig(String ip, int port) {
        super();
        this.ip = ip;
        this.port = port;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

}
