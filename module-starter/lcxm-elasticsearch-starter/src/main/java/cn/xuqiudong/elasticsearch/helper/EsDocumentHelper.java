package cn.xuqiudong.elasticsearch.helper;

import cn.xuqiudong.common.util.JsonUtil;
import cn.xuqiudong.elasticsearch.model.EsBaseModel;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.util.List;

/**
 * 说明 :  ES对文档的一些操作
 * @author Vic.xu
 * @since  2020/6/18 0018 11:17
 */
public class EsDocumentHelper {

    private RestHighLevelClient restHighLevelClient;

    public EsDocumentHelper() {
    }

    public EsDocumentHelper(RestHighLevelClient restHighLevelClient) {
        this.restHighLevelClient = restHighLevelClient;
    }

    /**
     *  单个保存(新增或修改)
     * @link https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-document-index.html
     */
    public <T extends EsBaseModel> void save(T data, String index) throws IOException {
        if (data == null || data.getId() == null) {
            //throw a exception
            return;
        }
        IndexRequest indexRequest = new IndexRequest(index);
        indexRequest.id(data.getId());
        // 此处并没有设置indexRequest的opType为CREATE/UPDATE(默认是)INDEX
        String json = JsonUtil.toJson(data);
        indexRequest.source(json, XContentType.JSON);
        // 保存文档数据
        restHighLevelClient.index(indexRequest, RequestOptions.DEFAULT);
    }

    /**
     * 批量保存
     * @link https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-document-bulk.html
     */
    public <T extends EsBaseModel> void saveAll(String index, List<T> list) throws IOException {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        // 批量请求
        BulkRequest bulkRequest = new BulkRequest();
        list.forEach(data -> {
            bulkRequest.add(new IndexRequest(index).id(data.getId()).source(JsonUtil.toJson(data),
                    XContentType.JSON));
        });
        restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

    }

    /**
     * 根据id获取对象
     * @link https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-document-get.html
     */
    public <T> T get(String index, Object id, Class<T> resultType) throws IOException {
        GetRequest getRequest = new GetRequest(index, String.valueOf(id));
        GetResponse response = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
        String resultAsString = response.getSourceAsString();
        return JsonUtil.jsonToObject(resultAsString, resultType);
    }

    /**
     * 根据id删除对象
     * @link https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-document-delete.html
     */
    public void delete(String id, String index) throws IOException {
        DeleteRequest deleteRequest = new DeleteRequest(index, id);
        restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
    }

    /**
     * 根据id集合批量删除
     * @param idList
     * @throws IOException
     */
    public void deleteAll(List<String> idList, String index) throws IOException {
        if (CollectionUtils.isEmpty(idList)) {
            return;
        }
        BulkRequest bulkRequest = new BulkRequest();
        idList.forEach(id -> bulkRequest.add(new DeleteRequest(index, id)));
        restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
    }

}
