package cn.xuqiudong.common.util.encrypt;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;

/**
 * 授权信息model
 * @author VIC.xu
 *
 */
public class LicenseModel {

    //用户
    private String user;
    //公司
    private String company;

    //版本
    private String version;

    //有效期
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date validUntil;

    /**
     * 签名
     */
    private String sign;


    public LicenseModel() {
        super();
    }

    public LicenseModel(String user, String company, String version, Date validUntil) {
        super();
        this.user = user;
        this.company = company;
        this.version = version;
        this.validUntil = validUntil;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }


    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }


    static String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDg9HSHm2ikji9XTVbdbqcOnzOuezYUEiLCwwlvdZBzjQUdY8DkEFQ9oluRBsM_aOb3aZ3LXmFoIXZienJ8dMoFJRzz4l9UYHR9j6VG9H7MSSY3myFTGbSjpcSUrkhJWGk_wiIRni_tSfju9eo08zQDck_ZD-gOpWWOeOYVTCLyBwIDAQAB";

    static String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOD0dIebaKSOL1dNVt1upw6fM657NhQSIsLDCW91kHONBR1jwOQQVD2iW5EGwz9o5vdpncteYWghdmJ6cnx0ygUlHPPiX1RgdH2PpUb0fsxJJjebIVMZtKOlxJSuSElYaT_CIhGeL-1J-O716jTzNANyT9kP6A6lZY545hVMIvIHAgMBAAECgYEAvn_lquz9mdTa3FifRhetmyFQt-KZcjfFfh-CfwQVFvWGLlRKhhgZ7rPuTQtoDuu8JCspuI4teOxVf6HoD-MrK8b9SHGA4nX35pk9wsIXSyGtaJwjPMtTjquDVj7egvsAlMsUVzqYuQ_u55mc1SAAho7H-fdCOaC6bB7xjNpv0DkCQQD_Yv8nBVsaZ5xbMtYft1okCI-_sGmVNON5rpfDTmctxD8DSDLLf_pY5DH9VnHJ4luYAPueoUynUXtKVscVmIZlAkEA4X7AAnyoILV4m1KEKxPo1Zq0XULwEKPsZyyrcWCZg6YF9G53i4cIBfjY6-r_tVpQ1AcY5PnLpHjSTqbePD0p-wJAHrMBxOkJ6ISn8PaNcsaRmV9GGF7Wck8nQQtRxvJ3yLKceIKHSJsbtu_HapKnnwMMy5ripFDza_GJhogj6l6pWQJAFoCbkKP38yuo2XcmkVXvSkGh9hyT8FUrZjBi-3F0YFrVBUtwyo-zEUWAACr3UZWtszryFQeIduVHt2mXi9NeuQJACjAWuU1-u7IZTGzFJwcG3YlgjxXlTqFVexegpxypOyMQ2QYI5d_XnvbCrETZVlSxxwK62zx_mRN6gYr2kJv_Tw";


    static ObjectMapper mapper = new ObjectMapper();

    public static void main(String[] args) throws Exception {
        LicenseModel model = new LicenseModel("gongyin", "北京", "1.0", new Date());

        String json = mapper.writeValueAsString(model);
        System.out.println(json);


        String encryptStr = RsaUtils.publicEncrypt(json, publicKey);
        System.out.println("密文:" + encryptStr);
        String original = RsaUtils.privateDecrypt(encryptStr, privateKey);
        System.out.println("解密后:" + original);


    }

}
