package cn.xuqiudong.common.util.encrypt;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;


public class TestRsa {
    public static void main(String[] args) throws Exception {
        System.out.println(System.currentTimeMillis());
        if (!test2()) {
            return;
        }
        RsaUtils.RsaKeyPair keyMap = RsaUtils.createKeys(1024);
        String publicKey = keyMap.getPublicKey();
        String privateKey = keyMap.getPrivateKey();
        System.out.println("公钥: \n\r" + publicKey);
        System.out.println("私钥： \n\r" + privateKey);

        System.out.println("公钥加密——私钥解密");
        String str = "123456_timestamp";
        System.out.println("\r明文：\r\n" + str);
        System.out.println("\r明文大小：\r\n" + str.getBytes().length);
        //公钥加密
        String encodedData = RsaUtils.publicEncrypt(str, publicKey);

        System.out.println("密文：\r\n" + encodedData);
        //私钥解密
        String decodedData = RsaUtils.privateDecrypt(encodedData, privateKey);

        System.out.println("解密后文字: \r\n" + decodedData);


    }

    public static boolean test2() throws Exception {
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDg9HSHm2ikji9XTVbdbqcOnzOuezYUEiLCwwlvdZBzjQUdY8DkEFQ9oluRBsM_aOb3aZ3LXmFoIXZienJ8dMoFJRzz4l9UYHR9j6VG9H7MSSY3myFTGbSjpcSUrkhJWGk_wiIRni_tSfju9eo08zQDck_ZD-gOpWWOeOYVTCLyBwIDAQAB";

        String privateKey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAOD0dIebaKSOL1dNVt1upw6fM657NhQSIsLDCW91kHONBR1jwOQQVD2iW5EGwz9o5vdpncteYWghdmJ6cnx0ygUlHPPiX1RgdH2PpUb0fsxJJjebIVMZtKOlxJSuSElYaT_CIhGeL-1J-O716jTzNANyT9kP6A6lZY545hVMIvIHAgMBAAECgYEAvn_lquz9mdTa3FifRhetmyFQt-KZcjfFfh-CfwQVFvWGLlRKhhgZ7rPuTQtoDuu8JCspuI4teOxVf6HoD-MrK8b9SHGA4nX35pk9wsIXSyGtaJwjPMtTjquDVj7egvsAlMsUVzqYuQ_u55mc1SAAho7H-fdCOaC6bB7xjNpv0DkCQQD_Yv8nBVsaZ5xbMtYft1okCI-_sGmVNON5rpfDTmctxD8DSDLLf_pY5DH9VnHJ4luYAPueoUynUXtKVscVmIZlAkEA4X7AAnyoILV4m1KEKxPo1Zq0XULwEKPsZyyrcWCZg6YF9G53i4cIBfjY6-r_tVpQ1AcY5PnLpHjSTqbePD0p-wJAHrMBxOkJ6ISn8PaNcsaRmV9GGF7Wck8nQQtRxvJ3yLKceIKHSJsbtu_HapKnnwMMy5ripFDza_GJhogj6l6pWQJAFoCbkKP38yuo2XcmkVXvSkGh9hyT8FUrZjBi-3F0YFrVBUtwyo-zEUWAACr3UZWtszryFQeIduVHt2mXi9NeuQJACjAWuU1-u7IZTGzFJwcG3YlgjxXlTqFVexegpxypOyMQ2QYI5d_XnvbCrETZVlSxxwK62zx_mRN6gYr2kJv_Tw";

        LicenseModel model = new LicenseModel("gongyin", "背景", "1.0", new Date());
        ObjectMapper mapper = new ObjectMapper();
        String data = mapper.writeValueAsString(model);
        System.out.println("原数据:" + data);

        String data2 = RsaUtils.publicEncrypt(data, publicKey);
        System.out.println("密文:" + data2);
        String data3 = RsaUtils.privateDecrypt(data2, privateKey);
        System.out.println("解密后:" + data3);
        return false;
    }

}