package cn.xuqiudong.common.util.text;

import org.apache.commons.text.StringSubstitutor;

import java.util.HashMap;
import java.util.Map;

/**
 * 说明:
 *
 * @author Vic.xu
 * @since 2023/5/12/0012 14:04
 */
public class CommonTextTest {



    public static void template(){
        String template = "我是${name}, 年龄${age}岁,住址是${address.location}.门牌号是${address.number}";
        Map<String, Object> model = new HashMap<>();
        model.put("name", "张三");
        model.put("age",23);
        Map<String, Object> address = new HashMap<>();
        address.put("location", "合肥市长江东大街某小区");
        address.put("number", "55号");
        //注意key是变量
        model.put("address", address);
        System.out.println(model);
        StringSubstitutor stringSubstitutor = new StringSubstitutor(model);
        //允许嵌套变量
        stringSubstitutor.setEnableSubstitutionInVariables(true);
        String replace = stringSubstitutor.replace(template);
        System.out.println();
        System.out.println(replace);

    }

    public static void main(String[] args) {
        template();
    }
}
