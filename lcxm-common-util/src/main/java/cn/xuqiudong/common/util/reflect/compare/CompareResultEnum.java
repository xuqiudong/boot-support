/**
 * 
 */
package cn.xuqiudong.common.util.reflect.compare;

/**
 *  说明 :  比较两个对象的结果操作类型
 *  @author Vic.xu
 * @since  2020年4月21日下午1:01:39
 */
public enum CompareResultEnum {
	/**
	 * 新增
	 */
	ADD,
	/**
	 * 修改
	 */
	MODIFY,
	/**
	 * 删除
	 */
	DELETE,
	/**
	 * 无操作
	 */
	NONE;
	

}
