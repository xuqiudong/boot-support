pipeline {
    agent any

    options { timestamps () }

    environment {
        consoleDir = '/xqd/server/console'
        ssoDir = '/xqd/server/sso'
        duoduoDir = '/xqd/server/duoduo'
        blogDir = '/xqd/server/blog'
    }

    stages {
        stage('buildParent') {
            steps {
                echo "install springboot-parent"
                sh 'cd springboot-parent && mvn clean install'
                sh 'pwd'
            }
        }

         stage('buildUtil') {
            steps {
                echo "install common util"
                sh 'cd lcxm-common-util && mvn clean install'
                echo "lcxm-common-util install success"
            }
        }

         stage('buildBase') {
            steps {
                echo "install common base"
                sh 'cd lcxm-common-base && mvn clean install'
                echo "lcxm-common-base install success"
                echo "install common blog"
                sh 'cd module-project/lcxm-blog-common && mvn clean install'
                echo "lcxm-blog-base install success"
            }
        }
        stage('buildCasSSO') {
            steps {
                echo "install cas sso"
                sh 'mvn clean install -f module-cas-sso/cas-sso-common/pom.xml'
                sh 'mvn clean install -f module-cas-sso/cas-sso-client/pom.xml'
                echo "module cas sso install success"
            }
        }
        stage('buildStarter') {
            steps {
                echo "install starter"
                sh 'mvn clean install -f module-starter/lcxm-attachment-starter/pom.xml'
                echo "lcxm-attachment-starter installed"
                sh 'mvn clean install -f module-starter/lcxm-elasticsearch-starter/pom.xml'
                echo "lcxm-elasticsearch-starter installed"
                sh 'mvn clean install -f module-starter/lcxm-generate-starter/pom.xml'
                echo "lcxm-generate-starter installed"
                echo "module starter install success"
            }
        }

        stage('buildProject:cas-sso-server') {
            steps {
                sh 'mvn clean package -Denv=prod -f module-cas-sso/cas-sso-server/pom.xml'
                echo "copy cas-sso-server.jar to ${ssoDir}"
                sh 'cp module-cas-sso/cas-sso-server/target/cas-sso-server.jar  ${ssoDir}'
                sh 'cd ${ssoDir} && ./springboot.sh restart cas-sso-server.jar'
                echo "start sso-server success"
            }
        }

        stage('buildProject:duoduo') {
            steps {
                sh 'mvn clean package -Denv=prod -f module-project/project-duoduo/pom.xml'
                echo "copy duoduo.jar to ${duoduoDir}"
                sh 'cp module-project/project-duoduo/target/duoduo.jar ${duoduoDir}/'
                sh '''
                  BUILD_TIME=$(date '+%Y-%m-%d-%H%M%S')
                  BACKUP=${duoduoDir}/backup_${BUILD_TIME}
                  mkdir ${BACKUP}
                  mv ${duoduoDir}/blog.jar ${BACKUP}/ 2>/dev/null || :
                  mv -r ${duoduoDir}/lib ${BACKUP}/ 2>/dev/null || :
                  mv -r ${duoduoDir}/resources ${BACKUP}/ 2>/dev/null || :
                  echo "copy jar"
                  cp module-project/project-duoduo/target/duoduo.jar ${duoduoDir}/
                  echo "copy lib"
                  cp -r module-project/project-duoduo/target/lib ${duoduoDir}/
                  cp -r module-project/project-duoduo/target/resources ${duoduoDir}
                '''

                sh 'cd ${duoduoDir} && ./springboot.sh restart duoduo.jar'
                echo "start duoduo success"
            }
        }

        stage('buildProject:blog') {
            steps {
                sh 'mvn clean package -Pprod -f module-project/project-blog/pom.xml'
                echo "start copy resources to ${blogDir}"
                sh '''
                  BUILD_TIME=$(date '+%Y-%m-%d-%H%M%S')
                  BACKUP=${blogDir}/backup_${BUILD_TIME}
                  mkdir ${BACKUP}
                  mv ${blogDir}/blog.jar ${BACKUP}/ 2>/dev/null || :
                  mv -r ${blogDir}/lib ${BACKUP}/ 2>/dev/null || :
                  mv -r ${blogDir}/resources ${BACKUP}/ 2>/dev/null || :
                  echo "copy jar"
                  cp module-project/project-blog/target/blog.jar ${blogDir}/
                  echo "copy lib"
                  cp -r module-project/project-blog/target/lib ${blogDir}/
                  cp -r module-project/project-blog/target/resources ${blogDir}/
                '''
                sh 'cd ${blogDir} && ./springboot.sh restart blog.jar'
                echo "start blog success"
            }
        }

        stage('buildProject:console') {
            steps {
                sh 'mvn clean package -Pprod -f module-project/project-console/pom.xml'
                echo "start copy resources to ${consoleDir}"
                sh '''
                  BUILD_TIME=$(date '+%Y-%m-%d-%H%M%S')
                  BACKUP=${consoleDir}/backup_${BUILD_TIME}
                  mkdir ${BACKUP}
                  mv ${consoleDir}/console.jar ${BACKUP}/ 2>/dev/null || :
                  mv -r ${consoleDir}/lib ${BACKUP}/ 2>/dev/null || :
                  mv -r ${consoleDir}/resources ${BACKUP}/ 2>/dev/null || :
                  echo "copy jar"
                  cp module-project/project-console/target/console.jar  ${consoleDir}/
                  echo "copy lib"
                  cp -r module-project/project-console/target/lib ${consoleDir}/
                  cp -r module-project/project-console/target/resources ${consoleDir}/
                '''
                sh 'cd ${consoleDir} && ./springboot.sh restart console.jar'
                echo "start console success"
            }
        }
    }
    post {
        always {
            emailext body: '$DEFAULT_CONTENT', subject: '$DEFAULT_SUBJECT', to: '84597585@qq.com'
        }
        success {
            echo "success"
        }
        failure {
            echo "failure"
        }
    }
}
