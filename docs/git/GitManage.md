### [Git基础 - git tag ](https://blog.csdn.net/qq_39505245/article/details/124705850)

- 查看标签：  `git tag`  `git tag -l xxx`
-  查看标签的提交信息： `git show 标签名`
- 创建标签：`git tag 标签名` 
  - `git tag 标签名 提交版本号` ：给指定的提交版本创建一个 【轻量标签】
  - 创建附注标签：`git tag -a 标签名称 -m 附注信息`
    - `git tag -a 标签名称 提交版本号 -m 附注信息`
- 删除标签：`git tag -d 标签名称`

- 推送到远程仓库：
  - `git push origin 标签名称` : 将指定的标签上传到远程仓库
  - `git push origin --tags` : 将所有不在远程仓库中的标签上传到远程仓库
-  删除远程仓库的标签
  - `git push origin  :regs/tags/标签名称`
  -  `git push origin --delete 标签名称`
- 检出标签：以标签指定的版本为基础版本，新建一分支，继续其他的操作。
  因此 ，就是 新建分支的操作了。
  - `git checkout -b 分支名称 标签名称`

#### [git的分支管理](https://blog.csdn.net/silence_pinot/article/details/111478596)

- 查看分支：`git branch`

- 创建分支：`git branch `

- 切换分支：`git checkout `

- 创建+切换分支：`git checkout -b `

- 合并某分支到当前分支：`git merge `

- 删除分支：`git breach -d `

**查看带有冲突解决的日志**

- `git log --graph -- pretty=oneline`

**分支管理策略**

> 通常，合并分支时，如果没有冲突，并且分支是单向一条线路继承下来的，git会使用 fast forword 模式，但是有些快速合并不能成功，但是又没有冲突时，就会触发分支管理策略，git会自动做一次新的提交。

当两个分支对工作区都进行了修改，但是修改的并不是同一个文件，而是两个不同的文件，也就是不会产生冲突。此时合并的时候，不能使用**“快速合并”**，就会弹框需要你输入一段合并说明。使用快捷键 **ctrl+x** 退出

**合并时禁止快速合并模式**

```shell
# 合并dev到master，禁止快速合并模式，同时添加说明
git merge --no-ff -m '' dev
```

##### bug分支

**描述和说明**

使用场景：当在某个分支上正在工作，突然有一个紧急的bug需要修复，此时可以使用 `stash`功能，将当前正在工作的`现场存储起来`，等bug修复之后，在返回继续工作。

操作顺序：

1. 将当前的工作现场临时存储

   ```shell
   # 对当前现场进行存储
   git stash
   ```

2. 切换到bug出现的分支上，比如bug出现在 `master`分支。如果bug就是在当前分支，可以操作此步骤

   ```shell
   git checkout master
   ```

3. 新添加一个bug临时分支

   ```shell
   git checkout -b bug001
   ```

4. 对代码进行修复。

5. 切换回master分支

   ```shell
   git checkout master
   ```

6. 合并bug分支到主master上

   ```shell
   git merge --no-ff -m '合并bug分支到master' bug001
   ```

7. 删除bug001分支

   ```shell
   git branch -d bug001
   ```

8. 回到之前的工作现场所在的分支

   ```shell
   git checkout dev
   ```

9. 查看当前分支保存那些工作现场(之前封冻存储的工作现场)

   ```shell
   git stash list
   ```

10. 恢复存储的现场

    ```shell
    git stash pop
    ```

#### 小结：

修复bug时，通过创建新的bug分支进行修复，然后合并，最后删除。

当手头工作没有做完时，先把工作现场git stash一下，然后去修复bug，修复后，再git stash pop，恢复工作现场。